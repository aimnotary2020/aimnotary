<?php
ini_set('display_errors', 1);
require_once ('vendor/autoload.php');

$client_id = '109700331601-h9840s12brehg5vgcbt6s2umoikijnlv.apps.googleusercontent.com'; //Client ID
// $service_account_name = '{your_email}@developer.gserviceaccount.com'; //Email Address
$service_account_name = 'aimnotary-service-account@aimnotary-project-001.iam.gserviceaccount.com'; //Email Address
$key_file_location = $_SERVER['DOCUMENT_ROOT'] .'/aimnotary-project-001-e95602b28bda.json'; //key.p12
//INSTATIATE NEEDED OBJECTS (In this case, for freeBusy query, and Create New Event)
$client = new Google_Client();
$service = new Google_Service_Calendar($client);
$id = new Google_Service_Calendar_FreeBusyRequestItem($client);
$item = new Google_Service_Calendar_FreeBusyRequest($client);
$event = new Google_Service_Calendar_Event($client);
$startT = new Google_Service_Calendar_EventDateTime($client);
$endT = new Google_Service_Calendar_EventDateTime($client);




// <?php


// require_once __DIR__.'/vendor/autoload.php';
session_start();     


$client = new Google_Client();
$application_creds =$key_file_location;  //the Service Account generated cred in JSON
$credentials_file = file_exists($application_creds) ? $application_creds : false;
define("APP_NAME","Google Calendar API PHP");   //whatever
$client->setAuthConfig($credentials_file);
$client->setApplicationName(APP_NAME);
$client->addScope("https://www.googleapis.com/auth/calendar");


//Setting Complete

//Go Google Calendar to set "Share with ..."  Created in Service Account (xxxxxxx@sustained-vine-198812.iam.gserviceaccount.com)

//Example of Use of API
$service = new Google_Service_Calendar($client);  


$calendarId = 'info@aimnotary.com';   //NOT primary!! , but the email of calendar creator that you want to view
$optParams = array(
  'maxResults' => 10,
  'orderBy' => 'startTime',
  'singleEvents' => TRUE,
  'timeMin' => date('c'),
);
$results = $service->events->listEvents($calendarId, $optParams);

// if (count($results->getItems()) == 0) {
//   print "No upcoming events found.\n";
// } else {
//   echo "Upcoming events:";
//   echo "<hr>";
//   echo "<table>";
//   foreach ($results->getItems() as $event) {
//     $start = $event->start->dateTime;
//     if (empty($start)) {
//       $start = $event->start->date;
//     }
//     echo "<tr>";
//     echo"<td>".$event->getSummary()."</td>";
//     echo"<td>".$start."</td>";
//     echo "</tr>";
//   }
//   echo "</table>";
// }  



function GetFreeBusy($calendar_id, $calendar_date) {
  global $id; //GET OBJECTS FROM OUTSIDE
  global $item;
  global $service;
  $arrayTime = array();
  $id->setId($calendar_id);
  $item->setItems(array($id));
  $item->setTimeZone('America/Los_Angeles');
  $item->setTimeMax("{$calendar_date}T18:00:00+02:00");
  $item->setTimeMin("{$calendar_date}T08:00:00+02:00");
  $query = $service->freebusy->query($item);

  $start = $query["calendars"][$calendar_id]["busy"];
  $end = $query["calendars"][$calendar_id]["busy"];

  $length = count($start);
  for ($i = 0; $i < $length; $i++) {
    $startTime = $start[$i]["start"];
    $endTime = $start[$i]["end"];

    list($a, $b) = explode('T', $startTime);
    list($startHour, $d) = explode(':00+', $b);
    list($e, $f) = explode('T', $endTime);
    list($endHour, $g) = explode(':00+', $f);
    array_push($arrayTime, array($startHour, $endHour));
    // I CREATED AN ARRAY FOR MY NEEDS ex. [ ["8:00", "10:00"], ["14:00", "14:30"] ]
  }
  return $arrayTime;
}

function CreateEvent($calendarId="info@aimnotary.com", $summary="Hello", $location="India", $description="Dhur", $date="2020-09-30", $start="05:30", $end="06:00") {

  global $service;
  global $event;
  global $startT;
  global $endT;

  $startT->setTimeZone("America/Los_Angeles");
  $startT->setDateTime($date."T".$start.":00");
  $endT->setTimeZone("America/Los_Angeles");
  $endT->setDateTime($date."T".$end.":00");

  $event->setSummary($summary);
  $event->setLocation($location);
  $event->setDescription($description);
  $event->setStart($startT);
  $event->setEnd($endT);

  if($insert = $service->events->insert($calendarId, $event)) {
    return true;
  }

}

if($_POST['timezone']=="-8"){
  $timezone="Pacific Time (US & Canada)";
}elseif($_POST['timezone']=="-7"){
  $timezone="Mountain Time (US & Canada)";
}elseif($_POST['timezone']=="-6"){
  $timezone="Central Time (US & Canada)";
}elseif($_POST['timezone']=="-5"){
  $timezone="Eastern Time (US & Canada)";
}
$desc.="<b>Name : </b> ".$_POST['name']."<br>";
$desc.="<b>Email : </b> ".$_POST['email']."<br>";
$desc.="<b>Time Zone : </b> ".$timezone."<br>";
$desc.="<b>Location : </b>".$_POST['location']."<br>";
$desc.="<b>Description : </b>".$_POST['description']."<br>";

$time = strtotime($_POST['time']);
$endTime = date("H:i", strtotime('+30 minutes', $time));


CreateEvent("info@aimnotary.com","New Appointment Created",$_POST['location'],$desc,$_POST['date'],$_POST['time'],$endTime);


?>