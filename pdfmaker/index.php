<html>
    <head>
        <title>PDF Creator</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
        <script src="https://aimnotary.com/pdfmaker/justify/plugin.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style>
        #draggable { width: 150px; height: 150px; padding: 0.5em; }
        </style>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    </head>
    <body style="background-color: #E9EDF0;">
        <div class="container">
            <div class="row">
                <div class="col-md-7" style="height: 700px;background-color: #fff;margin-top : 50px; margin-bottom: 50px;box-shadow: 3px 3px 0px 0px rgba(36,42,77,0.18);">
                    <div id="titleFieldFromCkEditpr" style="margin-left:50px; text-align: justify; margin-right: 50px; margin-top: 50px;">
                       
                    </div>
                    <div id="bodyFieldFromCkEditpr" style="margin-left:20px; text-align: justify; margin-right: 50px; margin-top: 50px;">
                       
                    </div>
                    <div id="draggable" style="display: none;" class="ui-widget-content">
                        <img src="" style="height: 135px; width : 135px;" id="blah" />
                    </div>
                </div>
                <div class="col-md-5" style="margin-top : 50px;margin-bottom: 50px;">
                    <div class="form-group">
                        <label>PDF Title</label>
                        <textarea id="editor1" name="editor1"></textarea>
                    </div>
                    <div class="form-group">
                        <label>PDF Body</label>
                        <textarea id="editorNew2" name="editorNew2"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Stamp</label>
                        <input type="file" id="imgInp" name="stamp" class="form-control"/>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <script>
                var editorNew=CKEDITOR.replace( 'editor1' , { 
                    toolbar: [
                        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Save', 'NewPage', 'Preview', 'Print', 'Templates' ] },
                        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Paste' ] },
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', ] },
                        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align','bidi', 'paragraph', 'justify' ], 
                            items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
                        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                        { name: 'tools', items: [ 'ShowBlocks' ] },
                    ]
                }); 
                var editorNew2=CKEDITOR.replace( 'editorNew2' , { 
                    toolbar: [
                        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Save', 'NewPage', 'Preview', 'Print', 'Templates' ] },
                        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Paste' ] },
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', ] },
                        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align','bidi', 'paragraph', 'justify' ], 
                            items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
                        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                        { name: 'tools', items: [ 'ShowBlocks' ] },
                    ]
                }); 
                CKEDITOR.config.width = '100%'; 
                CKEDITOR.config.extraPlugins = "justify";
                editorNew.on('change',function(){
                    var value = CKEDITOR.instances.editor1.getData();
                    $("#titleFieldFromCkEditpr").html(value);
                });
                editorNew2.on('change',function(){
                    var value = CKEDITOR.instances.editorNew2.getData();
                    $("#bodyFieldFromCkEditpr").html(value);
                });
                $(function() {
                    $("#draggable").draggable();
                });
                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $('#blah').attr('src', e.target.result);
                            $("#draggable").css({ display : "block", });
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
                $("#imgInp").change(function(){
                    readURL(this);
                });
        </script>
    </body>
</html>