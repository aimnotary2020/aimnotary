const queryString1 = window.location.search;
const urlParams1 = new URLSearchParams(queryString1);
var nme = urlParams1.get('nme');
var docname = atob(nme);
const initialDoc = 'https://www.aimnotary.com/public/' + docname;
const IDS = {
    initialDoc: 'foo-12',
};

function base64ToBlob(base64) {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; ++i) {
        bytes[i] = binaryString.charCodeAt(i);
    }

    return new Blob([bytes], { type: 'application/pdf' });
};
WebViewer({
        path: './lib',
        pdftronServer: 'https://demo.pdftron.com/', // comment this out to do client-side only
        initialDoc: initialDoc,
        documentId: IDS[initialDoc],
    },
    document.getElementById('viewer')
).then(instance => {
    instance.disableElements(['toolbarGroup-Shapes']);
    instance.enableElements(['toolbarGroup-Edit']);
    instance.disableElements(['toolbarGroup-Measure']);

    samplesSetup(instance);
    const { docViewer, annotManager } = instance;
    


    document.getElementById('mySubmitButton').onclick = function(e) {
        submitAnnotation();
    };

    const submitAnnotation = async data => {
        // Import the annotation based on xfdf command
        const doc = docViewer.getDocument();
        const xfdfString = await annotManager.exportAnnotations();
        const options = { xfdfString };
        const data2 = await doc.getFileData(options);
        const arr = new Uint8Array(data2);
        const blob = new Blob([arr], { type: 'application/pdf' });
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const user_id = urlParams.get('tenant');
        const pdfname = atob(urlParams.get('nme'));
        // const name = urlParams.get('name');
        const csrf = urlParams.get('frsc');
        var ajaxurl = 'https://www.aimnotary.com/pdfsigner/updateSignerPdf';
        var myReader = new FileReader();
        myReader.readAsDataURL(blob);
        var pdfurl = window.URL.createObjectURL(blob);
        alert(pdfurl);

        myReader.onload = function(event) {

            var url = (window.URL || window.webkitURL).createObjectURL(blob);
            var data = new FormData();
            data.append('file', blob);
            data.append('docname', docname);
            $.ajax({
                url :  ajaxurl,
                type: 'POST',
                data: data,
                contentType: false,
                processData: false,
                success: function(data) {
                    swal("Good job!", "File Save Successfully!", "success");
                },    
                error: function() {
                    swal("Oops!", "Somethig Went Wrong!", "error");
                }
            });

        };

    };

});