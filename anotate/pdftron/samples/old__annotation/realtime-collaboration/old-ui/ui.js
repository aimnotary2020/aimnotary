// @link WebViewerInstance: https://www.pdftron.com/api/web/WebViewerInstance.html
// @link WebViewerInstance.setHeaderItems: https://www.pdftron.com/api/web/WebViewerInstance.html#setHeaderItems__anchor
// @link WebViewerInstance.enableElements: https://www.pdftron.com/api/web/WebViewerInstance.html#enableElements__anchor
// @link WebViewerInstance.disableElements: https://www.pdftron.com/api/web/WebViewerInstance.html#disableElements__anchor

// @link Header: https://www.pdftron.com/api/web/Header.html
// @link Header.getItems: https://www.pdftron.com/api/web/Header.html#getItems__anchor
// @link Header.update: https://www.pdftron.com/api/web/Header.html#update__anchor
const queryString1 = window.location.search;
const urlParams1 = new URLSearchParams(queryString1);
var nme = urlParams1.get('nme');
var docname = atob(nme);
const initialDoc = 'https://www.aimnotary.com/public/' + docname;
const IDS = {
    initialDoc: 'foo-12',
    'https://pdftron.s3.amazonaws.com/downloads/pl/report.docx': 'foo-13',
    'https://pdftron.s3.amazonaws.com/downloads/pl/presentation.pptx': 'foo-14',
};

WebViewer({
        path: '../../../lib',
        // pdftronServer: 'https://demo.pdftron.com/', // comment this out to do client-side only
        initialDoc: initialDoc,
        documentId: IDS[initialDoc],
    },
    document.getElementById('viewer')
).then(instance => {
    samplesSetup(instance);
    const { docViewer, annotManager } = instance;

    // const documentId = docViewer.getDocument().getDocumentId();
    //server.selectDocument(documentId);
    const onAnnotationCreated = async data => {
        // Import the annotation based on xfdf command
        const annotations = await annotManager.importAnnotCommand(data.val().xfdf);
        const annotation = annotations[0];
        if (annotation) {
            await annotation.resourcesLoaded();
            // Set a custom field authorId to be used in client-side permission check
            annotation.authorId = data.val().authorId;
            annotManager.redrawAnnotation(annotation);

            // viewerInstance.fireEvent('updateAnnotationPermission', [annotation]); //TODO
        }
    };
    const onAnnotationUpdated = async data => {
        // Import the annotation based on xfdf command
        const annotations = await annotManager.importAnnotCommand(data.val().xfdf);
        const annotation = annotations[0];
        if (annotation) {
            await annotation.resourcesLoaded();
            // Set a custom field authorId to be used in client-side permission check
            annotation.authorId = data.val().authorId;
            annotManager.redrawAnnotation(annotation);
        }
    };
    document.getElementById('mySubmitButton').onclick = function(e) {
        submitAnnotation();
    };
    const submitAnnotation = async data => {
        // Import the annotation based on xfdf command
        const doc = docViewer.getDocument();
        const xfdfString = await annotManager.exportAnnotations();
        const options = { xfdfString };
        const data2 = await doc.getFileData(options);
        const arr = new Uint8Array(data2);
        const blob = new Blob([arr], { type: 'application/pdf' });
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const user_id = urlParams.get('tenant');
        const pdfname = atob(urlParams.get('nme'));
        // const name = urlParams.get('name');
        const csrf = urlParams.get('frsc');
        var ajaxurl = 'https://www.aimnotary.com/update-pdf/';
        var myReader = new FileReader();
        myReader.readAsDataURL(blob);
        var pdfurl = window.URL.createObjectURL(blob) + "#view=FitW";
        alert(pdfurl);
        myReader.onload = function(event) {
            result = event.target.result;
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    // document.getElementById("demo").innerHTML = this.responseText;
                }
            };
            xhttp.open("POST", ajaxurl, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send("pdffile=" + result);
        };

    };
    const onAnnotationDeleted = data => {
        // data.key would return annotationId since our server method is designed as
        // annotationsRef.child(annotationId).set(annotationData)
        const command = `<delete><id>${data.key}</id></delete>`;
        annotManager.importAnnotCommand(command);
    };
    const openReturningAuthorPopup = authorName => {
        if (hasSeenPopup) {
            return;
        }
        // The author name will be used for both WebViewer and annotations in PDF
        annotManager.setCurrentUser(authorName);
        // Open popup for the returning author
        // window.alert(`Welcome back ${authorName}`);
        hasSeenPopup = true;
    };
    const updateAuthor = authorName => {
        // The author name will be used for both WebViewer and annotations in PDF
        annotManager.setCurrentUser(authorName);
        // Create/update author information in the server
        //server.updateAuthor(authorId, { authorName });
    };
    const openNewAuthorPopup = () => {
        // Open prompt for a new author
        // const name = window.prompt('Welcome! Tell us your name :)');
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const user_id = urlParams.get('user_id');
        // const name = Math.floor(Math.random() * 10000);
        const name = user_id;
        if (name) {
            updateAuthor(name);
        }
    };
    // Bind server-side authorization state change to a callback function
    // The event is triggered in the beginning as well to check if author has already signed in






    const toggleElement = (e, dataElement) => {
        // Enable/disable individual element
        if (e.target.checked) {
            instance.enableElements([dataElement]);
        } else {
            instance.disableElements([dataElement]);
        }
    };

    if (NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = Array.prototype.forEach;
    }

    if (HTMLCollection && !HTMLCollection.prototype.forEach) {
        HTMLCollection.prototype.forEach = Array.prototype.forEach;
    }

    document.getElementsByName('header').forEach(radioInput => {
        radioInput.onchange = () => {
            reverseHeaderItems();
        };
    });

});