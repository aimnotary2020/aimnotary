<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Aimnotary - Legally Notarize Your Documents Online</title>
</head>
<body style="margin: 0; padding: 0;">
	<table data-module="Layou-1" data-bgcolor="Background Color" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#494c50" align="center">
		<tr>
			<td data-bg="Main BG" style="background:#fff url(http://econstrademosite.com/AimNotary/assets/img/email-template-bg.jpg) no-repeat center top/cover;" align="center">
				<table cellspacing="0" cellpadding="0" border="0" align="center">
					<tr>
						<td height="50">
						</td>
					</tr>
					<!-- logo -->
					<tr>
						<td style="line-height: 0px;" align="center">
							<a href="#" style="background-color: #fff;display: inline-block;padding: 12px 20px;border-radius: 10px;box-shadow: 0px 3px 0px #D4D2D2;">
								<img src="http://econstrademosite.com/AimNotary/assets/img/logo.png" alt="logo" style="display:block;" data-crop="false">
							</a>
						</td>
					</tr>
					<!-- end logo -->
					<tr>
						<td height="20">
						</td>
					</tr>
					<tr>
						<td height="40">
						</td>
					</tr>
					<tr>
						<td width="600" align="center">
							<table style="border-radius:4px; box-shadow: 0px -3px 0px #D4D2D2;" width="95%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF" align="center">
								<tr>
									<td height="50">
									</td>
								</tr>
								<tr>
									<td align="center">
										<table width="90%" cellspacing="0" cellpadding="0" border="0" align="center">
											<tr>
												<td data-link-style="text-decoration:none; color:#21b6ae;" data-link-color="Content Link" data-color="Headline" data-size="Headline" style="font-family: 'Open Sans', Arial, sans-serif; font-size:36px; color:#3b3b3b; font-weight: bold; letter-spacing:2px;" align="center">
													<singleline>
														Welcome to Aimnotary
													</singleline>
												</td>
											</tr>
											<tr>
												<td align="center">
													<table width="25" cellspacing="0" cellpadding="0" border="0">
														<tr>
															<td data-border-bottom-color="Dash" style="border-bottom:2px solid #21b6ae;" height="20">
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td height="20">
												</td>
											</tr>
											<!-- content -->
											<tr>
												<td data-link-style="text-decoration:none; color:#21b6ae;" data-link-color="Content Link" data-color="Main Text" data-size="Main Text" style="font-family: 'Open Sans', Arial, sans-serif; font-size:15px; color:#7f8c8d; line-height:30px; text-align: left;" align="center">
													<multiline>
														<b>Hello {{ ucwords($user['name']) }},</b>
														<br/>
														Thanks for subscribing to our One Document Notarization Plan/ Business PRO Plan
														<br/>
														<br/>
														Regards,
														<br/>
														<b>Aimnotary Team</b>
													</multiline>
												</td>
											</tr>
											<!-- end content -->
										</table>
									</td>
								</tr>
								<tr>
									<td height="40">
									</td>
								</tr>
								<!-- button -->
								<tr>
									<td align="center">
										<table data-bgcolor="Button" class="textbutton" style=" border-radius:5px; box-shadow: 0px 1px 0px #D4D2D2;" cellspacing="0" cellpadding="0" border="0" bgcolor="#1a28a8" align="center">
											<tr>
												<td data-link-style="text-decoration:none; color:#FFFFFF;" data-link-color="Button Link" data-size="Button" style="font-family: 'Open Sans', Arial, sans-serif; font-size:18px; color:#FFFFFF; font-weight: bold;padding-left: 25px;padding-right: 25px;" height="55" align="center">
													<a href="http://aimnotary.com/beta" target="_blank" style="color:#ffffff;text-decoration:none;" data-color="Button Link">View Notary</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- end button -->
								<tr>
									<td height="45">
									</td>
								</tr>
								<!-- option -->
								<tr>
									<td data-bgcolor="Link BG" style=" border-bottom-left-radius:4px; border-bottom-right-radius:4px;" bgcolor="#f3f3f3" align="center">
										<table cellspacing="0" cellpadding="0" border="0" align="center">
											<tr>
												<td height="15">
												</td>
											</tr>
											<tr>
												<td data-link-style="text-decoration:none; color:#7f8c8d;" data-link-color="Text Link" data-color="Main Text" style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#7f8c8d;" align="center">
													<webversion>
														&nbsp;
													</webversion>
													<unsubscribe>
														&nbsp;
													</unsubscribe>
												</td>
											</tr>
											<tr>
												<td height="15">
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<!-- end option -->
							</table>
						</td>
					</tr>
					<tr>
						<td style="line-height:0px;" align="center">
							<img style="display:block; line-height:0px; font-size:0px; border:0px;" src="http://econstrademosite.com/AimNotary/assets/img/point.png" alt="img" />
						</td>
					</tr>
					<tr>
						<td height="30">
						</td>
					</tr>
					<tr>
						<td height="30">
						</td>
					</tr>
					<!-- social -->
					<tr>
						<td align="center">
							<table cellspacing="0" cellpadding="0" border="0" align="center">
								<tr>
									<td style="line-height:0xp;" align="center">
										 <a href="#"><img src="http://econstrademosite.com/AimNotary/assets/img/fb.png" alt="img" style="display:block; line-height:0px; font-size:0px; border:0px;" data-crop="false" width="16" /></a>
									</td>
									<td width="20">
									</td>
									<td style="line-height:0xp;" align="center">
										 <a href="#"><img src="http://econstrademosite.com/AimNotary/assets/img/tw.png" alt="img" style="display:block; line-height:0px; font-size:0px; border:0px;" data-crop="false" width="16" /></a>
									</td>
									<td width="20">
									</td>
									<td style="line-height:0xp;" align="center">
										 <a href="#"><img src="http://econstrademosite.com/AimNotary/assets/img/in.png" alt="img" style="display:block; line-height:0px; font-size:0px; border:0px;" data-crop="false" width="16" /></a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- end social -->
					<tr>
						<td height="15">
						</td>
					</tr>
					<!-- copyright -->
					<tr>
						<td data-link-style="text-decoration:none; color:#21b6ae;" data-link-color="Copyright Link" data-color="Copyright" data-size="Copyright" style="font-family: 'Open Sans', Arial, sans-serif; font-size:13px; color:#FFFFFF;" align="center">
							<singleline>
								&copy;2020 All Rights Reserved AimNotary
							</singleline>
						</td>
					</tr>
					<!-- end copyright -->
					<tr>
						<td height="30">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
