<!DOCTYPE html >
<html lang="en-US">
<head>
  <meta charset="utf-8">
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>

<table id="customers">
  
 	<tr>
 		<td>Name :</td>
 		<td>{{ ucwords($user['f_name']) }} </td>
 	</tr>
 	<tr>
 		<td>Email id :</td>
 		<td>{{ $user['user_eml'] }} </td>
 	</tr>
 	<tr>
 		<td>Phone Number : </td>
 		<td>{{ $user['user_contact_no'] }}</td>
 	</tr>
    @if(isset($user['business_type']))
	 	<tr>
	 		<td>Requirement Type : </td>
	 		<td>{{ $user['business_type'] }}</td>
	 	</tr>
	@endif
 	<tr>
 		<td>Message :</td>
 		<td>{{ $user['message'] }} </td>
 	</tr>
</table>

</body>
</html>
