@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Tennessee</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Tennessee</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Tennessee</span>
							<h2>TENNESSEE</h2>

							<p>In April of 2018, the Tennessee Legislature passed the Senate Bill 1758, which allows an online notary public to be in a different physical location than the signer using two-way audio-video communication. This process is called Remote Online Notarization. By passing this law, the state of Tennessee is permitting notary services online. By allowing an online notary public to handle notarizations, the state of Tennessee is creating an environment where a signer can have access to a virtual notary 24/7.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/tennessee-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

						<p>Chapter 1360-07-03 permits authorized Tennessee notaries to perform an online notarization authorized under Chapter 1360-07-03 that meets the requirement regardless of whether the principal is physically located in this state at the time of the online notarization. The amendment addresses criteria for proper identification of the principal and permitted forms of signature and seal for notaries public. The guidelines for a Tennessee notary public to qualify and apply for remote online notarization commission are listed within this amendment. The credentials for verifying the signer’s identification, storage of video files, and the notaries public electronic signature and seal are provided in Chapter 1360-07-03 for Online Notaries Public.</p>

						<h2>Requirements For Remote Online Notaries in Tennessee</h2>

						<p>If you are interested in becoming a notary public in the state of Tennessee, below are a some, but not all requirements, for becoming a Remote Online Notary Public:</p>

						<ul>
							<li>Be an active Tennessee notary public</li>
							<li>No convictions for felonies or crimes of moral turpitude</li>
							<li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
							<li>Obtain an electronic seal (that meets state requirements)</li>
						</ul>

						<h2>How to Apply to be a Tennessee Remote Online Notary Public</h2>

						<p>The state of Tennessee allows individuals to apply to become a Remote Online Notary Public completely online. Click <a href="https://sos.tn.gov/products/business-services/online-notaries-public" target="_blank">HERE</a>.</p>

						<p>The following is an overview of information you will need to provide throughout the approval process:</p>

						<ul>
							<li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
							<li>Acknowledgment of state requirements</li>
							<li>Contracted with an approved third-party vendor (prior to applying)</li>
							<li>Copy of your digital seal, signature, identity proofing process, and more</li>
						</ul>

						<p>Information in this article is subject to be updated by the Tennessee Secretary of State. Therefore, it is essential that Tennessee notaries public review necessary information via the website. Click <a href="https://publications.tnsosfiles.com/rules/1360/1360-07/1360-07-03.20200107.pdf" target="_blank">HERE!</a></p>

						<p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>						
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>

@endsection