@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Hawaii</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Hawaii</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Hawaii</span>
							<h2>HAWAII</h2>

                            <p>On January 1, 2021, Hawaii joined many other states by allowing their notaries to perform remote online notarizations in the state of Hawaii. Previously, notary services online were temporarily approved; however, they are now permanent for notaries in Hawaii. The state of Hawaii is allowing current notaries public to apply to become a virtual notary through the remote online notarization process. Having access to an online notary will assist signers in Hawaii to safely and securely notarize documents real time.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/hawaii-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

                        <h2>Requirements For Remote Online Notaries in Hawaii</h2>

                        <p>If you are interested in becoming a remote online notary public in the state of Hawaii, below are a some, but not all, requirements for becoming a Remote Online Notary Public:</p>

                        <ul>
                            <li>Be an active Hawaiian notary public</li>
                            <li>Acknowledge Hawaiian remote notary provider protocols</li>
                            <li>Register with a remote online technology provider that meets state requirements</li>
                            <li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
                            <li>Obtain an electronic seal (that meets state requirements)</li>
                        </ul>

                        <h2>How to Apply to be a Hawaiian Remote Online Notary Public</h2>

                        <p>The state of Hawaii allows individuals to apply to become a Remote Online Notary Public. Click <a href="https://notary.ehawaii.gov/notary/public/welcome.html" target="_blank">HERE</a>.</p>

                        <p>The following is an overview of information you will need to provide throughout the approval process:</p>

                        <ul>
                            <li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
                            <li>Acknowledgment of state requirements</li>
                            <li>Copy of your digital seal, signature, identity proofing process, and more</li>
                        </ul>

                        <p>Information in this article is subject to be updated by the Hawaii Secretary of State. Therefore, it is essential that Hawaii notaries public review necessary information via the website. Click <a href="https://notary.ehawaii.gov/notary/public/welcome.html" target="_blank">HERE</a>!</p>

                        <p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>

@endsection