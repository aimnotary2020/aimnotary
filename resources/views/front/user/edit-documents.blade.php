@extends('front.layouts.app')
  @section('content')
	<?php 
		$front=url('/').'/'.$documents['front']; 
		$back=url('/').'/'.$documents['back']; 
		$doc_type = $documents['doc_type'];
		
		function timthumb($path = NULl, $height = NULL, $width = NULL) { 
			
			if (strpos(substr($path, 0,6), 'htt') === false) {
				$path = if_exist_file($path);
			}
			$imgPath = "";
			if($path != ""){
				$imgPath = url('/').'/timthumb.php?src='.$path;
			}
			if($height != ""){
				$imgPath .= "&h=".$height;
			}
			if($width != ""){
				$imgPath .= "&w=".$width;
			}
			
			$imgPath .= "&zc=1q=100";
			return $imgPath;
		}
	?>
	<style>
		.butn{
			background-color: #4CAF50; /* Green */
			border: none;
			color: white;
			padding: 15px 32px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			margin: 4px 2px;
			cursor: pointer;
		}
	</style>
	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Please upload any identity proof</h2>
						<p>Make sure we require both front & back of your identity card</p>
					</div>
					<div class="upload-box">
						<form id="confirm-detail" method="POST" action="javascript:void(0);">
							<div class="row">
								<div class="col-lg-5 col-sm-12" align="left">
									<div class="form-group">
										<input type='hidden' name='doc_id' value="{{ $new_doc_id }}" />
										<label style="margin-left : 50px; font-size: 20px;font-weight: 700;" class="pull-left">Select type of photo id you add<span class="text-danger">*</span></label> <br><br>
										<input type="radio" style="margin-left : 50px;" name="id_type" value="DL" <?php if($doc_type=='DL'){ echo "checked"; } ?>/> US Driver's License <br><br>
										<input type="radio" style="margin-left : 50px;" name="id_type" value="OG" <?php if($doc_type=='OG'){ echo "checked"; } ?>/> Others US Government-Issued ID &nbsp;&nbsp;<img src="{{ url('public/question.jpg') }}" /> <br><br>
										<input type="radio" style="margin-left : 50px;" name="id_type" value="PS" <?php if($doc_type=='PS'){ echo "checked"; } ?>/> Passport <br><br>
									</div>
								</div>
								

								<div class="col-md-3">
									<div class="form-group" style="margin:0 auto;">
										<label for="l301"></label>
										<div class="cropme" id='my_camera' style="width: 225px; height: 150px;border:1px solid #ccc;background-image: url('{{ timthumb($front,125,225) }}');"></div>
										<textarea name="front" style="display: none;" id="imgCrop" class="form-control"></textarea>
										<br>
										<b>Or</b><br>
										<a onclick="activateCam();" id="snapActivate" class="btn btn-success"><i class="fas fa-camera"></i> Activate Camera</a>
										<a style="display : none; color : white;" class="btn btn-success" id="Capture" onclick="take_snapshot();"><i class="fas fa-camera"></i> Capture</a>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group" style="margin:0 auto;">
										<label for="l301"></label>
										<div class="cropme1" id="my_camera1" style="width: 225px; height: 150px;border:1px solid #ccc;background-image: url('{{ timthumb($back,125,225) }}');"></div>
										<textarea name="back" style="display: none;" id="imgCrop1" class="form-control"></textarea>
										<br>
										<b>Or</b><br>
										<a onclick="activateCam2();" id="snapActivate1" class="btn btn-success"><i class="fas fa-camera"></i> Activate Camera</a>
										<a style="display : none; color : white;" id="Capture1" class="btn btn-success" onclick="take_snapshot1();"><i class="fas fa-camera"></i> Capture</a>
									</div>
								</div>
								
								<div class="col-lg-12 col-sm-12">
									<div class="form-group mt-30">
										<button type="submit" class="default-btn page-btn pull-left">Upload</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					
					
					
				</div>
			</div>
		</div>
	</section>
		
   @endsection
   	@section('script')
	    <link href="{{ url('') }}/public/crop/css/style.css" rel="stylesheet" type="text/css"/>
		<link href="{{ url('') }}/public/crop/css/style-example.css" rel="stylesheet" type="text/css"/>
		<link href="{{ url('') }}/public/crop/css/jquery.Jcrop.css" rel="stylesheet" type="text/css"/>

		<script src="{{ url('') }}/public/crop/scripts/jquery.Jcrop.js"></script>
		<script src="{{ url('') }}/public/crop/scripts/jquery.SimpleCropper.js"></script>

		<script src="{{ url('public/webcam/webcam.min.js') }}"></script>
       <script type="text/javascript">
	   	 $('.cropme').simpleCropper();
		 $('.cropme1').simpleCropperNew();
		 $("#confirm-detail").validate({
	     	rules:{
	            id_type: {
	                required: true,
	            },
	        },
	        messages: {
	            id_type: {
	                required:'First Name is required',
	            },
	        },
	        submitHandler: function() { 
				
				if($("#imgCrop").val()=="" || $("#imgCrop1").val()=="")
				{
					swal("Oops!", "Front / Back side of your ID is missing", "error");
				}
				else
				{
					var form = $('#confirm-detail').serialize();
					loadingfunc("block");
					$.ajax({
							url: '{{url("verify-documents-submit")}}',
							type: 'POST',
							headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
							data: form,
							beforeSend:function(){
									// $('#contact-btn').html('Please Wait...');
									// $('#contact-btn').attr('disabled','disabled');
							},
							success: function(data){
								loadingfunc("none");
								toastr.success('Document Updated!.');
								setTimeout(function(){ window.close(); },2000);
							}
						});
				}

	           	
	        }
	    });
	   </script>
	   <script language="JavaScript">
		  function activateCam()
		  {
			  $("#snapActivate").css({ display : 'none', });
			  $("#Capture").css({ display : 'block', });
			  
				Webcam.set({
					width: 1000,
					height: 600,
					image_format: 'jpeg',
					jpeg_quality: 90
				});
				Webcam.attach( '#my_camera' );
		  } 

		  function activateCam2()
		  {
			$("#snapActivate1").css({ display : 'none', });
			$("#Capture1").css({ display : 'block', });
			Webcam.set({
				width: 1000,
				height: 600,
				image_format: 'jpeg',
				jpeg_quality: 200
			});
			Webcam.attach( '#my_camera1' );
		  }
		
		</script>
		
		<!-- A button for taking snaps -->
		<form>
			<input type=button value="Take Snapshot" onClick="take_snapshot()">
		</form>
		
		<!-- Code to handle taking the snapshot and displaying it locally -->
		<script language="JavaScript">
			function take_snapshot() {
				// take snapshot and get image data
				Webcam.snap( function(data_uri) {
					// display results in page
						$(".cropme").html('<img src="'+data_uri+'"/>');
						$("#imgCrop").val(data_uri);
				} );
			}

			function take_snapshot1()
			{
				// take snapshot and get image data
				Webcam.snap( function(data_uri) {
					// display results in page
					$(".cropme1").html('<img src="'+data_uri+'"/>');
					$("#imgCrop1").val(data_uri);
				} );

			}
		</script>
	@endsection