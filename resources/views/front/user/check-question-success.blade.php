@extends('front.layouts.app')

  @section('content')



	<section class="inner-page-container pt-50 pb-100 user-area-all-style">

		<div class="container">

			<div class="company-us-area">

				<div class="company-tab-wrap upload-page-cont">

					<div class="dashboard-top">

						<h2>Verify Your Identity</h2>

					</div>

					<div class="upload-box">

						<img class="img-responsive margin-b20" src="{{asset('public/front_assets/img/icon-success.png')}}" alt="SSN ICON" />

						<div class="clearfix"></div>

						<h3 class="margin-b20">Success</h3>

						<div class="clearfix"></div>

					</div>

					<a class="btn btn-success btn-lg pull-left margin-b20" href="{{ url('start-video-call').'/'.base64_encode(session('Document_ID').'~~'.Auth()->user()->id)  }}">Next</a>

				</div>

			</div>

		</div>

	</section>

		

  @endsection