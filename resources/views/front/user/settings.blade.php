@extends('front.layouts.app')
@section('content')
<style>

/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  min-height: 755px;
}
/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}
/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}
/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}
/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 70%;
	min-height: 755px;
}
</style>
	<!-- Start Sign Up Area -->
	<div class="page-title-area bg-18">
		<div class="container">
			<div class="page-title-content">
				<h2>Settings</h2>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li>Settings</li>
				</ul>
			</div>
		</div>
	</div>
	<section class="inner-page-container pt-50 pb-100 user-area-all-style sign-up-area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="contact-form-action" style="max-width : 100% !important">
						@if(session('message'))
							<b style='color : green;'>{{session('message')}}</b>
						@elseif(session('error'))
							<b style='color : red;'>{{session('message')}}</b>
						@endif
						<div class="tab">
							<button class="tablinks" onclick="openCity(event, 'au')" id="defaultOpen">Account Update</button>
							<button class="tablinks" onclick="openCity(event, 'cp')">Change Password</button>
						</div>
						<div id="au" class="tabcontent" style="padding : 30px;">
							<h3>Update Account Information</h3><br>
							<form action="{{ url('update-account') }}" id="account_info" method="post">
								
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>First Name</label>
											<input type="text" value="<?php echo explode(" ",$data[0]['name'])[0]; ?>" disabled name="first_name" class="form-control" />
											<input type="hidden" value="<?php echo csrf_token(); ?>" name="_token"/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Last Name</label>
											<input type="text" value="<?php echo explode(" ",$data[0]['name'])[1]; ?>" disabled name="last_name" class="form-control" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Email Address</label>
											<input type="text" value="<?php echo $data[0]['email']; ?>" disabled name="email" class="form-control" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Contact Number</label>
											<input type="text" value="<?php echo $data[0]['personal_mobile']; ?>" name="personal_mobile" class="form-control" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Enter State</label>
											<input type="text" value="<?php echo $data[0]['state']; ?>" name="state" class="form-control" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Enter City</label>
											<input type="text" value="<?php echo $data[0]['city']; ?>" name="city" class="form-control" />
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Zip code</label>
											<input type="text" value="<?php echo $data[0]['zip_code']; ?>" name="zip_code" class="form-control" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Address</label>
											<input type="text" value="<?php echo $data[0]['add1']; ?>" name="add1" class="form-control" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<input type="submit" class="btn btn-success" />
										</div>
									</div>
								</div>
							</form>
							
						</div>
						<div id="cp" class="tabcontent" style="padding : 30px;">
							<h3>Change Password</h3><br>
							<form id="password" action="{{ url('change-user-password') }}" method="post">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Old Password</label>
											<input type="text" name="old_pass" class="form-control" />
											<input type="hidden" value="<?php echo csrf_token(); ?>" name="_token"/>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>New Password</label>
											<input type="text" name="new_pass" id="new_pass" class="form-control" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label>Confirm Password</label>
											<input type="text" name="cnf_pass" class="form-control" />
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<input type="submit" class="btn btn-success" />
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
		function openCity(evt, cityName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(cityName).style.display = "block";
			evt.currentTarget.className += " active";
		}
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();

		
	</script>
	<!-- End Sign Up Area -->
@endsection
@section('script')
 <script>
		$("#account_info").validate({
			rules : {
				first_name : {
					required : true,
				},
				last_name : {
					required : true,
				},
				email : {
					required : true,
					emsil : true,
				},
				personal_mobile : {
					required : true,
				},
				state : {
					required : true,
				},
				city : {
					required : true,
				},
				zip_code : {
					required : true,
				},
				add1 : {
					required : true,
				},
			},
			submitHandler : function(form){
				document.getElementById("account_info").submit();
			},
		});

		$("#password").validate({
			rules : {
				old_pass : {
					required : true,
					// remote : "{{ url('checkPasswordAjax') }}",
				},
				new_pass : {
					required : true,
				},
				cnf_pass : {
					required : true,
					equalTo : "#new_pass",
				},
			},
			messages : {
				old_pass : {
					required : "This is a mendatory field",
					remote : "Old Password not matched",
				},
				new_pass : {
					required : "This is a mendatory field",
				},
				cnf_pass : {
					required : "This is a mendatory field",
					equalTo : "New & Confirm Password is not match",
				},
			},
			submitHandler : function(form){
				document.getElementById("password").submit();
			}
		})
 </script>
@endsection