@extends('front.layouts.app')
  @section('content')
	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Confirm Your Details</h2>
						<p>Make sure your full name is entered as it appears on your ID. It will be used to generate your signature and initials.</p>
					</div>
					<div class="upload-box">
						<form id="confirm-detail" method="POST" action="javascript:void(0);">
							<div class="row">
								<div class="col-lg-4 col-sm-12">
									<div class="form-group">
										<label class="pull-left">First Name<span class="text-danger">*</span></label>
										<input class="form-control" type="text" placeholder="First Name" name="f_name" value="" required="">
									</div>
								</div>
								<div class="col-lg-4 col-sm-12">
									<div class="form-group">
										<label class="pull-left">Middle Name</label>
										<input class="form-control" type="text" placeholder="Middle Name" name="m_name" value="">
									</div>
								</div>
								<div class="col-lg-4 col-sm-12">
									<div class="form-group">
										<label class="pull-left">Last Name <span class="text-danger">*</span></label>
										<input class="form-control" type="text" placeholder="Last Name" name="l_name" value="" required="">
									</div>
								</div>
								<input type="hidden" name="veify_name" value="veify_name">
								<div class="col-lg-12 col-sm-12">
									<div class="form-group">
										<button type="submit" class="default-btn page-btn pull-left">
											Confirm
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					
					
					
				</div>
			</div>
		</div>
	</section>
		
   @endsection
   	@section('script')
       <script type="text/javascript">
		 $("#confirm-detail").validate({
	     	rules:{
	            f_name: {
	                required: true,
	            },
	            l_name: {
	                required: true,
	               
	            }
	        },
	        messages: {
	            f_name: {
	                required:'First Name is required',
	            },
	            l_name: {
	                required:'Last Name is required',
	            }
	        },
	        submitHandler: function() { 
				 loadingfunc("block");
	             var form = $('#confirm-detail').serialize();
	             $.ajax({
			            url: '{{url("verify_ssn_nm")}}',
			            type: 'POST',
			            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			            data: form,
			            beforeSend:function(){
			            		 // $('#contact-btn').html('Please Wait...');
			            		 // $('#contact-btn').attr('disabled','disabled');
			            },
			            success: function(data){
						  var obj=JSON.parse(data);
						  loadingfunc("none");
			              toastr.success('Name Verified Successfully!.');
						   setTimeout(function(){ window.location ="{{ url('check-question-two') }}"  },1000);
			                
			                
			            }
			        });
	        }
	    });
       </script>
	@endsection