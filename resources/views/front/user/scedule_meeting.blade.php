@extends('front.layouts.app')
  @section('content')
  

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap">
					<h2>Scedule Meeting</h2>
					<div class="dashboard-top">						
						<div id='calendar'></div>
						<div id='datepicker'></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modal1" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" id="scedule-form">
					<div class="modal-body" style="padding-top: 25px; padding-bottom: 25px;">
						<div class="row">
							<div class="col-md-12">
								<input type="hidden" value="New Appointment is Scheduled" class="form-control" name="title" id="title" />
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label>Timezone</label>
									<select name="timezone" id="timezone" class="form-control">
										<option value="">Select a Timezone</option>
										<option value="-8">Pacific Time (US & Canada)</option>
										<option value="-7">Mountain Time (US & Canada)</option>
										<option value="-6">Central Time (US & Canada)</option>
										<option value="-5">Eastern Time (US & Canada)</option>
									</select>
								</div>
							</div>
						    <div class="col-md-12">
								<div class="form-group">
									<label for="starts-at">Location</label>
									<select name="location" id="location" class="form-control">
										<option value="">Select a Location</option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="DC">District of Columbia</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
									</select>
								</div>
							</div>
						    <div class="col-md-12">
								<div class="form-group">
									<label for="starts-at">Next Schedule Date</label>
									<input type="text" class="form-control datepicker" name="date" id="starts_at" />
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="starts-at">Next Schedule Time</label>
									<select name="time" id="time" class="form-control">
										<option> Select Timing-</option>
										<?php
											$start = "06:00";
											$end = "22:00";

											$tStart = strtotime($start);
											$tEnd = strtotime($end);
											$tNow = $tStart;

											while($tNow <= $tEnd){ 
												$tNow = strtotime('+30 minutes',$tNow);
												?> 
													<option><?php echo date("H:i",$tNow)."\n"; ?></option> 
												<?php		
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label for="starts-at">Description</label>
									<textarea name="description" id="description" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-md-12" style="margin-top: 15px;">
								<input type="button" id="save-event" value="Schedule Appointment" class="btn btn-success"/>
							</div>
						</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>


	<div class="modal fade" id="modal2" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" id="scedule-form">
					<div class="modal-body" style="">
						<div class="row">
							<div class="col-md-12">
								<span id="eventName"></span>
							</div>
						</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>
          
@endsection
@section('script')

<script src="https://momentjs.com/downloads/moment.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.js'></script>
<link rel='stylesheet' href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
	.fc-day-header
	{
		background-color: #4424a8 !important;
		color : white !important;
	}
</style>
	<script type="text/javascript">
	$(document).ready(function() {
	$( ".datepicker" ).datepicker({
		dateFormat : 'yy-mm-dd',
		minDate : 'today',
		changeMonth: true, 
    	changeYear: true, 
	});
	$('#calendar').fullCalendar({
		header: {
			left: 'next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		defaultDate: '<?php echo date("Y-m-d"); ?>',
		navLinks: true, // can click day/week names to navigate views
		selectable: true,
		selectHelper: true,
		select: function(start, end) {
			// Display the modal.
			// You could fill in the start and end fields based on the parameters
			let date_ob = new Date();
			let startDta=new Date(start);

			date_ob_day = date_ob.getDate();
			startDta_month = startDta.getDate();

			if(date_ob<=startDta){
				let date_ob = new Date(start);
				// adjust 0 before single digit date
				let date = ("0" + date_ob.getDate()).slice(-2);
				let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
				let year = date_ob.getFullYear();
				$("#starts_at").val(year + "-" + month + "-" + date);
				$('#modal1').modal('show');
			}
			if(date_ob_day == startDta_month){
				let date_ob = new Date(start);
				// adjust 0 before single digit date
				let date = ("0" + date_ob.getDate()).slice(-2);
				let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
				let year = date_ob.getFullYear();
				$("#starts_at").val(year + "-" + month + "-" + date);
				$('#modal1').modal('show');
			}

		},
		eventSources: [
		{
			url: '{{ url("get-scedules?document_id=").$document_id }}',
			method: 'POST',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			failure: function() {
				alert('there was an error while fetching events!');
			},
			color: '#007bff',   // a non-ajax option
			textColor: 'white' // a non-ajax option
		}],
		eventClick: function(event, element) {
			// Display the modal and set the values to the event values.
			var urlData="document_id=<?php echo $document_id; ?>";
			$.ajax({
				     url: '{{ url("get-single-scedules") }}', 
				     type: 'post',
				     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				     data: urlData,
				     success: function (response) {
						$("#modal2").modal("show");
						$("#eventName").html(response);
						
				     }
				});

			
			
		},
		editable: true,
		eventLimit: true // allow "more" link when too many events

	});
	
	$(".fc-day").click(function(){
		var moment = $('#calendar').fullCalendar('getDate');
		$('#modal1').modal('show');
		$('#modal1').find('#title').val(event.title);
		$('#modal1').find('#starts_at').val(event.start);
	});
	// Whenever the user clicks on the "save" button om the dialog
	$('#save-event').on('click', function() {
		var title = $('#title').val();
		var starts_at = $('#starts_at').val();
		var time = $('#time').val();
		var description=$("#description").val();
		var timezone=$("#timezone").val();
		var location=$("#location").val();
		if(title!="" && starts_at!="" && time!="")
		{
			loadingfunc("block");
			var urlData="document_id=<?php echo $document_id; ?>&timezone="+timezone+"&location="+location+"&description="+description+"&title="+title+"&starts_at="+starts_at+"&time="+time+"&token=<?php echo csrf_token(); ?>";
				$.ajax({
				     url: "{{ url('submit-schedule') }}", 
				     type: 'post',
				     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				     data: urlData,
				     success: function (response) {
						var obj=JSON.parse(response);
						if(obj.status==1)
						{
							loadingfunc("none");
							swal({
								title: "Thank You",
								text: obj.msg,
								icon: "success",
							}).then((value) => {
								window.location.href = "{{ url('dashboard') }}";
							});
						}
						else
						{
							loadingfunc("none");
							swal({
								title: "Oops!",
								text: obj.msg,
								icon: "warning",
							});
						}
				     }
				});
		}
		
	});

});
	</script>
@endsection     