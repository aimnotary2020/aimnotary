@extends('front.layouts.app')
  @section('content')
	
	<?php //print_r($result); ?>
	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap">
					<div class="dashboard-top">
						@if(session('payment_success'))
							<p style="color: green; font-weight : 800;">{{ session('payment_success') }} </p>
						@endif
						<h2 class="float-left">Sign Documents</h2>
						<?php
							// echo "<pre>"; print_r($documents->toArray());
							if($result[0]->doc>=5){
							?>
								<a style='margin-left: 15px;font-size : 13px;' class="btn btn-info" href="javascript:void(0);">
									Documents Subscribed -  <?php echo ($result[0]->doc); ?>
								</a>	
								<a style='margin-left: 15px;font-size : 13px;' class="btn btn-success" href="javascript:void(0);">
									Balance Documents -  <?php echo ($result[0]->doc - count($complete_docs)) ?>
								</a>	
							<?php
							}
						?>
						<?php
							if(isset($payment_button)){
								echo $payment_button;
							}
						?>
						
						<!-- <a class="default-btn page-btn text-white float-right" href="{{ url('verify-identity') }}">Verify</a> -->
						<div class="clearfix"></div>
					</div>
					<div class="tab company-tab">
						<ul class="tabs">
							<li>
								<a href="javascript:void(0);">All</a>
							</li>
							<li>
								<a href="javascript:void(0);">Unsigned</a>
							</li>
							<li>
								<a href="javascript:void(0);">Completed</a>
							</li>
						</ul>
						<div class="tab_content">
							<div class="tabs_item">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th scope="col">File</th>
												<th scope="col">Status</th>
												<th scope="col">Action</th>
												@php
												print_r($documents->toArray());
												@endphp
											</tr>
										</thead>
										<tbody>
											@if(!$documents->isEmpty())
												@foreach($documents as $eachDoc)
													<?php
														$nndoc=str_replace("upload/user_doc/","",$eachDoc->docname);
														$docNameLatest="https://www.aimnotary.com/signer/uploads/files/".$nndoc;
													?>
														
														<tr id="remove_tr_{{ $eachDoc->id  }}">
															<td><a href="javascript:void();" download><i class="fa fa-download"  aria-hidden="true"></i> {{ $eachDoc->doc_nm }} </a></td>
															<td>
																@if($eachDoc->status=='1')
																	<span class="text-primary"><i class="fa fa-check-square-o" aria-hidden="true"></i> Signed</span>
																@elseif($eachDoc->status=='2')	
																	<span class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i>Completed</span>
																@else
																	<span class="text-warning"><i class="fa fa-square" aria-hidden="true"></i> Unsigned</span>
																@endif
																
															</td>
															<td>
																@if($eachDoc->status=='1')
																   <a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																   	@if($eachDoc->user_require_stamp!=NULL && $eachDoc->user_require_signer!=NULL)
																	   <a class="btn btn-success btn-sm" onclick="openAdditionalModal('<?php echo $eachDoc->id; ?>');"  href="javascript:void(0);" >Continue to Notarizations</a>
																		@if($eachDoc->title==NULL || $eachDoc->title=="")	
																			<a class="btn btn-info btn-sm" href="{{ url('/scedule-meeting/'.$eachDoc->id) }}">Schedule</a>
																		@endif 
																		{{-- <a class="btn btn-info btn-sm" href="https://www.aimnotary.com/signer/document/{{ $eachDoc->document_key }}?typ=edit-documents" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Document</a> --}}
																		<?php /* ?><a class="btn btn-info btn-sm" style="color : #fff;" href="https://signer.aimnotary.com/index.html?uid={{ Auth::user()->id }}&auth_token=<?= md5(rand(11111,9999999)) ?>&lisence=true&frsc=<?= csrf_token(); ?>&nme=<?= base64_encode($documents[0]->docname); ?>&client_name={{ $user_info->name }}&tenant={{ $eachDoc->id }}&utp=<?= base64_encode("User"); ?>" target="_blank">Edit Document</a> <?php */ ?>																		
																	@else
																		<a class="btn btn-success btn-sm" onclick="openAdditionalModal('<?php echo $eachDoc->id; ?>');"  href="javascript:void(0);" >Continue to Notarizations</a>
																		 @if($eachDoc->title==NULL || $eachDoc->title=="")	
																			<a class="btn btn-info btn-sm" href="{{ url('/scedule-meeting/'.$eachDoc->id) }}">Schedule</a>
																		 @else
																		 	<a class="btn btn-warning btn-sm" id="scheduleTake<?php echo $eachDoc->id; ?>" onclick="openScheduleModal('<?php echo $eachDoc->id; ?>')" href="javascript:void();">Already Scheduled</a>
																		 @endif
																		{{-- <a class="btn btn-info btn-sm" href="https://www.aimnotary.com/signer/document/{{ $eachDoc->document_key }}?typ=edit-documents" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Document</a> --}}
																		<?php /* ?><a class="btn btn-info btn-sm" style="color : #fff;" href="https://signer.aimnotary.com/index.html?uid={{ Auth::user()->id }}&auth_token=<?= md5(rand(11111,9999999)) ?>&lisence=true&frsc=<?= csrf_token(); ?>&nme=<?= base64_encode($documents[0]->docname); ?>&client_name={{ $user_info->name }}&tenant={{ $eachDoc->id }}&utp=<?= base64_encode("User"); ?>" target="_blank">Edit Document</a> <?php */ ?>
																	@endif
																@elseif($eachDoc->status=='2')	
																  	@if($result[0]->doc==1)
																	    @if($eachDoc->doc==1)
																			@if($eachDoc->payment_status=='P')
																				<a title="To view & Download this document you have to pay first" class="btn btn-success btn-sm" href="{{ url('pay-for-documents').'/'.base64_encode($eachDoc->id) }}"><i class="fa fa-money" aria-hidden="true"></i>Pay</a>
																				<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="remove_doc('{{ $eachDoc->id }}');"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a>
																			@else
																				<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																				<a class="btn btn-primary btn-sm" download href="{{ url('/').'/public/'.$eachDoc->docname }}">Download<a>
																			@endif
																		@elseif($eachDoc->doc>=5)
																			@if($eachDoc->user_require_stamp==1 && $eachDoc->user_require_signer==1)
																				<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																				<a class="btn btn-primary btn-sm" download href="{{ url('/').'/public/'.$eachDoc->docname }}">Download<a>
																			@elseif(($eachDoc->user_require_stamp>1 || $eachDoc->user_require_signer>1) && $eachDoc->payment_status=='P')
																				<a title="To view & Download this document you have to pay first" class="btn btn-success btn-sm" href="{{ url('pay-for-documents').'/'.base64_encode($eachDoc->id) }}"><i class="fa fa-money" aria-hidden="true"></i>Pay</a>
																				<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="remove_doc('{{ $eachDoc->id }}');"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a>
																			@elseif(($eachDoc->user_require_stamp>1 || $eachDoc->user_require_signer>1) && $eachDoc->payment_status!='P')
																				<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																				<a class="btn btn-primary btn-sm" download href="{{ url('/').'/public/'.$eachDoc->docname }}">Download<a>
																			@endif
																		@endif
																		
																	@elseif($result[0]->doc>=5)
																		@if($eachDoc->doc==1)
																			@if($eachDoc->payment_status=='P')
																				<a title="To view & Download this document you have to pay first" class="btn btn-success btn-sm" href="{{ url('pay-for-documents').'/'.base64_encode($eachDoc->id) }}"><i class="fa fa-money" aria-hidden="true"></i>Pay</a>
																				<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="remove_doc('{{ $eachDoc->id }}');"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a>
																			@else
																				<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																				<a class="btn btn-primary btn-sm" download href="{{ url('/').'/public/'.$eachDoc->docname }}">Download<a>
																			@endif
																		@elseif($eachDoc->doc>=5)
																			@if($eachDoc->user_require_stamp==1 && $eachDoc->user_require_signer==1)
																				<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																				<a class="btn btn-primary btn-sm" download href="{{ url('/').'/public/'.$eachDoc->docname }}">Download<a>
																			@elseif(($eachDoc->user_require_stamp>1 || $eachDoc->user_require_signer>1) && $eachDoc->payment_status=='P')
																				<a title="To view & Download this document you have to pay first" class="btn btn-success btn-sm" href="{{ url('pay-for-documents').'/'.base64_encode($eachDoc->id) }}"><i class="fa fa-money" aria-hidden="true"></i>Pay</a>
																				<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="remove_doc('{{ $eachDoc->id }}');"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a>
																			@elseif(($eachDoc->user_require_stamp>1 || $eachDoc->user_require_signer>1) && $eachDoc->payment_status!='P')
																				<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $docNameLatest }}')"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
																				<a class="btn btn-primary btn-sm" download href="{{ url('/').'/public/'.$eachDoc->docname }}">Download<a>
																			@endif
																		@endif
																	@endif
																@else
																	{{-- <a class="btn btn-info btn-sm" href="https://www.aimnotary.com/signer/document/{{ $eachDoc->document_key }}?typ=edit-documents" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Document</a> --}}
																	<?php /* ?><a class="btn btn-info btn-sm" style="color : #fff;" href="https://signer.aimnotary.com/index.html?uid={{ Auth::user()->id }}&auth_token=<?= md5(rand(11111,9999999)) ?>&lisence=true&frsc=<?= csrf_token(); ?>&nme=<?= base64_encode($documents[0]->docname); ?>&client_name={{ $user_info->name }}&tenant={{ $eachDoc->id }}&utp=<?= base64_encode("User"); ?>" target="_blank">Edit Document</a> <?php */ ?>
																	<a class="btn btn-success btn-sm" onclick="openAdditionalModal('<?php echo $eachDoc->id; ?>');"  href="javascript:void(0);" >Continue to Notarizations</a>
																	<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="remove_doc('{{ $eachDoc->id }}');"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a>
																@endif
																
															</td>
														</tr>
												@endforeach
											@endif
											
										<!-- 	<tr>
												<td><i class="fa fa-download" aria-hidden="true"></i> Huntbuilder-Development.pdf</td>
												<td>
													<span class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Completed</span>
												</td>
												<td><a class="btn btn-primary btn-sm" href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
											</tr>
											<tr>
												<td><i class="fa fa-file-text" aria-hidden="true"></i> Huntbuilder-Development.pdf</td>
												<td>
													<span class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Completed</span>
												</td>
												<td><a class="btn btn-primary btn-sm" href="#"><i class="fa fa-eye" aria-hidden="true"></i> View</a></td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div>
							<div class="tabs_item">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th scope="col">File</th>
												<th scope="col">Status</th>
												<th scope="col">Action</th>
											</tr>
										</thead>
										<tbody>
											@if(!$documents->isEmpty())
												@foreach($documents as $eachDoc)
												  
												   @if($eachDoc->status=='0')
														<tr>
															<td><a href="javascript:void();" download><i class="fa fa-download"  aria-hidden="true"></i> {{ $eachDoc->doc_nm }} </a></td>
															<td>
																<span class="text-warning"><i class="fa fa-square" aria-hidden="true"></i> Unsigned</span>
															</td>
															<td>
																<a class="btn btn-success btn-sm" href="https://www.aimnotary.com/signer/document/{{ $eachDoc->document_key }}" target="_blank" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sign</a>
																<!-- @if($eachDoc->title==NULL || $eachDoc->title=="")	
																	<a class="btn btn-info btn-sm" href="{{ url('/scedule-meeting/'.$eachDoc->id) }}">Scedule</a>
																@endif -->
															</td>
														</tr>
													@endif
											    @endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
							<div class="tabs_item">
								<div class="table-responsive">
									<table class="table">
										<thead>
											<tr>
												<th scope="col">File 2</th>
												<th scope="col">Status</th>
												<th scope="col">Action</th>
											</tr>
										</thead>
										<tbody>
											@if(!$documents->isEmpty())
												@foreach($documents as $eachDoc)
												   @if($eachDoc->status=='2')
														<tr>
															<td><a href="javascript:void(0)"><i class="fa fa-download"  aria-hidden="true"></i> {{ $eachDoc->doc_nm }} </a></td>
															<td>
																<span class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Completed</span>
															</td>
															{{-- <td>
																<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf('{{ $eachDoc->docname }}')"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</a>
															</td> --}}
														</tr>
													@endif
											    @endforeach
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<div class="modal fade" id="scheduleModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form method="post" id="scedule-form">
					<div class="modal-body" style="">
						<div class="row">
							<div class="col-md-12">
								<span id="eventName"></span>
							</div>
						</div>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div>

	  <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header" style="background-color: #007bff; color : white;">
                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title" style="color : white;">View PDF </h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                           <div class="col-md-12 grid-margin stretch-card">
                              <div class="card">
                                <div class="card-body">
                                	
	                                 <iframe src="" id="display_pdf" width="100%" height="500px"></iframe>
	                                 
                                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                  <!--   <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="target_btn" disabled >Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>
               
              </div>
          </div>	


		  <div id="myModal2" class="modal fade" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<form method="post" id="additional_stamp_add">
					<div class="modal-content">
						<div class="modal-header" style="background-color: #007bff; color : white;">
							<h4 class="modal-title" style="color : white;">Additional Signer & Stamp</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<div class="col-lg-12 col-sm-12" style='display : none;'>
								<div class="form-group">
									<label class="pull-left">Select a Product <span class="text-danger">*</span></label>
									<select required class="form-control margin-b20" name="product_price" id="product_price" required onchange="product_price_change();">
										<option value="">Select Product</option>
										<option <?php if($result[0]->doc==1){ echo "selected"; } ?> value="25">$25 One Document</option>
										<option <?php if($result[0]->doc>=5){ echo "selected"; } ?> value="0">$99 for Five</option>
										<option <?php if($result[0]->doc>5){ echo "selected"; } ?> value="0">$99 for Five</option>
									</select>
								</div>
							</div>
							<div class="col-lg-12 col-sm-12">
								<div class="form-group">
									<label class="pull-left">Number of Signers Per Document <span class="text-danger">*</span></label>
									<select class="form-control margin-b20" onchange="setTheValue(this.value);" required name="no_of_signers" id="no_of_signers">
										<!-- <option value="0" selected>Select signers</option> -->
										<option selected value="0">1</option>
										<option value="1">2</option>
										<option value="2">3</option>
										<option value="3">4</option>
										<option value="4">5</option>
										<option value="5">6</option>
									</select>
								</div>
							</div>
							<div class="col-lg-12 col-sm-12">
								<div class="form-group">
									<label class="pull-left">Number of Notary Stamps You Need <span class="text-danger">*</span></label>
									<select class="form-control margin-b20" onchange="setStampValue(this.value);" name="no_of_stamps" required id="no_of_stamps">
										<!-- <option value="" selected>Select No of stamps</option> -->
										<option selected value="0">1</option>
										<option value="1">2</option>
										<option value="2">3</option>
										<option value="3">4</option>
										<option value="4">5</option>
										<option value="5">6</option>
									</select>
								</div>
							</div>
							<div class="col-lg-12 col-sm-12">
								<div class="form-group" style="margin-top:20px;">
									<label class="pull-left"><b><span class="text-danger">*</span>$5 per additional signers or stamps</b></label>
								</div>
							</div>
							<div class="col-lg-12 col-sm-12">
								<input type="hidden" name="doc_id" id="doc_id" />
								<input type="hidden" name="coupon_id" id="coupon_id">
								<input type="hidden" name="total_cost" id="total_cost" >
								<input type="hidden" name="new_total_cost" id="new_total_cost" >
								<input type="hidden" name="actual_subs_amount" id="actual_subs_amount" >
									<div style='font-size : 17px;font-weight: 600;'>
									
									<span class="text-left" id="documentsName"></span> 
									<span id="myStampsHtml" style="display : none;"> + 1 Stamp</span>
									<span id="mySignersHtml" style="display : none;"> + 1 Signer</span> 
									
									<span class="text-right" id="product_amt"></span></div>
							</div>
							
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-info">Continue To Next Step</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</form>
				</div>
			</div>


			<div class="modal" id="paymentModal">
				<div class="modal-dialog">
					<div class="modal-content">
					
						<!-- Modal body -->
						<div class="modal-body">
							<img class="payment-cards margin-b20" src="{{asset('public/front_assets/img/payment-cards.png')}}" alt="Payment Cards" />
							<form id="m-payment" method="post">
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-12">
										<div class="row">
											<div class="col-md-8 col-lg-8">
												<h3><b>No Of Documents - </b> <?php echo $result[0]->doc; ?><br><br>
												<b>Amount - </b> $<?php echo $result[0]->amount; ?></h3><br><br>
											</div>
										</div>
										<div class="row">
											<div class="col-md-8 col-lg-8">
												<div class="form-group">
													<label class="pull-left">Card Number <span class="text-danger">*</span></label>
													<input type="hidden" name="subscription_amount" value="<?php echo $result[0]->amount; ?>"/>
													<input class="form-control margin-b20 card_number" type="text" placeholder="Card Number" name="card_number"   maxlength="16" required>
												</div>
											</div>
											<div class="col-md-4 col-lg-4">
												<div class="form-group">
													<label class="pull-left">MM/YY <span class="text-danger">*</span></label>
													<input class="form-control margin-b20 card_number" type="text" placeholder="MM/YY" onchange="validate_expiry(this.value);" name="card_expiry"  id="card_expiry"  minlength="5" maxlength="5" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-12 col-lg-12">
										<div class="row">
											<div class="col-md-8 col-lg-8">
												<div class="form-group">
													<label class="pull-left">Card Holder <span class="text-danger">*</span></label>
													<input class="form-control margin-b20" type="text" placeholder="Card Holder" name="card_holder" required>
												</div>
											</div>
											<div class="col-md-4 col-lg-4">
												<div class="form-group">
													<label class="pull-left">CVV/CVC <span class="text-danger">*</span></label>
													<input class="form-control margin-b20 card_number" type="password" placeholder="CVV/CVC"  name="card_cvv"  minlength="3" maxlength="3" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-md-12 col-lg-12">
										<input type='submit' name="submit" class="btn btn-info" value="Make Payment" />
									</div>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>

			<div class="modal" id="resubscribeModal">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<form method="post" id="resubscribeForm">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Choose Subscription</label>
											
											<select required class="form-control" onchange="changeSubscription(this.value);" name="package">
												<option>-Select Subscription Package-</option>
												{{-- <option value="25">One Documents - $25</option> --}}
												<option value="99">Five Documents - $99</option>
											</select>

										</div>
									</div>
								</div>
								<div class="row" id="myResubscriptionRow">
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label class="pull-left">Card Number <span class="text-danger">*</span></label>
											<input class="form-control margin-b20 card_number" type="text" placeholder="Card Number" name="card_number"   maxlength="16" required>
										</div>
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label class="pull-left">MM/YY <span class="text-danger">*</span></label>
											<input class="form-control margin-b20 card_number" type="text" placeholder="MM/YY" onchange="validate_expiry2(this.value);" name="card_expiry"  id="card_expiry2"  minlength="5" maxlength="5" required>
										</div>
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label class="pull-left">Card Holder <span class="text-danger">*</span></label>
											<input class="form-control margin-b20" type="text" placeholder="Card Holder" name="card_holder" required>
										</div>
									</div>
									<div class="col-md-12 col-lg-12">
										<div class="form-group">
											<label class="pull-left">CVV/CVC <span class="text-danger">*</span></label>
											<input class="form-control margin-b20 card_number" type="password" placeholder="CVV/CVC"  name="card_cvv"  minlength="3" maxlength="3" required>
										</div>
									</div>
									
								</div>
								<div class="col-sm-12 col-md-12 col-lg-12">
									<input type='submit' class='btn btn-info' value='Submit' />
								</div>
							</form>
							
							
							
						</div>
					</div>
				</div>
			</div>
          
@endsection
@section('script')
	<script type="text/javascript">
			function openScheduleModal(doc_id){
				var urlData="document_id="+doc_id;
				$("#scheduleTake"+doc_id).text("Wait...");
				$.ajax({
				     url: '{{ url("get-single-scedules") }}', 
				     type: 'post',
				     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				     data: urlData,
				     success: function (response) {
						$("#scheduleTake"+doc_id).html("Already Scheduled");
						$("#scheduleModal").modal("show");
						$("#eventName").html(response);
						
				     }
				});
			}
			function changeSubscription(valcu){
				if(valcu==25){
					$("#myResubscriptionRow").css({ display : 'none', });
				}else{
					$("#myResubscriptionRow").css({ display : 'block', });
				}
			}

			function validate_expiry(val) {
				
				var rs=val.replace('/','');
				var result = rs.replace(/^(.{2})(.*)$/, "$1/$2");
				$("#card_expiry").val(result);
			}

			function validate_expiry2(val) {
				
				var rs=val.replace('/','');
				var result = rs.replace(/^(.{2})(.*)$/, "$1/$2");
				$("#card_expiry2").val(result);
			}

			$("#resubscribeForm").validate({
				submitHandler : function(form){
						$.ajax({
							url: '{{ url("resubscription") }}', 
							type: 'post',
							headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
							data: $(form).serialize(),
							success: function (response) {
								toastr.success("Updated");
								window.location.href=response;
							}
						});
				}
			});

							
			$(".card_number").keydown(function(e) {
				-1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 67 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 88 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault()
			});
			
			function openAdditionalModal(id)
			{
				$("#myModal2").modal('show');
				$("#doc_id").val(id);

			}
			$("#additional_stamp_add").validate({
					rules : {
						user_require_stamp : {
							required : true,
						},
						user_require_signer : {
							required : true,
						},	
					},
					submitHandler : function(form){
						// console.log($(form).serialize());
						$.ajax({
							url: '{{ url("add_additional_docs") }}', 
							type: 'post',
							headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
							data: $(form).serialize(),
							success: function (response) {
								toastr.success("Updated");
								window.location.href=response;
							}
						});
					}
			});

			$("#m-payment").validate({
					
					submitHandler : function(form){
						// console.log($(form).serialize());
						// loadingfunc("block");
						$.ajax({
							url: '{{ url("make-subscription-payments") }}', 
							type: 'post',
							headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
							data: $(form).serialize(),
							success: function (response) {
								var obj=JSON.parse(response);
								if(obj.status==1){
									loadingfunc("none");
									toastr.success(obj.msg);
									setInterval(() => {
										location.reload();
									}, 1000);
								}else{
									loadingfunc("none");
									toastr.error(obj.msg);
								}
								
								// location.reload();
							}
						});
					}
			});

			

			function product_price_change()
			{
				var product_base_price=$("#product_price").val();
				var productName=(product_base_price==25.00) ? 'One Document' : '';
				$("#mySignersHtml").css({ display : 'inline-block' });
				$("#myStampsHtml").css({ display : 'inline-block' });
				$("#documentsName").html(productName);
				$("#product_amt").html(" - $"+product_base_price);
				$("#total_cost").val(product_base_price);
				$("#no_of_signers").removeAttr('disabled');
				$("#no_of_stamps").removeAttr('disabled');
			}

			function setTheValue(newPrice)
			{
				// mySignersHtml
				// myStampsHtml
				product_price_change();
				var product_base_price=$("#product_price").val();
				var no_of_signer=$("#no_of_signers").val();
				var no_of_stamps=$("#no_of_stamps").val();

				var product_base_price=parseInt(product_base_price);
				var no_of_signer=parseInt(no_of_signer);
				var no_of_stamps=parseInt(no_of_stamps);

				if(no_of_signer > 0 && no_of_stamps > 0)
				{
					var price = (product_base_price + (no_of_signer * 5) + (no_of_stamps * 5));
				}
				else if(no_of_signer == 0 && no_of_stamps > 0)
				{
					var price = (product_base_price + (no_of_stamps * 5));
				}
				else if(no_of_signer > 0 && no_of_stamps == 0)
				{
					var price = (product_base_price + (no_of_signer * 5));
				}
				else if(no_of_signer == 0 && no_of_stamps == 0)
				{
					var price = (product_base_price);
				}
				$("#product_amt").html(" - $"+price);
				var printstamp=(no_of_stamps > 0 && no_of_stamps >= 1) ? no_of_stamps+1 : '1';
				var printsigner=(no_of_signer > 0 && no_of_signer >= 1) ? no_of_signer+1 : '1';

				if(product_base_price==25.00){
					$("#mySignersHtml").html(" + "+printsigner+" Signer ");
					$("#myStampsHtml").html(" + "+printstamp+" Stamp ");
				}else{
					$("#mySignersHtml").html(" + "+printsigner+" Signer ");
					$("#myStampsHtml").html(""+printstamp+" Stamp ");
				}
				
				$("#total_cost").val(price);
			}

			function setStampValue(newPrice)
			{
				product_price_change();
				var product_base_price=$("#product_price").val();
				var no_of_signer=$("#no_of_signers").val();
				var no_of_stamps=$("#no_of_stamps").val();

				var product_base_price=parseInt(product_base_price);
				var no_of_signer=parseInt(no_of_signer);
				var no_of_stamps=parseInt(no_of_stamps);

				if(no_of_signer > 0 && no_of_stamps > 0)
				{
					var price = (product_base_price + (no_of_signer * 5) + (no_of_stamps * 5));
				}
				else if(no_of_signer == 0 && no_of_stamps > 0)
				{
					var price = (product_base_price + (no_of_stamps * 5));
				}
				else if(no_of_signer > 0 && no_of_stamps == 0)
				{
					var price = (product_base_price + (no_of_signer * 5));
				}
				else if(no_of_signer == 0 && no_of_stamps == 0)
				{
					var price = (product_base_price);
				}
				$("#product_amt").html(" - $"+price);
				var printstamp=(no_of_stamps > 0 && no_of_stamps >= 1) ? no_of_stamps+1 : '1';
				var printsigner=(no_of_signer > 0 && no_of_signer >= 1) ? no_of_signer+1 : '1';

				if(product_base_price==25.00){
					$("#mySignersHtml").html(" + "+printsigner+" Signer ");
					$("#myStampsHtml").html(" + "+printstamp+" Stamp ");
				}else{
					$("#mySignersHtml").html(" + "+printsigner+" Signer ");
					$("#myStampsHtml").html(""+printstamp+" Stamp ");
				}
				$("#total_cost").val(price);

			}


		click_me();
	    function remove_doc(id) {
   	     	if(confirm("Are you sure you want to remove this document!.")){
	     	    $.ajax({
				     url: '{{ url("remove_document") }}', 
				     type: 'post',
				     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				     data: {id:id},
				     success: function (response) {
			            var newData = JSON.parse(response);
	                    if(newData.status == 200){
	                        toastr.success(newData.msg);
	                    }else{
	                    	toastr.error(newData.msg);
	                    }
	                    $("#remove_tr_"+id).remove();
				     }
				});
	     	}
   	     }
   	     function view_pdf(val) {
   	     	// var url="{{ url('/').'/public/' }}"+val;
				var url=val;
				$("#display_pdf").attr('src',url);
				$("#myModal").modal('show');
   	     }

		  function click_me() {
        	// var myWindow;
        	//   myWindow=window.open(
			//   'https://www.aimnotary.com/signer/',
			//   '_blank', // <- This is what makes it open in a new window.
			//   'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,top=1000000,width=20,height=20'
			// );
			// setTimeout(function(){ myWindow.close(); }, 1000);
        }
   	     
	</script>
@endsection     