@extends('front.layouts.app')
  @section('content')

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Second Signer, please provide your name</h2>
						<p>Make sure your full name is entered as it appears on your ID. We'll use this name to generate your signature and initials.</p>
					</div>
					<div class="upload-box">
						<form action="verify-identity.php">
							<div class="row">
								<div class="col-lg-4 col-sm-12">
									<div class="form-group">
										<label class="pull-left">First Name <span class="text-danger">*</span></label>
										<input class="form-control" type="text" placeholder="First Name" required="">
									</div>
								</div>
								<div class="col-lg-4 col-sm-12">
									<div class="form-group">
										<label class="pull-left">Middle Name</label>
										<input class="form-control" type="text" placeholder="Middle Name" value="">
									</div>
								</div>
								<div class="col-lg-4 col-sm-12">
									<div class="form-group">
										<label class="pull-left">Last Name <span class="text-danger">*</span></label>
										<input class="form-control" type="text" placeholder="Last Name" required="">
									</div>
								</div>
								<div class="col-lg-12 col-sm-12">
									<div class="form-group">
										<label class="pull-left">Email Address <span class="text-danger">*</span></label>
										<input class="form-control" type="text" placeholder="Email Address" required="">
									</div>
								</div>
								<div class="col-lg-12 col-sm-12">
									<p class="margin-b20 text-left">By continuing you agree to the Notarize <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
								</div>
								<div class="col-lg-12 col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-lg pull-left">
											Submit
										</button>
										<a class="btn btn-secondary btn-lg pull-left margin-l15" href="verify-identity.php">Cancel</a>
									</div>
								</div>
							</div>
						</form>
					</div>
					
					
					
				</div>
			</div>
		</div>
	</section>
		
   @endsection