@extends('front.layouts.app')
  @section('content')

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Does your document ask for a second signer or witness?</h2>
						<p>If so, we can handle that. Both of you will need to use the same device and be present for the meeting with the notary.</p>
					</div>
					<div class="upload-box">
						<a class="btn btn-outline-info float-left" href="#">Skip <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
						<a class="btn btn-primary float-right" href="second-signer-add.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Add Second Signer</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
		
   @endsection