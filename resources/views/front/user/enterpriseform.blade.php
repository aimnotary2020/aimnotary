@extends('front.layouts.app')
  @section('content')

    <div class="page-title-area bg-18">
		<div class="container">
			<div class="page-title-content">
				<h2>Enterprise Form</h2>
				<ul>
					<li><a href="{{ url('/') }}">Home</a></li>
					<li>Enterprise Form</li>
				</ul>
			</div>
		</div>
	</div>

    <section class="inner-page-container checkout-area pt-50 pb-100 user-area-all-style">
		<div class="container">
			
				<div class="row">
					
					<div class="col-lg-6 col-md-12">
						<div class="order-details">
							<div class="order-table table-responsive">
								<h3 class="title">Run your business on Aim Notary Cam</h3>
								<p>See why the fortune 500 to small startups choose Aim notary to get notarized documents back from customers quicker, cheaper, and more efficiently.</p>
								<ul class="enterprise-ul">
									<li><i class="flaticon-checked"></i> Online, any device, anywhere in the world</li>
									<li><i class="flaticon-checked"></i> Get your customers documents notarized in minutes 24/7/365…yes even Christmas</li>
									<li><i class="flaticon-checked"></i> Secure and legal</li>
									<li><i class="flaticon-checked"></i> Integrate with your existing apps for faster document notarization</li>
									<li><i class="flaticon-checked"></i> Custom workflows to match even the most complex requirements</li>
								</ul>
								<p>We look forward to showing you the power, convenience, and ease of Online Notarizations!</p>
							</div>
						</div>
					</div>
					
					<div class="col-lg-6 col-md-12">
						<div class="billing-details">
							<h3 class="title">Contact Sales</h3>
							<p>Thank you for your interest! Please fill the information below and a representative will be in touch.</p>
							
							@if(session('message'))
								<b style='color : green;'>{{session('message')}}</b>
							@endif
							<form method='post' id='enterprise-form' action='{{url("enterprise-form-submit")}}'>
								<div class="row">
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>First Name <span class="required">*</span></label>
											<input type="text" name="first_name" class="form-control">
											<input type="hidden" name="_token" value="<?= csrf_token(); ?>" />
										</div>
									</div>

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Last Name <span class="required">*</span></label>
											<input type="text" name="last_name" class="form-control">
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Business Email Address <span class="required">*</span> (Will be used for login)</label>
											<input type="email" name="u_email" class="form-control">
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Company Name</label>
											<input type="text" name="company_name" class="form-control">
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Mobile <span class="required">*</span></label>
											<input type="text" name="mobile" class="form-control">
										</div>
									</div>

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Password <span class="required">*</span></label>
											<input type="password" id="password" name="password" class="form-control">
										</div>
									</div>

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Confirm Password <span class="required">*</span></label>
											<input type="text" name="cnf_pass" class="form-control">
										</div>
									</div>

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>State <span class="required">*</span></label>
											<input type="text" name="state" class="form-control">
										</div>
									</div>

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>City <span class="required">*</span></label>
											<input type="text" name="city" class="form-control">
										</div>
									</div>

									

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Zip Code <span class="required">*</span></label>
											<input type="text" name="zip_code" class="form-control">
										</div>
									</div>
									

									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>Address <span class="required">*</span></label>
											<input type="text" name="add1" class="form-control">
										</div>
									</div>

									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>How Many Notarizations Does Your Business Complete Monthly? <span class="required">*</span></label>
											<div class="select-box">
												<select name="notarize_no" class="form-control">
													<option value="">Select One</option>
													<option value="1-24">1-24</option>
													<option value="25-49">25-49</option>
													<option value="50-99">50-99</option>
													<option value="100-249">100-249</option>
													<option value="250-499">250-499</option>
													<option value="500-749">500-749</option>
													<option value="750-999">750-999</option>
													<option value="1000+">1000+</option>
												</select>
											</div>
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>What best describes your company's Notarization needs? <span class="required">*</span></label>
											<div class="select-box">
												<select name="describe" class="form-control">
													<option value="">Select One</option>
													<option value="Real Estate Professional (Ex. Title, Escrow, Mortgage Professional)">Real Estate Professional (Ex. Title, Escrow, Mortgage Professional)</option>
													<option value="Business Professional (Ex. All other Businesses)">Business Professional (Ex. All other Businesses)</option>
												</select>
											</div>
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<label>License and/or Outsource? (License=Your Notary Publics Complete Outsource=Aim notary’s Professional Notary Publics Complete) <span class="required">*</span></label>
											<div class="select-box">
												<select name="lisence" class="form-control">
													<option value="">Select One</option>
													<option value="I am interested in Outsource Aim notary">I am interested in Outsource Aim notary</option>
													<option value="I am interested in Licensing Aim notary">I am interested in Licensing Aim notary</option>
													<option value="I am interested in both, Outsourcing & Licensing Aim notary">I am interested in both, Outsourcing & Licensing Aim notary</option>
												</select>
											</div>
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="clearfix"><hr></div>
									</div>
									
									<div class="col-lg-12 col-md-12">
										<div class="form-group">
											<input class="default-btn three" type="submit" value="Submit" style="border:none;">
										</div>
									</div>
									
									
								</div>
							</form>


						</div>
					</div>

					
				</div>
		</div>
    </section>    

  @endsection
  @section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
<script>

$("#enterprise-form").validate({
	rules: {
		first_name : {
			required : true,
		},
		last_name : {
			required : true,
		},
		u_email : {
			required : true,
			email : true,
			remote : "{{ url('checkEmailAjax') }}"
		},
		company_name : {
			required : true,
		},
		mobile : {
			required : true,
			digits : true,
		},
		notarize_no : {
			required : true,
		},
		describe : {
			required : true,
		},
		lisence : {
			required : true,
		},
		password : {
			required : true,
			minlength : 6,
		},
		city : {
			required : true,
		},
		state : {
			required : true,
		},
		add1 : {
			required : true,
		},
		zip_code : {
			required : true,
			digits : true,
		},
		cnf_pass : {
			required : true,
			minlength : 6,
			equalTo : "#password",
		}
	},
	messages : {
		first_name : {
			required : "This is a mendatory field",
		},
		last_name : {
			required : "This is a mendatory field",
		},
		u_email : {
			required : "This is a mendatory field",
			email : "Enter an valida email address",
			remote : "This email address is already exist",
		},
		company_name : {
			required : "This is a mendatory field",
		},
		mobile : {
			required : "This is a mendatory field",
			digits : "Mobile number should be in digits",
		},
		notarize_no : {
			required : "This is a mendatory field",
		},
		describe : {
			required : "This is a mendatory field",
		},
		lisence : {
			required : "This is a mendatory field",
		},
		password : {
			required : "This is a mendatory field",
			minlength : "Minimum password should be 6 character in length",
		},
		city : {
			required : "This is a mendatory field",
		},
		state : {
			required : "This is a mendatory field",
		},
		add1 : {
			required : "This is a mendatory field",
		},
		zip_code : {
			required : "This is a mendatory field",
			digits : "Zip code only take numaric inputs",
		},
		cnf_pass : {
			required : "This is a mendatory field",
			minlength : "Minimum password should be 6 character in length",
			equalTo : "New Password & Confirm Password should be same",
		}
	}
	// submitHandler : function (form) {
	// 	$.ajax({
	// 		url: '{{url("enterprise-form-submit")}}',
	// 		type: 'POST',
	// 		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	// 		data: form,
	// 		beforeSend:function(){
	// 			loadingfunc("block");
	// 				// $("#subscription-submit-form").hide(), $("#subcsription_loader").show() ,$("#sub_btn").attr('disabled','disabled');
	// 		},
	// 		success: function(data){
	// 			loadingfunc("none");
	// 			var obj=JSON.parse(data);
	// 			if(obj.status==1){
	// 				swal({
	// 					title: "Thank You!",
	// 					text: "Thank you our executive will call you soon",
	// 					icon: "success",
	// 				}).then((value) => {
	// 					location.reload();
	// 				});
	// 			}else{
					
	// 			}
				
	// 		}
	// 	});
	// },	
})
</script>
@endsection
