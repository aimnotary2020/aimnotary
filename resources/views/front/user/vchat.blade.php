@extends('front.layouts.app')
  @section('content')
  <script src="https://www.gstatic.com/firebasejs/3.5.3/firebase.js"></script>
  <script>
      const firebaseConfig = {
                                apiKey: "AIzaSyCQu4sc2UN_9vhLyLrgCs1NM0hTrthB6Uo",
                                authDomain: "aimnotary-project-001.firebaseapp.com",
                                databaseURL: "https://aimnotary-project-001-default-rtdb.firebaseio.com",
                                projectId: "aimnotary-project-001",
                                storageBucket: "aimnotary-project-001.appspot.com",
                                messagingSenderId: "109700331601",
                                appId: "1:109700331601:web:ca271a82b8f179db842070",
                                measurementId: "G-R15X8TG245"
                            };
                            firebase.initializeApp(firebaseConfig);
                            firebase.auth().signInAnonymously().catch(error => {
                                if (error.code === 'auth/operation-not-allowed') {
                                    alert('You must enable Anonymous auth in the Firebase Console.');
                                } else {
                                    console.error(error);
                                }
                            });
                            var database = firebase.database();
  </script>
  <section class="inner-page-container pt-50 pb-100 user-area-all-style">
    <div class="container">
      <div class="company-us-area">
        <div class="company-tab-wrap upload-page-cont">
          <div class="dashboard-top">
            <div class="row">
                @if(Auth::user()->user_type!='Lawyer')
                <div class="col-md-6">
                    <h2>Connect with Aimnotary</h2>
                    <a target="_blank" href="<?= url('update-documents/'.base64_encode($user_doc_info['id'])); ?>" class="btn btn-info">Update Photo ID</a>
                    <p>Next we will start a video call with the notary.</p>
                </div>
                @else
                    <select onchange="writeUserData(this.value)">
                        <option value=""> Enable / Disable Signin button </option>
                        <option value="NO">Disable Signer Button</option>
                        <option value="YES">Enable Signer Button</option>
                    </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="onlineOffline">  </span>
                @endif
                <div class="col-md-6">
                    @if(Auth::user()->user_type!='Lawyer')
                        <div class="type-cont" style="font-weight : 700; font-size : 16px; color : #007bff"></div>
                        <script>
                            document.addEventListener('DOMContentLoaded', function(event) {
                        var dataText = [    "Thank you for your patience, our notaries are currently assisting other customers, we will be with you shortly."
                                            ,"While waiting for your notary, double check that there are no blank fields on your document. If so, please go back to your dashboard and click on edit to fill in the missing information."
                                            ,"Thank you for choosing AIM Notary for your notarizing needs!"
                                            ,"We appreciate your patience, while our notaries are assisting other customers. We will be with you shortly. "
                                            ,"Does your document require a witness to be present during signing? If so, you must provide the witness and they must appear on camera with you during your notarizing session. "];

                        function typeWriter(text, i, fnCallback) {
                            if (i < (text.length)) {
                                document.querySelector(".type-cont").innerHTML = text.substring(0, i + 1) + '<span class="type-cursor" aria-hidden="true"></span>';
                                setTimeout(function() {
                                    typeWriter(text, i + 1, fnCallback)
                                }, 100);
                            } else if (typeof fnCallback == 'function') {
                                setTimeout(fnCallback, 200);
                            }
                        }

                        function StartTextAnimation(i) {
                            if (typeof dataText[i] == 'undefined') {
                                setTimeout(function() {
                                    StartTextAnimation(0);
                                }, 200);
                            }
                            if (i < dataText[i].length) {
                                typeWriter(dataText[i], 0, function() {
                                    StartTextAnimation(i + 1);
                                });
                            }
                        }
                        StartTextAnimation(0);
                    });
                        </script>
                    @endif
                   
                </div>
            </div>
          </div>
          <?php
                // echo "<pre>"; print_r($user_doc_info);
          ?>
          <div class="upload-box">            
            <div class="container">
                <div class="row align-items-center h-100">
                    <div class="col-lg-6 mx-auto">
                        <div class="card" id="agora_wrapper">
                            <div class="card-body" id="agora_local" style="background-color: var(--color-background);">
                            </div>
                            <div class="text-center button-controls">
                                {{-- <a title="Sign Your Document" target="_blank" href="{{ url('/redirect-to-new-doc/'.$user_doc_info['id']) }}"><i class="fas fa-file-pdf fa-custom fa-3x"></i>&nbsp;</a> --}}
                                @if(Auth::user()->user_type!='Lawyer')
                                    <a title="Sign Your Document" id="docuSign" style="color : #fff; display: none;" href="https://signer.aimnotary.com/index.html?uid={{ Auth::user()->id }}&auth_token=<?= md5(rand(11111,9999999)) ?>&lisence=true&frsc=<?= csrf_token(); ?>&nme=<?= base64_encode($user_doc_info['doc']); ?>&client_name={{ $user_info->name }}&tenant={{ $doc_id }}&utp={{ base64_encode(Auth::user()->user_type) }}" target="_blank"><i class="fas fa-file-pdf fa-custom fa-3x"></i>&nbsp;</a>
                                    <script>
                                        
                                        firebase.database().ref('users<?= $doc_id ?>').set({
                                            "<?= $doc_id ?>" : "NO",
                                        });
                                        var starCountRef = database.ref('users<?= $doc_id ?>');
                                        starCountRef.on('value', (snapshot) => {
                                            const data = snapshot.val();
                                            if(data['<?= $doc_id ?>']=="YES"){
                                                document.getElementById("docuSign").style.display="inline-block";
                                            }else{
                                                document.getElementById("docuSign").style.display="none";
                                            }
                                            
                                        });
                                    </script>
                                @else
                                    <a title="Sign Your Document" style="color : #fff;" href="https://signer.aimnotary.com/index.html?uid={{ Auth::user()->id }}&auth_token=<?= md5(rand(11111,9999999)) ?>&lisence=true&frsc=<?= csrf_token(); ?>&nme=<?= base64_encode($user_doc_info['doc']); ?>&client_name={{ Auth::user()->name }}&tenant={{ $doc_id }}&utp={{ base64_encode(Auth::user()->user_type) }}" target="_blank"><i class="fas fa-file-pdf fa-custom fa-3x"></i>&nbsp;</a>
                                    
                                @endif
                                
                                <a  title="Full Screen" onclick="fullScreen();"><i class="fas fa-arrows-alt fa-custom fa-3x" id="fulls"></i>&nbsp;</a>
                                <a  title="Screen Share" onclick="screenshare();"><i class="fas fa-desktop fa-custom fa-3x" id="screenshare"></i>&nbsp;</a>
                                <a  title="Start Video Call" onclick="playVideo();"><i class="fas fa-video fa-custom fa-3x" id="play"></i>&nbsp;</a>
                                <a  title="Mute / Unmute" onclick="toggleMic()"><i class="fas fa-microphone fa-custom fa-3x" id="mute_unmute"></i>&nbsp;</a>
                                  @if(Auth::user()->user_type=='Lawyer')
                                    <!-- <a onclick="end_call()" title="End Call"><i class="fas fa-stop fa-custom fa-2x" style="color: red;" ></i>&nbsp;</a> -->
                                    <a onclick="endCallModal();" title="End Call"><i class="fas fa-stop fa-custom fa-2x" style="color: red;" ></i>&nbsp;</a>
                                  @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mx-auto">
                        <div class="card" id="agora_wrapper_remote">
                            <div class="card-body" id="agora_remote_area" style="background-color: var(--color-background);">
                                <div id="remote_view" style="height: 100%"></div>
                            </div>
                            <div class="text-center button-controls">
                                <a onclick="fullScreenAudience();"><i class="fas fa-arrows-alt fa-custom fa-3x" id="fullsAud"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Agora Configuration-->
                <div class="select" style="display:none;">
                <label for="audioSource">Audio source: </label>
                <select id="audioSource"></select>
                <label for="videoSource">Video source: </label>
                <select id="videoSource"></select>
                
                <input id="video" type="checkbox" checked>
                <input id="screen" type="checkbox">
                
                <!-- Test Buttons -->
                <button id="join" class="btn btn-primary" onclick="join()">Join</button>
                <button id="leave" class="btn btn-primary" onclick="leave()">Leave</button>
                <button id="publish" class="btn btn-primary" onclick="publish()">Publish</button>
                <button id="unpublish" class="btn btn-primary" onclick="unpublish()">Unpublish</button>
                <!-- // Test Buttons -->
                
                <!-- Change App Id from here --><!-- 
                <input id="appId" type="hidden" value="5b711b49e0904a42a2c8378b066e4ba3"> -->
                <input id="appId" type="hidden" value="636c5db740af42848bbd8c12d1df9a9a">             
                <!-- Change Channel id, that is a unique id which will connect between two streams from here -->
                <!--  <input id="channel" type="hidden" value="1084"> -->
                <input id="channel" type="hidden" value="{{ str_replace(' ', '', $user_cannel) }}">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <div id="endCallModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style='background-color : #4424a8; color : white;'>
                <h4 class="modal-title">Stamps & User Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <ul>
                    <li>User Required Stamps - <?php echo $user_doc_info['user_require_stamp']; ?></li>
                    <li>User Required Signer - <?php echo $user_doc_info['user_require_signer']; ?></li>
                </ul>
                <h4>Fill in total number of stamps & signers from today’s notary session</h4>
                <div class="form-group">
                    <label>Used Stamp</label>
                    <input type="text" name="notary_confirm_stamp" id="notary_confirm_stamp" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Used Signer</label>
                    <input type="text" name="notary_confirm_signer" id="notary_confirm_signer" class="form-control" />
                </div>
                <p style="color : red; font-weight: 700;">*$5 per additional signers or stamps</p>

            </div>
            <div class="modal-footer">
                <button type="button" id="end_call" onclick="end_call();" class="btn btn-info">Confirm</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

  
   @endsection

   @section('script')
    
        <!-- Axios -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
        <script>
            function writeUserData(valcu) {
                firebase.database().ref('users<?= $doc_id ?>').set({
                    "<?= $doc_id ?>" : valcu,
                });
            }
            
        var isFirstTime = true;
        var audio = true;
        var usersCount = 0;
        var video = 0;
        var isScreenShare = false;
        var isVideo = true;
        var localStreamUid = 0, subsStreamUid = 0;
    
        var client, localStream = null, camera, microphone;

        function getLawyerVideoStatus(){
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                // Typical action to be performed when the document is ready:
                    var obj = JSON.parse(xhttp.responseText);
                    if(obj.is_call_started=='D'){
                        window.location.href="<?php echo url('/dashboard'); ?>";
                    }
                }
            };
            xhttp.open("GET", "<?php echo url('admin/getVideoStopStatus').'/'.$doc_id; ?>", true);
            xhttp.send();
        }
        setInterval(function(){ getLawyerVideoStatus(); }, 2000);
    
        function readyToPublish() {
            if(localStream != null && video == 0) {
                unpublish();
                localStream.close();
                localStream = null;
                $('#play').addClass('fa-video');
                $('#play').removeClass('fa-play-circle');
                $('#play').removeClass('fa-pause-circle');
                $('#screenshare').addClass('fa-desktop');
                $('#screenshare').removeClass('fa-pause-circle');
                $('#screenshare').removeClass('fa-play-circle');
                $("#mute_unmute").removeClass('fa-microphone-slash');
                $("#mute_unmute").addClass('fa-microphone');
            }
            if(video == 0) {
                if(isVideo) {
                    $('#play').removeClass('fa-video');
                    $('#play').addClass('fa-pause-circle');
                } else if(isScreenShare){
                    $('#screenshare').removeClass('fa-desktop');
                    $('#screenshare').addClass('fa-pause-circle');
                }
    
                video = 1;
                join();
                publish()
            }else if(video == 1) {
    
                if(isVideo) {
                    $('#play').removeClass('fa-pause-circle');
                    $('#play').addClass('fa-play-circle');
                } else if(isScreenShare){
                    $('#screenshare').removeClass('fa-pause-circle');
                    $('#screenshare').addClass('fa-play-circle');
                }
    
                video = 2;
                localStream.disableAudio();
                localStream.disableVideo();
            }else if(video == 2) {
                if(isVideo) {
                    $('#play').removeClass('fa-play-circle');
                    $('#play').addClass('fa-pause-circle');
                } else if(isScreenShare){
                    $('#screenshare').removeClass('fa-play-circle');
                    $('#screenshare').addClass('fa-pause-circle');
                }
    
                video = 1;
                localStream.enableAudio();
                localStream.enableVideo();
            }
        }
        function readyToLeave() {
            swal({
                title: "Are you sure?",
                text: "You are about to leave the live session!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                $('#problem_text').fadeIn();
                    leave();
                    unpublish();
                    window.close();
                } else {
                    //swal("Your imaginary file is safe!");
                }
            });
    
        }
        function screenshare() {
            if(isVideo) {
                video = 0;
                isScreenShare = true;
                isVideo = false;
                leave();
                unpublish();
                readyToPublish();
            } else readyToPublish();
        }

        firebase.database().ref('users_online_<?= $doc_id ?>').set({
                            "online" : 1,
                    });

        <?php 
            if(Auth::user()->user_type!='Lawyer'){ ?>
                
                setInterval(function(){
                    firebase.database().ref('users_online_<?= $doc_id ?>').set({
                            "online" : 1,
                    });
                }, 15000);
        <?php }else{
            
            ?>
                setInterval(function(){
                        var onoff = database.ref('users_online_<?= $doc_id ?>');
                        onoff.on('value', (snapshot2) => {
                            const data2 = snapshot2.val();
                            if(data2['online'] == 1){
                                document.getElementById('onlineOffline').style.color = "green";
                                document.getElementById('onlineOffline').innerHTML = "<b>User Online</b>";
                            }else{
                                document.getElementById('onlineOffline').style.color = "red";
                                document.getElementById('onlineOffline').innerHTML = "<b>User Disconnected</b>";
                            }
                        });
                }, 5000);
            
            <?php

        } ?>

        var ref = firebase.database().ref('users_online_<?= $doc_id ?>').onDisconnect().set({
            "online" : 0,
        });
        
        

    
        function playVideo() {
            if(isScreenShare) {
                video = 0;
                isScreenShare = false;
                isVideo = true;
                leave();
                unpublish();
                readyToPublish();
            } else readyToPublish();
        }
        function fullScreen() {
            if(screenfull.isFullscreen) {
                screenfull.exit();
                $('#fulls').removeClass('fa-compress');
                $('#fulls').addClass('fa-arrows-alt');
            } else {
                screenfull.request(document.getElementById('agora_wrapper'));
                $('#fulls').removeClass('fa-arrows-alt');
                $('#fulls').addClass('fa-compress');
            }
        }
        function fullScreenAudience() {
            if(screenfull.isFullscreen) {
                screenfull.exit();
                $('#fullsAud').removeClass('fa-compress');
                $('#fullsAud').addClass('fa-arrows-alt');
            } else {
                screenfull.request(document.getElementById('agora_wrapper_remote'));
                $('#fullsAud').removeClass('fa-arrows-alt');
                $('#fullsAud').addClass('fa-compress');
            }
        }
        if(!AgoraRTC.checkSystemRequirements()) {
            alert("Your browser does not support WebRTC!");
        }
    
        var audioSelect = document.querySelector('select#audioSource');
        var videoSelect = document.querySelector('select#videoSource');
        function join() {
            document.getElementById("join").disabled = true;
            document.getElementById("video").disabled = true;
            var channel_key = null;
    
            console.log("Init AgoraRTC client with App ID: " + appId.value);
            client = AgoraRTC.createClient({mode: 'live'});
            client.init(appId.value, function () {
                console.log("AgoraRTC client initialized");
                client.join(channel_key, channel.value, null, function(uid) {
                    if (isVideo) {
                        camera = videoSource.value;
                        microphone = audioSource.value;
                        localStream = AgoraRTC.createStream({streamID: uid, audio: true, cameraId: camera, microphoneId: microphone, video: isVideo, screen: isScreenShare});
                    } else if(isScreenShare){
                        camera = videoSource.value;
                        microphone = audioSource.value;
                        localStream = AgoraRTC.createStream({streamID: uid, audio: true, cameraId: camera, microphoneId: microphone, video: isVideo, screen: isScreenShare});
                    }
                    if (isVideo || isScreenShare) {
                        localStream.setVideoProfile('720p_3');
                    }
    
                    // The user has granted access to the camera and mic.
                    localStream.on("accessAllowed", function() {
                        console.log("accessAllowed");
                    });
    
                    // The user has denied access to the camera and mic.
                    localStream.on("accessDenied", function() {
                        console.log("accessDenied");
                    });
    
                    localStream.init(function() {
                        console.log("getUserMedia successfully");
                        localStream.play('agora_local');
    
                        client.publish(localStream, function (err) {
                            console.log("Publish local stream error: " + err);
                        });
    
                        client.on('stream-published', function (evt) {
                            localStreamUid = uid;
                            console.log("Publish local stream successfully");
                        });
                    }, function (err) {
                        console.log("getUserMedia failed", err);
                    });
    
                }, function(err) {
                    console.log("Join channel failed", err);
                });
            }, function (err) {
                console.log("AgoraRTC client init failed", err);
            });
    
            channelKey = "";
            client.on('error', function(err) {
                console.log("Got error msg:", err.reason);
                if (err.reason === 'DYNAMIC_KEY_TIMEOUT') {
                client.renewChannelKey(channelKey, function(){
                    console.log("Renew channel key successfully");
                }, function(err){
                    console.log("Renew channel key failed: ", err);
                });
                }
            });
    
    
            client.on('stream-added', function (evt) {
                var stream = evt.stream;
                console.log("New stream added: " + stream.getId());
                console.log("Subscribe ", stream);
                client.subscribe(stream, function (err) {
                    console.log("Subscribe stream failed", err);
                });
            });
    
            client.on('stream-subscribed', function (evt) {
                var stream = evt.stream;
                console.log("Subscribe remote stream successfully: " + stream.getId());
                
                if(isFirstTime) {
                    //alert("Starting recording...");
                    getResouceId(localStreamUid, stream.getId());
                    isFirstTime = false;
                }
    
                if ($('div#agora_remote_area #agora_remote'+stream.getId()).length === 0) {
                    $('div#remote_view').html('<div class="remote" id="agora_remote_wrapper'+stream.getId()+'"><span id="agora_remote'+stream.getId()+'" class="video_audience"></span></div></div>');
                }
    
                stream.play('agora_remote' + stream.getId());
            });
    
            client.on('stream-removed', function (evt) {
                var stream = evt.stream;
                stream.stop();
    
                $('#agora_remote_wrapper' + stream.getId()).remove();
                console.log("Remote stream is removed " + stream.getId());
                usersCount -= 1;
                $('#actv').html(usersCount);
            });
    
            client.on('peer-leave', function (evt) {
                var stream = evt.stream;
                if (stream) {
                stream.stop();
                $('#agora_remote_wrapper' + stream.getId()).remove();
                console.log(evt.uid + " leaved from this channel");
                usersCount -= 1;
                $('#actv').html(usersCount);
                }
            });
            client.setClientRole('host', function() {
                console.log('Client role set as host.');
            }, function(e) {
                console.log('setClientRole failed', e);
            });
        }
    
        function muteAudience(id) {
            console.log(id);
            var video=document.getElementById(id);
            video.muted = !video.muted;
        }
    
        function leave() {
            document.getElementById("leave").disabled = true;
            client.leave(function () {
                console.log("Leavel channel successfully");
            }, function (err) {
                console.log("Leave channel failed");
            });
        }
    
        function publish() {
            document.getElementById("publish").disabled = true;
            document.getElementById("unpublish").disabled = false;
            client.publish(localStream, function (err) {
                console.log("Publish local stream error: " + err);
            });
        }
    
        function unpublish() {
            document.getElementById("publish").disabled = false;
            document.getElementById("unpublish").disabled = true;
            video = 0;
            localStream.stop();
            client.unpublish(localStream, function (err) {
                console.log("Unpublish local stream failed" + err);
            });
        }
    
        function getDevices() {
            AgoraRTC.getDevices(function (devices) {
                for (var i = 0; i !== devices.length; ++i) {
                    var device = devices[i];
                    var option = document.createElement('option');
                    option.value = device.deviceId;
                    if (device.kind === 'audioinput') {
                        option.text = device.label || 'microphone ' + (audioSelect.length + 1);
                        audioSelect.appendChild(option);
                    } else if (device.kind === 'videoinput') {
                        option.text = device.label || 'camera ' + (videoSelect.length + 1);
                        videoSelect.appendChild(option);
                    } else {
                        console.log('Some other kind of source/device: ', device);
                    }
                }
            });
        }
    
        function toggleMic() {
            firebase.onDisconnect().ref('users_online_<?= $doc_id ?>').set({
                    "online" : 0,
            });
            
            if (audio == false) {
                localStream.enableAudio(); 
                $("#mute_unmute").removeClass('fa-microphone-slash');
                $("#mute_unmute").addClass('fa-microphone');
                audio = true;
            } else {
                localStream.disableAudio(); // mute the local mic
                $("#mute_unmute").removeClass('fa-microphone');
                $("#mute_unmute").addClass('fa-microphone-slash');
                audio = false;
            }
        }
        
        //Recording 
        async function getResouceId(uid, streamUid) {
            recordingUid = {{ time() }};
            const result = await axios({
              method: 'post',
              url: 'https://api.agora.io/v1/apps/636c5db740af42848bbd8c12d1df9a9a/cloud_recording/acquire',
              headers: {'Content-Type': 'application/json', Authorization: "Basic OTM1Y2Q0ODExNzZmNGUwM2E1Yjc5OTJjZjJiNGUxMTY6NjFhN2RjNDBmZGUzNDY0ZGIxMWUxODg0YzdmOGRhM2Q="},
              data: {
                  "cname": ""+$('#channel').val(),
                  "uid": ""+recordingUid,
                  "clientRequest":{
                    
                  }
              }
            });
            console.clear();
            startRecording(recordingUid, uid, streamUid, result.data.resourceId);
        }
        
        async function startRecording(recordingUid, uid, streamUid, resourceId) {
            localStorage.setItem("recordingUid", recordingUid);
            localStorage.setItem("resourceId", resourceId);
            const result = await axios({
                method: 'post',
                url: 'https://api.agora.io/v1/apps/636c5db740af42848bbd8c12d1df9a9a/cloud_recording/resourceid/'+resourceId+'/mode/mix/start',
                headers: {'Content-Type': 'application/json', Authorization: "Basic OTM1Y2Q0ODExNzZmNGUwM2E1Yjc5OTJjZjJiNGUxMTY6NjFhN2RjNDBmZGUzNDY0ZGIxMWUxODg0YzdmOGRhM2Q="},
                data: {
                    "cname": ""+$('#channel').val(),
                    "uid": ""+recordingUid,
                    "clientRequest":{
                        "recordingConfig":{
                            "channelType":1,
                            "streamTypes":2,
                            "audioProfile":1,
                            "videoStreamType":1,
                            "maxIdleTime": 30,
                            "transcodingConfig":{
                                "width":360,
                                "height":640,
                                "fps":15,
                                "bitrate":400,
                                "mixedVideoLayout":1,
                            },
                            /*"subscribeVideoUids": [""+uid, ""+streamUid],
                            "subscribeAudioUids": [""+uid, ""+streamUid],
                            "subscribeUidGroup": 0*/
                        },
                        "storageConfig":{
                            "vendor":1,
                            "region":8,
                            "bucket":"aimnotary-singapore",
                            "accessKey":"AKIA22VQ2Y4TXYA3AVJK",
                            "secretKey":"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"
                        }   
                    }
            
                }
            });
            
            console.clear();
            console.log(result);
            localStorage.setItem("sid", result.data.sid);
        }
        async function stopRecording(sid,resourceId,recordingUid) {
            const result = await axios({
                method: 'post',
                url: 'https://api.agora.io/v1/apps/636c5db740af42848bbd8c12d1df9a9a/cloud_recording/resourceid/'+resourceId+'/sid/'+sid+'/mode/mix/stop',
                headers: {'Content-Type': 'application/json', Authorization: "Basic OTM1Y2Q0ODExNzZmNGUwM2E1Yjc5OTJjZjJiNGUxMTY6NjFhN2RjNDBmZGUzNDY0ZGIxMWUxODg0YzdmOGRhM2Q="},
                data: {
                    "cname": ""+$('#channel').val(),
                    "uid": ""+recordingUid,
                    "clientRequest":{
                      
                    }
                }
            });
            
            console.clear();
        
            var videoUrl="https://aimnotary-singapore.s3-ap-southeast-1.amazonaws.com/"+result.data.serverResponse.fileList;
            $.ajax({
                    url: '{{ url("update_call_status") }}', 
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {doc_id: '{{ $doc_id }}',mode : "video", video : videoUrl},
                    success: function (response) {
                        // window.close();
                        // setTimeout(function(){ window.location ="{{ url('completed-document').'/2' }}"  },1000);
                    }
                });

            //console.log(videoUrl);
            return result;
        }
        
        async function queryRecording(sid,resourceId) {
            const result = await axios({
                method: 'get',
                url: 'https://api.agora.io/v1/apps/636c5db740af42848bbd8c12d1df9a9a/cloud_recording/resourceid/'+resourceId+'/sid/'+sid+'/mode/mix/query',
                headers: {'Content-Type': 'application/json', Authorization: "Basic OTM1Y2Q0ODExNzZmNGUwM2E1Yjc5OTJjZjJiNGUxMTY6NjFhN2RjNDBmZGUzNDY0ZGIxMWUxODg0YzdmOGRhM2Q="},
            });
            
            console.clear();
            console.log(result);
            
        }
        
        /*function warning() {
            const result = await axios({
                method: 'get',
                url: 'https://api.agora.io/v1/apps/636c5db740af42848bbd8c12d1df9a9a/cloud_recording/resourceid/'+resourceId+'/sid/'+sid+'/mode/mix/query',
                headers: {Authorization: "Basic OTM1Y2Q0ODExNzZmNGUwM2E1Yjc5OTJjZjJiNGUxMTY6NjFhN2RjNDBmZGUzNDY0ZGIxMWUxODg0YzdmOGRhM2Q="},
            });
            
            console.clear();
            console.log(result);
        }
        window.onbeforeunload = warning;*/
        function end_call() {

                $("#end_call").html('Please Wait...');
                $("#end_call").attr('disabled');
                var notary_confirm_stamp = $("#notary_confirm_stamp").val();
                var notary_confirm_signer = $("#notary_confirm_signer").val();
                //Do whatever you want using result
                $.ajax({
                     url: '{{ url("update_call_status") }}', 
                     type: 'post',
                     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                     data: {doc_id: '{{ $doc_id }}' , notary_confirm_stamp : notary_confirm_stamp,notary_confirm_signer : notary_confirm_signer },
                     success: function (response) {
                           // window.close();
                            // setTimeout(function(){ window.location ="{{ url('completed-document').'/2' }}"  },4000);

                     }
                });

                var result = stopRecording(localStorage.getItem("sid"), localStorage.getItem("resourceId"), localStorage.getItem("recordingUid"));
                localStorage.removeItem("recordingUid");
                localStorage.removeItem("resourceId");
                localStorage.removeItem("recordingUid");
                
                
            
        }

        function endCallModal(){
            $("#endCallModal").modal('show');
        }
        getDevices();
        </script>
   @endsection