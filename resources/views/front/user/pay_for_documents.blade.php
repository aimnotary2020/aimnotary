@extends('front.layouts.app')
  @section('content')

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Payment</h2>
					</div>
						<form  id="submit-payment-form" method="post" action="javascript:void(0)">
							<div class="row">
								<div class="col-sm-6 col-md-6 col-lg-6 cont-left">
									<div class="upload-box" style="text-align: left;">
										<h3 class="text-left">Billing Address</h3>
										<hr>
										<div class="row">
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">First Name <span class="text-danger">*</span></label>
													<input class="form-control margin-b20" type="text" placeholder="Enter First Name" name="f_nm" id="f_nm" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Last Name <span class="text-danger">*</span></label>
													<input class="form-control margin-b20" type="text" placeholder="Enter Last Name" name="l_nm" id="l_nm" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Email <span class="text-danger">*</span></label>
													<input class="form-control margin-b20" type="text" placeholder="Enter Email" name="eml" id="eml" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Phone Number <span class="text-danger">*</span></label>
													<input class="form-control margin-b20 card_number" type="text" placeholder="Enter Phone Number" name="phone" minlength="10" maxlength="10" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Address 1 <span class="text-danger">*</span></label>
													<input class="form-control margin-b20" type="text" name="add1" placeholder="Enter Address 1" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Address 2 </label>
													<input class="form-control margin-b20" type="text" name="add2" placeholder="Enter Address 2" >
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Country <span class="text-danger">*</span></label>
													<select class="form-control margin-b20" name="country" required>
														<option value="">Select Country</option>
														<option value="231">USA</option>
													</select>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">City <span class="text-danger">*</span></label>
													<input class="form-control margin-b20" type="text" placeholder="Enter City" name="city" required>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label class="pull-left">Postal Code <span class="text-danger">*</span></label>
													<input class="form-control margin-b20 card_number" type="text" placeholder="Enter Postal Code" minlength="5" name="zip" required>
												</div>
											</div>
											
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="checkbox text-left">
													
													<label class="margin-l15"><input type="checkbox" checked="checked" name="is_terms" id="is_terms" value="1" required> I agree to the <a href="{{ url('terms-and-conditions') }}" target="_blank"><u>Terms & Conditions</u></a></label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-6 col-lg-6 cont-right">
									<div class="upload-box" style="text-align: left;">
										<h3 class="text-left">Card Information</h3>
										<hr>
										<img class="payment-cards margin-b20" src="{{asset('public/front_assets/img/payment-cards.png')}}" alt="Payment Cards" />
										<div class="row">
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="row">
													<div class="col-md-8 col-lg-8">
														<div class="form-group">
															<label class="pull-left">Card Number <span class="text-danger">*</span></label>
															<input class="form-control margin-b20 card_number" type="text" placeholder="Card Number" name="card_number"   maxlength="16" required>
														</div>
													</div>
													<div class="col-md-4 col-lg-4">
														<div class="form-group">
															<label class="pull-left">MM/YY <span class="text-danger">*</span></label>
															<input class="form-control margin-b20 card_number" type="text" placeholder="MM/YY" onchange="validate_expiry(this.value);" name="card_expiry"  id="card_expiry"  minlength="5" maxlength="5" required>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="row">
													<div class="col-md-8 col-lg-8">
														<div class="form-group">
															<label class="pull-left">Card Holder <span class="text-danger">*</span></label>
															<input class="form-control margin-b20" type="text" placeholder="Card Holder" name="card_holder" required>
														</div>
													</div>
													<div class="col-md-4 col-lg-4">
														<div class="form-group">
															<label class="pull-left">CVV/CVC <span class="text-danger">*</span></label>
															<input class="form-control margin-b20 card_number" type="password" placeholder="CVV/CVC"  name="card_cvv"  minlength="3" maxlength="3" required>
														</div>
													</div>
												</div>
											</div>

											<div class="col-sm-12 col-md-12 col-lg-12">
												<table id="couponTbl" class="table table-bordered">
													<tr>
														<td>Items</td>
														<td>Price</td>
													<tr>
													<?php
														if($docs_info['doc']==1)
														{
															if($docs_info['is_usa']=='Y'){
																$myproductprice=79;
															}else{
																$myproductprice=25;
															}
															
															?>
																<tr>
																	<td>Base Price ( One Document + 1 Signer + 1 Stamp )</td>
																	<td>$<?= $myproductprice ?></td>
																<tr>
															<?php
														}
														else
														{
															$myproductprice=0;
														}
														/*else
														{
															?>
																<!-- <tr>
																	<td>Base Price ( One Document + 1 Signer + 1 Stamp )</td>
																	<td>$25</td>
																<tr> -->
															<?php
														}*/
													?>
													
													<?php
														if($docs_info['user_require_signer']>0 || $docs_info['user_require_signer']!="")
														{
															?>
																<tr>
																	<td>Additional Signer ( <?php 
																								if($docs_info['notary_confirm_signer']==NULL || $docs_info['notary_confirm_signer']=="")
																								{
																									$signerTotal=($docs_info['user_require_signer'] - 1)*5;
																									echo ($docs_info['user_require_signer']-1)." X $5";
																								} 
																								else
																								{
																									$signerTotal=($docs_info['notary_confirm_signer'] - 1)*5;
																									echo ($docs_info['notary_confirm_signer']-1)." X $5";
																								}

																							?> )</td>
																	<td><?php echo '$'.$signerTotal; ?></td>
																<tr>
																<tr>
																	<td>Additional Stamp ( <?php 
																								if($docs_info['notary_confirm_stamp']==NULL || $docs_info['notary_confirm_stamp']=="")
																								{
																									$StampTotal=($docs_info['user_require_stamp'] - 1)*5;
																									echo ($docs_info['user_require_stamp'] - 1)." X $5";
																								} 
																								else
																								{
																									$StampTotal=($docs_info['notary_confirm_stamp'] - 1)*5;
																									echo ($docs_info['notary_confirm_stamp']-1)." X $5";
																								}

																							?> )</td>
																	<td><?php echo '$'.$StampTotal; ?></td>
																<tr>
																<tr>
																    <td> Total </td>
																	<td id="couponUpdatePriceInTabl"> $<?php echo $totalAmount=$myproductprice+$signerTotal+$StampTotal; ?> </td>
																</tr>
															<?php
														}
													?>
													

												</table>
											</div>

											

										</div>
									</div>
									<div class="upload-box" style="text-align: left;">
										<div class="row">
											<div class="col-sm-12 col-md-12 col-lg-12">
												<div class="captcha_main_class">
													<label class="pull-left">Enter Captcha Code <span class="text-danger">*</span></label>
													<div class="clearfix"></div>
													<hr>
													<div class="clearfix"></div>
													<?php $rand=rand(0000,9999); ?>
													<div class="row">
														<div class="col-sm-4 col-md-4 col-lg-4"> 
															<input type="hidden" name="rand" class="randval" value="<?php echo $rand; ?>">
															<input class="captcha rand form-control" type="text" value="<?php echo $rand; ?>" readonly="" id="ransubs" style="background-image:url({{asset('public/front_assets/img/cat.png')}}); font-size:30px; border: 1px solid; text-align: center; max-width:100%;">
														</div>
														<div class="col-sm-2 col-md-2 col-lg-2 text-center">
															<i class="fa fa-refresh capcharefresh1 refresh" aria-hidden="true" style="font-size: 30px;cursor: pointer;margin-top: 10px;"></i> 
														</div>
														<div class="col-sm-6 col-md-6 col-lg-6">
															<input class="form-control card_number"  name="chk" maxlength="4" required="required" id="chk2" placeholder="Enter Captcha" autocomplete="off">
															<br><span id="error1" class="color" style="float: left;color:#FF0000;"></span>
														</div>
													</div>
												</div>
											</div>
											<div style="display : none;" class="col-sm-12 col-md-12 col-lg-12" id="appliedsuccessfully">
												<div id="codeCoupon" style="padding : 10px; border : 2px solid green; width : 100%; background-color: #b8f4b8; float : left;"></div>
												<div onclick="removeCoupon();" style="float:right">X</div>
											</div>
											<div style="margin-top : 10px;" class="col-sm-9 col-md-9 col-lg-9">
												<input type="text" id="couponset" placeholder="Enter Coupon Code" class="form-control" />
											</div>
											<div style="margin-top : 10px;" class="col-sm-3 col-md-3 col-lg-3">
												<a style="cursor : pointer;" class="btn btn-info" id="couponApplyBtn" onclick="check_coupon($('#couponset').val());">Apply</a>
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<hr>
												
											</div>
											<div class="col-sm-12 col-md-12 col-lg-12">
												<input class="btn btn-success btn-lg pull-left" type="submit" value="Submit & Payment" onclick="javascript:return check_captcha1();" id="sub_btn">
												<span id="subs_msg"></span>
											</div>
											<input type="hidden" name="coupon_id" id="coupon_id"/>
											<input type="hidden" name="total_cost" value="<?php echo $totalAmount; ?>" id="total_cost" >
											<input type="hidden" name="doc_id" value="<?php echo $docs_info['id']; ?>" />
											<input type="hidden" name="new_total_cost" id="new_total_cost" >
											<input type="hidden" name="actual_subs_amount" id="actual_subs_amount">
										</div>
									</div>
								</div>
							</div>
						</form>		
				</div>
			</div>
		</div>
	</section>
		
   @endsection
  @section('script')
  <script type="text/javascript">
		// product_price_change();
		$(".capcharefresh1").click(function(e) {
			document.getElementById("ransubs").value = Math.floor(1e4 * Math.random() + 1)
		});
		$(".card_number").keydown(function(e) {
			-1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 67 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 88 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault()
		});

		function check_coupon(coupon_code) {
			var  url = $("#site_url").val();
			$.ajax({ 
					url:'{{url("check_coupon")}}',
					type:"POST",
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					data: {coupon_code:coupon_code},
					success:function(data){
						console.log(data);
						var obj1=JSON.parse(data);
						if (obj1.status==400) {
							$("#coupon_msg").css("color","red");
							$("#coupon_msg").html('Invalid Coupon');
							swal("Oops!",obj1.msg, "warning");
						}else{
							swal("Thank You!", "Coupon Applied Successfully", "success");
							$("#couponset").hide();
							$("#couponApplyBtn").hide();
							$("#appliedsuccessfully").show();
							$("#codeCoupon").html(coupon_code);
							$("#coupon_msg").html('');
							var obj=obj1.result;
							$("#coupon_id").val(obj.coupon_id);
							var total_cost=parseFloat($("#total_cost").val()-obj.discount_amt).toFixed(2);
							$("#couponTbl").append("<tr> <td> Coupon Discount </td> <td>-$"+obj.discount_amt+"</td> </tr>");
							$("#couponUpdatePriceInTabl").html("$"+total_cost);
							$("#discount_li").show();
							$("#discount_amount").html('$'+parseFloat(obj.discount_amt).toFixed(2));
							$("#order_grand_total").html('$'+total_cost);
							$("#total_cost").val(total_cost);
							$("#coupon_msg").css("color","green");
							$("#coupon_msg").html('Aplied Successfully');
						}
					}
			});
		}
		function removeCoupon(){
			swal({
                    title: "Confirmation",
                    text: "Do you want to remove the coupon ?",
                    icon: "warning",
                  }).then((value) => {
                        location.reload();
                    });
		}
		function check_captcha1() {
			return "" == $("#chk2").val() ? (document.getElementById("error1").innerHTML = "Enter Captcha!", $("#chk2").focus(), !1) : $("#ransubs").val() != $("#chk2").val() ? (document.getElementById("error1").innerHTML = "Captcha Not Matched!", $("#chk2").focus(), !1) : void("" != $("#chk2").val() && $("#ransubs").val() == $("#chk2").val() && (document.getElementById("error1").style.color = "green", document.getElementById("error1").innerHTML = "Captcha  Matched!"))
		}

		function validate_expiry(val) {
			var rs=val.replace('/','');
			var result = rs.replace(/^(.{2})(.*)$/, "$1/$2");
			$("#card_expiry").val(result);
		}


   	  		$("#submit-payment-form").validate({
		        //onkeyup:
		     	rules:{
		            f_nm: {
		                required: true,
		            },
		            l_nm: {
		                required: true,
		            },
		            eml: {
		                required: true,
						email : true,
		            },
					phone : 
					{
						required : true,
					},
					add1 : 
					{
						required : true,
					},
					country : 
					{
						required : true,
					},
					city : 
					{
						required : true,
					},
					zip : 
					{
						required : true,
					},
		        },
		        messages: {
		            f_nm: {
		                required:'First Name is required',
		            },
		            l_nm: {
		                required:'Last Name id is required',
		            },
		            eml: {
		                required:'Email id is required',
		            }
		        },
		        submitHandler: function() { 
					loadingfunc("block");
		           var form = $('#submit-payment-form').serialize();
		             $.ajax({
				            url: '{{url("final_payment")}}',
				            type: 'POST',
				            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				            data: form,
				            beforeSend:function(){
								loadingfunc("block");
				            		// $("#subscription-submit-form").hide(), $("#subcsription_loader").show() ,$("#sub_btn").attr('disabled','disabled');
				            },
				            success: function(data){
				                
								loadingfunc("none");
				            	$("#sub_btn").removeAttr('disabled');
				            	
				            	//console.log(data);
				                var obj=JSON.parse(data);
				                
				                if(obj.status==200){
				                   
										$("#subs_msg").css('color','green');
										$("#subs_msg").html(obj.msg);
										
										swal("Thank You!", "Your Payment hasbeen successfull", "success");
										setTimeout(function(){ window.location ="{{ url('dashboard') }}"  },1000);
				                }else if(obj.status==202){
				                	    $("#subs_msg").css('color','red');
										$("#subs_msg").html(obj.msg);
										setTimeout(function(){ window.location ="{{ url('login') }}"  },1000);
				                }else{
				                	$("#subs_msg").css('color','red');
									$("#subs_msg").html(obj.msg);
				                }
				            }
				        });
		        }
		    });

			function setTheValue(newPrice)
			{
				if($('#product_price').val()!=""){
					
					$("#no_of_stamps").removeAttr("disabled");
					var product_price=parseInt($("#product_price").val());
					var no_of_stamp=parseInt($("#no_of_stamps").val());
					var newPrice=parseInt(newPrice);
					if(newPrice==0)
					{
						var new_product_price=(product_price+no_of_stamp);
					}
					else
					{
						var new_product_price=(product_price+newPrice+no_of_stamp);
					}
					
					$("#total_cost").val(new_product_price);
					$("#actual_subs_amount").val(new_product_price);
					$("#product_amt").html('$'+new_product_price);
					$("#order_grand_total").html('$'+(new_product_price));
					$("#sub_btn").removeAttr('disabled');
					$("#new_total_cost").val(new_product_price);
					document.getElementById("no_of_stamps").disabled=false;
					var my_no_of_stamps=($("#no_of_stamps").val()/5) + 1;
					///alert(my_no_of_stamps);
					var signerall=(newPrice/5)+1;
					var signerall=parseInt(signerall);
					if(new_product_price>25.00)
					{
						$("#documentsName").html((signerall==1) ? "One Document + " + signerall+" Signer <span id='myStampsHtml'>+ "+(my_no_of_stamps)+" Stamp</span>" : "One Document + "+signerall+" Signers <span id='myStampsHtml'> + "+(my_no_of_stamps)+" Stamp</span>");
					}
					else if(new_product_price==25.00)
					{
						$("#documentsName").html("One Document + 1 Signer <span id='myStampsHtml'>+ "+(my_no_of_stamps)+" Stamp</span>");
					}
					else
					{
						$("#documentsName").html("Five Documents  + 1 Signer <span id='myStampsHtml'></span>");
					}
				}
			}

			function setStampValue(newPrice)
			{
				if($('#product_price').val()!=""){
					var product_price=parseInt($("#new_total_cost").val());
					var newPrice=parseInt(newPrice);
					var no_of_stamp=parseInt($("#no_of_signers").val());
					if(newPrice==0)
					{
						var new_product_price=(product_price+no_of_stamp);
					}
					else
					{
						var new_product_price=(product_price+newPrice+no_of_stamp);
					}
					var new_product_price=(product_price+newPrice);
					$("#total_cost").val(new_product_price);
					$("#actual_subs_amount").val(new_product_price);
					$("#product_amt").html('$'+new_product_price);
					$("#order_grand_total").html('$'+(new_product_price));
					$("#sub_btn").removeAttr('disabled');
					var signerall=(newPrice/5)+1;
					var signerall=parseInt(signerall);
					console.log(new_product_price);
					if(new_product_price>25.00)
					{
						$("#myStampsHtml").html((signerall==1) ? " + "+signerall+" Stamp" : " + "+signerall+" Stamps");
					}
					else if(new_product_price==25.00)
					{
						$("#documentsName").html("One Document + 1 Signer <span id='myStampsHtml'></span>");
					}
					else
					{
						$("#documentsName").html("Five Documents 1<span id='myStampsHtml'></span>");
					}
				}
			}
   </script>
  @endsection