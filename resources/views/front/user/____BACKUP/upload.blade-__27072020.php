@extends('front.layouts.app')
  @section('content')
	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Upload Documents for AimNotary</h2>
						<p>Get ready to connect with a live notary in minutes.</p>
					</div>
					<div class="upload-box">
						<!-- <form id="upload-multiple-doc" method="POST" > -->
							<input id="file-1" class="inputfile inputfile-1" type="file" name="doc[]" data-multiple-caption="{count} files selected" multiple />
							<label class="up-cont" for="file-1" >
								<p style="font-size: 27px;"><b>Drag drop or Click here to select the document</b></p>
								<p style="padding-bottom: 30px; color: #4424a8; font-size: 17px; font-weight: bold;">After selecting the document click the button below</p>
								<!-- <a  href="javascript:void(0);"></a> -->
								<button type="button" class="btn btn-primary btn-lg"  id="upload_muliple_file"> <i class="fa fa-upload" aria-hidden="true" ></i> Upload Document </button>
								<span id="doc_error_msg1"></span>
							</label>
						<!-- </form> -->
					</div>
					<div class="upload-cont-two upload-file-view">
						<div class="row">
							<div class="col-sm-12 col-md-8 col-lg-8 cont-left">
								<div class="table-responsive">
									<table class="table">
										<tbody id="append_all_docs">
											
											
										</tbody>
									</table>
								</div>
								<!--<p class="add-more-doc text-primary">
									<label for="upload-photo" id="add_more_doc"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add More Document</label>
									<input type="file" name="photo" id="upload-photo" />									
								</p>-->
							</div>
							<div class="col-sm-12 col-md-4 col-lg-4 cont-right">
								<a class="default-btn page-btn text-white float-right" href="{{ url('dashboard') }}">Back To Dashboard</a>
							</div>
						</div>
					</div>
					<!--<div class="upload-cont-two">
						<h3 class="margin-b20">Upload commonly notarized documents</h3>
						<p>Click on one of the following links to automatically upload the document. We also notarize Power of Attorney, Credit Card Authorizations, Lease Agreements, Child Support Documents, <a href="#">and more...</a></p>
						<p><a href="#"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Application for Delivery of Mail Through an Agent</a></p>
						<p><a href="#"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Issuance of a U.S. Passport for Minor</a></p>
						<p><a href="#"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Letter of Consent for Travel of a Minor</a></p>
					</div>-->
					
				</div>
			</div>
		</div>
	</section>

		  <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title">View PDF </h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                           <div class="col-md-12 grid-margin stretch-card">
                              <div class="card">
                                <div class="card-body">
                                	
	                                 <iframe src="" id="display_pdf" width="100%" height="500px"></iframe>
	                                 
                                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>	
   @endsection
   @section('script')
   <script type="text/javascript">
   	     get_docs();
   	     function get_docs() {
   	     	$.ajax({
					     url: '{{ url("fetch_documents") }}', 
					     type: 'GET',
					     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					     data: {get_docs:"get docs"},
					     contentType: false,
					     processData: false,
					     success: function (response) {
				            console.log(response);
				            var obj=JSON.parse(response);
				            $("#append_all_docs").html('');
				            $.each(obj,function(key,val){
				            	var sign_status='';
				            	if(val.status==1){
				            		 sign_status=' <a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="view_pdf(\''+val.doc+'\')"><i class="fa fa-eye" aria-hidden="true"></i> View </a>  <a class="btn btn-success btn-sm"  href="{{ url("verify-identity")."/" }}'+window.btoa(val.id)+'" > Continue to Notarization</a>';
				            	}else if(val.status==2){
				            		 sign_status='<span class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i>Completed</span>';
				            	}else{
				            		 sign_status='<a class="btn btn-success btn-sm" href="https://www.aimnotary.com/signer/document/'+val.document_key+'" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Sign</a>';
				            	}
				            	$("#append_all_docs").append('<tr><td><i class="fa fa-file-text" aria-hidden="true"></i> <span id="txt_doc_nm_'+val.id+'">'+val.doc_nm+'</span> <input type="hidden" id="edit_doc_nm_'+val.id+'" value="'+val.doc_nm+'" > </td><td class="text-right"><a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="remove_doc('+val.id+');"><i class="fa fa-trash" aria-hidden="true"></i> Remove</a>'+ sign_status+' </td></tr>');
				            });
					     }
					});
   	     }
   	     function remove_doc(id) {
   	     	if(confirm("Are you sure you want to remove this document!.")){
	     	    $.ajax({
				     url: '{{ url("remove_document") }}', 
				     type: 'post',
				     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				     data: {id:id},
				     success: function (response) {
			            var newData = JSON.parse(response);
	                    if(newData.status == 200){
	                        toastr.success(newData.msg);
	                    }else{
	                    	toastr.error(newData.msg);
	                    }
	                     get_docs();
				     }
				});
	     	}
   	     }
   	     function rename_doc(id) {
   	     	 $("#txt_doc_nm_"+id).hide();
   	     	 $("#edit_doc_nm_"+id).attr('type','file');
   	     	 $("#edit_doc_nm_"+id).click();
   	     	 $("#edit_doc_nm_"+id).change(function(){
                   var form_data = new FormData();
	   	     	   var totalfiles = document.getElementById('edit_doc_nm_'+id).files.length;
	   	     	   if(totalfiles>0){
	   	     	   	   form_data.append('doc_id',id);
					   for (var index = 0; index < totalfiles; index++) {
					      form_data.append("files[]", document.getElementById('edit_doc_nm_'+id).files[index]);
					   }
				      $.ajax({
						     url: '{{ url("edit_documents") }}', 
						     type: 'post',
						     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
						     data: form_data,
						     contentType: false,
						     processData: false,
						     success: function (response) {
					            var newData = JSON.parse(response);
			                    if(newData.status == 200){
			                        toastr.success(newData.msg);
			                    }else{
			                    	toastr.error(newData.msg);
			                    }
			                    get_docs();
			                    $('edit_doc_nm_'+id).trigger("reset");
								$('edit_doc_nm_'+id).validate().resetForm();
						     }
						   });
				  }
   	     	 });
   	     }
   	     function view_pdf(val) {
   	     	var url="{{ url('/').'/public/' }}"+val;
   	     	$("#display_pdf").attr('src',url);
   	     	$("#myModal").modal('show');
   	     }
   	     $("#upload_muliple_file").click(function(){
   	     	   var form_data = new FormData();
   	     	   var totalfiles = document.getElementById('file-1').files.length;
   	     	   if(totalfiles>0){
   	     	   	   $("#doc_error_msg1").text('');
				   for (var index = 0; index < totalfiles; index++) {
				      form_data.append("files[]", document.getElementById('file-1').files[index]);
				   }
			      $.ajax({
					     url: '{{ url("upload_documents") }}', 
					     type: 'post',
					     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					     data: form_data,
					     contentType: false,
					     processData: false,
					     success: function (response) {
					     	// console.log(response);
				            var newData = JSON.parse(response);
		                    if(newData.status == 200){
		                        toastr.success(newData.msg);
		                    }else{
		                    	toastr.error(newData.msg);
		                    }
		                     get_docs();
		                    $('#file-1').trigger("reset");
							$("#file-1").validate().resetForm();
					     }
					   });
			  }else{
			  	  $("#file-1").focus();
			  	  $("#doc_error_msg1").css('color','red');
			  	  $("#doc_error_msg1").text('Please choose file!');
			  }
   	    });
   	     $("#upload-photo").change(function(){
               var form_data = new FormData();
   	     	   var totalfiles = document.getElementById('upload-photo').files.length;
   	     	   //alert(totalfiles);
   	     	   if(totalfiles>0){
   	     	   	   $("#doc_error_msg1").text('');
				   for (var index = 0; index < totalfiles; index++) {
				      form_data.append("files[]", document.getElementById('upload-photo').files[index]);
				   }
			      $.ajax({
					     url: '{{ url("upload_documents") }}', 
					     type: 'post',
					     headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					     data: form_data,
					     contentType: false,
					     processData: false,
					     success: function (response) {
				            var newData = JSON.parse(response);
		                    if(newData.status == 200){
		                        toastr.success(newData.msg);
		                    }else{
		                    	toastr.error(newData.msg);
		                    }
		                     get_docs();
		                    $('#upload-photo').trigger("reset");
							$("#upload-photo").validate().resetForm();
					     }
					   });
			  }else{
			  	  $("#upload-photo").focus();
			  	  // $("#doc_error_msg1").css('color','red');
			  	  // $("#doc_error_msg1").text('Please choose file!');
			  }        
   	     });
   	   
   </script>
   @endsection