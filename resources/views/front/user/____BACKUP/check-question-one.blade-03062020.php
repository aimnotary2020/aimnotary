@extends('front.layouts.app')
  @section('content')

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Verify Your Identity</h2>
						<p>Question <b>1</b> of <b>2</b></p>
					</div>
					<div class="upload-box@">
						<form  id="check-question-one" method="post" action="javascript:void(0)">
							<h3 class="text-left">Address</h3>
							<div class="row">
								
								<div class="col-lg-6 col-sm-12">
									<div class="form-group">
										<label class="pull-left">What is your State? <span class="text-danger">*</span></label>
										<select class="form-control" required name="state" id="state_id" onchange="get_city(this.value);">
											<option value="">Select State</option>
											
										</select>
									</div>
								</div>
								<div class="col-lg-12 col-sm-12">
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-6 col-sm-12">
									<div class="form-group">
										<label class="pull-left">What is your City? <span class="text-danger">*</span></label>
										<select class="form-control margin-b20" required name="city" id="city_id">
											<option value="">Select City</option>
										</select>
									</div>
								</div>
								<div class="col-lg-12 col-sm-12">
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-6 col-sm-12">
									<div class="form-group">
										<label class="pull-left">What is your Zip Code? {{ Session::get('session_time') }} <span class="text-danger">*</span></label>
										<input class="form-control margin-b20 card_number" type="text" placeholder="Enter Zipcode" required name="zip" minlength="5" maxlength="6">
									</div>
								</div>
								<div class="col-lg-12 col-sm-12">
									<div class="clearfix"></div>
								</div>
								<div class="col-lg-6 col-sm-12">
									<div class="form-group">
										<label class="pull-left">Address<span class="text-danger">*</span></label>
										 <textarea class="form-control" required name="addr1" id="Addr1_id" placeholder="Address" ></textarea>
									</div>
								</div>
								
								<div class="col-lg-12 col-sm-12">
									<div class="form-group">
										<label class="pull-left text-danger margin-b20"><span id="time"></span> REMANING</label>
										<input type="hidden" name="time_val" id="time_val">
									</div>
								</div>
								<div class="col-lg-12 col-sm-12">
									<div class="form-group">
										<a class="btn btn-secondary btn-lg pull-left" href="{{ url('check-ssn-verification') }}">Back</a>
										<button type="submit" class="btn btn-success btn-lg pull-left margin-l15" id="submit_btn">
											Next
										</button>
									</div>
								</div>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
	</section>
		
  @endsection
  @section('script')
       <script type="text/javascript">
       	    function startTimer(duration, display) {
			    var timer = duration, minutes, seconds;
			    setInterval(function () {
			        minutes = parseInt(timer / 60, 10)
			        seconds = parseInt(timer % 60, 10);

			        minutes = minutes < 10 ? "0" + minutes : minutes;
			        seconds = seconds < 10 ? "0" + seconds : seconds;

			        display.textContent = minutes + ":" + seconds;

			        if (--timer < 0) {
			            timer = duration;
			        }
			    }, 1000);


			}

			window.onload = function () {
			    var twoMinutes = <?php echo (Session::has('session_time')?Session::get('session_time'):120);  ?> ,
			        display = document.querySelector('#time');
			    startTimer(twoMinutes, display);
               

       //         var reloading = sessionStorage.getItem("reloading");
			    // if (reloading) {
			    //     sessionStorage.removeItem("reloading");
			    //     alert("hii");
			    // }
			     // $.ajax({
		      //           url: '{{url("set_time_session")}}',
		      //           type: 'POST',
		      //           headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
		      //           data: {get_states:"Get States"},
		      //           success:function(res){
		      //           	console.log(res);
		                 
		      //           }
		      //       });
			};

			$(".card_number").keydown(function(e) {
		        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 67 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 88 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault()
		    });

			get_states();
			function get_states() {
		            $.ajax({
		                url: '{{url("get-states")}}',
		                type: 'GET',
		                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
		                data: {get_states:"Get States"},
		                success:function(res){
		                  var obj=JSON.parse(res);
		                  $('#state_id').children('option:not(:first)').remove();
		                  //$("#state_id").closest('.list').children('li:not(:first)').remove();
		                  $.each(obj, function (key, val) {
		                         $("#state_id").append('<option value="'+val.state_code+'">'+val.state_name+'</option>');
		                         
		                   });
		                }
		            });
		    }

		    function get_city(state_id) {
		            $.ajax({
		                url: '{{url("get-city")}}',
		                type: 'POST',
		                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
		                data: {state_id:state_id},
		                success:function(res){
		                   var obj=JSON.parse(res);
		                   $('#city_id').children('option:not(:first)').remove();
		                   $.each(obj, function (key, val) {
		                         $("#city_id").append('<option value="'+val.city+'">'+val.city+'</option>');
		                   });
		                }
		            });
		    }
	
			window.onbeforeunload = function() {
				console.log($("#time").text());
				$.ajax({
		                url: '{{url("set_time_session")}}',
		                type: 'POST',
		                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
		                data: {session_time:$("#time").text()},
		                cache       : false,
		                contentType : false,
               			processData : false,
		                success:function(res){
		                	console.log(res);
		                	alert(res);
		                 
		                }
		            });
			       // return "Dude, are you sure you want to refresh? Think of the kittens!";
			}

			 $("#check-question-one").validate({
	     
		        submitHandler: function() { 
		           var form = $('#check-question-one').serialize();
		             $.ajax({
				            url: '{{url("verify_ssn_address")}}',
				            type: 'POST',
				            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				            data: form,
				            beforeSend:function(){
				            		 $('#submit_btn').html('Please Wait...');
				            		 $('#submit_btn').attr('disabled','disabled');
				            },
				            success: function(data){
				               var obj=JSON.parse(data);
				                console.log(data);
				                if(obj.status==200){
										toastr.success(obj.msg);
										setTimeout(function(){ window.location ="{{ url('check-question-two') }}"  },1000);
				                	  
				                }else{
				                	   $('#submit_btn').html('Next');
				            		   $('#submit_btn').removeAttr('disabled');
				                	    toastr.error(obj.msg);
										$('#check-question-one').trigger("reset");
										$("#check-question-one").validate().resetForm();
				                }

				      //           setTimeout(function(){ $("#contact-msg").html(''); },800);

				                
				            }
				        });
	        }
	    });
       </script>
  @endsection