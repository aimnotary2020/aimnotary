@extends('front.layouts.app')
  @section('content')
<style type="text/css">
	.valid{
	  border:1px solid blue;
	}
	.invalid{
	  border:1px solid red;
	}
</style>
	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Verify Your Identity</h2>
						<p>Now it's time to verufy your identity. You'll need the following things for a valid notarization.</p>
					</div>
					<form id="submit-ssn-form" method="post" action="javascript:void(0)" > 
						<div class="upload-box">
							<img class="img-responsive pull-left margin-b20" src="{{asset('public/front_assets/img/ssn-icon.png')}}" style="width: 150px;" alt="SSN ICON" />
							<div class="clearfix"></div>
							<p class="text-left"><b>Please enter the SSN</b></p>
							<div class="clearfix"></div>
							<div class="ssn-input-cont">
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<span>- <input class="form-control ssn_class" type="hidden" name="ssn[]" maxlength="1" required value="-"></span>
								

								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<span>- <input class="form-control ssn_class" type="hidden" name="ssn[]" maxlength="1" required value="-"></span>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
								<input class="form-control ssn_class ssn_nxt" type="password" name="ssn[]" maxlength="1" required>
							</div>
							<p class="text-left">Your SSN is only used to verifyyour identity as required by law. We never store this information. <a href="{{ url('privacy-policy') }}" target="_blank">Privacy Policy</a></p>
							<button class="btn btn-success btn-lg pull-left margin-b20" href="javascript:void(0);" id="verify_ssn_btn"  >Verify</button>
							<div class="clearfix"></div>
							<a class="pull-left" href="#"><b>I don't have an SSN</b></a>
							<div class="clearfix"></div>
						</div>
					</form>
					
					
					
				</div>
			</div>
		</div>
	</section>
		
	@endsection
	@section('script')
       <script type="text/javascript">


	  	

// function myFunc() {
// 			   var patt = new RegExp("\d{3}[\-]\d{2}[\-]\d{4}");
// 			   var ssn_arr_val =  $("input[name='ssn[]']").map(function(){return $(this).val();}).get();
// 			  // alert(ssn_arr_val);
// 			   var ssn=ssn_arr_val;
// 			   var x=ssn_arr_val.split(',').join('');
// 	          alert(x);
// 			   var res = patt.test(x.value);
// 			   if(!res){
// 			    x.value = x.value
// 			        .match(/\d*/g).join('')
// 			        .match(/(\d{0,3})(\d{0,2})(\d{0,4})/).slice(1).join('-')
// 			        .replace(/-*$/g, '');
// 			   }
// 			}
		 $("#submit-ssn-form").validate({
	        //onkeyup:
	        submitHandler: function() { 
	        
	           var form = $('#submit-ssn-form').serialize();
	             $.ajax({
			            url: '{{url("check_ssn")}}',
			            type: 'POST',
			            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			            data: form,
			            beforeSend:function(){
			            		 // $('#contact-btn').html('Please Wait...');
			            		 // $('#contact-btn').attr('disabled','disabled');
			            },
			            success: function(data){
			               var obj=JSON.parse(data);
			                // console.log(data);
			                if(obj.status==200){
									toastr.success(obj.msg);
									setTimeout(function(){ window.location ="{{ url('confirm-detail') }}"  },1000);
			                	  
			                }else{
			                	    toastr.error(obj.msg);
									$('#submit-ssn-form').trigger("reset");
									$("#submit-ssn-form").validate().resetForm();
			                }

			      //           setTimeout(function(){ $("#contact-msg").html(''); },800);

			                
			            }
			        });
	        }
	    });
		$(".ssn_nxt").keyup(function () {
		    if (this.value.length == this.maxLength) {
		      var $next = $(this).next('.ssn_nxt');
		      if ($next.length)
		          $(this).next('.ssn_nxt').focus();
		      else
		          $(this).blur();
		    }
		}); 	

       </script>
	@endsection