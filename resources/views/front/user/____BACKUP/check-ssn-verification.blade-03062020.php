@extends('front.layouts.app')
  @section('content')

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Verify Your Identity</h2>
					</div>
					<div class="upload-box">
						<img class="img-responsive pull-left margin-b20" src="{{asset('public/front_assets/img/ssn-verify.png')}}" style="width: 150px;" alt="SSN ICON" />
						<div class="clearfix"></div>
						<p class="text-left"><b>Verification Questions</b></p>
						<p class="text-left text-secondary">Please answer a few questions so we can confirm your identity</p>
						<div class="clearfix"></div>
						<p class="text-left">You will have two minutes to complete the questions.</p>
						<a class="btn btn-success btn-lg pull-left margin-b20" href="{{ url('check-question-one') }}">Answer The Questions</a>
						<div class="clearfix"></div>
					</div>					
				</div>
			</div>
		</div>
	</section>
		
   @endsection