@extends('front.layouts.app')
  @section('content')

	<section class="inner-page-container pt-50 pb-100 user-area-all-style">
		<div class="container">
			<div class="company-us-area">
				<div class="company-tab-wrap upload-page-cont">
					<div class="dashboard-top">
						<h2>Connect with Aimnotary</h2>
						<p>Next we will start a video call with the notary.</p>
					</div>
					<div class="upload-box">						
						<img class="img-responsive margin-b20 margin-t20" src="assets/img/icon-video-chat.png" alt="ICON" />
						<div class="clearfix"></div>
						<p class="margin-b20">Notarizations start at $ 25. Additional seals $7.</p>
						<div class="clearfix"></div>
						<a class="btn btn-success btn-lg margin-b20" href="payment.php">Let's Get Started</a>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
   @endsection