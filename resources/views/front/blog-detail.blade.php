@extends( 'front.layouts.app' )
@section( 'content' )
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<p class="inner-title">Read and Ask</p>
			<ul>
				<li><a href="{{ url('/') }}">Home</a></li>
				<li><a href="{{ url('blog') }}">Blog</a></li>
				<li>Blog Detail</li>
			</ul>
		</div>
	</div>
</div>
<section class="inner-page-container pt-50 pb-100 news-details-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="blog-details-desc">
					<div class="article-content">
					    <div class="row">
							<div class="col-lg-12 col-md-12">
							    <?php
                                    /*echo $blog_detail;
                                    exit;*/
                                ?>
								<div class="entry-meta">
									<div class="article-image" style="float: left;width: 100%;margin: 0 30px 30px 0;">
										<img src="{{asset('public/').'/'.$blog_detail['image'] }}" alt="image">
									</div>
									<ul>
										<li><span>Posted On:</span> <a href="#">{{ date_format(date_create($blog_detail['created_at']),'F d, Y') }}</a>
										</li>
										<li>
											<span>Posted By: Esther Bobo</span>
										</li>
									</ul>
								</div>
								<h1 class="blog-title">
									@if(isset($blog_detail['title']))
										{{ $blog_detail['title'] }}
									@endif  
								</h1>
								@if(isset($blog_detail['description'])) {!! $blog_detail['description'] !!} @endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection