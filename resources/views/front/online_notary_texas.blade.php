@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Texas</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Texas</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Texas</span>
							<h2>TEXAS</h2>

							<p>Great News, Texas now allows notary services online! Texas recently joined a number of other states allowing their commissioned notaries public to notarize the signature of a principal that appears via two-way video and audio conferencing. The process of meeting with a virtual notary public online is called Remote Online Notarization. Texas legislators passed an amendment to the Texas Government Code Title 406 Subchapter C.  This amendment provides state guidelines for online notary services.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/texas-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

						<h2>Title 406 Subchapter C</h2>

						<p>Sec.406.110 permits authorized Texas notaries to “perform an online notarization authorized under Section 406.107 that meets the requirements…regardless of whether the principal is physically located in this state at the time of the online notarization.” The amendment addresses criteria for proper identification of the principal and permitted forms of signature and seal for notaries public. The guidelines for a Texas notary public to qualify and apply for remote online notarization commission are listed within this amendment. Section 406.104 permits the secretary of state to “develop and maintain standards for online notarization…including for credential analysis and identity proofing.”</p>

						<h2>Requirements For Remote Online Notaries in Texas</h2>

						<p>If you are interested in becoming a notary public in the state of Texas, below are a some, but not all requirements, for becoming a Remote Online Notary Public:</p>

						<ul>
							<li>Be an active Texas notary public</li>
							<li>No convictions for felonies or crimes of moral turpitude</li>
							<li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
							<li>Obtain an electronic seal (that meets state requirements)</li>
						</ul>

						<h2>How to Apply to be a Texas Remote Online Notary Public</h2>

						<p>The state of Texas allows individuals to apply to become a Remote Online Notary Public completely online. Click <a href="https://direct.sos.state.tx.us/onlinenotary/OnlineNotaryLogin.aspx" target="_blank">HERE</a>.</p>

						<p>The following is an overview of information you will need to provide throughout the approval process:</p>

						<ul>
							<li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
							<li>Acknowledgment of state requirements</li>
							<li>Signed statement of officer form</li>
							<li>Copy of your digital seal</li>
						</ul>

						<p>Information in this article is subject to be updated by the Texas Secretary of State. Therefore, it is essential that Texas notaries public review necessary information via the website. Click <a href="https://statutes.capitol.texas.gov/Docs/GV/htm/GV.406.htm" target="_blank">HERE</a>.</p>

						<p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>						
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>
@endsection