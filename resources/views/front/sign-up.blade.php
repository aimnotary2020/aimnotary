@extends('front.layouts.app')
@section('content')
<?php
$price=base64_decode($_GET['pr']);
 if(isset($price))
 {
	if($price!=25 && $price!=99 && $price!=79)
	{
		header("location : ".url('/pricing'));
	}
 }
 else
 {
	header("location : ".url('/pricing'));
 }

?>

	<!-- Start Sign Up Area -->
	<section class="inner-page-container pt-50 pb-100 user-area-all-style sign-up-area">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="contact-form-action">
						<div class="form-heading text-center">
							<h3 class="form-title">Create an account!</h3>
						</div>
						<form method="post" id="submit-reg-form" action="javascript:void(0);" >
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<input class="form-control" type="text" name="f_name"  id="f_name" placeholder="First Name">
									</div>
								</div>
								<div class="col-md-12 col-sm-12 ">
									<div class="form-group">
										<input class="form-control" type="text" name="l_name" id="l_name" placeholder="Last Name">
									</div>
								</div>
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<input class="form-control" type="email" name="u_email" id="u_email" placeholder="Email Address">
									</div>
								</div>
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<input class="form-control number" type="text" name="personal_mobile"  id='personal_mobile' placeholder="Phone Number">
									</div>
								</div>
								<div class="col-md-12 col-sm-12">
									<div class="form-group">
										<input class="form-control" type="password" name="u_password" id="u_password" placeholder="Password" autocomplete="off">
									</div>
								</div>
								<div class="col-md-12 col-sm-12 ">
									<div class="form-group">
										<input class="form-control" type="password" name="cnf_password"  id="cnf_password" placeholder="Confirm Password" autocomplete="off">
 										<input type="hidden" name="doc_no" value="<?php echo $_GET['doc']; ?>" class="form-control" />
										 <input type="hidden" name="is_usa" value="<?php echo ($price==79) ? 'Y' : 'N' ?>" class="form-control" />
									</div>
								</div>

								 <?php
									if($price==99){
										?>
											<div class="upload-box" style="text-align: left;">
											<h3 class="text-left">Card Information</h3>
											<hr>
											<img class="payment-cards margin-b20" src="https://www.aimnotary.com/public/front_assets/img/payment-cards.png" alt="Payment Cards">
											<div class="row">
												<div class="col-sm-12 col-md-12 col-lg-12">
													<div class="row">
														<div class="col-md-12 col-lg-12">
															<h3>Price Breakup & Documents Information</h3>
															<table class="table table-bordered">
																<tr>
																	<td>Price</td>
																	<td>$99</td>
																</tr>
																<tr>
																	<td>Documents</td>
																	<td>5</td>
																</tr>
																<tr>
																	<td>Signer / Document</td>
																	<td>1</td>
																</tr>
																<tr>
																	<td>Stamp / Document</td>
																	<td>1</td>
																</tr>
															</table>
															<p style='color : red; font-weight : 700;'>* Additional signers & stamps will charge $5</p>
														</div>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<h3>Payment Information</h3>
														<div class="col-md-8 col-lg-8">
															<div class="form-group">
																<label class="pull-left">Card Number <span class="text-danger">*</span></label>
																<input class="form-control margin-b20 card_number" type="text" placeholder="Card Number" name="card_number" maxlength="16" required="">
															</div>
														</div>
														<div class="col-md-4 col-lg-4">
															<div class="form-group">
																<label class="pull-left">MM/YY <span class="text-danger">*</span></label>
																<input class="form-control margin-b20 card_number" type="text" placeholder="MM/YY" onchange="validate_expiry(this.value);" name="card_expiry" id="card_expiry" minlength="5" maxlength="5" required="">
															</div>
														</div>
													</div>
												</div>
												<div class="col-sm-12 col-md-12 col-lg-12">
													<div class="row">
														<div class="col-md-8 col-lg-8">
															<div class="form-group">
																<label class="pull-left">Card Holder <span class="text-danger">*</span></label>
																<input class="form-control margin-b20" type="text" placeholder="Card Holder" name="card_holder" required="">
															</div>
														</div>
														<div class="col-md-4 col-lg-4">
															<div class="form-group">
																<label class="pull-left">CVV/CVC <span class="text-danger">*</span></label>
																<input class="form-control margin-b20 card_number" type="password" placeholder="CVV/CVC" name="card_cvv" minlength="3" maxlength="3" required="">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php
									}
								 ?>


								<div class="col-md-12 col-sm-12 col-xs-12 form-condition">
									<div class="agree-label">
										<input type="checkbox" id="pp_checked" name="privacy">
										<label for="pp_checked" >
											&nbsp; I agree with
											<a target="_blank" href="privacy-policy">Privacy Policy</a>
										</label>
									</div>
									<div class="agree-label">
										<input type="checkbox" id="tnd_checked" name="terms">
										<label for="tnd_checked">
											&nbsp; I agree with
											<a target="_blank" href="terms-and-conditions">Terms of Services</a>
										</label>
									</div>
								</div>
								<div class="col-12">
									<button class="default-btn btn-two" type="submit" id="reg_btn">
										Register Account
									</button>
									<span id="reg-msg"></span>
								</div>
								<div class="col-12">
									<p class="account-desc">
										Already have an account?
										<a href="login"> Login</a>
									</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Sign Up Area -->
@endsection
@section('script')
 <script>
 	$(".card_number").keydown(function(e) {
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 67 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 88 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault()
    });
	function validate_expiry(val) {
        var rs=val.replace('/','');
        var result = rs.replace(/^(.{2})(.*)$/, "$1/$2");
        $("#card_expiry").val(result);
    }
    $("#submit-reg-form").validate({
        //onkeyup:
     	rules:{
            f_name: {
                required: true,
            },
            l_name: {
                required: true,
            },
            u_email: {
                required: true,
				remote : '{{ url("/checkEmailAjax") }}',
            },
            personal_mobile: {
                required: true,
                minlength:10,
				maxlength:10,
            },
            u_password: {
                required: true,
                minlength:6,
            },
            cnf_password:{
            	required:true,
            },
			privacy : {
				required : true,
			},
			terms : {
				required : true,
			}
        },
        messages: {
            f_name: {
                required:'First Name is required',
            },
            l_name: {
                required:'Last Name is required',
            },
            u_email: {
                required:'Email Address is required',
				remote : 'This email address is already registered',
            },
            personal_mobile: {
                required:'Phone no is required',
            },
            u_password: {
                required:'Password is required',
            },
            cnf_password: {
                required:'Confirm Password is required',
            },
			privacy : {
				required : "Please Agree with the privacy section",
			},
			terms : {
				required : "Please Agree with the Terms section",
			}
        },
        submitHandler: function() {
		     loadingfunc("block");
           	 var form = $('#submit-reg-form').serialize();
             $.ajax({
		            url: '{{url("user_registration")}}',
		            type: 'POST',
		            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		            data: form,
		            beforeSend:function(){
						$('#reg_btn').html('Please Wait...');
						$('#reg_btn').attr('disabled','disabled');
		            },
		            success: function(data){
						loadingfunc("none");
		                var obj=JSON.parse(data);
		                
		                if(obj.status==200){
		                    
								$('#reg_btn').removeAttr('disabled');
								$('#reg_btn').html('Register Account');
								$("#reg-msg").css('color','green');
								$("#reg-msg").html(obj.message);
								
								swal("Thank You!", obj.message, "success");
								if(obj.mode=='redirect'){
									setInterval(function(){
										window.location.href='{{ url("/login") }}';
									}, 1000);//run this thang every 2 seconds
								}
		                }else{
		                	    $('#reg_btn').html('Register Account');
		                	    $("#reg-msg").css('color','red');
								$("#reg-msg").html(obj.message);
								swal("oops!", obj.message, "error");
		                }
		                setTimeout(function(){ $('#submit-reg-form').trigger("reset");
								$("#submit-reg-form").validate().resetForm(); $("#reg-msg").html(''); },1000);

		            }
		        });
        }
    });
    $(".number").keydown(function(e) {
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 67 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 88 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault()
    });
    </script>
@endsection