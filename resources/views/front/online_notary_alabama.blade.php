@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Alabama</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Alabama</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Alabama</span>
							<h2>ALABAMA</h2>

							<p>Signers in the state of Alabama are now able to connect with a commissioned remote online notary via video chat. This is GREAT news for Alabama signers! Connecting with a virtual notary will save time and money for many signers. </p>

							<p>Singers maybe wondering, how do I personally appear for a remote online notarization? Instead of appearing physically with a notary public, the signer is “appearing” via two-way videoconference with the notary public to receive notary services online.</p>

							<p>Alabama’s Senate Bill 275 became effective on July 1, 2021, permitting remote online notarization permanent within the state of Alabama. Details surrounding SB275 can be found <a href="http://alisondb.legislature.state.al.us/ALISON/SearchableInstruments/2021RS/PrintFiles/SB275-enr.pdf" target="_blank">HERE</a>.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
				<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/alabama-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

						<h2>Requirements For Remote Online Notaries in Alabama</h2>

						<p>If you are interested in becoming a remote online notary public in the state of Alabama, below are a some, but not all, requirements for becoming a Remote Online Notary Public:</p>

						<ul>
							<li>Be an active Alabama notary public</li>
							<li>No convictions for felonies or crimes of moral turpitude</li>
							<li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
							<li>Obtain an electronic seal (that meets state requirements)</li>
						</ul>

						<h2>How to Apply to be an Alabama Remote Online Notary Public</h2>

						<p>The state of Alabama allows individuals to apply to become a Remote Online Notary Public. Click <a href="https://www.sos.alabama.gov/administrative-services/notaries-public" target="_blank">HERE</a>.</p>

						<p>The following is an overview of information you will need to provide throughout the approval process:</p>

						<ul>
							<li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
							<li>Acknowledgment of state requirements</li>
							<li>Copy of your digital seal, signature, identity proofing process, and more</li>
						</ul>

						<p>Information in this article is subject to be updated by the Alabama Secretary of State. Therefore, it is essential that Alabama notaries public review necessary information via the website. Click <a href="https://www.sos.alabama.gov/administrative-services/notaries-public" target="_blank">HERE!</a></p>

						<p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>						
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>

@endsection