@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Florida</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Florida</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Florida</span>
							<h2>FLORIDA</h2>

							<p>Florida has now joined numerous other states allowing their current notaries public to participate in Remote Online Notarization. Notary services online are a convenient way for signers to have access to an online notary. This allows Florida notaries public to assist signers all over the United States. Having access to a virtual notary allows signers to have their documents signed at convenient times and locations for them. On June 7, 2019, HB 409 “Electronic Legal Documents” was signed into law (Chapter 2019-71, Laws of Florida). The law went into effect on January 1, 2020. Additionally, 1N-7001, Florida Administrative Code outlines the duties and responsibilities of remote online notaries as well as the procedures for how an individual or current notary public for the state of Florida should apply. This law authorizes Florida notaries public to perform remote online notarizations after completing the application and the training requirements.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
				<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/florida-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

						<h2>Requirements For Remote Online Notaries in Florida</h2>

						<p>If you are interested in becoming a remote online notary public in the state of Florida, below are a some, but not all requirements, for becoming a Remote Online Notary Public:</p>

						<ul>
							<li>Be an active Florida notary public</li>
							<li>No convictions for felonies or crimes of moral turpitude</li>
							<li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
							<li>Obtain an electronic seal (that meets state requirements)</li>
						</ul>

						<h2>How to Apply to be a Florida Remote Online Notary Public</h2>

						<p>The state of Florida allows individuals to apply to become a Remote Online Notary Public. Click <a href="https://www.flrules.org/gateway/RuleNo.asp?id=1N-7.001" target="_blank">HERE</a>.</p>

						<p>The following is an overview of information you will need to provide throughout the approval process:</p>

						<ul>
							<li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
							<li>Acknowledgment of state requirements</li>
							<li>Contracted with an approved third-party vendor</li>
							<li>Copy of your digital seal, signature, identity proofing process, and more</li>
						</ul>

						<p>Information in this article is subject to be updated by the Florida Secretary of State. Therefore, it is essential that Florida notaries public review necessary information via the website. Click <a href="https://www.flrules.org/gateway/RuleNo.asp?id=1N-7.001" target="_blank">HERE!</a></p>

						<p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>						
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>
@endsection