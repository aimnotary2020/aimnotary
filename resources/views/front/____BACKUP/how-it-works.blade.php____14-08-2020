@extends('front.layouts.app')
@section('content')
	<!-- Start Page Title Area -->
	<div class="page-title-area bg-18">
		<div class="container">
			<div class="page-title-content">
				<h2>How it Works</h2>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li>How it Works</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Page Title Area -->
	<!-- Start Opportunity Area -->
	<section class="inner-page-container pt-50 pb-100 how-it-work">
		<div class="opportunity-area pb-20">			
			<div class="container">
				
				<div class="section-title">
					<!--<span>Workflow</span>-->
					<h2 style="font-family: Playfair Display,serif; font-size: 60px;">WORKFLOW</h2>
				</div>
				
				<div class="row how-it-work-top">
					<div class="col-lg-3 col-sm-6 hiw-cont">
						<div class="single-feature-two">
							<i class="flaticon-send"></i>
							<h3>Upload Your Document</h3>
							<p>Upload a PDF or capture the image of your valuable paper documents.</p>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 hiw-cont">
						<div class="single-feature-two">
							<i class="flaticon-interview"></i>
							<h3>Connect to a Live Notary</h3>
							<p>We will connect you with one of our experienced notaries on a live session, confirming your identity face-to-face on a webcam.</p>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 hiw-cont">
						<div class="single-feature-two">
							<i class="flaticon-quality"></i>
							<h3>eSign and Get eNotary Seal</h3>
							<p>You can review and sign the document(s) on any device in just a few minutes. Your eSignatures are secure, certified and 100% legal with eNotary seal.</p>
							</a>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 hiw-cont">
						<div class="single-feature-two">
							<i class="flaticon-checked"></i>
							<h3>Instantly Download or Email</h3>
							<p>Once the online notarization is complete, the sealed document is available for download in your User Dashboard.</p>
							</a>
						</div>
					</div>
				</div>
				
				<div class="how-it-work-video">
					<div class="video-cont">
						<iframe width="560" height="315" src="https://www.youtube.com/embed/ychbLJSddZ0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
				
			</div>			
		</div>
	</section>
	<!-- End Opportunity Area -->
	<section class="how-it-work-content-div">
		<div class="container">		
			<div class="how-it-work-content-wrap">
				<div class="how-it-work-content">
					<div class="how-it-work-content-title">
						<span>How Works</span>
						<h2>How AimNotary Works</h2>
						<h3>Get your forms and documents notarized in just a few simple steps.</h3>
						<ol>
							<li>
								<strong>Disclaimer:</strong> Notarize is not a law firm or a substitute for an attorney or law firm. We and our notaries are not licensed to provide and do not provide any legal advice, and we do not accept any legal fees. While we provide general information throughout the website, we cannot provide any kind of advice, opinion, or recommendation about your specific legal situation, including whether any particular document may be electronically signed or whether an electronically signed or notarized document will be valid for your intended use or accepted by your intended recipient. If you have any questions at all about validity or acceptance of your document by the intended recipient, please check with them first. We encourage all our users to seek legal advice for any questions about your documents or the transactions they relate to. Please review our Terms of Use and Privacy Policy, which govern your use of our app, services and site.
							</li>
							<li>
								<strong>Upload a Document:</strong> Notarize any document by uploading it to your computer, iPhone, or Android phone. You can access documents from your email, by taking a picture on your phone, or through cloud storage services like Dropbox.
							</li>
							<li>
								<strong>Prove Your Identity:</strong> Notarize uses a patent-pending forensic analysis to verify government issued photo IDs and passports. Take a picture of your government issued ID, answer a few questions, and Notarize will confirm your identity in seconds.
							</li>
							<li>
								<strong>Connect with a Live Notary Agent:</strong> Connect with a licensed electronic notary public over live video to sign your document. The Notarize agent will confirm your identity, witness your signature and assist you throughout the process.
							</li>
							<li>
								<strong>Save and share your notarized documents:</strong> Now you can download or share your notarized document. Completed document will be stored in your safe and secure Notarize account, if you ever need it in the future.
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>		
	</section>
@endsection