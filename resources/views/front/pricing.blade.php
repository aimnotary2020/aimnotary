@extends('front.layouts.app')
  @section('content')
	<!-- Start Page Title Area -->
	<div class="page-title-area bg-18">
		<div class="container">
			<div class="page-title-content">
				<h2>Solutions For Online Notarization</h2>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li>Online Notarization Pricing</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Page Title Area -->
	<section class="inner-page-container pt-50 pb-70 pricing-area pricing-page">
		<div class="container">
			<div class="tab">
				<div class="tab_content">
					<div class="tabs_item">
						<div class="row">
							
							<div class="col-lg-3">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">$25</span>
											<span class="pric-title">One Document</span>
										</div>
									</div>
									<ul>
										<li><i class="flaticon-checked"></i> Includes one notarization with seal</li>
										
										<li><i class="flaticon-checked"></i> $5 per additional signers or stamps</li>
										<li><i class="flaticon-checked"></i> Video meeting with notary</li>
										<li><i class="flaticon-checked"></i> Track your signature progress</li>
										<li><i class="flaticon-checked"></i> Get real time notifications</li>
										<li><i class="flaticon-checked"></i> See full transaction details</li>
									</ul>
									<!-- <a class="default-btn two" href="{{ url('payment').'/25' }}">Get Started</a> -->
									<a class="default-btn two" href="{{ url('sign-up?mode=sign-up&template=default&xss-filter=true&auth='.md5(rand(11,9999)).'&mobile=false&arcitecture=FUSS_AR&pr='.base64_encode('25')).'&doc=1' }}">Get Started</a>
								</div>
							</div>	

							<div class="col-lg-3">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">$79</span>
											<span class="pric-title">One Document (Outside USA)</span>
										</div>
									</div>
									<ul>
										<li><i class="flaticon-checked"></i> Includes one notarization with seal</li>
										
										<li><i class="flaticon-checked"></i> $5 per additional signers or stamps</li>
										<li><i class="flaticon-checked"></i> Video meeting with notary</li>
										<li><i class="flaticon-checked"></i> Track your signature progress</li>
										<li><i class="flaticon-checked"></i> Get real time notifications</li>
										<li><i class="flaticon-checked"></i> See full transaction details</li>
									</ul>
									<a class="default-btn two" href="{{ url('sign-up?mode=sign-up&template=default&xss-filter=true&auth='.md5(rand(11,9999)).'&mobile=false&arcitecture=FUSS_AR&pr='.base64_encode('79')).'&doc=1' }}">Get Started</a>
								</div>
							</div>				
							
							<div class="col-lg-3">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">$99 for Five</span>
											<span class="pric-title">Business Pro</span>
										</div>
									</div>
									<ul>
										<li><i class="flaticon-checked"></i> Includes five notarizations good for one year</li>
										<li><i class="flaticon-checked"></i> $5 per additional signers or stamps</li>
										<li><i class="flaticon-checked"></i> Video meeting with notary</li>
										<li><i class="flaticon-checked"></i> Track your signature progress</li>
										<li><i class="flaticon-checked"></i> Get real time notifications</li>
										<li><i class="flaticon-checked"></i> See full transaction details</li>
										<li><i class="flaticon-checked"></i> Email and chat support for your signers</li>
									</ul>
									<!-- <a class="default-btn two" href="{{ url('payment').'/99' }}">
										Get Started
									</a> -->
									<a class="default-btn two" href="{{ url('sign-up?mode=sign-up&template=default&xss-filter=true&auth='.md5(rand(11,9999)).'&mobile=false&arcitecture=FUSS_AR&pr='.base64_encode('99')).'&doc=5' }}">Get Started</a>
									<strong class="popular">Popular</strong>
								</div>
							</div>
							
							<div class="col-lg-3">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">Small/Medium Business or Enterprise</span>
											<span class="pric-title">5 or More Documents</span>
										</div>
									</div>
									<ul>
										<li><i class="flaticon-checked"></i> Small Businesses or Enterprises notarize 5 or more documents per month</li>
										<li><i class="flaticon-checked"></i> Includes personalized options for your business</li>
										<li><i class="flaticon-checked"></i> Video meeting with Notary</li>
										<li><i class="flaticon-checked"></i> Get real time notifications</li>
										<li><i class="flaticon-checked"></i> See full transaction details</li>
										<li><i class="flaticon-checked"></i> Email and chat support for your signers</li>
									</ul>
									<a class="default-btn two" href="{{ url('enterprise-form') }}">Get a Proposal</a>
								</div>
							</div>
							
						</div>
					</div>
					<div class="tabs_item">
						<div class="row">
							<div class="col-lg-4">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">$49</span>
											<span class="pric-title">Basic</span>
										</div>
									</div>
									<ul>
										<li>
											<i class="flaticon-checked"></i>
											24/7 system monitoring
										</li>
										<li>
											<i class="flaticon-checked"></i>
											Secure finance backup
										</li>
										<li>
											<i class="flaticon-checked"></i>
											<del>24/7 System monitoring</del>
										</li>
										<li>
											<i class="flaticon-checked"></i>
											Remote support system
										</li>
									</ul>
									<a class="default-btn two" href="#">
										Get Started
									</a>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">$59</span>
											<span class="pric-title">Standard</span>
										</div>
									</div>
									<ul>
										<li>
											<i class="flaticon-checked"></i>
											24/7 system monitoring
										</li>
										<li>
											<i class="flaticon-checked"></i>
											Secure finance backup
										</li>
										<li>
											<i class="flaticon-checked"></i>
											<del>24/7 System monitoring</del>
										</li>
										<li>
											<i class="flaticon-checked"></i>
											Remote support system
										</li>
									</ul>
									<a class="default-btn two" href="#">
										Get Started
									</a>
									<strong class="popular">Popular</strong>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="single-pricing">
									<div class="pricing-wrap">
										<div class="pric-wrap">
											<span class="pric-1">$79</span>
											<span class="pric-title">Extended</span>
										</div>
									</div>
									<ul>
										<li>
											<i class="flaticon-checked"></i>
											24/7 system monitoring
										</li>
										<li>
											<i class="flaticon-checked"></i>
											Secure finance backup
										</li>
										<li>
											<i class="flaticon-checked"></i>
											24/7 System monitoring
										</li>
										<li>
											<i class="flaticon-checked"></i>
											Remote support system
										</li>
									</ul>
									<a class="default-btn two" href="{{ url('payment').'/99' }}">
										Get Started
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection