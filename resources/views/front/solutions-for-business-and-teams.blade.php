@extends('front.layouts.app')
@section('content')
	<!-- Start Page Title Area -->
	<div class="page-title-area bg-18">
		<div class="container">
			<div class="page-title-content">
				<h2>Solutions For Business and Teams</h2>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li>Solutions For Business and Teams</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Page Title Area -->
	<!-- Start Opportunity Area -->
	<section class="inner-page-container pt-50 pb-100 solutions-for-business-and-teams">
		<div class="opportunity-area">
			<div class="container">
				<div class="section-title">
					<span>STREAMLINE WORK</span>
					<h2>Hundreds of industries are powered by AimNotary</h2>
				</div>
				<div class="row">
					<div class="col-lg-4 col-sm-6">
						<div class="single-opportunity">
							<img src="{{asset('public/front_assets/img/business-and-teams-img1.jpg')}}" alt="Image">
							<div class="opportunity-content">
								<h3>Law Firms</h3>
								<p>Collect AimNotaryd documents from one place, improving the way your firm or business operates.</p>
								<a href="#">
									<i class="flaticon-right"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="single-opportunity">
							<img src="{{asset('public/front_assets/img/business-and-teams-img2.jpg')}}" alt="Image">
							<div class="opportunity-content">
								<h3>Private Wealth Management</h3>
								<p>Get powers of attorney, beneficiary designations, IRA rollovers, and other critical forms AimNotaryd in minutes.</p>
								<a href="#">
									<i class="flaticon-right"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="single-opportunity">
							<img src="{{asset('public/front_assets/img/business-and-teams-img3.jpg')}}" alt="Image">
							<div class="opportunity-content">
								<h3>Auto Insurance</h3>
								<p>Streamline your claims process and reduce salvage cycle time by more than 80 percent.</p>
								<a href="#">
									<i class="flaticon-right"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="single-opportunity">
							<img src="{{asset('public/front_assets/img/business-and-teams-img4.jpg')}}" alt="Image">
							<div class="opportunity-content">
								<h3>Commercial Construction</h3>
								<p>Manage contractor payments and run projects smoothly from the jobsite or office.</p>
								<a href="#">
									<i class="flaticon-right"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="single-opportunity">
							<img src="{{asset('public/front_assets/img/business-and-teams-img5.jpg')}}" alt="Image">
							<div class="opportunity-content">
								<h3>Real Estate</h3>
								<p>Home buyers can sign and AimNotary documents online, delivering faster, more secure transactions for lenders and title agents.</p>
								<a href="#">
									<i class="flaticon-right"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-sm-6">
						<div class="single-opportunity">
							<img src="{{asset('public/front_assets/img/business-and-teams-img6.jpg')}}" alt="Image">
							<div class="opportunity-content">
								<h3>Financial Services</h3>
								<p>AimNotary’s bank-grade security and encrypted audit trail deliver a secure, seamless document management platform.</p>
								<a href="#">
									<i class="flaticon-right"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Opportunity Area -->
	<section class="how-it-work-content-div">
		<div class="container">		
			<div class="how-it-work-content-wrap">
				<div class="how-it-work-content">
					<div class="how-it-work-content-title">
						<span>How Works</span>
						<h2>How Aimnotary Works</h2>
						<h3>Get your forms and documents Aimnotaryd in just a few simple steps.</h3>
						<ol>
							<li>
								<strong>Upload a Document:</strong> Aimnotary any document by uploading it to your computer, iPhone, or Android phone. You can access documents from your email, by taking a picture on your phone, or through cloud storage services like Dropbox.
							</li>
							<li>
								<strong>Prove Your Identity:</strong> Aimnotary uses a patent-pending forensic analysis to verify government issued photo IDs and passports. Take a picture of your government issued ID, answer a few questions, and Aimnotary will confirm your identity in seconds.
							</li>
							<li>
								<strong>Connect with a Live Notary Agent:</strong> Connect with a licensed electronic notary public over live video to sign your document. The Aimnotary agent will confirm your identity, witness your signature and assist you throughout the process.
							</li>
							<li>
								<strong>Save and share your Aimnotaryd documents:</strong> Now you can download or share your Aimnotaryd document. Completed document will be stored in your safe and secure Aimnotary account, if you ever need it in the future.
							</li>
						</ol>
					</div>
				</div>
			</div>
		</div>		
	</section>
@endsection