@extends('front.layouts.app')
  @section('content')
	<!-- Start Page Title Area -->
	<div class="page-title-area bg-18">
		<div class="container">
			<div class="page-title-content">
				<h2>FAQ</h2>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li>FAQ</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- End Page Title Area -->
	<!-- Start FAQ Area -->
	<section class="inner-page-container pt-50 pb-100 faq-area faq-page">
		<div class="container-fluid">
			<div class="faq-accordion">
				<h2>Frequently Asked Questions</h2>
				<ul class="accordion">
					<!--@if(!$detail_list->isEmpty())
					    @foreach($detail_list as $key=>$eachCms)
							<li class="accordion-item">
								<a class="accordion-title @if($key=='0') {{ 'active' }} @endif" href="javascript:void(0)">
									<i class="flaticon-plus"></i>
									{{ $eachCms->title }}
								</a>
								<p class="accordion-content @if($key=='0') {{ 'show' }} @endif">
									{!! $eachCms->description !!}
								</p>
							</li>
						@endforeach
					@endif-->
					<li class="accordion-item">
						<a class="accordion-title active" href="javascript:void(0)">
							<i class="flaticon-plus"></i>
							What is an online notary?
						</a>
						<p class="accordion-content show">An online notary is a notary public who is authorized to use audio/visual technology to complete a notarial act when the principal is not in the same physical location as the notary public.</p>
					</li>
					<li class="accordion-item">
						<a class="accordion-title" href="javascript:void(0)">
							<i class="flaticon-plus"></i>
							Is there a difference between a mobile notary and a remote notary?
						</a>
						<p class="accordion-content">Yes. A mobile notary public travels to a person’s physical location for notarizations. A remote notary public performs the notarization entirely over a two-way audio/video stream. All aspects of the notarization are handled electronically.</p>
					</li>
					<li class="accordion-item">
						<a class="accordion-title" href="javascript:void(0)">
							<i class="flaticon-plus"></i>
							What is online notarization?
						</a>
						<p class="accordion-content">An online notarization is a notarial act performed 100% online between participants and a notary public. The document is signed online by the participant. The notary public signs and places their seal on the document electronically.</p>
					</li>
					<li class="accordion-item">
						<a class="accordion-title" href="javascript:void(0)">
							<i class="flaticon-plus"></i>
							What do I need to notarize a document online?
						</a>
						<p class="accordion-content">You will need a device with internet access (cell phone, computer/laptop, tablet), a microphone or headset, camera, your id, and an electronic copy of the document you wish to notarize.</p>
					</li>
					<li class="accordion-item">
						<a class="accordion-title" href="javascript:void(0)">
							<i class="flaticon-plus"></i>
							If Aim Notary is unable to notarize my document, will I be charged?
						</a>
						<p class="accordion-content">No. If an Aim Notary is unable to successfully notarize your document, then the session will be ended with no charge to you.</p>
					</li>
					<li class="accordion-item">
						<a class="accordion-title" href="javascript:void(0)">
							<i class="flaticon-plus"></i>
							What categories of documents can be notarized online?
						</a>
						<p class="accordion-content">You are able to authenticate a variety of documents, i.e. letters, forms and certificates. However, certain categories of documents such as wills, codicils, I-9 forms, etc., cannot be notarized clectronically due to legal reasons.</p>
					</li>					
				</ul>
			</div>
		</div>
	</section>
	<!-- End FAQ Area -->
	@endsection