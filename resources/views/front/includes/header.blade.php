<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php
    if(isset($meta_details) && !empty($meta_details)){
        ?>
            <title><?php echo $meta_details->title ?></title>
            <meta name="description" content="<?php echo $meta_details->description ?>">
            <meta name="keywords" content="<?php echo $meta_details->keyword ?>">
            <?php echo $meta_details->extratag ?>
        <?php
    }
?>

<?php $actual_link_canonical = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

<link rel="canonical" href="<?=$actual_link_canonical;?>" />


<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('/public/assets/plugins/toastr/toastr.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/boxicons.min.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/flaticon.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/meanmenu.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/nice-select.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/progressbar.min.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/odometer.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/style.css')}}">
<link rel="stylesheet" href="{{asset('public/front_assets/css/responsive.css')}}">
<link rel="icon" type="image/png" href="{{asset('public/front_assets/img/favicon.png')}}">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<style>
@media (max-height:479px) {
    @supports (-webkit-touch-callout: none) {
        .chat_icon,
        .chat_window {
            display:none!important;
        }
    }
}
@media (max-height:479px) {
    body .chat_window {
        max-height: 95vh!important;
        max-width: 60vw!important;
    }
    body .chat_header {
        margin-top: 11.5vh !important;
        margin-left: 4.5vw !important;
        width: 51.5vw !important;
    }
    body .chat_header img {
        right: 6vw !important;
        top: 13vh !important;
    }
    body .chat_body, .chat_body1 {
        margin-left: 4.5vw!important;
        width: 51.5vw!important;
        height: 55vh!important;
    }
    body .chat_bottom, .chat_bottom1 {
        margin-left: 4.5vw!important;
        width: 51.5vw!important;
    }
}
@media screen and (max-width:374px) {
    .chat_window {
        width: 236px!important;
        height: 480px!important;
        background-size: 100% auto!important;
    }
    .chat_header {
        margin-top: 60px!important;
        margin-left: 18px!important;
        width: 202px!important;
    }
    .chat_header img {
        right: 25px!important;
        top: 66px!important;
    }
    .chat_body, .chat_body1 {
        margin-left: 18px!important;
        width: 202px!important;
        height: 238px!important;
    }
    .chat_body11 {
        margin: auto 26px!important;
        width: 186px!important;
        height: 250px!important;
    }
    .chat_wbody1 .chat_body11 {
        top: 125px!important;
    }
    .chat_bottom, .chat_bottom1 {
        margin-left: 18px!important;
        width: 202px!important;
    }
}
@media (width:375px) and (height:553px) {
    body .chat_window {
        width: 260px !important;
        height: 530px !important;
        background-size: 100% auto !important;
    }
    body .chat_header {
        margin-top: 68px !important;
        margin-left: 20px !important;
        width: 222px !important;
    }
    body .chat_header img {
        right: 37px !important;
        top: 74px !important;
    }
    body .chat_body, .chat_body1 {
        margin-left: 20px !important;
        width: 222px !important;
        height: 314px !important;
    }
    body .chat_body11 {
        margin: auto 29px!important;
        width: 204px!important;
        height: 250px!important;
    }
    body .chat_wbody1 .chat_body11 {
        top: 204px!important;
    }
    body .chat_bottom, .chat_bottom1 {
        margin-left: 20px!important;
        width: 222px!important;
    }
}
@media (width:375px) and (height:812px) {
    body .chat_window {
        width: 350px !important;
        height: 710px !important;
        background-size: 100% auto !important;
    }
    body .chat_header {
        margin-top: 88px !important;
        margin-left: 27px !important;
        width: 298px !important;
    }
    body .chat_header img {
        right: 40px !important;
        top: 92px !important;
    }
    body .chat_body, .chat_body1 {
        margin-left: 27px !important;
        width: 298px !important;
        height: 410px !important;
    }
    body .chat_body11 {
        margin: auto 39px !important;
        width: 274px !important;
        height: 220px !important;
    }
    body .chat_wbody1 .chat_body11 {
        top: 274px!important;
    }
    body .chat_bottom, .chat_bottom1 {
        margin-left: 27px!important;
        width: 298px!important;
    }
}
@media (min-width:375px) and (max-width:413px) {
    .chat_window {
        width: 315px !important;
        height: 640px !important;
        background-size: 100% auto!important;
    }
    .chat_header {
        margin-top: 81px !important;
        margin-left: 24px !important;
        width: 269px !important;
    }
    .chat_header img {
        right: 37px !important;
        top: 85px !important;
    }
    .chat_body, .chat_body1 {
        margin-left: 24px !important;
        width: 269px !important;
        height: 356px !important;
    }
    .chat_body11 {
        margin: auto 35px !important;
        width: 247px !important;
        height: 225px !important;
    }
    .chat_wbody1 .chat_body11 {
        top: 247px!important;
    }
    .chat_bottom, .chat_bottom1 {
        margin-left: 24px!important;
        width: 269px!important;
    }
}
@media (min-width:414px) and (max-width:767px) {
    .chat_window {
        width: 345px !important;
        height: 700px !important;
        background-size: 100% auto !important;
    }
    .chat_header {
        margin-top: 87px !important;
        margin-left: 27px !important;
        width: 294px !important;
    }
    .chat_header img {
        right: 39px !important;
        top: 92px !important;
    }
    .chat_body, .chat_body1 {
        margin-left: 27px !important;
        width: 294px !important;
        height: 402px !important;
    }
    .chat_body11 {
        margin: auto 39px !important;
        width: 270px !important;
        height: 220px !important;
    }
    .chat_wbody1 .chat_body11 {
        top: 270px!important;
    }
    .chat_bottom, .chat_bottom1 {
        margin-left: 27px!important;
        width: 294px!important;
    }
}
.chat_window {
    width: 300px;
    height: 610px;
    background: url(https://www.aimnotary.com/public/front_assets/images/mobile_bg_1.png) no-repeat center bottom/auto 100%;
    position: fixed;
    border-radius: 10px 10px 0px 0px;
    z-index: 99999999;
    right: 10px;
    /* top: calc(100vh - 610px); */
    bottom: 0;
    max-height: 100%;
}
.chat_icon {
    /* background-color: #7e0460;
    border-radius: 35px; */
    width: 210px;
    height: 70px;
    position: fixed;
    z-index: 100;
    right: 10px;
    cursor: pointer;
    /*top: calc(100vh - 70px);*/
    bottom: 5px;
}
.chat_icon img {
    border-radius: 35px;
}
.chat_icon a,
.chat_icon img,
.chat_icon a img {
    display: inline-block;
}
/* .chat_icon {
  animation-duration: 1.7s;
  animation-iteration-count: infinite;
  animation-name: shine;
  animation-direction: forwards;
  background-image: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.25), rgba(255, 255, 255, 0) );
  background-position: -500px 0;
  background-repeat: no-repeat;
}
@keyframes shine {
  100% {
    background-position: 500px 0;
  }
} */
.chat_header {
    margin-top: 77px;
    margin-left: 24px;
    width: 255px;
    height: 80px;
}
.chat_window.offline-form .chat_header {
    height: 40px;
}
.chat_header .chat_header_top {
    background-color: #4424a8;
    height: 40px;
}
.chat_header h3 {
    text-align: center;
    padding-top: 13px;
    color: #ffffff;
    font-size: 13px;
}
.chat_header img {
    position: absolute;
    margin-top: 6px;
    right: 30px;
    top: 82px;
    cursor: pointer;
}
.chat_header .chat_header_bottom {
    /* background-color: #25D366; */
    background-image: linear-gradient(#60d36b, #26b43d);
    height: 40px;
}
.chat_window.offline-form .chat_header_bottom {
    display: none;
}
.chat_header .chat_header_bottom h4 {
    font-size: 14px;
    text-align: center;
    color: #fff;
    line-height: 40px;
    font-weight: normal;
}
.chat_body, .chat_body1 {
    clear: both;
    margin-left: 24px;
    background: rgba(221, 221, 221, 0.58);
    padding: 0 5px 5px 5px;
    width: 255px;
    height: 333px;
    overflow: auto;
}
.chat_body11 {
    position: absolute;
    background: rgba(221, 221, 221, 0.58);
    padding: 0 5px 5px 5px;
    top: 0;
    margin: auto 34px;
    width: 235px;
    height: 230px;
    background-color: #cccccc;
    overflow: auto;
    z-index: 100000000;
}
.chat_wbody1 .chat_body11 {
    top: 235px;
}
/*.chat_body_overlay {*/
/*    position: absolute;*/
/*    top:0px;*/
/*    left:0px;*/
/*    background-color: rgba(0, 0, 0, 0.6);*/
/*    width: 100%;*/
/*    height: 100%;*/
/*    z-index: 1000000;*/
/*}*/
.chat_window.offline-form .chat_body1  {
    height: 373px;
}
.chat_body1 h4 {
    color: #000000!important;
    font-size: 14px!important;
    margin-top: 10px!important;
    margin-left: 5px!important;
    margin-right: 5px!important;
    margin-bottom: 5px!important;
}
.chat_body1 .form-group {
    color: #000000!important;
    font-size: 14px!important;
    margin-bottom: 5px!important;
}
.chat_bottom, .chat_bottom1 {
    clear: both;
    margin-left: 24px;
    width: 255px;
    height: 40px;
    border-top: solid 1px #cccccc;
    overflow: hidden;
}
.chat_text_input {
    float: left;
    padding: 10px;
    width: 80%;
    font-size: 14px;
    height: 40px;
    border: none;
}
.chat_body1 .chat_text_input1 {
    clear: both;
    padding: 5px!important;
    width: 100%!important;
    font-size: 14px!important;
    border: none!important;
    border-radius: 0!important;
    margin: 0!important;
}
.chat_submit_button {
    float: left;
    width: 20%;
    height: 40px;
    text-align: center;
    border: none;
    cursor: pointer;
}
.chat_submit_button i{
    text-align: center;
}

.chat_submit_button1 {
    width: 100%;
    height: 40px;
    text-align: center;
    background-color: #003f86;
    color: #ffffff;
    border: none;
    cursor: pointer;
}

.cnschat{
	clear: both; float: left; padding: 10px 20px; background: #a4c4ea; margin: 3px 30px 0 0; border-radius: 5px !important; color: #000000;
}
.usrchat{
	clear: both; float: right; padding: 10px 20px; background: #98d48c; margin: 3px 0 0 30px; border-radius: 5px !important; color: #000000;
}

.connecting_image {
    clear: both;
    text-align: center;
    padding: 5px 0;
}
</style>

<link rel="stylesheet" href="{{asset('public/front_assets/css/videostyle.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css"/>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>div#preload{margin:auto;position:fixed;width:100%;height:100%;background-color:#000000b8;z-index:9999999;display:flex;align-content:center;justify-content:center}.sk-folding-cube{top:50%;left:50%;margin-left:-25px;margin-top:-25px;width:50px;height:50px;position:absolute;-webkit-transform:rotateZ(45deg);transform:rotateZ(45deg);}.sk-folding-cube .sk-cube{float:left;width:50%;height:50%;position:relative;-webkit-transform:scale(1.1);-ms-transform:scale(1.1);transform: scale(1.1)}.sk-folding-cube .sk-cube2{-webkit-transform:scale(1.1) rotateZ(90deg);transform:scale(1.1) rotateZ(90deg)}.sk-folding-cube .sk-cube4{-webkit-transform:scale(1.1) rotateZ(270deg);transform:scale(1.1) rotateZ(270deg)}.sk-folding-cube .sk-cube3{-webkit-transform: scale(1.1) rotateZ(180deg);transform:scale(1.1) rotateZ(180deg)}.sk-folding-cube .sk-cube:before{content:'';position:absolute;top:0;left:0;width:100%;height:100%;-webkit-animation:sk-foldCubeAngle 2.4s infinite linear both;animation:sk-foldCubeAngle 2.4s infinite linear both;-webkit-transform-origin:100% 100%;-ms-transform-origin:100% 100%;transform-origin:100% 100%;background:#005dc1}.sk-folding-cube .sk-cube2:before{-webkit-animation-delay:.3s;animation-delay:.3s}.sk-folding-cube .sk-cube4:before{-webkit-animation-delay: .9s;animation-delay: .9s}.sk-folding-cube .sk-cube3:before{-webkit-animation-delay:.6s;animation-delay:.6s}@keyframes sk-foldCubeAngle{0%,10%{-webkit-transform:perspective(140px) rotateX(-180deg);transform:perspective(140px) rotateX(-180deg);opacity:0}25%,75%{-webkit-transform:perspective(140px) rotateX(0);transform:perspective(140px) rotateX(0);opacity:1}100%,90%{-webkit-transform:perspective(140px) rotateY(180deg);transform:perspective(140px) rotateY(180deg);opacity:0}}.drop_cus{background-color:#fff!important;color: #acacac!important;top:0px!important}.drop_cus a{font-size:13px!important;color:#acacac!important;}</style>
<!--Global-site-tag (gtag.js)-Google-Analytics--><script async src="https://www.googletagmanager.com/gtag/js?id=G-RRBSJBREBS"></script><script>  window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'G-RRBSJBREBS');</script></head>
<body class="downapp" onload="myFunction()">
   <div id="preload" style="display: none;">
      <div class="sk-folding-cube">
         <div class="sk-cube1 sk-cube"></div>
         <div class="sk-cube2 sk-cube"></div>
         <div class="sk-cube4 sk-cube"></div>
         <div class="sk-cube3 sk-cube"></div>
      </div>
   </div>
   <div class="preloader">
      <div class="lds-ripple">
         <div></div>
         <div></div>
      </div>
   </div>
   <header class="header-area">
      <div class="top-header-area">
         <div class="container">
            <div class="row align-items-center">
               <div class="col-lg-6 col-md-6">
                  <ul class="header-content-left">
                     <li><a href="tel:@if(isset($company_info['c_contact_no'])) {{ $company_info['c_contact_no'] }} @endif"><i class="flaticon-call"></i>@if(isset($company_info['c_contact_no'])) {{ $company_info['c_contact_no'] }} @endif</a></li>
                     <li><a href="mailto:info@aimnotary.com"><i class="flaticon-email"></i> @if(isset($company_info['email_id'])) {{ $company_info['email_id'] }} @endif</a></li>
                  </ul>
               </div>
               <div class="col-lg-6 col-md-6">
                  <div class="header-content-right">
                     <div class="social-icon">
                        <ul>
                           <li><a href="@if(isset($company_info['facebook'])) {{ $company_info['facebook'] }} @endif" target="_blank"><i class="bx bxl-facebook"></i></a></li>
                           <li><a href="@if(isset($company_info['twitter'])) {{ $company_info['twitter'] }} @endif" target="_blank"><i class="bx bxl-twitter"></i></a></li>
                           <li><a href="@if(isset($company_info['linkedin'])) {{ $company_info['linkedin'] }} @endif" target="_blank"><i class="bx bxl-linkedin"></i></a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="contolib-nav-style">
         <div class="navbar-area">
            <div class="mobile-nav"><a href="{{ url('/') }}" class="logo"><img src="{{asset('public/front_assets/img/logo.png')}}" srcset="{{asset('public/front_assets/img/logo-991w.png')}} 991w, {{asset('public/front_assets/img/logo-767w.png')}} 767w, {{asset('public/front_assets/img/logo-599w.png')}} 599w, {{asset('public/front_assets/img/logo-479w.png')}} 479w, {{asset('public/front_assets/img/logo-359w.png')}} 359w" alt="Aim Notary"></a></div>
            <div class="main-nav">
               <nav class="navbar navbar-expand-md navbar-light">
                  <div class="container">
                     <a class="navbar-brand" href="{{ url('/') }}"><img src="{{asset('public/front_assets/img/logo.png')}}" alt="Aim Notary"></a>
                     <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                           <li class="nav-item"><a href="{{ url('/') }}" class="nav-link">Home</a></li>
                           <li class="nav-item"><a href="https://www.aimnotary.com/about" class="nav-link">About</a></li>
                           <li class="nav-item"><a href="https://www.aimnotary.com/how-it-works" class="nav-link">How it Works</a></li>
                           <li class="nav-item"><a href="{{ url('pricing') }}" class="nav-link">Pricing</a></li>
                           <li class="nav-item"><a href="{{ url('blog') }}" class="nav-link">Blogs</a></li>
                           <li class="nav-item">
                               <a href="javascript:void(0);" class="nav-link dropdown-toggle">State Information <i class="bx bx-plus"></i></a>
                               <ul class="dropdown-menu">
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-alabama') }}" class="nav-link">Online Notary Alabama</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-alaska') }}" class="nav-link">Online Notary Alaska</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-arizona') }}" class="nav-link">Online Notary Arizona</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-arkansas') }}" class="nav-link">Online Notary Arkansas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-colorado') }}" class="nav-link">Online Notary Colorado</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-florida') }}" class="nav-link">Online Notary Florida</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-hawaii') }}" class="nav-link">Online Notary Hawaii</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-illinois') }}" class="nav-link">Online Notary Illinois</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-indiana') }}" class="nav-link">Online Notary Indiana</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-iowa') }}" class="nav-link">Online Notary Iowa</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-kansas') }}" class="nav-link">Online Notary Kansas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-kentucky') }}" class="nav-link">Online Notary Kentucky</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-tennessee') }}" class="nav-link">Online Notary Tennessee</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ url('online-notary-texas') }}" class="nav-link">Online Notary Texas</a>
                                    </li>
                               </ul>
                           </li>
                           <li class="nav-item"><a href="{{ url('contact') }}" class="nav-link">Contact</a></li>
                        </ul>
                        <div class="others-option navbar-nav">
                           <div class="dashboard nav-item">
                              @if(isset(Auth::user()->id) && Auth::user()->user_type== 'User')<a class="nav-link dropdown-toggle" href="javascript:void(0);">My Account <i class="bx bx-plus"></i></a>
                              <ul class="dropdown-menu">
                                 <li class="nav-item"><a href="{{ url('dashboard') }}" class="nav-link">My Documents</a></li>
                                 <li class="nav-item"><a href="{{ url('settings') }}" class="nav-link">Account Settings</a></li>
                              </ul>
                              @endif
                           </div>
                           <div class="subscribe">@if(isset(Auth::user()->id) && Auth::user()->user_type== 'User')<a href="{{ url('logout') }}">Logout</a>@else<a href="{{ url('login') }}">Sign In/Sign Up</a>@endif</div>
                        </div>
                     </div>
                  </div>
               </nav>
            </div>
         </div>
      </div>
   </header>
