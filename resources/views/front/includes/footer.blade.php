<?php
$date = new DateTime("now", new DateTimeZone('America/New_York'));
$hr = (int) $date->format('H');
$day = $date->format('w');

if ($hr >= 9 && $hr <= 18 && ($day > 0 && $day < 6 )) {
// if ($hr >= 0 && $hr <= 21 && ($day > 0 && $day < 6 )) {
    if (isset($_COOKIE['display_chat'])) {
        if ($_COOKIE['display_chat']=='open') {
            $display1 = 'none';
            $display2 = 'block';
        } else {
            $display1 = 'block';
            $display2 = 'none';
        }
    } else {
        $display1 = 'block';
        $display2 = 'none';
    }
} else {
    $display1 = 'none';
    $display2 = 'none';
}

//echo 'Cookie value : '. $_COOKIE["ipauserv"];

// $display1 = 'none';
// $display2 = 'none';
?>
<div class="chat_icon" style="display: <?php echo $display1; ?>;">
    <img class="openchat" src="https://www.aimnotary.com/public/front_assets/images/mobile-icon.gif" width="210px" height="70px" />
</div>
<?php
if(isset($_COOKIE["ipauserv"])) {
?>
<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $_COOKIE['ipauserv']; ?>" />
<?php
}
/*
} else {
?>
<input type="hidden" name="visit_id" id="visit_id" value="<?php echo $_SESSION['visit_id']; ?>" />
<?php
}
*/
?>
<input type="hidden" name="receive_chat" id="receive_chat" value="1" />
<?php
if (isset($_COOKIE['display_form'])) {
    if ($_COOKIE['display_form'] == 'open') {
?>
<input type="hidden" name="wait_chat" id="wait_chat" value="1" />
<?php
    } else {
?>
<input type="hidden" name="wait_chat" id="wait_chat" value="2" />
<?php
    }
} else {
?>
<input type="hidden" name="wait_chat" id="wait_chat" value="1" />
<?php
}
?>
<div class="chat_window" style="display: <?php echo $display2; ?>;">
    <div class="chat_header">
        <div class="chat_header_top">
            <h3>Aim Notary Chat</h3>
            <img class="closechat" src="https://www.aimnotary.com/public/front_assets/images/cancel_1.png" width="16px" height="16px" />
        </div>
        <div class="chat_header_bottom">
            <h4>We're live and ready to chat</h4>
        </div>
    </div>
    <div id="ch-loaddata" class="chat_body"></div>
    <!--<div id="ch-loaddata1" class="chat_body1" style="display: none;">-->
    <!--    <div class="form-group"><h4>Sorry, I am with another customer. Can you leave me your number and I will call you once I am finished here.</h4></div>-->
        <!--<div class="form-group">-->
        <!--    <label>Name</label>-->
        <!--    <input type="text" name="arg_name" id="arg_name" class="chat_text_input1" required="required">-->
        <!--</div>-->
        <!--<div class="form-group">-->
        <!--    <label>Email</label>-->
        <!--    <input type="email" name="arg_email" id="arg_email" class="chat_text_input1" required="required">-->
        <!--</div>-->
        <!--<div class="form-group">-->
        <!--    <label>Phone No.</label>-->
        <!--    <input type="text" name="arg_contact_no" id="arg_contact_no" class="chat_text_input1" required="required" maxlength="10">-->
        <!--</div>-->
        <!--<div class="form-group">-->
        <!--    <label>Message</label>-->
        <!--    <textarea name="arg_message1" id="arg_message1" class="chat_text_input1"></textarea>-->
        <!--</div>-->
    <!--</div>-->
    <div class="chat_bottom">
        <input type="text" name="message_chat" id="message_chat" class="chat_text_input" />
        <Button class="chat_submit_button sbt_btn"><i class="fa fa-send"></i></Button>
    </div>
    <!--<div class="chat_bottom1" style="display: none;">-->
    <!--    <Button class="chat_submit_button1 sbt_btn1"><i class="fa fa-send"></i> Submit</Button>-->
    <!--</div>-->
</div>

<div class="down-app-cont-for-mobile">
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close down-app-close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <img class="mobile-logo-icon" src="{{asset('https://www.aimnotary.com/public/front_assets/img/logo-icon.png')}}" alt="AimNotary" />
        <div class="down-app-text">
            <h2>Aim Notary</h2>
            <p>Legally Notarize Your Documents Online</p>
            <div class="star-rating">
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="fa fa-star checked"></span>
                <span class="rating-count">(1,500)</span>
            </div>
            <a class="get-app android" href="https://play.google.com/store/apps/details?id=com.aimnotary.aimnotaryclient&hl=en_IN&gl=US" target="_blank">GET IT</a>
            <a class="get-app ios" href="https://apps.apple.com/in/app/aim-notary/id1560841365" target="_blank">GET IT</a>
        </div>
    </div>
</div>

<section class="download-app-area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="section-title">
                    <h2>Notarization is now a click away</h2>
                    <p>We are also available in</p>
                </div>
                <div class="download-app-btn-cont">
                    <a class="download-app-btn" href="https://play.google.com/store/apps/details?id=com.aimnotary.aimnotaryclient&hl=en_IN&gl=US" target="_blank">
                        <img src="{{asset('public/front_assets/img/download-now-button.png')}}" alt="Download App" />
                    </a>
                    <a class="download-app-btn" href="https://apps.apple.com/us/app/aim-notary/id1560841365" target="_blank">
                        <img src="{{asset('public/front_assets/img/download-now-button-ios.png')}}" alt="Download IOS App" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer-top-area pt-80 mt-minus-50@@@@ pb-70@@@@"><div class="container"><div class="row"><div class="col-lg-3 col-md-6"><div class="single-widget"><img src="{{asset('public/front_assets/img/home-one/footer-logo.png')}}" alt="Image"><p>Our goal is to make the notary process easier through our advanced and secure online system. We provide easy to use digital solutions for legal notary transactions.</p><div class="social-area"><ul><li><a href="@if(isset($company_info['facebook'])) {{ $company_info['facebook'] }} @endif" target="_blank"><i class="bx bxl-facebook"></i></a></li><li><a href="@if(isset($company_info['twitter'])) {{ $company_info['twitter'] }} @endif" target="_blank"><i class="bx bxl-twitter"></i></a></li><li><a href="@if(isset($company_info['linkedin'])) {{ $company_info['linkedin'] }} @endif" target="_blank"><i class="bx bxl-linkedin"></i></a></li></ul></div></div></div><div class="col-lg-3 col-md-6"><div class="single-widget"><h3>Contact Us</h3><ul><li><span>Address: </span>Aim Notary<br>100500 Norbotten Drive Suite 300<br>Florence KY, 41042</li><li><a href="tel: 859-392-0423"><span>Phone: </span>859-392-0423</a></li><li class="mb"><span>Hours of Operation: </span>Monday-Saturday, 9am-6pm.</li></ul></div></div><div class="col-lg-3 col-md-6"><div class="single-widget"><h3>Useful Links</h3><ul><li><a href="{{ url('/') }}"><i class="flaticon-right-chevron"></i> Home</a></li><li><a href="{{ url('how-it-works') }}"><i class="flaticon-right-chevron"></i> How It Works</a></li><li><a href="{{ url('pricing') }}"><i class="flaticon-right-chevron"></i> Pricing</a></li><li><a href="{{ url('faq') }}"><i class="flaticon-right-chevron"></i> FAQ’s</a></li></ul></div></div><div class="col-lg-3 col-md-6"><div class="single-widget"><h3>Other Links</h3><ul><li><a href="{{ url('blog') }}"><i class="flaticon-right-chevron"></i> Blogs</a></li><li><a href="{{ url('contact') }}"><i class="flaticon-right-chevron"></i> Contact</a></li><li><a href="{{ url('privacy-policy') }}"><i class="flaticon-right-chevron"></i> Privacy Policy</a></li><li><a href="{{ url('terms-and-conditions') }}"><i class="flaticon-right-chevron"></i> Terms & Conditions</a></li></ul></div></div></div></div><div class="footer-bottom-area footer-bottom-electronics-area"><div class="container"><div class="copy-right"><p>&copy;{{date('Y')}} All Rights Reserved Aim Notary</p></div></div></div></footer><div class="go-top"><i class='bx bx-chevrons-up'></i><i class='bx bx-chevrons-up'></i></div><script>function loadingfunc(displayset) { document.getElementById("preload").style.display=displayset; }</script><script src="{{asset('public/front_assets/js/jquery.min.js')}} "></script><script src="{{asset('public/front_assets/js/popper.min.js')}} "></script><script src="{{asset('public/front_assets/js/bootstrap.min.js')}} "></script><script src="{{asset('public/front_assets/js/jquery.meanmenu.js')}} "></script><script src="{{asset('public/front_assets/js/wow.min.js')}} "></script><script src="{{asset('public/front_assets/js/owl.carousel.js')}} "></script><script src="{{asset('public/front_assets/js/carousel-thumbs.js')}} "></script><script src="{{asset('public/front_assets/js/jquery.magnific-popup.min.js')}} "></script><script src="{{asset('public/front_assets/js/parallax.min.js')}} "></script><script src="{{asset('public/front_assets/js/jquery.mixitup.min.js')}} "></script><script src="{{asset('public/front_assets/js/jquery.appear.js')}} "></script><script src="{{asset('public/front_assets/js/odometer.min.js')}} "></script><script src="{{asset('public/front_assets/js/progressbar.min.js')}} "></script><script src="{{asset('public/front_assets/js/jquery.ajaxchimp.min.js')}} "></script><script src="{{asset('public/front_assets/js/form-validator.min.js')}}"></script><script src="{{asset('public/front_assets/js/contact-form-script.js')}} "></script><script src="{{asset('public/front_assets/js/chart.bundle.js')}} "></script><script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk1wUDCF1UUhvNuBwOP1kebPZhHFSeIkk&amp;callback=initMap"></script><script src="{{asset('public/front_assets/js/map.js')}} "></script><script src="{{asset('public/front_assets/js/custom.js')}} "></script><script src="{{asset('public/assets/plugins/toastr/toastr.min.js')}}"></script><script src="{{asset('public/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script><script src="{{asset('public/front_assets/js/AgoraRTCSDK-3.1.1.js')}}"></script><script src="{{asset('public/front_assets/js/screenfull.min.js')}}"></script>


<script>
    function myFunction() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        alert("Windows Phone");
    }

    if (/android/i.test(userAgent)) {
        //alert("Android");
        $(".downapp").addClass("app-android");
    }

    //iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        //alert("iOS");
        $(".downapp").addClass("app-ios");
    }

    //return "unknown";
    }
</script>
<script>
    $(".down-app-close").click(function(){
        $("body").removeClass("downapp");
    });
</script>
<?php ?>
<script>
var myVar;
var myVar1;
var myVar2;

$( document ).ready( function () {
    function abfoo() {
        $.ajax({method:'GET', url: '{{route("check_cookie")}}', data:{'chat_data':"1"}, success: function(result){
            console.log(result);
            if (result != '') {
                var vid = $("#visit_id").val();
                if (vid != result) {
                    console.log("replace");
                    document.getElementById("visit_id").value = result;
                    document.getElementById("receive_chat").value = '1';
                    document.getElementById("wait_chat").value = '1';
                }
            }
		}});
    }
    setInterval(abfoo, 2000);
});

function startTimer() {
    $("#ch-loaddata").stop().animate({ scrollTop: $("#ch-loaddata")[0].scrollHeight}, 1000);
    myVar = setInterval(function() {
        $.ajax({type:"GET", url: '{{route("get_chat_list")}}', data:{'chat_data':"1", 'visit_id': $("#visit_id").val()}, success: function(result){
            console.log(result);
            if (result != '') {
                var oldscrollHeight = $("#ch-loaddata")[0].scrollHeight - 20; //Scroll height before the request
                $(".chat_window").css("display", "block");
                $(".chat_icon").hide();
	            $("#ch-loaddata").html(result);
	    	    // var objDiv = document.getElementById("ch-loaddata");
                // objDiv.scrollTop = objDiv.scrollHeight;
                
                var newscrollHeight = $("#ch-loaddata")[0].scrollHeight - 20; //Scroll height after the request
                if(newscrollHeight > oldscrollHeight){
                    $("#ch-loaddata").animate({ scrollTop: newscrollHeight }, 'normal'); //Autoscroll to bottom of div
                }
            }
		}});
    }, 1000);
}

function starnotiTimer() {
    var r_chat = $("#receive_chat").val();
    if (r_chat == 1) {
        myVar1 = setInterval(function() {
            $.ajax({type:"GET", url: '{{route("notification_timer")}}', data:{'visit_id': $("#visit_id").val()}, success: function(result){
                console.log(result);
                if (result == 'FAIL') {
                    document.getElementById("receive_chat").value = "2";
                    clearInterval(myVar1);
                }
    		}});
        }, 15000);
    } else {
        clearInterval(myVar1);
    }
}

function startofflinemsg() {
    var w_chat = $("#wait_chat").val();
    if (w_chat == 1) {
        myVar2 = setInterval(function() {
            $.ajax({type:"GET", url: '{{route("user_reply1")}}', data:{'visit_id': $("#visit_id").val()}, success: function(result){
                console.log(result);
                if (result == 'FAIL') {
                    setCookie('display_form', 'open', 1);
                    
                    clearInterval(myVar2);
                } else {
                    document.getElementById("wait_chat").value = '2';
                    
                    setCookie('display_form', 'close', 1);
                    
                    clearInterval(myVar2);
                }
    		}});
        }, 45000);
    } else {
        setCookie('display_form', 'close', 1);
                    
        clearInterval(myVar2);
    }
}

$( document ).ready( function () {
<?php
    if (isset($_COOKIE['display_chat']) && $_COOKIE['display_chat'] == 'open') {
?>
        startTimer();
<?php
        if (isset($_COOKIE['display_form'])) {
            if ($_COOKIE['display_form'] == 'open') {
?>
                clearInterval(myVar2);
<?php
            } else {
?>
                clearInterval(myVar2);
<?php
            }
        } else {
?>
            clearInterval(myVar2);
<?php
        }
    }
?>
});

function setCookie(cname, cvalue, exhours) {
  var d = new Date();
  d.setTime(d.getTime() + (exhours*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

$(document).on('click', '.openchat', function(){
    setCookie('display_chat', 'open', 1);
    startTimer();
    // starnotiTimer();
    $.ajax({type:"GET", url: '{{route("open_chat")}}', data:{'chat_open':"1", 'visit_id': $("#visit_id").val()}, success: function(result){
       // alert(result);
        if (result != '') {
            $(".chat_icon").hide();
            $(".chat_window").show();
        }
	}});
});

$(document).on('click', '.closechat', function(){
    setCookie('display_chat', 'close', 1);
    clearInterval(myVar);
    $.ajax({type:"GET", url: '{{route("close_chat")}}', data:{'chat_close':"1", 'visit_id': $("#visit_id").val()}, success: function(result){
       // alert(result);
        if (result != '') {
            $(".chat_icon").show();
            $(".chat_window").hide();
        }
	}});
});

$(document).on('click', '.sbt_btn', function(){
    var msg = $("#message_chat").val();
    var r_chat = $("#receive_chat").val();
    var w_chat = $("#wait_chat").val();
    if(msg != '') {
        $.ajax({type:"GET", url: '{{route("send_chat")}}', data:{'send_chat':msg, 'visit_id': $("#visit_id").val()}, success: function(result){
            //alert(result);
            document.getElementById("message_chat").value = "";
			$("#ch-loaddata").html(result);
			// var objDiv = document.getElementById("ch-loaddata");
            // objDiv.scrollTop = objDiv.scrollHeight;
            $("#ch-loaddata").stop().animate({ scrollTop: $("#ch-loaddata")[0].scrollHeight}, 1000);
            if (r_chat == 1) {
                starnotiTimer();
            } else {
                clearInterval(myVar1);
            }
            if (w_chat == 1) {
                startofflinemsg();
            } else {
                clearInterval(myVar2);
            }
		}});
    } else {
        alert("Please enter text");
        $("#message_chat").focus();
    }
});

$("#message_chat").keyup(function(event) {
    if (event.keyCode === 13) {
        var msg = $("#message_chat").val();
        var r_chat = $("#receive_chat").val();
        var w_chat = $("#wait_chat").val();
	    if(msg != '') {
	        $.ajax({type:"GET", url: '{{route("send_chat")}}', data:{'send_chat':msg, 'visit_id': $("#visit_id").val()}, success: function(result){
	            //alert(result);
	            document.getElementById("message_chat").value = "";
				$("#ch-loaddata").html(result);
				// var objDiv = document.getElementById("ch-loaddata");
                // objDiv.scrollTop = objDiv.scrollHeight;
                $("#ch-loaddata").stop().animate({ scrollTop: $("#ch-loaddata")[0].scrollHeight}, 1000);
                if (r_chat == 1) {
                    starnotiTimer();
                } else {
                    clearInterval(myVar1);
                }
                if (w_chat == 1) {
                    startofflinemsg();
                } else {
                    clearInterval(myVar2);
                }
    		}});
	    } else {
	        alert("Please enter text");
	        $("#message_chat").focus();
	    }
    }
});

$(document).on('click', '.sbt_btn1', function(){
    //var msg = $("#arg_message1").val();
    var w_chat = $("#wait_chat").val();
    /*if ($("#arg_name").val() == '') {
        alert("Please enter the name");
        $("#arg_name").focus();
    } else if ($("#arg_email").val() == '') {
        alert("Please enter the email");
        $("#arg_email").focus();
    } else*/ if ($("#arg_contact_no").val() == '') {
        alert("Please enter the phone no");
        $("#arg_contact_no").focus();
    } else if ($("#arg_contact_no").val().length !== 10) {
        alert("Please enter the correct phone no");
        $("#arg_contact_no").focus();
    }/* else if ($("#arg_message1").val() == '') {
        alert("Please enter the message");
        $("#arg_message1").focus();
    }*/ else {
        $.ajax({method:"GET", url: '{{route("send_offline_chat")}}', data:{'name':'', 'email':'', 'contact_no':$("#arg_contact_no").val(), 'message':'', 'visit_id': $("#visit_id").val()}, success: function(result){
            //alert(result);
            //document.getElementById("arg_name").value = '';
            //document.getElementById("arg_email").value = '';
            document.getElementById("arg_contact_no").value = '';
            //document.getElementById("arg_message1").value = '';
            document.getElementById("wait_chat").value = '2';
            
            $(".chat_bottom").show();
            $(".chat_bottom1").hide();
            $(".chat_icon").hide();
            $(".chat_window").show();
            $(".chat_body").show();
            $(".chat_body1").hide();
            $(".chat_window").removeClass("offline-form");
            
            setCookie('display_form', 'close', 1);
            
            clearInterval(myVar2);
            clearInterval(myVar1);
            
            setTimeout(function(){
              alert("Message send successfully, our representative will connect you soon.");
            },10);
            
		}});
    }
});
</script>
<?php ?>
