@extends('front.layouts.app')

@section('content')



	<div class="page-title-area bg-18">

		<div class="container">

			<div class="page-title-content">

				<h2>About Us</h2>

				<ul>

					<li><a href="index.php">Home</a></li>

					<li>About</li>

				</ul>

			</div>

		</div>

	</div>



	<section class="stakeholder-area pt-50">

		<div class="container-fluid">

			<div class="stakeholder">

				<div class="row">

					<div class="col-lg-7">

						<div class="stakeholder-content-wrap">

							<div class="stakeholder-title">

								<span>ABOUT</span>

								@if(!$about_us->isEmpty())

									@foreach($about_us as $eachAbt)

										<h2>{{ $eachAbt->title }}</h2>

										{!! $eachAbt->description !!}

									@endforeach

								@endif

							</div>

							

						</div>

					</div>

					<div class="col-lg-5">

						<div class="stakeholder-img"></div>

					</div>

				</div>

			</div>

		</div>

	</section>

	<section class="more-customers-area">
			<div class="container">
				<div class="more-customers-wrap">
					<div class="row">
						<div class="col-lg-12 pr-0">
							<div class="more-customers-content">
								<h2>Our Mission</h2>
								<p class="mb-0">Our mission is to provide our customers with an easy and convenient way to notarize their documents from any location around the world. Our priority is to quickly and effectively meet our customer’s notarizing needs. This includes working diligently behind the scenes to ensure we meet legal and personal security standards. At Aim Notary, we believe that notarizing documents should be quick and professional, so we have created a company and platform, to do just that.</p>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</section>



	

		

@endsection