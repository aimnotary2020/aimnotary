@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Iowa</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Iowa</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Iowa</span>
							<h2>IOWA</h2>

                            <p>Effective July 1st, 2020, Iowa notaries public are able to register to perform remote online notarizations. To become a virtual notary, Iowa requires applicants to complete a RON training course as part of the registration process. For a notary to perform notary services online in Iowa, they must acknowledge additional requirements over and above a traditional in-person notary public. To become an online notary, there are certain criteria required by the state of Iowa that each applicant must acknowledge throughout the certification process. </p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/iowa-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

                        <h2>Requirements For Remote Online Notaries in Iowa</h2>

                        <p>If you are interested in becoming a remote online notary public in the state of Iowa, below are a some, but not all, requirements for becoming a Remote Online Notary Public:</p>

                        <ul>
                            <li>Be an active Iowa notary public</li>
                            <li>Take the state-required RON training course with proof of completion</li>
                            <li>Register with a remote online technology provider that meets state requirements</li>
                            <li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
                            <li>Obtain an electronic seal (that meets state requirements)</li>
                        </ul>

                        <h2>How to Apply to be an Iowa Remote Online Notary Public</h2>

                        <p>The state of Iowa allows individuals to apply to become a Remote Online Notary Public. Click <a href="https://sos.iowa.gov/covid19/remotenotarization/form.aspx" target="_blank">HERE</a>.</p>

                        <p>The following is an overview of information you will need to provide throughout the approval process:</p>

                        <ul>
                            <li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
                            <li>Acknowledgment of state requirements</li>
                            <li>Copy of your digital seal, signature, identity proofing process, and more</li>
                        </ul>

                        <p>Information in this article is subject to be updated by the Iowa Secretary of State. Therefore, it is essential that Iowa notaries public review necessary information via the website. Click <a href="https://sos.iowa.gov/notaries/about.html" target="_blank">HERE</a>!</p>

                        <p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>

@endsection