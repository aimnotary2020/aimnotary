@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Alaska</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Alaska</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Alaska</span>
							<h2>ALASKA</h2>

							<p>Starting January 1, 2021, Notaries public in Alaska are permitted to perform remote online notarizations! This is awesome news for notaries in Alaska. Signers are now able to connect to a virtual notary and have access to notary services online. Alaskan notaries can now perform online notary services for their signers. <a href="https://ltgov.alaska.gov/wp-content/uploads/sites/3/HB0124Z.pdf" target="_blank">HB-124</a> went into place on January 1, 2021. HB-124 provides the information for the remote online notarization process for Alaskan notaries public. Information regarding the notarial process, storage of electronic records, signer identification processes, and much more are detailed in HB-124.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/alaska-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

                        <h2>Requirements For Remote Online Notaries in Alaska</h2>

                        <p>If you are interested in becoming a remote online notary public in the state of Alaska, below are a some, but not all, requirements for becoming a Remote Online Notary Public:</p>

                        <ul>
                            <li>Be an active Alaskan notary public</li>
                            <li>Contract with a remote online technology provider that meets state requirements</li>
                            <li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
                            <li>Obtain an electronic seal (that meets state requirements)</li>
                        </ul>

                        <h2>How to Apply to be an Alaskan Remote Online Notary Public</h2>

                        <p>The state of Alaska allows individuals to apply to become a Remote Online Notary Public. Click <a href="https://ltgov.alaska.gov/notaries-public/" target="_blank">HERE</a>.</p>

                        <p>The following is an overview of information you will need to provide throughout the approval process:</p>

                        <ul>
                            <li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
                            <li>Acknowledgment of state requirements</li>
                            <li>Copy of your digital seal, signature, identity proofing process, and more</li>
                        </ul>

                        <p>Information in this article is subject to be updated by the Alaskan Secretary of State. Therefore, it is essential that Alaskan notaries public review necessary information via the website. Click <a href="https://ltgov.alaska.gov/notaries-public/" target="_blank">HERE</a>!</p>

                        <p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>

@endsection