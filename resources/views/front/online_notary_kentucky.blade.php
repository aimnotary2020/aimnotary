@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
	<div class="container">
		<div class="page-title-content">
			<h1>Online Notary Kentucky</h1>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li>Online Notary Kentucky</li>
			</ul>
		</div>
	</div>
</div>
<section class="stakeholder-area pt-50">
	<div class="container-fluid">
		<div class="stakeholder">
			<div class="row">
				<div class="col-lg-7">
					<div class="stakeholder-content-wrap">
						<div class="stakeholder-title">

							<span>Kentucky</span>
							<h2>KENTUCKY</h2>

							<p>On January 1, 2020, the state of Kentucky allowed their notaries public to perform remote online notarizations. When a singer seeks notary services online, they may now connect with a virtual notary from the state of Kentucky. An online notary in the state of Kentucky may assist a signer with remotely notarizing documents 24/7, as long as the notary public meets the state guidelines for remote notarization.</p>

							<p>Kentucky notaries may use video-conference technology to notarize documents for remotely located individuals. 423.455 clarifies the type of technology, identity proofing, and administrative regulations required to perform remote online notarizations.</p>

						</div>
					</div>
				</div>
				<div class="col-lg-5">
				<div class="stakeholder-img" style="background-image: url(https://www.aimnotary.com/public/front_assets/img/kentucky-map.jpg);"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="more-customers-area">
	<div class="container">
		<div class="more-customers-wrap">
			<div class="row">
				<div class="col-lg-12 pr-0">
					<div class="more-customers-content">

						<h2>Requirements For Remote Online Notaries in Kentucky</h2>

						<p>If you are interested in becoming a remote online notary public in the state of Kentucky, below are a some, but not all requirements, for becoming a Remote Online Notary Public:</p>

						<ul>
							<li>Be an active Kentucky notary public</li>
							<li>No convictions for felonies or crimes of moral turpitude</li>
							<li>Obtain a digital certificate with an electronic signature (that meets state requirements)</li>
							<li>Obtain an electronic seal (that meets state requirements)</li>
						</ul>

						<h2>How to Apply to be a Kentucky Remote Online Notary Public</h2>

						<p>The state of Kentucky allows individuals to apply to become a Remote Online Notary Public. Click <a href="https://web.sos.ky.gov/notaries/SubmitRegistration" target="_blank">HERE</a>.</p>

						<p>The following is an overview of information you will need to provide throughout the approval process:</p>

						<ul>
							<li>Personal info (notary identification number, date of birth, SSN, name, email)</li>
							<li>Acknowledgment of state requirements</li>
							<li>Copy of your digital seal, signature, identity proofing process, and more</li>
						</ul>

						<p>The notary public may perform a notarial act remotely if they have personal knowledge pursuant to KRS 423.325(1) of the identity of the signer, have satisfactory evident of the identity of the signer by oath or affirmation from a credible witness appearing before the notary public under KRS 423.325(2), or reasonably can identify the individual by at least two (2) different types of identity-proofing processes or services. There are other requirements in addition to identifying a signer for completing a successful remote online notarization listed in the Kentucky Secretary of State’s criteria.</p>

						<p>Information in this article is subject to be updated by the Kentucky Secretary of State. Therefore, it is essential that Kentucky notaries public review necessary information via the website. Click <a href="https://apps.legislature.ky.gov/law/statutes/statute.aspx?id=49520" target="_blank">HERE!</a></p>

						<p style="font-size: 12px;"><i>The information on this site is intended only as a general overview and should not be viewed or relied upon as legal advice or guidance. It is advisable to conduct your own research into the applicable laws, statutes, or regulations pertaining to your requirements or situation.</i></p>

					</div>
				</div>						
			</div>
		</div>
	</div>
</section>

<style>
	.more-customers-content ul {
		padding-left: 15px!important;
	}
	.more-customers-content ul li {
		font-weight: normal !important;
		list-style: disclosure-closed;
	}
</style>
@endsection