@extends('front.layouts.app') @section('content')
<section class="contolib-slider-area">
    <div class="contolib-slider-wrap owl-theme owl-carousel" style="height: 490px !important;">
        <div class="contolib-slider-item">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="contolib-slider-text one">
                            <div class="banner-title">Secured Electronic Notarization of your Document in No time</div>
                            <div class="slider-btn"><a href="/pricing" class="default-btn">Notarize a Document</a><a href="/how-it-works" class="default-btn">How It Works</a></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="slider-img"><img src="{{asset('public/front_assets/img/home-one/slider/slide-1.png')}}" srcset="{{asset('public/front_assets/img/home-one/slider/slide-1-991w.png')}} 991w, {{asset('public/front_assets/img/home-one/slider/slide-1-767w.png')}} 767w, {{asset('public/front_assets/img/home-one/slider/slide-1-599w.png')}} 599w, {{asset('public/front_assets/img/home-one/slider/slide-1-479w.png')}} 479w, {{asset('public/front_assets/img/home-one/slider/slide-1-359w.png')}} 359w" alt="Image"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contolib-slider-item">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <div class="contolib-slider-text one">
                            <div class="banner-title">Get Your Documents Notarized: Fast & Effortless</div>
                            <div class="slider-btn"><a href="/pricing" class="default-btn">Notarize a Document</a><a href="/how-it-works" class="default-btn">How It Works</a></div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="slider-img"><img src="{{asset('public/front_assets/img/home-one/slider/slide-2.png')}}" srcset="{{asset('public/front_assets/img/home-one/slider/slide-2-991w.png')}} 991w, {{asset('public/front_assets/img/home-one/slider/slide-2-767w.png')}} 767w, {{asset('public/front_assets/img/home-one/slider/slide-2-599w.png')}} 599w, {{asset('public/front_assets/img/home-one/slider/slide-2-479w.png')}} 479w, {{asset('public/front_assets/img/home-one/slider/slide-2-359w.png')}} 359w" alt="Image"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="home-h1" style="position: relative;top: -100px;text-align: center;font-family: Playfair Display,serif;">
    <div class="container">
        <h1 class="home-title-h1">Notarized Services 100% Online</h1>
    </div>
</section>
<section class="feature-area pb-50 mt-minus-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 p-0 service-cont">
                <div class="feature-wrap">
                    <div class="single-feature">
                        <div class="title">
                            <div class="icon"><i class="flaticon-umbrella"></i></div>
                            <h3>Secure and Legal</h3>
                        </div>
                        <p>At Aim Notary your valuable documents are safe and secure. We utilize SSL encryption and Amazon’s Virtual Private Cloud secured platform to ensure the highest standards in security measures.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 p-0 service-cont">
                <div class="feature-wrap">
                    <div class="single-feature">
                        <div class="title">
                            <div class="icon"><i class="flaticon-research"></i></div>
                            <h3>Identity Proofing</h3>
                        </div>
                        <p>Aim Notary meets and exceeds the identity proofing process to verify and authenticate your identity, the legitimate customer, preventing fraudulent users from accessing sensitive data. This guarantees that the good gets in and the garbage stays out.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 offset-md-3 offset-lg-0 p-0 service-cont">
                <div class="feature-wrap">
                    <div class="single-feature">
                        <div class="title">
                            <div class="icon"><i class="flaticon-wallet"></i></div>
                            <h3>Affordable</h3>
                        </div>
                        <p>Aim Notary empowers you to complete the notarization process remotely and digitally utilizing flexible features in the most cost-effective way. We help simplify and expedite the process so you can handle your business without unnecessary inconvenience.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="opportunity-area pb-20">
    <section>
        <div class="container">
            <div class="section-title" style="max-width: 100%;">
                <h2 style="font-family: Playfair Display,serif; font-size: 50px;">Explore Hassle-free Notary Services 24/7</h2>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single-feature-two"><i class="flaticon-send"></i>
                        <h3>Upload Your Document</h3>
                        <p>Upload a PDF or capture the image of your valuable paper documents.</p><a href="/how-it-works" class="read-more">Learn More <i class="flaticon-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-feature-two"><i class="flaticon-interview"></i>
                        <h3>Connect to a Live Notary</h3>
                        <p>We will connect you with one of our experienced notaries on a live session, confirming your identity face-to-face on a webcam.</p><a href="/how-it-works" class="read-more">Learn More <i class="flaticon-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-feature-two"><i class="flaticon-quality"></i>
                        <h3>eSign and Get eNotary Seal</h3>
                        <p>You can review and sign the document(s) on any device in just a few minutes. Your eSignatures are secure, certified and 100% legal with eNotary seal.</p><a href="/how-it-works" class="read-more">Learn More <i class="flaticon-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-feature-two more">
                        <h3>Security is Our First Priority</h3>
                        <p>10x more secure than visiting a traditional notary, every notarized transaction receives sophisticated ID verification, audit trail and the highest level of security features.</p><a href="/how-it-works" class="default-btn">Learn More</a><span class="flaticon-tools-1"></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<section class="plan-area">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-lg-6 pl-0 plan-cont">
                <div class="plan-img"><img src="{{asset('public/front_assets/img/home-one/plan-img.jpg')}}" srcset="{{asset('public/front_assets/img/home-one/plan-img.jpg')}} 1199w, {{asset('public/front_assets/img/home-one/plan-img-991w.jpg')}} 991w, {{asset('public/front_assets/img/home-one/plan-img-767w.jpg')}} 767w, {{asset('public/front_assets/img/home-one/plan-img-599w.jpg')}} 599w, {{asset('public/front_assets/img/home-one/plan-img-479w.jpg')}} 479w, {{asset('public/front_assets/img/home-one/plan-img-359w.jpg')}} 359w" alt="Image">
                    <div class="plan-shape"><img src="{{asset('public/front_assets/img/home-one/plan-shape.png')}}" srcset="{{asset('public/front_assets/img/home-one/plan-shape.png')}} 1199w, {{asset('public/front_assets/img/home-one/plan-shape-991w.png')}} 991w, {{asset('public/front_assets/img/home-one/plan-shape-767w.png')}} 767w, {{asset('public/front_assets/img/home-one/plan-shape-599w.png')}} 599w, {{asset('public/front_assets/img/home-one/plan-shape-479w.png')}} 479w, {{asset('public/front_assets/img/home-one/plan-shape-359w.png')}} 359w" alt="Image"></div>
                </div>
            </div>
            <div class="col-lg-6 pr-0 plan-cont">
                <div class="plan-area-wrap">
                    <div class="plan-content">
                        <div class="plan-title">
                            <h3 style="font-family: Playfair Display,serif; font-size: 50px;">Remote Online Notarization Now Made Easy</h3>
                            <p>Now avail 10x more secured notarized services right from the comfort of your home. Forget visiting the local notary and get your business documents legalized with top-notch ID verification, audit trail & more in no time. Our notarized documents are globally accepted. From large corporates to small start-ups, Aim Notary serves everyone with our efficient, online notarize platform.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="faq-area">
    <div class="container-fluid">
        <div class="faq-accordion">
            <h4 style="font-family: Playfair Display,serif; font-size: 35px; margin-bottom: 30px;color: #fff;text-align: center;">What do our clients ask?</h4>
            <ul class="accordion">
                <li class="accordion-item"><a class="accordion-title active" href="javascript:void(0)"><i class="flaticon-plus"></i>What is an online notary?</a>
                    <p class="accordion-content show">An online notary is a notary public who is authorized to use audio/visual technology to complete a notarial act when the principal is not in the same physical location as the notary public.</p>
                </li>
                <li class="accordion-item"><a class="accordion-title" href="javascript:void(0)"><i class="flaticon-plus"></i> Is there a difference between a mobile notary and a remote notary?</a>
                    <p class="accordion-content">Yes. A mobile notary public travels to a person’s physical location for notarizations. A remote notary public performs the notarization entirely over a two-way audio/video stream. All aspects of the notarization are handled electronically.</p>
                </li>
                <li class="accordion-item"><a class="accordion-title" href="javascript:void(0)"><i class="flaticon-plus"></i>What is online notarization?</a>
                    <p class="accordion-content">An online notarization is a notarial act performed 100% online between participants and a notary public. The document is signed online by the participant. The notary public signs and places their seal on the document electronically.</p>
                </li>
                <li class="accordion-item"><a class="accordion-title" href="javascript:void(0)"><i class="flaticon-plus"></i> What do I need to notarize a document online?</a>
                    <p class="accordion-content">You will need a device with internet access (cell phone, computer/laptop, tablet), a microphone or headset, camera, your id, and an electronic copy of the document you wish to notarize.</p>
                </li>
                <li class="accordion-item"><a class="accordion-title" href="javascript:void(0)"><i class="flaticon-plus"></i> If Aim Notary is unable to notarize my document, will I be charged?</a>
                    <p class="accordion-content">No. If an Aim Notary is unable to successfully notarize your document, then the session will be ended with no charge to you.</p>
                </li>
                <li class="accordion-item"><a class="accordion-title" href="javascript:void(0)"><i class="flaticon-plus"></i> What categories of documents can be notarized online?</a>
                    <p class="accordion-content">You are able to authenticate a variety of documents, i.e. letters, forms and certificates. However, certain categories of documents such as wills, codicils, I-9 forms, etc., cannot be notarized clectronically due to legal reasons.</p>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="testimonial-area ptb-100">
    <div class="container">
        <div class="section-title">
            <h2 style="font-family: Playfair Display,serif; font-size: 50px;">The Words of Trust</h2>
        </div>
        <div class="testimonial-wrap owl-theme owl-carousel">@if(!$cms_list->isEmpty()) @foreach($cms_list as $eachCms) @if($eachCms->category=='1' && $eachCms->sub_category=='6') <div class="single-testimonial">
                <div class="testimonial-img"><img src="{{asset('public/front_assets/img/home-one/testimonial/shape.png')}}" alt="Image">
                    <div class="testimonial-shape"><img src="{{asset('public/').'/'.$eachCms->file_image}}" alt="Image"></div>
                </div>
                <div class="testimonial-content">
                    <h3>{{ $eachCms->name }}</h3><span>{{ $eachCms->occupation }}</span> {!! $eachCms->description !!} <ul>
                        <li><i class="bx bxs-star"></i></li>
                        <li><i class="bx bxs-star"></i></li>
                        <li><i class="bx bxs-star"></i></li>
                        <li><i class="bx bxs-star"></i></li>
                        <li><i class="bx bxs-star"></i></li>
                    </ul>
                </div>
            </div> @endif @endforeach @endif </div>
    </div>
</section>
<section class="help-area margin-b100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 p-0">
                <div class="faq-img-left"></div>
            </div>
            <div class="col-lg-6 p-0">
                <div class="faq-img-right"></div>
            </div>
        </div>
        <div class="help-touch-wrap">
            <h2 style="font-family: Playfair Display,serif;">Get In Touch</h2>
            <form id="contact-us-form" action="javascript:void(0)" method="post">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="form-group"><input type="text" class="form-control" id="f_name" placeholder="Name*" name="f_name"></div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="form-group"><input type="email" class="form-control" id="user_eml" name="user_eml" placeholder="Email*"></div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="form-group"><input type="text" class="form-control" id="user_contact_no" name="user_contact_no" placeholder="Phone Number*"></div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="form-group">
                            <select id="business_type" class="form-control" name="business_type">
                                <option value="">Requirement</option>
                                <option value="Individual">Individual</option>
                                <option value="Organization">Organization</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group"><textarea name="message" class="form-control" id="message" cols="30" rows="5" placeholder="Your Message"></textarea></div><span id="contact-msg"></span>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="captcha_main_class"><label class="pull-left">Enter Captcha Code <span class="text-danger">*</span></label>
                            <div class="clearfix"></div>
                            <hr>
                            <div class="clearfix"></div><?php $rand = rand(0000, 9999); ?><div class="row">
                                <div class="col-sm-4 col-md-4 col-lg-4"><input type="hidden" name="rand" class="randval" value="<?php echo $rand; ?>"><input class="captcha rand form-control" type="text" value="<?php echo $rand; ?>" readonly="" id="ransubs" style="background-image:url({{asset('public/front_assets/img/cat.png')}}); font-size:30px; border: 1px solid; text-align: center; max-width:100%;"></div>
                                <div class="col-sm-2 col-md-2 col-lg-2 text-center"><i class="fa fa-refresh capcharefresh1 refresh" aria-hidden="true" style="font-size: 30px;cursor: pointer;margin-top: 10px;"></i></div>
                                <div class="col-sm-6 col-md-6 col-lg-6"><input class="form-control card_number" name="chk" maxlength="4" required="required" id="chk2" placeholder="Enter Captcha" autocomplete="off"><br><span id="error1" class="color" style="float: left;color:#FF0000;"></span></div>
                            </div>
                        </div>
                    </div>
                </div><button type="submit" class="default-btn" id="contact-btn" onclick="javascript:return check_captcha1();">Consult Today</button>
            </form>
        </div>
    </div>
</section>
<section id="subscribe" class="subscribe-area">
    <div class="container">
        <div class="subscribe-area-wrap">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="subscribe-content">
                        <h2 style="font-family: Playfair Display,serif; font-size: 32px;">Keep me Updated</h2>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="subscribe-wrap">
                        <form class="newsletter-form" data-toggle="validator"><input type="email" class="form-control" placeholder="Enter Your Email" name="user_email" required autocomplete="off"><button class="default-btn" type="submit" id="">Subscribe</button>
                            <div id="validator-newsletter" class="form-result"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>@endsection @section('script') <script type="text/javascript">
    /*--Subscribe-Form--*/
    $(".newsletter-form").validator().on("submit", function(event) {
        if (event.isDefaultPrevented()) {
            /*--Handle-The-Invalid-Form--*/
            formErrorSub();
            submitMSGSub(false, "Please enter your email correctly.");
        } else {
            /*--Everything-Looks-Good!--*/
            event.preventDefault();
            $.ajax({
                url: '{{url("subscribe")}}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: $(this).serialize(),
                beforeSend: function() {},
                success: function(data) {
                    var obj = JSON.parse(data);
                    if (obj.status == 200) {
                        $(".newsletter-form")[0].reset();
                        submitMSGSub(true, obj.message);
                        setTimeout(function() {
                            $("#validator-newsletter").addClass('hide');
                        }, 4000);
                    } else if (obj.status == 201) {
                        $(".newsletter-form")[0].reset();
                        submitMSGSub(false, obj.message);
                        setTimeout(function() {
                            $("#validator-newsletter").addClass('hide');
                        }, 4000);
                    } else {
                        $(".newsletter-form").addClass("animated shake");
                        setTimeout(function() {
                            $(".newsletter-form").removeClass("animated shake");
                        }, 1000);
                    }
                }
            });
        }
    });

    function submitMSGSub(valid, msg) {
        if (valid) {
            var msgClasses = "validation-success";
        } else {
            var msgClasses = "validation-danger";
        }
        $("#validator-newsletter").removeClass().addClass(msgClasses).text(msg);
    }
    $("#contact-us-form").validate({
        /*--Onkeyup--*/
        rules: {
            f_name: {
                required: true,
            },
            user_eml: {
                required: true,
            },
            user_contact_no: {
                required: true,
            },
            business_type: {
                required: true,
            },
            message: {
                required: true,
            }
        },
        messages: {
            f_name: {
                required: 'Name is required',
            },
            user_eml: {
                required: 'Email id is required',
            },
            user_contact_no: {
                required: 'Phone no is required',
            },
            business_type: {
                required: 'Please select your requirement type',
            },
            message: {
                required: 'Message is required',
            }
        },
        submitHandler: function() {
            var form = $('#contact-us-form').serialize();
            $.ajax({
                url: '{{url("contact_us")}}',
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: form,
                beforeSend: function() {
                    $('#contact-btn').html('Please Wait...');
                    $('#contact-btn').attr('disabled', 'disabled');
                },
                success: function(data) {
                    var obj = JSON.parse(data);
                    if (obj.status == 200) {
                        $('#contact-btn').removeAttr('disabled');
                        $('#contact-btn').html('Consult Today');
                        $("#contact-msg").css('color', 'green');
                        $("#contact-msg").html(obj.message);
                        $('#contact-us-form').trigger("reset");
                        $("#contact-us-form").validate().resetForm();
                    } else {
                        $("#contact-msg").css('color', 'red');
                        $("#contact-msg").html(obj.message);
                        $('#contact-us-form').trigger("reset");
                        $("#contact-us-form").validate().resetForm();
                    }
                    setTimeout(function() {
                        $("#contact-msg").html('');
                    }, 800);
                }
            });
        }
    });

    function check_captcha1() {
        return "" == $("#chk2").val() ? (document.getElementById("error1").innerHTML = "Enter Captcha!", $("#chk2").focus(), !1) : $("#ransubs").val() != $("#chk2").val() ? (document.getElementById("error1").innerHTML = "Captcha Not Matched!", $("#chk2").focus(), !1) : void("" != $("#chk2").val() && $("#ransubs").val() == $("#chk2").val() && (document.getElementById("error1").style.color = "green", document.getElementById("error1").innerHTML = "Captcha  Matched!"))
    }
    $(".capcharefresh1").click(function(e) {
        document.getElementById("ransubs").value = Math.floor(1e4 * Math.random() + 1)
    });
</script>@endsection