@extends('front.layouts.app')
@section('content')
<div class="page-title-area bg-18">
    <div class="container">
        <div class="page-title-content">
            <h1 class="inner-title">Blog</h1>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li>Blog</li>
            </ul>
        </div>
    </div>
</div>
<section class="inner-page-container pt-50 pb-100 articles-area articles-area-two">
    <div class="container">
        <div class="row">
            @if(!$blog_list->isEmpty()) 
                @foreach($blog_list as $eachVal)
                    <div class="col-lg-4 col-md-6">
                        <div class="single-articles">
                            <a href="{{ url('blog-detail/').'/'.$eachVal->generate_url }}">
                                <img src="{{asset('public/').'/'.$eachVal->image}}" alt="Image">
                            </a>
                            <div class="articles-content">
                                <ul>
                                    <li><a href="{{ url('blog-detail/').'/'.$eachVal->generate_url }}">
                                        @if(isset($eachVal->get_user_dtl['name'])) 
                                            {{ $eachVal->get_user_dtl['name'] }} 
                                        @endif</a></li>
                                    <li><i class="bx bx-calendar"></i>{{ date_format(date_create($eachVal->created_at),'F d, Y') }}</li>
                                </ul>
                                <a href="{{ url('blog-detail/').'/'.$eachVal->generate_url }}">
                                    <h3>{{ $eachVal->title }}</h3>
                                </a>
                                <a href="{{ url('blog-detail/').'/'.$eachVal->generate_url }}" class="read-more">Read More</a>
                            </div>
                        </div>
                    </div>
                @endforeach 
            @endif
        </div>
    </div>
</section>
@endsection