@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <?php 
                  if(isset($message)){
                    echo "<b style='color : green;'>".$success."</b>";
                  }
                ?>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">

                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Address</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            
            </div>

          </div>
         

          <div class="modal" id="appointment">
            <div class="modal-dialog" style="max-width : 1000px !important;">
                <div class="modal-content">
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td>Image</td>
                                  <td>Name</td>
                                  <td>Email</td>
                                  <td>Contact No.</td>
                                  <td>Priority</td>
                                  <td>NAA Cerification</td>
                                  <td>Completed Docs</td>
                                  <td>Action</td>
                              </tr>
                            </thead>
                            <tbody id="alllawyer">
                            
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
          

  @endsection


  @section('script')
<script>
      function OpenAssignModal(doc_id){
        $("#alllawyer").html("<tr align='center'><td colspan='8'><img src='https://www.aimnotary.com/beta/public/loader.gif'/></td></tr>");
        $.ajax({
            url: '{{ url("user-list/fetch_lawyer") }}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: "doc_id="+doc_id,
            success: function(data){
                $("#alllawyer").html("");
                $("#alllawyer").html(data);
            }
        });
          $("#appointment").modal('show');
      }
     function makeAssign(lid,userlid,doc_id){
        $.ajax({
            url: '{{ url("user-list/assign-user") }}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: "lawyer_id="+lid+"&lawyer_user_id="+userlid+"&doc_id="+doc_id,
            success: function(data){
                swal({
                    title: "Thank You!",
                    text: "Notary Assigned Successfully",
                    type: "success"
                }).then(function() {
                    location.reload();
                });
            }
        });
     }

    $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4',
            });
    });

    fetch_data();
  
    function fetch_data(){
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                ordering : false,
                ajax: '{{route("fetch-schedule-user")}}',
                columns: [
                    { data: 'name'},
                    { data: 'email'},
                    { data: 'country'},
                    { data: 'city'},
                    { data: 'add1'},
                    { data: 'date'},
                    { data: 'time'},
                    { data: 'action' },
                ]
               // "dom": 'l<"toolbar>frtip',
            });
            //$("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
    }

    $("#submit-reg-form").validate({
        //onkeyup:
     	rules:{
            f_name: {
                required: true,
            },
            l_name: {
                required: true,
            },
            u_email: {
                required: true,
				        remote : 'https://www.aimnotary.com/beta/checkEmailAjax',
            },
            personal_mobile: {
                required: true,
                minlength:10,
				        maxlength:10,
            },
            u_password: {
                required: true,
                minlength:6,
            },
            cnf_password:{
            	required:true,
            },
            privacy : {
              required : true,
            },
            terms : {
              required : true,
            },
            city : {
              required : true,
            },
            address : {
              required : true,
            },
            amount : {
              required : true,
              digits : true,
            }
            
        },
        messages: {
            f_name: {
                required:'First Name is required',
            },
            l_name: {
                required:'Last Name is required',
            },
            u_email: {
                required:'Email Address is required',
				        remote : 'This email address is already registered',
            },
            personal_mobile: {
                required:'Phone no is required',
            },
            u_password: {
                required:'Password is required',
            },
            cnf_password: {
                required:'Confirm Password is required',
            },
            privacy : {
              required : "Please Agree with the privacy section",
            },
            terms : {
              required : "Please Agree with the Terms section",
            }
        },
        submitHandler: function() { 
           	 var form = $('#submit-reg-form').serialize();
             $.ajax({
		            url: '{{ url("user-list/enterprise-user-registration") }}',
		            type: 'POST',
		            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		            data: form,
		            beforeSend:function(){
                    $('#reg_btn').html('Please Wait...');
                    $('#reg_btn').attr('disabled','disabled');
		            },
		            success: function(data){
		                var obj=JSON.parse(data);
                    swal({
                        title: "Thank You!",
                        text: obj.message,
                        type: "success"
                    }).then(function() {
                        location.reload();
                    });
		            }
		        });
        }
    });

    </script>
@endsection