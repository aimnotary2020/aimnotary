@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">Transaction List</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <!-- <a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#myModal">+ Add New User</a> -->
                <?php 
                  if(session('message')){
                    echo "<b style='color : green;'>".session('message')."</b>";
                  }
                ?>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">

                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Amount</th>
                                  <th>Documents</th>
                                  <th>Transaction Details</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php
                                if(!empty($transaction_list)){
                                  foreach($transaction_list as $trlist){
                                    // echo "<pre>"; print_r($trlist); exit;
                                    ?>
                                      <tr>
                                        <td><?= $trlist->name ?></td>
                                        <td><?= $trlist->email ?></td>
                                        <td>$<?= $trlist->amount ?></td>
                                        <td><a target="_blank" href="<?php echo url('/').'/public/'.$trlist->doc; ?>">View Doc</a></td>>
                                        <td>
                                            <?php
                                                $tobj=json_decode($trlist->transaction_json,true);
                                                // echo "<pre>"; print_r($tobj);
                                                echo "Transaction Tag - ".$tobj['transaction_tag']."<br><br>";
                                                echo "Authorization Number - ".$tobj['authorization_num']."<br><br>";
                                                echo "Date - ".$trlist->crated_date."<br><br>";
                                                echo "Time  - ".$trlist->created_time."<br><br>";
                                                // echo "<pre>".$tobj['ctr'];
                                                
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                              
                                              if($trlist->status=='A'){
                                                ?>
                                                    <a class='btn btn-info' id="reg_btn<?= $trlist->txn_id ?>" onclick="getRefund('<?php echo $trlist->txn_id ?>','<?php echo $tobj['authorization_num'] ?>','<?= $tobj['transaction_tag'] ?>','<?php echo str_replace('.','',$tobj['amount']); ?>');">Refund</a>
                                                <?php
                                              }else{
                                                echo "Refunded";
                                              }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                  }
                                }
                             ?>
                                
                              
                            </tbody>

                        </table>
                    </div>
                  </div>
              </div>
            
            </div>

          </div>
          <!-- content-wrapper ends -->




          

  @endsection


  @section('script')
<script>
function getRefund(txnid,authno,trantag,amount){
  $.ajax({
        url: '{{ url("admin/refund") }}',
        type: 'POST',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data: "transaction_tag="+trantag+"&txnid="+txnid+"&authno="+authno+"&amount="+amount,
        beforeSend:function(){
            $('#reg_btn'+txnid).html('Please Wait...');
            $('#reg_btn'+txnid).attr('disabled','disabled');
        },
        success: function(data){
            var obj=JSON.parse(data);
            console.log(obj);
            swal({
                  title: "Thank You!",
                  text: "Refund has been successfully initiated",
                  type: "success"
              }).then(function() {
                location.reload();
            });
            $('#reg_btn'+txnid).html('Refund');

            // if(obj.transaction_status==true){
            //   swal({
            //       title: "Thank You!",
            //       text: "Refund Hasbeen successfully initiated",
            //       type: "success"
            //   }).then(function() {
            //       location.reload();
            //   });
            // }
            
            // if(obj.Error.messages[0].code){
            //   swal({
            //       title: "Oops!",
            //       text: obj.Error.messages[0].description,
            //       type: "error"
            //   });
            //   $('#reg_btn'+txnid).html('Refund');
            // }else{
            //   swal({
            //       title: "Thank You!",
            //       text: "Refund Hasbeen successfully initiated",
            //       type: "success"
            //   }).then(function() {
            //       location.reload();
            //   });
            // }

            
        }
    });
}
</script>
@endsection