@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-md-12">
                        <div class="d-flex pull-right">
                          <a href="javascript:void(0);" onclick="get_dtl('');">+ Add New</a>
                        </div>
                      </div>
                     
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            
            </div>
          </div>
          <!-- content-wrapper ends -->
          <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
               
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title">Edit </h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                           <div class="col-md-12 grid-margin stretch-card">
                              <div class="card">
                                <div class="card-body">
                                  <form class="forms-sample" id="submit-cms-form" method="POST">
                                     <div class="form-group">
                                      <label for="exampleInputName1">Category * </label>
                                       <select class="form-control" id="category_id" name="category" required onchange="get_subcategory(this.value);">
                                        <option value="">--select--</option>
                                         @if(!$cms_list->isEmpty())
                                              @foreach($cms_list as $eachCms)
                                                  <option value="{{$eachCms->id}}">{{$eachCms->name}}</option>
                                              @endforeach
                                          @endif
                                      </select>
                                      <input type="hidden" name="hidden_id" id="hidden_id">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputName1">Name</label>
                                      <select class="form-control" id="sub_category_id" name="sub_category" required>
                                        <option value="">--select--</option>
                                        <option value="2">2</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="title">Title</label>
                                      <input type="text" class="form-control" id="title" name="title" placeholder="Title">
                                    </div>                              
                                    <div class="form-group">
                                      <label>File upload</label>
                                      
                                      <div class="input-group col-xs-12">
                                        <input type="file" class="form-control file-upload-info" name="upload_img" id="upload_img"  placeholder="Upload Image">
                                        <input type="hidden" name="old_img" id="old_img">
                                        <img src="" id="display_img" style="display: none;" width="60" height="60">
                                       <!--  <span class="input-group-append">
                                          <button class="file-upload-browse btn btn-info" type="button">Upload</button>
                                        </span> -->
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="author_name">Name</label>
                                      <input type="text" class="form-control"  id="author_name" name="name" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                      <label for="occupation">Designation</label>
                                      <input type="text" class="form-control" id="occupation" name="occupation" placeholder="Designation">
                                    </div>
                                    <div class="form-group">
                                      <label for="url_link">URL</label>
                                      <input type="text" class="form-control" id="url_link" name="url_link" placeholder="Link...">
                                    </div>
                                    <div class="form-group">
                                      <label for="editor">Description</label>
                                      <textarea class="form-control" id="editor"  rows="2"></textarea>
                                      <input type="hidden" name="description" id="description">
                                    </div>
                                    <button type="submit" class="btn btn-primary" id="target_btn">Submit</button>
                                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                  <!--   <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="target_btn" disabled >Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>
               
              </div>
          </div>
  @endsection
  @section('script')
   <script src="{{asset('public/assets/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'editor' );
</script>
<script>
    $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4',
            });
    });
    fetch_data();
  
    function fetch_data(){
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route("fetch-cms")}}',
                columns: [
                    { data: 'category'},
                    { data: 'sub_category'},
                    { data: 'name'},
                    { data: 'file_image'},
                    { data: 'description'},
                    { data: 'action'}
                ]
               // "dom": 'l<"toolbar>frtip',
            });
            //$("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
    }
    // $("#submit-cms-form").validate({
    //     //onkeyup:
    //     rules: {
    //         category: {
    //             required:true,
    //         }
    //     },
    //     messages: {
    //         categorys: {
    //             required: 'Please select  Category',
    //         }
    //     },
    //     submitHandler: function() { 
    //        var form = $('#submit-cms-form').serialize();
    //         var type = 'basicinformation';
    //         dataObj  = {};
    //         $(data).each(function(i, field){
    //             dataObj[field.name] = field.value;
    //         });
    //         var data = JSON.stringify(dataObj);
    //        //var data = new FormData("#submit-cms-form");
    //         //console.log(data);
    //         $.ajax({
    //             url: '{{route("add_edit_cms")}}',
    //             type: 'POST',
    //             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
    //             data: form,
    //             success:function(data){
    //               console.log(data);
    //                 var newData = JSON.parse(data);
    //                 if(newData.status == 200){
    //                     if(id!=''){
    //                        toastr.success('Department successfully updated.');
    //                     }else{
    //                         toastr.success('Department successfully added');
    //                     }
    //                     $("#myDeptModal").modal('hide');
    //                     $('.table').DataTable().destroy();
    //                     fetch_data();
    //                     //fetch_department();
    //                 }else if(newData.status == 404){
    //                     toastr.error(name + ' already exist');
    //                 }else{
    //                     toastr.error('Something went wrong please try again later');
    //                 }
    //             }
    //         });
    //     }
    // });
    function get_subcategory(category_id) {
       $.ajax({
                url: '{{route("get_subcat")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
                data: {category_id:category_id},
                success:function(res){
                  var obj=JSON.parse(res);
                  $('#sub_category_id').children('option:not(:first)').remove();
                   $.each(obj, function (key, val) {
                         $("#sub_category_id").append('<option value="'+val.id+'">'+val.name+'</option>');
                   });
                }
            });
    }
     // $("#category_id").change(function(){
        
     // });
      $("#target_btn").mouseover(function(){
           var text = CKEDITOR.instances.editor.getData();
           $("#description").val(text);
      });
    $("#submit-cms-form").submit(function(e){
        e.preventDefault();
         $.ajax({
                url: '{{route("add_edit_cms")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                
                cache       : false,
                contentType : false,
                processData : false,
                data: new FormData(this),
                success:function(res){
                  //console.log(res);
                 var obj=JSON.parse(res);
                       if(obj.status==200){
                          toastr.success(obj.message);
                       }else{
                         toastr.error('Something went wrong please try again later.');
                       }
                       $("#myModal").modal('hide');
                      $('.table').DataTable().destroy();
                      fetch_data();    
                   
                }
            });
    });
    function get_dtl(id) {
        if(id!=''){
          $("#display_img").show();
          $(".modal-title").text('Edit Data');
        }else{
          $("#display_img").hide();
          $(".modal-title").text('Add Data');
          CKEDITOR.instances.editor.setData('');
        }
        $("#myModal").modal('show');
         $.ajax({
                    url: '{{route("get-cms-dtl")}}',
                    type: 'POST',
                    dataType:"html",
                    data:{id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(response){
                      var res=JSON.parse(response);
                        $("#hidden_id").val(id);
                        $("#category_id").val(res.category);
                        get_subcategory(res.category);
                        setTimeout(function(){ $("#sub_category_id").val(res.sub_category); },800);
                        $("#author_name").val(res.name);
                        
                        $("#title").val(res.title);
                        $("#old_img").val(res.file_image);
                        
                         var image    = "{{asset('/public/')}}";
                        $("#display_img").attr('src',image+'/'+res.file_image);
                        $("#occupation").val(res.occupation);
                          CKEDITOR.instances.editor.setData(res.description);
                        $("#url_link").val(res.url_link);
                        
                        
                    }
          });
    }
    function delete_cms(id) {
           $.ajax({
                    url: '{{route("remove-cms")}}',
                    type: 'POST',
                    dataType:"json",
                    data:{id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(response){
                     //console.log(response);
                         var obj=response;
                         if(obj.status==200){
                            toastr.success(obj.message);
                         }else{
                           toastr.error('Something went wrong please try again later.');
                         }
                         $('.table').DataTable().destroy();
                         fetch_data();
                    }
          });
    }
    </script>
@endsection