@extends('admin.layouts.app')

@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
           
           <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    
                                    <th>Full Name</th>
                                    <th>Email ID</th>
                                    <th>Contact No.</th>
                                    <th>Suject</th>
                                    <th>Message</th>
                                    <th>Type</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if(sizeof($cms_list)>0)
                            {?>
                            @foreach($cms_list as $allweb)
                            
                            <tr>
                                <td>{{$allweb->uname}}</td>
                                <td>{{$allweb->email}}</td>
                                <td>{{$allweb->phone}}</td>
                                <td>{{$allweb->subj}}</td>
                                <td>{{$allweb->msg}}</td>
                                <td>{{$allweb->type}}</td>
                                <td>{{$allweb->date}}</td>
                                <td><a class="delete" href="remove-weblit/{{$allweb->id}}" title="View" style="font-size:20px; color:red;" ><i class="typcn typcn-trash view_icon"></i></a></td>
                             </tr>   
                            @endforeach
                            <?php } 
                            else
                            {?>
                            <tr><td colspan="8">No Data available</td></tr> 
                           <?php }?>
                            
                        </table>
                    </div>
                  </div>
              </div>
            
            </div>
          </div>
         
  @endsection
  @section('script')
   <script src="{{asset('public/assets/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'editor' );
</script>
<script>
    $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4',
            });
    });
    fetch_data();
  
  
  
    </script>
@endsection