@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-md-12">
                        <h3>Add Meta Data</h3>

                        <form method="post" id="submit-metaform">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Link</label>
                              <textarea class="form-control" name="link"><?= @$meta_details->link ?></textarea>
                              @if(isset($meta_details))
                                <input type="hidden" value="<?= $meta_details->id ?>" name="id" />
                              @endif
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Meta Title</label>
                              <textarea class="form-control" name="title"><?= @$meta_details->title ?></textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Meta Keyword</label>
                              <textarea class="form-control" name="keyword"><?= @$meta_details->keyword ?></textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Meta Description</label>
                              <textarea class="form-control" name="description"><?= @$meta_details->description ?></textarea>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Extra Tag</label>
                              <textarea class="form-control" name="extratag"><?= @$meta_details->extratag ?></textarea>
                            </div>
                          </div>
                          <div class="col-md-3">
                            <input type="submit" class="btn btn-info" value="Add Meta Data"/>
                          </div>
                        </form>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Link</th>
                                    <th>Meta Title</th>
                                    <th>Action</th>
                                </tr>
                                <?php
                                  if(!empty($meta_list)){
                                    foreach($meta_list as $mt){
                                      ?>
                                        <tr>
                                          <th><?= $mt->link; ?></th>
                                          <th><?= $mt->title; ?></th>
                                          <th><a href="{{ url('admin/meta-title-management') }}/<?= $mt->id; ?>">Edit</a></th>
                                        </tr>
                                      <?php
                                    }
                                  }
                                ?>
                            </thead>
                        </table>
                        </div>
                    </div>
                  </div>
              </div>
            
            </div>
          </div>
        

          

          

  @endsection
  @section('script')
  <script>
    
    $("#submit-metaform").submit(function(e){
        e.preventDefault();
         $.ajax({
                url: '{{route("add-meta-details")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                cache       : false,
                contentType : false,
                processData : false,
                data: new FormData(this),
                success:function(res){
                  //console.log(res);
                  var obj=JSON.parse(res);
                       if(obj.status==200){
                          toastr.success(obj.message);
                       }else{
                          toastr.error('Something went wrong please try again later.');
                       }
                       window.location.href='{{ url("admin/meta-title-management/0") }}';
                }
            });
        });
        
        
    </script>
@endsection