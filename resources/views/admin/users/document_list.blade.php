<style>
  
</style>
@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif
                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email ID</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Date</th>
                                    <th>Lawyer Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
          </div>



          <div id="myModalMerge" style="width: 80% !important;" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content" style="width : 800px;">
              <form action='{{ url("/mergerDocuments") }}' method='post' id='mergeDocument'>
                  <div class="modal-header" style="border-radius : 0px; background-color : #1c45ef">
                    <h4 class="modal-title" style="color : white;">Merge Additional Documents</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <div class="container">
                      <div class="row">
                          <div class="col-md-12">
                            
                              @csrf
                              <div id="uploadedDocumentGet"></div>
                             
                            
                          </div>                
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <input type='submit' class="btn btn-info" value='Merge Documents' />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </form>
              </div>
            </div>
          </div>


  @endsection
  @section('script')

<script>
    fetch_data();

    function waveOff(doc_id){
        $.ajax({
            url: '{{url("user-list/wave-off-documents")}}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
            data: "doc_id="+doc_id,
            success:function(res){
              var obj=JSON.parse(res);
              if(obj.status==200){
                  toastr.success(obj.message);
                }else if(obj.status==201){
                  toastr.error(obj.message);
                }
                $('#grouplist').DataTable().destroy();
                fetch_data();
            }
        });
    } 

    @if(Auth()->user()->user_type=='Lawyer')
      click_me();
    @endif
    function fetch_data(){
          @if(Auth()->user()->user_type=='Admin')
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                orderable : false,
                ajax: '{{ url("user-list/user-docs")."/".$user_id }}',
                columns: [
                    { data: 'name', orderable: false},
                    { data: 'email', orderable: false},
                    { data: 'country', orderable: false},
                    { data: 'city', orderable: false},
                    { data: 'dateTime', orderable : false },
                    { data: 'lawyer_detail', orderable: false},
                    { data: 'action', orderable: false}

                ]
            }); 
        @else  
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                orderable : false,
                ajax: '{{ url("user-list/user-docs")."/".$user_id }}',
                columns: [
                    { data: 'name', orderable: false},
                    { data: 'email', orderable: false},
                    { data: 'country', orderable: false},
                    { data: 'city', orderable: false},
                    { data: 'dateTime', orderable : false },
                    { data: 'action', orderable: false}

                ]
            });   
        @endif
    }
    
    function send_video_link(id,user_id,name,email) {
         //var link='{{ url("start-video")."/".time()}}';
          $.ajax({
                url: '{{url("start_call")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
                data: {id,user_id,name,email},
                success:function(res){
                  var obj=JSON.parse(res);
                  if(obj.status==200){
                      toastr.success(obj.message);
                   }else if(obj.status==201){
                      toastr.error(obj.message);
                   }
                   $('#grouplist').DataTable().destroy();
                   fetch_data();
                }
            });
    }
    function click_me() {
      var myWindow;
        	  myWindow=window.open(
			  'https://www.aimnotary.com/signer/',
			  '_blank', // <- This is what makes it open in a new window.
			  'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,top=1000000,width=20,height=20'
			);
			setTimeout(function(){ myWindow.close(); }, 1000);
    }


    function getMergeDocumentList(doc_id)
    {
        $("#uploadedDocumentGet").html("<div align='center'><img style='width : 50px;marign : 0 auto;' src='https://thumbs.gfycat.com/KlutzyMadeupHarpseal-max-1mb.gif' /></div>");
          $.ajax({
              url: '{{ url("/get-merge-documents-list?doc_id=") }}'+doc_id,
              type: 'POST',
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data: {},
              success:function(res){
                  $("#uploadedDocumentGet").html(res);
              }
        });
    }



    </script>
@endsection