@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <!-- <a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#myModal">+ Add New User</a> -->
                <?php 
                  if(session('message')){
                    echo "<b style='color : green;'>".session('message')."</b>";
                  }
                ?>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">

                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            
            </div>

          </div>
          <!-- content-wrapper ends -->

          <div class="modal" id="myModal">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <form method="post" id="submit-reg-form" action="javascript:void(0);" novalidate="novalidate">
                    <div class="row">
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input class="form-control" type="text" name="f_name" id="f_name" placeholder="First Name">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 ">
                        <div class="form-group">
                          <input class="form-control" type="text" name="l_name" id="l_name" placeholder="Last Name">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input class="form-control error" type="email" name="u_email" id="u_email" placeholder="Email Address">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input class="form-control number" type="text" name="personal_mobile" id="personal_mobile" placeholder="Phone Number">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input class="form-control" type="text" name="city" id="city" placeholder="Enter City">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input class="form-control" type="text" name="address" id="address" placeholder="Enter Address">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                          <input class="form-control valid" type="password" name="u_password" id="u_password" placeholder="Password" autocomplete="off" aria-invalid="false">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 ">
                        <div class="form-group">
                          <input placeholder="No Of Documents" autocomplete="off" type="text" name="doc_no" value="" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-12 col-sm-12 ">
                        <div class="form-group">
                          <input placeholder="Amount" autocomplete="off" type="text" name="amount" value="" class="form-control">
                        </div>
                      </div>

                      <div class="col-md-12 col-sm-12 ">
                        <input type="submit" id="reg_btn" class="btn btn-success" value='Create User'/>
                      </div>
                      
                    </div>
                  </form>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>


          

  @endsection


  @section('script')
<script>
    $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4',
            });
    });

    fetch_data();
  
    function fetch_data(){
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                ordering : false,
                ajax: '{{route("fetch-enterprise-user")}}',
                columns: [
                    { data: 'name'},
                    { data: 'email'},
                    { data: 'country'},
                    { data: 'city'},
                    { data: 'add1'},
                    { data: 'action' },
                ]
               // "dom": 'l<"toolbar>frtip',
            });
            //$("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
    }

    $("#submit-reg-form").validate({
        //onkeyup:
     	rules:{
            f_name: {
                required: true,
            },
            l_name: {
                required: true,
            },
            u_email: {
                required: true,
				        remote : 'https://www.aimnotary.com/beta/checkEmailAjax',
            },
            personal_mobile: {
                required: true,
                minlength:10,
				        maxlength:10,
            },
            u_password: {
                required: true,
                minlength:6,
            },
            cnf_password:{
            	required:true,
            },
            privacy : {
              required : true,
            },
            terms : {
              required : true,
            },
            city : {
              required : true,
            },
            address : {
              required : true,
            },
            amount : {
              required : true,
              digits : true,
            }
            
        },
        messages: {
            f_name: {
                required:'First Name is required',
            },
            l_name: {
                required:'Last Name is required',
            },
            u_email: {
                required:'Email Address is required',
				        remote : 'This email address is already registered',
            },
            personal_mobile: {
                required:'Phone no is required',
            },
            u_password: {
                required:'Password is required',
            },
            cnf_password: {
                required:'Confirm Password is required',
            },
            privacy : {
              required : "Please Agree with the privacy section",
            },
            terms : {
              required : "Please Agree with the Terms section",
            }
        },
        submitHandler: function() { 
           	 var form = $('#submit-reg-form').serialize();
             $.ajax({
		            url: '{{ url("user-list/enterprise-user-registration") }}',
		            type: 'POST',
		            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		            data: form,
		            beforeSend:function(){
                    $('#reg_btn').html('Please Wait...');
                    $('#reg_btn').attr('disabled','disabled');
		            },
		            success: function(data){
		                var obj=JSON.parse(data);
                    swal({
                        title: "Thank You!",
                        text: obj.message,
                        type: "success"
                    }).then(function() {
                        location.reload();
                    });
		            }
		        });
        }
    });

    </script>
@endsection