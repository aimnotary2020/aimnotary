@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">Coupon List</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <a href="javascript:void(0);" class="btn btn-success" data-toggle="modal" data-target="#exampleModalCenter">+ Add New Coupon</a>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">

                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                  <th>Coupon Code</th>
                                  <th>Expaire Date</th>
                                  <th>Discount Amount</th>
                                  <th>Coupon Limit</th>
                                  <th>Usage Count</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                             <?php
                                if(!empty($couponlists)){
                                  foreach($couponlists as $couponlist){
                                    // echo "<pre>"; print_r($couponlist); exit;
                                    $resCheck = DB::table('tbl_coupon_usage_count')->where(['coupon_id'=>$couponlist->coupon_id])->get();
                                    ?>
                                      <tr>
                                        <td><input class="openInputBox" onkeyup="updateCouponValue(this.value,<?= $couponlist->coupon_id ?>,'coupon_code')" onclick="openInputBox(this)" style="border : none;" type="text" value="<?= $couponlist->coupon_code ?>" /></td>
                                        <td><input class="openInputBox" onchange="updateCouponValue(this.value,<?= $couponlist->coupon_id ?>,'coupon_dt')" onclick="openInputBox(this)" style="border : none;" type="date" value="<?= $couponlist->coupon_dt ?>" /></td>
                                        <td><input class="openInputBox" onkeyup="updateCouponValue(this.value,<?= $couponlist->coupon_id ?>,'discount_amt')" onclick="openInputBox(this)" style="border : none;" type="text" value="<?= $couponlist->discount_amt ?>" /></td>
                                        <td><input class="openInputBox" onkeyup="updateCouponValue(this.value,<?= $couponlist->coupon_id ?>,'usage_count')" onclick="openInputBox(this)" style="border : none;" type="text" value="<?= $couponlist->usage_count ?>" /></td>
                                        <td>Count : <?php 
                                                  echo $cou = count($resCheck); ?><br>
                                             Amount Waved : $<?= $cou * $couponlist->discount_amt; ?>     
                                        </td>
                                        <td>
                                          <?php
                                            if($couponlist->status == 1){
                                               ?>
                                                  <a>Inactivate</a>
                                               <?php
                                            }else{
                                              ?>
                                                  <a>Activate</a>
                                               <?php
                                            }
                                          ?>
                                            <a></a>
                                        </td>
                                      </tr>
                                    <?php
                                  }
                                }
                             ?>
                                
                              
                            </tbody>

                        </table>
                    </div>
                  </div>
              </div>
            
            </div>

          </div>
          <!-- content-wrapper ends -->


          <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <form method="post" id="addCouponSet">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Add Coupon</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                      <div class="formgroup">
                          <label>Coupon Code</label>
                          <input type="text" class="form-control" name="coupon_code"/>
                      </div>
                      <div class="formgroup">
                        <label>Expairy Date</label>
                        <input type="date" class="form-control" name="coupon_dt"/>
                      </div>
                      <div class="formgroup">
                      <label>Discount Amount</label>
                        <input type="text" class="form-control" name="discount_amt"/>
                      </div>
                      <div class="formgroup">
                        <label>Coupon Limit</label>
                        <input type="text" class="form-control" name="usage_count"/>
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          

  @endsection


  @section('script')
  <style>
    .error {
      margin-bottom: 0.2rem !important;
      font-size: 11px !important;
    }
  </style>
<script>
  function openInputBox(myObj){
    $(".openInputBox").css({ border : 'none', });
    $(myObj).css({ border : '1px solid black', });
  }

  $("#addCouponSet").validate({
      rules : {
        coupon_code : {
            required : true,
        },
        coupon_dt : {
            required : true,
        },
        discount_amt : {
            required : true,
            number : true,
        },
        usage_count : {
            required : true,
            number : true,
        },
      },
      submitHandler: function(form){
          $.ajax({
            url: '{{ url("admin/addCoupon") }}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: $(form).serialize(),
            success: function(data){
              swal({
                    title: "Thank You!",
                    text: "Coupon Added successfully",
                    type: "success"
                }).then(function() {
                  location.reload();
              });
            }
        });
      }

  });

  function updateCouponValue(valcu,ID,coloumn){
    $.ajax({
          url: '{{ url("admin/updateCouponValue") }}',
          type: 'POST',
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
          data: "myVal="+valcu+"&coupon_id="+ID+"&coloumn="+coloumn,
          success: function(data){
            
          }
      });
  }

</script>
@endsection