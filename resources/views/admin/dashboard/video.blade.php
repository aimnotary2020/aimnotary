@extends('admin.layouts.app')

@section('content')

 <div class="container-scroller">

      <!-- partial:partials/_navbar.html -->

      

       @include('admin.includes.nav')

      <!-- partial -->

      <div class="container-fluid page-body-wrapper">

        <!-- partial:partials/_sidebar.html -->

         @include('admin.includes.sidebar')

        <!-- partial -->

        <div class="main-panel">

          <div class="content-wrapper">

            <!-- Page Title Header Starts-->

            <div class="row page-title-header">

              <div class="col-12">

                <div class="page-header">

                  <h4 class="page-title">{{$page_title}}</h4>

                  <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">

                    <ul class="quick-links">

                      <li><a href="#">Dashboard</a></li>

                     

                    </ul>

                  

                  </div>

                </div>

              </div>

             

            </div>

            <!-- Page Title Header Ends-->

            <div class="row">

              <div class="col-md-12 grid-margin">

                  <?php
                      $video=$_GET['video'];
                  ?>
                  <!-- CSS  -->
                  <link href="https://vjs.zencdn.net/7.2.3/video-js.css" rel="stylesheet">


                  <!-- HTML -->
                  <video id='hls-example'  class="video-js vjs-default-skin" width="400" height="300" controls>
                  <source type="application/x-mpegURL" src="<?php echo $video; ?>">
                  </video>


                  <!-- JS code -->
                  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
                  <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
                  <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
                  <script src="https://vjs.zencdn.net/7.2.3/video.js"></script>

                  <script>
                  var player = videojs('hls-example');
                  player.play();
                  </script>

              </div>

            </div>

            



          </div>

          <!-- content-wrapper ends -->

  @endsection