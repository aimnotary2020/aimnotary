<!doctype html>
<html>
<head>
    @include('admin.includes.header')
</head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">    
        <!-- login-box -->
        @yield('content')
        <!-- /.login-box -->
        @include('admin.includes.footer')
    <script>
        $('.nav-link-custom').on('click',function(){
            $('body').toggleClass('downtown');
        }); 
    </script>
    @yield('script')
</body>
</html>