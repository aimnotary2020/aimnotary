<!doctype html>
<html>
<head>
    @include('admin.includes.login_header')
</head>
    <body class="hold-transition login-page">
    <!-- login-box -->
    @yield('content')
    <!-- /.login-box -->
    @include('admin.includes.login_footer')
    @yield('script')
    </body>
</html>