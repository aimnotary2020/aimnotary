<script>
    /* dropzone js */
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(
                                    "div#kyc_files", 
                                    { 
                                        url: '{{route("upload-temp")}}',
                                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                        maxFiles: 4,
                                        addRemoveLinks: true,
                                        autoProcessQueue: false,
                                        acceptedFiles: 'image/*,application/pdf',
                                        parallelUploads: 10
                                    }
                                );

    myDropzone.on("complete", function(file) {
        myDropzone.getQueuedFiles(file)
    });

    myDropzone.on('sending', function(file, xhr, formData){
        var doc_type = $('#doc-type').val();
        formData.append('account_id', account_id);
        formData.append('account_type', account_type);
        formData.append('doc_type', doc_type);
        formData.append('meta', $('#'+doc_type+'_no').val());
    });

    /* uplaod modal */
    $('.document-upload').on('click',function(){
        var doc_type = $(this).attr('data-type');
        $('#doc-type').val(doc_type); 
        $('#upload-modal').modal('show'); 
    });

    /* upload area */
    $('.upload-file').on('click',function(){
        var files = myDropzone.files;
        if(files.length > 0){
            myDropzone.processQueue();
        }
    });

    /* DYNAMIC FORM VALIDATION */
    $('.data-update').on('click',function(){
        var type = $(this).attr('data-type');
        if(type == 'kyc'){
            $('.'+type+'_form').validate({
                rules: {
                    gst_no: {
                        required: false,
                        pattern: false,
                    },
                    tin_no: {
                        required: false
                    },
                    pan_no: {
                        required: true
                    },
                    kyc_files: {
                        required: true
                    }
                },
                messages: {
                    gst_no: {
                        required: 'Please type GST no'
                    },
                    gst_no: {
                        required: true
                    },
                    gst_no: {
                        required: true
                    }
                },
                submitHandler: function() { 
                    var data  = $('.'+type+'_form').serializeArray();
                    dataObj  = {};
                    $(data).each(function(i, field){
                        dataObj[field.name] = field.value;
                    });

                    
                     var data = JSON.stringify(dataObj);
                     $.ajax({
                        url: '{{route("update-kyc")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                        },
                        data: {data,account_id,account_type,type},
                        complete: function(payload, xhr, settings){
                            if(payload.status == 200){
                                toastr.success('KYC updated');
                            }else{
                                toastr.error('Something went wrong, Please try again later.');
                            }
                        }
                     });
                }
            });
        }
        else if(type == 'additional_info'){
            $('.'+type+'_form').validate({
                rules: {
                    product_type: {
                                    required: {
                                        depends: function(element) {
                                        if(account_type <= 18 || account_type <= 40 || account_type > 46){
                                            return true;
                                        }else{
                                            return false;
                                        } 
                                    }
                                }
                        }
                },
                messages: {
                    product_type: {
                            required: 'Please select at least one product',
                    }
                },
                submitHandler: function() { 
                    var data  = $('.'+type+'_form').serializeArray();
                    dataObj      = {};
                    products       = [];
                    other_products = [];
                    services = [];
                    $(data).each(function(i, field){
                        if(field.name == 'product_type'){
                            products.push(field.value);
                        }
                        if(field.name == 'otherproduct_type'){
                            other_products.push(field.value);
                        }
                        if(field.name == 'service_type'){
                            services.push(field.value);
                        }
                    }); 
                    var dataArray = {
                        'products' : products,
                        'other_products' : other_products,
                        'services' : services,
                    };
                    var data = JSON.stringify(dataArray);
                    $.ajax({
                            url: '{{route("update-information-additional")}}',
                            type: 'POST',
                            headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                            },
                            data: {data,account_id,account_type,type},
                            complete: function(payload, xhr, settings){
                            if(payload.status == 200){
                                toastr.success('Additional information updated');
                            }else{
                                toastr.error('Something went wrong, Please try again later.');
                            }
                        }
                    });
                }

            });
        }
        else if(type == 'basic_information')
        {
            $('.'+type+'_form').validate({
                rules: {
                    account_name: {
                        required: true
                    },
                    contact_name: {
                        required: true
                    },
                    email: {
                        required: true
                    },
                },
                messages: {
                    account_name: {
                        required: 'Please enter account name'
                    },
                    contact_name: {
                        required: 'Please enter contact name'
                    },
                    email: {
                        required: 'Please enter email id'
                    },
                },
                submitHandler: function() { 
                    var data  = $('.'+type+'_form').serializeArray();
                    dataObj  = {};
                    $(data).each(function(i, field){
                        dataObj[field.name] = field.value;
                    });
                     var data = JSON.stringify(dataObj);
                     //console.log(data);
                     $.ajax({
                        url: '{{route("account-update-info")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                        },
                        data: {data,account_id,account_type,type},
                        complete: function(payload, xhr, settings){
                            //console.log(payload);
                            if(payload.status == 200){
                                toastr.success('Basic Information updated');
                            }else{
                                toastr.error('Something went wrong, Please try again later.');
                            }
                        }
                     });
                }
            });
        }
        else if(type == 'communication_details')
        {
            
            $('.'+type+'_form').validate({
                rules: {
                    "state[]":  {
                        required:true,
                    }
                },
                submitHandler: function() { 
                    var data  = $('.'+type+'_form').serializeArray();
                    dataObj  = [];var isdefault = [];
                    var object1 = {};


                    $(data).each(function(i, field)
                    {
                        if(field.name == 'address_id[]')
                        {
                            object1.address_id = field.value;
                        }
                        if(field.name == 'address_type[]')
                        {
                            object1.address_type = field.value;
                        }
                        if(field.name == 'address1[]')
                        {
                            
                            object1.address1 = field.value;
                        }
                        if(field.name == 'address2[]')
                        {
                            
                            object1.address2 = field.value;
                        }
                        if(field.name == 'nearlandmark[]')
                        {
                          
                            object1.nearlandmark = field.value;
                        }
                        if(field.name == 'state[]')
                        {
                            
                            object1.state = field.value;
                        }
                        if(field.name == 'district[]')
                        {
                            
                            object1.district = field.value;
                        }
                        if(field.name == 'block_id[]')
                        {
                           
                            object1.block_id = field.value;
                        }
                        if(field.name == 'city_name[]')
                        {
                            
                            object1.city_name = field.value;
                        }
                        if(field.name == 'pin[]')
                        {
                            
                            object1.pin = field.value;
                        }

                        if(field.name == 'home')
                        {
                            isdefault.push(field.value);
                        }
                        if(field.name == 'office')
                        {
                            isdefault.push(field.value);
                        }

                        if(field.name == 'counter')
                        {
                            isdefault.push(field.value);
                        }
                        if(field.name == 'godown')
                        {
                            isdefault.push(field.value);  
                        }
                        if(field.name == 'route[]')
                        {
                            //console.log(object1);
                            object1.route = field.value;
                            dataObj.push(object1);
                            //console.log(dataObj);
                            object1 = {};
                        }
                        // if(field.name == 'home' || field.name == 'office' || field.name == 'counter' || field.name == 'godown')
                        // {
                        //     console.log(12)
                        //     console.log(object1);
                        //     dataObj.push(object1);
                        //     object1 = {};
                        // }

                        //dataObj[field.name] = field.value;
                    });
                    
                     var data = JSON.stringify(dataObj);
                     var isdefaultval = JSON.stringify(isdefault);
                     //console.log(data);
                     $.ajax({
                        url: '{{route("account-communication-update")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                        },
                        data: {data,account_id,account_type,type,isdefaultval},
                        complete: function(payload, xhr, settings){
                            //console.log(payload);
                            if(payload.status == 200){
                                toastr.success('Address Information updated');
                            }else{
                                toastr.error('Something went wrong, Please try again later.');
                            }
                        }
                     });
                }
            });
        }
        else if(type == 'tagging_employee')
        {
            $('.'+type+'_form').validate({
                rules: {
                    
                },
                submitHandler: function() { 
                    var data  = $('.'+type+'_form').serializeArray();
                    dataObj  = [];
                    var object1 = {};
                    var j = 0;
                    $(data).each(function(i, field)
                    {
                        if(field.name == 'agm_emp')
                        {
                            object1.agm = field.value;
                        }
                        if(field.name == 'asm_emp')
                        {
                            object1.asm = field.value;
                        }
                        if(field.name == 'pri_so_emp')
                        {
                            
                            object1.primaryso = field.value;
                        }
                        if(field.name == 'me_emp')
                        {
                            
                            object1.me = field.value;
                        }
                        if(field.name == 'dto_emp')
                        {
                          
                            object1.dto = field.value;
                        }
                        if(field.name == 'second_so_emp[]')
                        {
                            object1.secondaryso = field.value;
                            j++;
                        }
                        if(j > 0)
                        {
                            dataObj.push(object1);
                            object1 = {};
                        }
                    });
                    
                     var data = JSON.stringify(dataObj);
                     //console.log(data);
                     $.ajax({
                        url: '{{route("account-tagging-update")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                        },
                        data: {data,account_id,account_type,type},
                        complete: function(payload, xhr, settings){
                            //console.log(payload);
                            if(payload.status == 200){
                                toastr.success('Employeee Tagging updated');
                            }else{
                                toastr.error('Something went wrong, Please try again later.');
                            }
                        }
                     });
                }
            });
        }
        else if(type == 'areaoperational')
        {
            $('.'+type+'_form').validate({
                rules: {
                    "operation_district[]":  {
                        required:true,
                    }
                },
                messages: {
                    "operation_district[]": {
                        required: 'Please select district'
                    },
                },
                submitHandler: function() { 
                    var data  = $('.'+type+'_form').serializeArray();
                    dataObj  = [];
                    var district = [];
                    var block = [];
                    var object1 = {};
                    var j = 0;
                    $(data).each(function(i, field)
                    {
                        if(field.name == 'operation_district[]')
                        {
                            district.push(field.value);
                        }
                        if(field.name == 'operation_block_id[]')
                        {
                            block.push(field.value);
                        }
                    });
                    object1.district    = district;
                    object1.block       = block;
                    dataObj.push(object1);
                    var data = JSON.stringify(dataObj);
                    $.ajax({
                    url: '{{route("account-areaoperation-update")}}',
                    type: 'POST',
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                'Authorization': 'Bearer '+ bearer
                    },
                    data: {data,account_id,account_type,type},
                    complete: function(payload, xhr, settings){
                        //console.log(payload);
                        if(payload.status == 200){
                            toastr.success('Area Operational updated');
                        }else{
                            toastr.error('Something went wrong, Please try again later.');
                        }
                    }
                    });
                }
            });
        }
    });

    /* datepicker */
    var today = new Date();
    $('.basic_datepicker').daterangepicker({
        timePicker: false,
        drops: 'down', //position
        singleDatePicker: true, // enables single month
        maxDate: '-1D',  //disables previous date
        showDropdowns: true,
        minYear: 1901,
        //maxDate: "+1Y",
        locale: {
            format: 'DD-MM-YYYY'
        }
    });


    /* tab render */
    $('.tab-class').on('click',function(){
        var url = $(this).attr('href');
        /* an loader */
        $.ajax({
            url: '{{route("get-tabs")}}',
            type: 'GET',
            headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                    },
            data: {url,account_id,account_type},
            complete: function(payload, xhr, settings){
                var newData = payload.responseJSON;
                if(url == '#documents'){
                    $('.append-class').html(''); //prevent duplicate append
                    var html = '';
                    $.each(newData, function(key,value,e){
                        var newoption = '';
                        $.each(value, function(i,value){
                            var type = value.document_type;
                            var pdf = '';
                            var image = '';
                            if(type == 'png' || type == 'jpeg' || type == 'jpg'){
                                var image = '<li ><i class="fas fa-image"></i><a href="javascript:void(0)" data-path="'+value.path+'" class="load-doc">'+value.document_name+'</a></li>';
                            }else{
                                var pdf = '<li ><i class="fas fa-file-pdf"></i><a href="javascript:void(0)" data-path="'+value.path+'" class="load-doc">'+value.document_name+'</a></li>';
                            }
                            newoption += image + pdf
                        });
                        var html = '<div class="user-block mt-1 mb-2"><span class="username ml-0"><a href="#">'+key.toUpperCase()+'</a></span></div><div class="col-sm-12"><div class="document_page_list"><ul class="doc-page-list">'+newoption+'</ul></div></div>';
                        $('.append-class').append(html);
                    });
                }else if(url == '#location'){
                    map_render(payload);
                }
                else if(url == '#activity')
                {
                    //console.log(newData);
                    var html = '<div class="timeline timeline-inverse">';
                    $.each(newData, function(key,value,e)
                    {
                        html += '<div class="time-label"><span class="bg-danger">'+key+'</span></div>';
                        $.each(value, function(i,value){
                            html+='<div>';
                            html+='<i class="'+value.icon+' bg-primary"></i>';
                            html+= '<div class="timeline-item">';
                            html+= '<span class="time"><i class="far fa-clock"></i> '+value.difference+'</span>';
                            html+=' <h3 class="timeline-header">'+value.message+'</h3>';
                            if(value.remarks == '')
                            {
                                html+='<div class="timeline-body">'+value.remarks+'</div>';
                            } 
                            html+= '</div>';               
                            html+='</div>';
                        });
                    });
                    html += '</div>';
                    $('#activity').html(html);
                }
            }
        });
    });

    /* load image */
    $(document).on('click','.load-doc',function(e){
        e.preventDefault();
        var image = $(this).attr('data-path');
        window.open(image, 'imgWindow');
    });

    /* filter address by type */
    $(document).on('click','.render-filter',function(){
        var address_type = $(this).attr('data-type');
        var url = '#location';
        $.ajax({
            url: '{{route("get-tabs")}}',
            type: 'GET',
            headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ bearer
                    },
            data: {url,account_id,account_type,address_type},
            complete: function(payload, xhr, settings){
                map_render(payload);
            }
        });
    });


    /* map render */
    function map_render(payload){
        var response = payload.responseJSON;
        //var types = response.types;
        // if(types.length > 0){
        //     $.each(types, function(i){
        //         //1 = Home, 2 = Office, 3 = Counter/Shop, 4 = Godown
        //         if(types[i] == 1){
        //             var TYPE = 'Home';
        //             var STYLE = 'background:#cd0005;color:#fff;';
        //         }else if(types[i] == 2){
        //             var TYPE = 'Office';
        //             var STYLE = 'background:#006acd;color:#fff;';
        //         }else if(types[i] == 3){
        //             var TYPE = 'Counter';
        //             var STYLE = 'background:#f98202;color:#fff;';
        //         }else if(types[i] == 4){
        //             var TYPE = 'Godown';
        //             var STYLE = 'background:#ffe65b;color:#fff;';
        //         }
        //         var html = '<a href="javascript:void(0)" data-type="'+types[i]+'" class="btn btn-success mr-2 mb-2 render-filter" style="'+STYLE+'">'+TYPE+'</a>';
        //         $('#types-address').append(html);
        //     });
        // }
        if(payload.status == 200){
            var map;
            var locations = response;
            var myOptions = {
                center: new google.maps.LatLng(23.5937,80.9629),
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("map"),myOptions);
            //setMarkers(map,locations);
            var marker, i
            for (i = 0; i < locations.length; i++){  
                var loan = locations[i][0]
                var lat = locations[i][1]
                var long = locations[i][2]
                var type = locations[i][3]
                //1 = Home, 2 = Office, 3 = Counter/Shop, 4 = Godown
                if(type == 1){
                    icon_path = '{{asset("public/assets/images/home-map.png")}}';
                }else if(type == 2){
                    icon_path = '{{asset("public/assets/images/office-map.png")}}';
                }
                else if(type == 3){
                    icon_path = '{{asset("public/assets/images/store-map.png")}}';
                }
                else if(type == 4){
                    icon_path = '{{asset("public/assets/images/godown-map.png")}}';
                }else{
                    icon_path = '{{asset("public/assets/images/default-map.png")}}';
                }
                latlngset = new google.maps.LatLng(lat, long);
                var marker = new google.maps.Marker({  
                    map: map, 
                    title: loan , 
                    position: latlngset,
                    icon: icon_path
                });
                map.setCenter(marker.getPosition())
                var content = loan;
                var infowindow = new google.maps.InfoWindow()
                google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
                    return function() {
                    infowindow.setContent(content);
                    infowindow.open(map,marker);
                    };
                })(marker,content,infowindow));
            }
        }else{
            var location = {lat: 23.5937 , lng: 80.9629};
            var map;
            map = new google.maps.Map(document.getElementById('map'), {
                center: location,
                zoom: 5
            });
            toastr.info('No address found');
        }

        /* scroll to section */
        $('html, body').animate({
                scrollTop: $("#map").offset().top
        }, 500);
    }

    /* tagged account */
    function fetch_data(){
        var dataTable =  $('#mapped-list').DataTable({
                            processing: true,
                            serverSide: true,
                            pageLength: 1,
                            ajax: {
                                url: '{{route("get-mapped-accounts")}}',
                                data: {account_id, account_type}
                            },
                            columns: [
                                { data: 'code'},
                                { data: 'name'},
                                { data: 'owner_name'},
                                { data: 'status'},
                                { data: 'sap_code'},
                                { data: 'action'},
                            ]
                        });
    }

    $('.tab-class-account').on('click',function(){
        $('#mapped-list').DataTable().destroy();
        fetch_data();
    })


      /************************** getting a list of block ***************************/
    $(document).on('change','.operation_district',function(event){
        var district_id = '';
        $('.operation_district').each(function() {
            // this should loop through all the selected elements
            district_id = $(this).val();
        });
        var state_id =  $("#operation_state").val();
        var type = 3;
        $.ajax({
            url: '{{route("get-block-by-district")}}',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ bearer
                },
            data: {state_id,district_id,type},
            success: function(data){
                var newData = JSON.parse(data);
                console.log(newData);
                var content = '<option></option>';
                $.each(newData, function(i, item) {
                    content =content+' <optgroup label="'+i+'">';
                    $.each(item, function(j, value) {
                        content =content+ '<option value="'+value.block_id+'">'+value.block_name+'</option>';
                    });
                    content = content +'</optgroup>';
                });
                $("#operation_block_id").html(content);
                $("#operation_block_id").val([]).trigger('change');
            }
        });
    });


    function routelist(state_id,district_id,block_id,position_id)
    {
        $.ajax({
            url: '{{route("get-route-by-block")}}',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ bearer
                },
            data: {state_id,district_id,block_id},
            success: function(data){
                var newData = JSON.parse(data);
                console.log(newData);
                var content = '<option value="0"></option>';
                $.each(newData, function(i, item) 
                {
                    content += '<option value="'+item.route_id+'">'+item.route_name+'</option>';
                });
                $("#route-"+position_id).html(content);
                $("#route-"+position_id).val([]).trigger('change');
            }
        });
    }

    address_shopwing('S',0);
    $('#add_more_button').on('click',function(){
        var total_address     = $("#total_address").val();
        var new_address_count = parseInt(total_address)+1;
        $("#total_address").val(new_address_count);
        address_shopwing('A',new_address_count);
    });
 
  function address_shopwing(fun_type,position){
      $.ajax({
              url: '{{route("account-address-show")}}',
              type: 'POST',
              headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': 'Bearer '+ bearer
              },
              data: {account_id, account_type,fun_type,position},
              success: function(data){

              },
              complete: function(payload, xhr, settings){
                 //console.log(payload);
                 if(fun_type == 'S')
                 {
                    $("#address_content_show").html(payload.responseText);
                 }
                 else
                 {
                  $("#address_content_show").append(payload.responseText);
                 }
              }
        });
  }
</script>