        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{$page_title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                @if(Route::current()->getName() == 'get-lead-information')
                 <li class="breadcrumb-item"><a href="{{ route('lead-list') }}">Manage Lead</a></li>
                 @endif
                @if(Route::current()->getName() == 'get-user-profile2' && Auth::user()->id == 1)
                    <li class="breadcrumb-item"><a href="{{ route('user-list') }}">Users</a></li>
                @endif
                <li class="breadcrumb-item active">{{$page_title}}</li>
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div>