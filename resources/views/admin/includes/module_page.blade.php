<div class="modal fade" id="assign_module_modal">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Assign Module Section</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <ul class="js-errors"></ul>
                    <form role="form" name="assign-module-form" id="assign-module-form" novalidate="novalidate" action="javascript:void(0)" method="post">
                    <input type="hidden" id="module_group_id" value="" name="module_group_id" />
                    <input type="hidden" id="process_type" value="1" name="process_type" />
                    <div class="form-group clearfix row">
                    @foreach($moduleinfo as $key => $value)
                        <div class="col-sm-12 mb-3 mb-sm-0">
                            <div class="icheck-primary d-inline">
                                <input class="moduleclass" type="checkbox" name="buttoncheckbox[]" id="buttoncheckbox{{$value->menu_id}}" value="{{$value->menu_id}}">
                                <label for="buttoncheckbox{{$value->menu_id}}">
                               {{$value->menu_name}}
                                </label>
                                @if (count($value->group_parent) > 0 && $value->menu_permission == 1)
                                <i class="fas fa-angle-double-right" aria-hidden="true"></i>
                                @endif
                            </div>
                            @if (count($value->group_parent) > 0 && $value->menu_permission == 1)
                                @foreach($value->group_parent as $pkey => $pvalue)
                                    <div class="icheck-primary d-inline">
                                        <input class="childclass pclass{{$value->menu_id}}" type="checkbox" name="buttoncheckbox[]" id="buttoncheckbox{{$pvalue->menu_id}}" value="{{$pvalue->menu_id}}" disabled>
                                        <label for="buttoncheckbox{{$pvalue->menu_id}}">
                                            {{$pvalue->button_info[0]->button_name}}
                                        </label>
                                    </div>
                                @endforeach
                            @endif
                            <hr>
                        </div>
                        <div class="col-sm-12 mb-6 mb-sm-0">
                        @if (count($value->group_parent) > 0 && $value->menu_permission == 0)
                        @foreach($value->group_parent as $pkey => $pvalue)
                        <div class="row">
                                <div class="col-sm-6 mb-3 mb-sm-0" style="left:25px;">
                                    <div class="icheck-primary d-inline">
                                        <input class="childclass pclass{{$value->menu_id}}" type="checkbox" name="buttoncheckbox[]" id="buttoncheckbox{{$pvalue->menu_id}}" value="{{$pvalue->menu_id}}" disabled>
                                        <label for="buttoncheckbox{{$pvalue->menu_id}}">
                                            {{$pvalue->menu_name}}
                                        </label>
                                        <i class="fas fa-angle-double-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                @if (count($pvalue->group_parent) > 0 && $pvalue->menu_permission == 1)
                                    @foreach($pvalue->group_parent as $ckey => $cvalue)
                                        <div class="icheck-primary d-inline">
                                            <input class="mclass{{$value->menu_id}} cclass{{$pvalue->menu_id}} cclass" type="checkbox" name="buttoncheckbox[]" id="buttoncheckbox{{$cvalue->menu_id}}" value="{{$cvalue->menu_id}}" disabled>
                                            <label for="buttoncheckbox{{$cvalue->menu_id}}">
                                                {{$cvalue->button_info[0]->button_name}}
                                            </label>
                                        </div>
                                    @endforeach
                                @endif
                                </div>
                        </div>
						    <hr>
                        @endforeach
                        @endif
						</div>
                    @endforeach
                        
                        
                    </div>
                    <div class="justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary right" id="add_group_form">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
    </div>
   
   
    