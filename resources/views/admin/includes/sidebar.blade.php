<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="profile-image">
                  <img class="img-xs rounded-circle" src="{{asset('/public/assets/images/admin.png')}}" alt="profile image">
                  <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper">
                  <p class="profile-name">{{Auth::user()->name}}</p>
                  <!-- <p class="designation">Premium user</p> -->
                </div>
              </a>
            </li>
            <li class="nav-item nav-category">Main Menu</li>
            @if(Auth::user()->user_type=='Admin')
            <li class="nav-item">
              <a class="nav-link" href="{{ route('Dashboard') }}">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title" >Dashboard</span>
              </a>
            </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('about-us') }}">
                  <i class="menu-icon typcn typcn-shopping-bag"></i>
                  <span class="menu-title">Content Management</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/meta-title-management/0') }}">
                  <i class="menu-icon typcn typcn-shopping-bag"></i>
                  <span class="menu-title">Meta Title Management</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('blog-list') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title"> Blog List</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('lawyer-list') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title"> Lawyer List</span>
                </a>
              </li>
               <li class="nav-item">
                <a class="nav-link" href="{{ route('user-list') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title"> User List</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('website_traffic') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title"> Website Traffic</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('enterprise-user-list') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title">Enterprise User List</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('schedule-appointment') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title">Scheduled Appointment</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('transaction') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title">Transaction List</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('couponlist') }}">
                  <i class="menu-icon typcn typcn-th-large-outline"></i>
                  <span class="menu-title">Coupon List</span>
                </a>
              </li>
              
              
              <li class="nav-item">
                  <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon typcn typcn-document-add"></i>
                    <span class="menu-title">Notarization</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="collapse" id="auth">
                    <ul class="nav flex-column sub-menu">
                      <li class="nav-item">
                        <a class="nav-link" href="{{ url('pending-document').'/1' }}">Pending </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ url('completed-document').'/2' }}"> Completed </a>
                      </li>
                    </ul>
                  </div>
                </li>
              @endif

              @if(Auth::user()->user_type=='Lawyer')
                <!-- <li class="nav-item">
                  <a class="nav-link" href="{{ url('user-document').'/1' }}">
                    <i class="menu-icon typcn typcn-th-large-outline"></i>
                    <span class="menu-title"> User Document</span>
                  </a>
                </li> -->

                <li class="nav-item">
                  <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                    <i class="menu-icon typcn typcn-document-add"></i>
                    <span class="menu-title">Notarization</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div class="collapse" id="auth">
                    <ul class="nav flex-column sub-menu">
                      <li class="nav-item">
                        <a class="nav-link" href="{{ url('pending-document').'/1' }}">Pending </a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ url('completed-document').'/2' }}"> Completed </a>
                      </li>
                    </ul>
                  </div>
                </li>
                
                
               <!--  <li class="nav-item">
                  <a class="nav-link" href="{{ route('user-video-call') }}">
                    <i class="menu-icon typcn typcn-th-large-outline"></i>
                    <span class="menu-title">Video Call Attended</span>
                  </a>
                </li> -->
              @endif
          </ul>
        </nav>