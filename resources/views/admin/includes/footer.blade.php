 <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="container-fluid clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © {{date('Y')}} <a href="javascript:void(0);" target="_blank">Aimnotary</a>. All rights reserved.</span>
              <!-- <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i>
              </span> -->
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('public/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('public/assets/vendors/js/vendor.bundle.addons.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{asset('public/assets/js/shared/off-canvas.js')}}"></script>
    <script src="{{asset('public/assets/js/shared/misc.js')}}"></script>
     <!-- overlayScrollbars -->
    <script src="{{asset('public/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
     <script src="{{asset('public/assets/plugins/datatables/jquery.dataTables.js')}}"></script>
   
    <script src="{{asset('public/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{asset('public/assets/js/demo_1/dashboard.js')}}"></script>
    <!-- End custom js for this page-->
    <script src="{{asset('public/assets/plugins/toastr/toastr.min.js')}}"></script>
    <!-- <script src="{{asset('public/assets/dist/js/pages/account-validation.js')}}"></script> -->
    
    <script src="{{asset('public/assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/jquery-validation/additional-methods.min.js')}}"></script>
    <script src="{{asset('public/assets/plugins/select2/js/select2.full.min.js')}}"></script>
    <script type="text/javascript">
         function logout() {
             setTimeout(function(){ <?php if(Auth::user()->user_type=='Lawyer'){ ?> window.location='{{ url("/lawyer/login") }}'; <?php }else{ ?> window.location='{{ url("/admin/login") }}'; <?php } ?>  },300);
               $.ajax({
                url: '{{route("AdminLogout")}}',
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
               
                data: {},
                success:function(res){
                  //window.location='{{ route("login") }}';
                }
            });
         }
    </script>