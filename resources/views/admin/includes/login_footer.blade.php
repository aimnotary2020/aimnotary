
    <script src="{{asset('public/assets/plugins/toastr/toastr.min.js')}}"></script>


     <!-- plugins:js -->
<!--     <script src="{{asset('/public/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('/public/assets/vendors/js/vendor.bundle.addons.js')}}"></script> -->
    <!-- endinject -->
    <!-- inject:js -->
    <script src="{{asset('/public/assets/js/shared/off-canvas.js')}}"></script>
    <script src="{{asset('/public/assets/js/shared/misc.js')}}"></script>
    <!-- endinject -->
    <script>
        $('#sign_in').on('click',function()
        {
            var username  = $('#username').val();
            var password  = $('#password').val();
            var remember  = 0;
            if($("#remember").prop("checked") == true){
                remember = 1;
            }
            $.ajax({
                url: '{{route("check_login")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {username, password,remember},
                error: function(xhr){ 
                    
                    if(xhr.status==500){ toastr.success('Login Success!.'); window.location.href = '{{ url("admin/")."/" }}'; } 
                },
                
                success:function(data){
                    console.log(data);
                    
                    var newData = JSON.parse(data);
                    if(newData.status == 200){
                        toastr.success('Login Success!.');
                        window.location.href = '{{ url("admin/")."/" }}';
                    }else if(newData.status == 403){
                        toastr.error('Invalid username & passwrd.');
                    }else if(newData.status == 404){
                        toastr.error('User not found');
                    }else{
                         window.location.href = '{{ url("admin/")."/" }}';
                    }
                }
            });
        });
    </script>
