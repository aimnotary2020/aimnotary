    <aside class="control-sidebar control-sidebar-dark">
        <div class="" style="height: 100px;">
            <div class="os-resize-observer-host">
                <div class="os-resize-observer observed" style="left: 0px; right: auto;"></div>
            </div>
            <div class="os-size-auto-observer" style="height: calc(100% + 1px); float: left;">
                <div class="os-resize-observer observed"></div>
            </div>
            <div class="os-content-glue" style="margin: -16px; width: 249px; height: 251px;"></div>
            <div class="os-padding">
                <div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;">
                    <div class="os-content" style="padding: 16px; height: 100%; width: 100%;">
                    <h5>Profile</h5>
                    <hr class="mb-2">
                    <div class="mb-1"><a href="javascript:void(0)" data-toggle="modal" data-target="#modal-lg"><i class="fas fa-key"></i> <span>Change password</span></a></div>
                    <div class="mb-1"><a href="{{route('logout')}}"><i class="fas fa-sign-out-alt"></i> <span>Logout</span></a></div>
                </div>
            </div>
            <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden">
                <div class="os-scrollbar-track">
                    <div class="os-scrollbar-handle" style="transform: translate(0px, 0px); width: 100%;"></div>
                </div>
            </div>
            <div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden">
                <div class="os-scrollbar-track">
                    <div class="os-scrollbar-handle" style="transform: translate(0px, 0px); height: 19.5046%;"></div>
                </div>
            </div>
            <div class="os-scrollbar-corner"></div>
        </div>
    </aside>