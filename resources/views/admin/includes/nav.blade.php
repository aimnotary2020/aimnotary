      <style>
        /* .myFileStyle:hover
        {
            background-color : #ccc;
            opacity : 0.2;
            border-radius : 2px 2px 2px 2px;
        } */
        .myFileStyle
        {
           padding : 10px;
        }
        .upload-btn-wrapper {
            position: relative;
            overflow: hidden;
            display: inline-block;
            cursor : pointer;
          }

          #uploadBtn {
            color: white;
            background-color: #1c45ef;
            padding: 8px 20px;
            border-radius: 0px;
            font-size: 20px;
            font-weight: bold;
            cursor : pointer;
          }

          .upload-btn-wrapper input[type=file] {
            font-size: 100px;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
            cursor : pointer;
          }
          /* Dropdown Button */
          .dropbtn {
            background-color: #3498DB;
            color: white;
            padding: 3px;
            font-size: 16px;
            border: none;
            cursor: pointer;
            position : absolute;
          }

          /* Dropdown button on hover & focus */
          .dropbtn:hover, .dropbtn:focus {
            background-color: #2980B9;
          }

          /* The container <div> - needed to position the dropdown content */
          .dropdown {
            position: relative;
            display: inline-block;
          }

          /* Dropdown Content (Hidden by Default) */
          .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 120px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
          }

          /* Links inside the dropdown */
          .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
          }

          /* Change color of dropdown links on hover */
          .dropdown-content a:hover {background-color: #ddd}

          /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
          .show {display:block;}
        
      </style>
      <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
          <a class="navbar-brand brand-logo" href="javascript:void(0)">
            <img src="{{asset('/public/assets/images/logo.png')}}" alt="logo" /> </a>
          <a class="navbar-brand brand-logo-mini" href="index.html">
            <img src="{{asset('/public/assets/images/logo.png')}}" alt="logo" /> </a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
          <ul class="navbar-nav">
            <li class="nav-item font-weight-semibold d-none d-lg-block">Help : 8593601320</li>
           <!--  <li class="nav-item dropdown language-dropdown">
              <a class="nav-link dropdown-toggle px-2 d-flex align-items-center" id="LanguageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <div class="d-inline-flex mr-0 mr-md-3">
                  <div class="flag-icon-holder">
                    <i class="flag-icon flag-icon-us"></i>
                  </div>
                </div>
                <span class="profile-text font-weight-medium d-none d-md-block">English</span>
              </a>
              <div class="dropdown-menu dropdown-menu-left navbar-dropdown py-2" aria-labelledby="LanguageDropdown">
                <a class="dropdown-item">
                  <div class="flag-icon-holder">
                    <i class="flag-icon flag-icon-us"></i>
                  </div>English
                </a>
                <a class="dropdown-item">
                  <div class="flag-icon-holder">
                    <i class="flag-icon flag-icon-fr"></i>
                  </div>French
                </a>
                <a class="dropdown-item">
                  <div class="flag-icon-holder">
                    <i class="flag-icon flag-icon-ae"></i>
                  </div>Arabic
                </a>
                <a class="dropdown-item">
                  <div class="flag-icon-holder">
                    <i class="flag-icon flag-icon-ru"></i>
                  </div>Russian
                </a>
              </div>
            </li> -->
          </ul>
         <!--  <form class="ml-auto search-form d-none d-md-block" action="#">
            <div class="form-group">
              <input type="search" class="form-control" placeholder="Search Here">
            </div>
          </form> -->
          <ul class="navbar-nav ml-auto">
          <!--   <li class="nav-item dropdown">
              <a class="nav-link count-indicator" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <i class="mdi mdi-bell-outline"></i>
                <span class="count">7</span>
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="messageDropdown">
                <a class="dropdown-item py-3">
                  <p class="mb-0 font-weight-medium float-left">You have 7 unread mails </p>
                  <span class="badge badge-pill badge-primary float-right">View all</span>
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <img src="{{asset('/public/assets/images/faces/face10.jpg')}}" alt="image" class="img-sm profile-pic"> </div>
                  <div class="preview-item-content flex-grow py-2">
                    <p class="preview-subject ellipsis font-weight-medium text-dark">Marian Garner </p>
                    <p class="font-weight-light small-text"> The meeting is cancelled </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <img src="{{asset('/public/assets/images/faces/face12.jpg')}}" alt="image" class="img-sm profile-pic"> </div>
                  <div class="preview-item-content flex-grow py-2">
                    <p class="preview-subject ellipsis font-weight-medium text-dark">David Grey </p>
                    <p class="font-weight-light small-text"> The meeting is cancelled </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <img src="{{asset('/public/assets/images/faces/face1.jpg')}}" alt="image" class="img-sm profile-pic"> </div>
                  <div class="preview-item-content flex-grow py-2">
                    <p class="preview-subject ellipsis font-weight-medium text-dark">Travis Jenkins </p>
                    <p class="font-weight-light small-text"> The meeting is cancelled </p>
                  </div>
                </a>
              </div>
            </li> -->
           @if(Auth::user()->user_type=="Lawyer")
            <li class="nav-item dropdown">
              <a onclick="getDocumentList(); " data-toggle="modal" data-target="#myModalDocumentUpload" class="nav-link count-indicator" id="notificationDropdown" href="#" data-toggle="dropdown">
                <span class="btn btn-info btn-sm">Upload Additional Documents <i class="fa fa-upload"></i></span>
              </a>
            </li>
            
            @endif
            <li class="nav-item dropdown d-none d-xl-inline-block user-dropdown">
              <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <img class="img-xs rounded-circle" src="{{asset('/public/assets/images/admin.png')}}" alt="Profile image"> </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                  <img class="img-md rounded-circle" src="{{asset('/public/assets/images/admin.png')}}" alt="Profile image">
                  <p class="mb-1 mt-3 font-weight-semibold">{{Auth::user()->name}}</p>
                  <p class="font-weight-light text-muted mb-0">{{Auth::user()->email}}</p>
                </div>
               <!--  <a class="dropdown-item">My Profile <span class="badge badge-pill badge-danger">1</span><i class="dropdown-item-icon ti-dashboard"></i></a>
                <a class="dropdown-item">Messages<i class="dropdown-item-icon ti-comment-alt"></i></a>
                <a class="dropdown-item">Activity<i class="dropdown-item-icon ti-location-arrow"></i></a>
                <a class="dropdown-item">FAQ<i class="dropdown-item-icon ti-help-alt"></i></a> -->
                <!-- <a class="dropdown-item" href="javascript:void(0);" onclick="logout();">Sign Out<i class="dropdown-item-icon ti-power-off"></i></a> -->
                <a class="dropdown-item" href="{{ url('/notary/logout') }}">Sign Out<i class="dropdown-item-icon ti-power-off"></i></a>
                
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>



      <div id="myModalDocumentUpload" style="width: 80% !important;" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content" style="width : 800px;">
            <div class="modal-header" style="border-radius : 0px; background-color : #1c45ef">
              <h4 class="modal-title" style="color : white;">Upload Additional Documents</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="container">
                <div class="row">
                    <div class="col-md-3" style="height : auto; border-right : 1px solid #ccc;">
                      
                      <form id="fupForm" enctype="multipart/form-data">
                        <div class="upload-btn-wrapper">
                          <label>Upload only .pdf files<label>
                          <button id="uploadBtn" class="btn">Upload a file</button>
                          <input type="file" id="file" name="files[]" multiple />
                        </div>
                      </form>
                      
                    </div>
                    <div class="col-md-9">
                      <div id="uploadedDocument"></div>
                    </div>                
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>



     

      <script>
        function deleteOption(fid)
        {
          
          swal({
              title: "Are you sure?",
              text: "Once deleted, you will not be able to recover this pdf file!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            })
            .then((willDelete) => {
              if (willDelete) {
                  var fileName = $("#getDataVal"+fid).data('value');
                  $.ajax({
                      type: 'GET',
                      url: "{{ url('/deletDocument?file=') }}"+fileName,
                      data: {},
                      dataType: 'json',
                      contentType: false,
                      cache: false,
                      processData:false,
                      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                      success: function(response){
                          getDocumentList();
                      },
                      error : function(data){
                        getDocumentList();
                      }
                  });
              } else {
                
              }
            });
        }
        function getDocumentList()
        {
            $("#uploadedDocument").html("<div align='center'><img style='width : 50px;marign : 0 auto;' src='https://thumbs.gfycat.com/KlutzyMadeupHarpseal-max-1mb.gif' /></div>");
            $.ajax({
                url: '{{ url("/get-documents-list") }}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {},
                success:function(res){
                    $("#uploadedDocument").html(res);
                }
            });
        }

        $(document).ready(function(){
          // Submit form data via Ajax
          $("#fupForm").on('change', function(e){
              e.preventDefault();
              $("#uploadedDocument").html("<div align='center'><img style='width : 50px;marign : 0 auto;' src='https://thumbs.gfycat.com/KlutzyMadeupHarpseal-max-1mb.gif' /> Document Uploading Please Wait</div>");
              // <div align='center'><img style='width : 50px;marign : 0 auto;' src='https://thumbs.gfycat.com/KlutzyMadeupHarpseal-max-1mb.gif' /></div>
              $.ajax({
                  type: 'POST',
                  url: "{{ url('/uploadRepositoryDocument') }}",
                  data: new FormData(this),
                  dataType: 'json',
                  contentType: false,
                  cache: false,
                  processData:false,
                  headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                  beforeSend: function(){
                      $('#fupForm').css("opacity",".5");
                  },
                  success: function(response){
                      console.log(response);
                      var obj=JSON.parse(JSON.stringify(response));
                      if(obj.status==0)
                      {
                        swal("Oops!", obj.msg, "error");
                      }
                      else
                      {
                        swal("Thank You", obj.msg, "success");
                      }
                      $('#fupForm').css("opacity","1");
                      getDocumentList();
                  }
              });
          });
        
          // File type validation
          var match = ['application/pdf'];
          $("#file").change(function() {
              for(i=0;i<this.files.length;i++){
                  var file = this.files[i];
                  var fileType = file.type;
            
                  if(!((fileType == match[0]) || (fileType == match[1]) || (fileType == match[2]) || (fileType == match[3]) || (fileType == match[4]) || (fileType == match[5]))){
                      swal("Oops!", "Sorry, only PDF files are allowed to upload.", "error");
                      $("#file").val('');
                      return false;
                  }
              }
          });
      });

      /* When the user clicks on the button, 
      toggle between hiding and showing the dropdown content */
      function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
      }

      // Close the dropdown if the user clicks outside of it
      window.onclick = function(event) {
        if (!event.target.matches('.dropbtn')) {
          var dropdowns = document.getElementsByClassName("dropdown-content");
          var i;
          for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
              openDropdown.classList.remove('show');
            }
          }
        }
      }
        
               
      </script>

      get-documents-list