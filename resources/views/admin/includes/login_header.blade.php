
     <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$page_title}}</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('/public/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/public/assets/vendors/iconfonts/ionicons/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{asset('/public/assets/vendors/iconfonts/typicons/src/font/typicons.css')}}">
    <link rel="stylesheet" href="{{asset('/public/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
   <!--  <link rel="stylesheet" href="{{asset('/public/assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('/public/assets/vendors/css/vendor.bundle.addons.css')}}"> -->
    <link rel="stylesheet" href="{{asset('/public/assets/plugins/toastr/toastr.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('/public/assets/css/shared/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('/public/assets/images/favicon.png')}}" />
     <script src="{{asset('/public/assets/plugins/jquery/jquery.min.js')}}"></script>
