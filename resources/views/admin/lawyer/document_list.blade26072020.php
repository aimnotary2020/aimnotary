@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-md-12">
                        <div class="d-flex pull-right">
                          <a href="javascript:void(0);" onclick="get_dtl('');">+ Add New</a>
                        </div>
                      </div>
                     
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Email ID</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Document</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
          </div>
  @endsection
  @section('script')

<script>
    fetch_data();
    click_me();
    function fetch_data(){
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route("fetch_user_documents")}}',
                columns: [
                    { data: 'name'},
                    { data: 'email'},
                    { data: 'country'},
                    { data: 'city'},
                    { data: 'doc'},
                    { data: 'action'}
                ]
            });
           
    }
    
    function send_video_link(id,user_id,name,email) {
         //var link='{{ url("start-video")."/".time()}}';
          $.ajax({
                url: '{{url("start_call")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
                data: {id,user_id,name,email},
                success:function(res){
                  var obj=JSON.parse(res);
                  if(obj.status==200){
                      toastr.success(obj.message);
                   }else if(obj.status==201){
                      toastr.error(obj.message);
                   }
                   $('#grouplist').DataTable().destroy();
                   fetch_data();
                }
            });
    }
    function click_me() {
          var myWindow;
            myWindow=window.open(
        'https://www.aimnotary.com/signer/',
        '_blank' // <- This is what makes it open in a new window.
      );
      setTimeout(function(){ myWindow.close(); }, 3000);
        }
    </script>
@endsection