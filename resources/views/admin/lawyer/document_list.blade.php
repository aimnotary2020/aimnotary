<script src="https://www.gstatic.com/firebasejs/3.5.3/firebase.js"></script>
<script>
  const firebaseConfig = {
                            apiKey: "AIzaSyCQu4sc2UN_9vhLyLrgCs1NM0hTrthB6Uo",
                            authDomain: "aimnotary-project-001.firebaseapp.com",
                            databaseURL: "https://aimnotary-project-001-default-rtdb.firebaseio.com",
                            projectId: "aimnotary-project-001",
                            storageBucket: "aimnotary-project-001.appspot.com",
                            messagingSenderId: "109700331601",
                            appId: "1:109700331601:web:ca271a82b8f179db842070",
                            measurementId: "G-R15X8TG245"
                        };
                        firebase.initializeApp(firebaseConfig);
                        firebase.auth().signInAnonymously().catch(error => {
                            if (error.code === 'auth/operation-not-allowed') {
                                alert('You must enable Anonymous auth in the Firebase Console.');
                            } else {
                                console.error(error);
                            }
                        });
                        var database = firebase.database();
</script>
@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            @if(Auth()->user()->user_type=='Admin')
            {{-- <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-md-12">
                        <div class="d-flex pull-right">
                          <a href="javascript:void(0);" onclick="get_dtl('');">+ Add New</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> --}}
            @endif
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif
                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Full Name 
                                    <?php  $time = new DateTime('now', new DateTimeZone('UTC'));
                                           $aa = $time->setTimezone(new DateTimeZone('GMT-4'));   
                                           //echo $aa->date;?>
                                    </th>
                                    <th>Email ID</th>
                                    <th>Country</th>
                                    <th>City</th>
                                    <th>Document</th>
                                    <th>Date</th>
                                    @if(Auth()->user()->user_type=='Admin')
                                      <th>Lawyer Detail</th>
                                    @endif
                                    @if(Auth()->user()->user_type=='Lawyer')
                                      <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            </div>
          </div>


          <div class="modal" id="appointment">
            <div class="modal-dialog" style="max-width : 1000px !important;">
                <div class="modal-content">
                    <div class="modal-body">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td>Image</td>
                                  <td>Name</td>
                                  <td>Email</td>
                                  <td>Contact No.</td>
                                  <td>Priority</td>
                                  <td>NAA Cerification</td>
                                  <td>Completed Docs</td>
                                  <td>Action</td>
                              </tr>
                            </thead>
                            <tbody id="alllawyer">
                            
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>



          <div id="myModalMerge" style="width: 80% !important;" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content" style="width : 800px;">
              <form action='{{ url("/mergerDocuments") }}' method='post' id='mergeDocument'>
                  <div class="modal-header" style="border-radius : 0px; background-color : #1c45ef">
                    <h4 class="modal-title" style="color : white;">Merge Additional Documents</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                  </div>
                  <div class="modal-body">
                    <div class="container">
                      <div class="row">
                          <div class="col-md-12">
                            
                              @csrf
                              <div id="uploadedDocumentGet"></div>
                             
                            
                          </div>                
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <input type='submit' class="btn btn-info" value='Merge Documents' />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
                </form>
              </div>
            </div>
          </div>


  @endsection
  @section('script')

<script>
    fetch_data();
    @if(Auth()->user()->user_type=='Lawyer')
      click_me();
    @endif
    function fetch_data(){
          @if(Auth()->user()->user_type=='Admin')
            var dataTable =  $('#grouplist').DataTable({
                processing: false,
                serverSide: true,
                orderable : false,
                ajax: '{{ url("fetch_user_documents")."/".$type_id }}',
                columns: [
                    { data: 'name', orderable: false},
                    { data: 'email', orderable: false},
                    { data: 'country', orderable: false},
                    { data: 'city', orderable: false},
                    { data: 'doc', orderable: false},
                    { data: 'dateTime', orderable : false },
                    { data: 'lawyer_detail', orderable: false}
                ],
                drawCallback: function(settings) {
                    // for(i=0;i<=settings.json.data.length;i++){
                    //     var onoff = database.ref('users_online_'+settings.json.data[i].id);
                    //     if(onoff){
                    //         onoff.on('value', (snapshot2) => {
                    //           const data2 = snapshot2.val();
                    //           if(data2['online'] == 1){
                    //               document.getElementById('online_offline_'+settings.json.data[i].id).style.color = "green";
                    //               document.getElementById('online_offline_'+settings.json.data[i].id).innerHTML = "<b>User Online</b>";
                    //           }else{
                    //               document.getElementById('online_offline_'+settings.json.data[i].id).style.color = "red";
                    //               document.getElementById('online_offline_'+settings.json.data[i].id).innerHTML = "<b>User Disconnected</b>";
                    //           }
                    //       });
                    //     }else{
                    //       document.getElementById('online_offline_'+settings.json.data[i].id).style.color = "Orange";
                    //       document.getElementById('online_offline_'+settings.json.data[i].id).innerHTML = "<b>No Data Available</b>";
                    //     }
                    // }
                },
            }); 
            setInterval( function () {
              dataTable.ajax.reload( null, false);
            }, 10000 );
        @else  
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                orderable : false,
                ajax: '{{ url("fetch_user_documents")."/".$type_id }}',
                columns: [
                    { data: 'name', orderable: false},
                    { data: 'email', orderable: false},
                    { data: 'country', orderable: false},
                    { data: 'city', orderable: false},
                    { data: 'doc', orderable: false},
                    { data: 'dateTime', orderable : false },
                    { data: 'action', orderable: false}
                ]
            });   
        @endif
    }
    
    function send_video_link(id,user_id,name,email) {
         //var link='{{ url("start-video")."/".time()}}';
          $.ajax({
                url: '{{url("start_call")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
                data: {id,user_id,name,email},
                success:function(res){
                  var obj=JSON.parse(res);
                  if(obj.status==200){
                      toastr.success(obj.message);
                   }else if(obj.status==201){
                      toastr.error(obj.message);
                   }
                   $('#grouplist').DataTable().destroy();
                   fetch_data();
                }
            });
    }
    function click_me() {
      var myWindow;
        	  myWindow=window.open(
			  'https://www.aimnotary.com/signer/',
			  '_blank', // <- This is what makes it open in a new window.
			  'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,top=1000000,width=20,height=20'
			);
			setTimeout(function(){ myWindow.close(); }, 1000);
    }


    function getMergeDocumentList(doc_id)
    {
        $("#uploadedDocumentGet").html("<div align='center'><img style='width : 50px;marign : 0 auto;' src='https://thumbs.gfycat.com/KlutzyMadeupHarpseal-max-1mb.gif' /></div>");
          $.ajax({
              url: '{{ url("/get-merge-documents-list?doc_id=") }}'+doc_id,
              type: 'POST',
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              data: {},
              success:function(res){
                  $("#uploadedDocumentGet").html(res);
              }
        });
    }
    function OpenAssignModal(doc_id,lawyer_id=0){
        $("#alllawyer").html("<tr align='center'><td colspan='8'><img src='https://www.aimnotary.com/public/loader.gif'/></td></tr>");
        $.ajax({
            url: '{{ url("user-list/fetch_lawyer_docs") }}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: "doc_id="+doc_id+"&lawyer_id="+lawyer_id,
            success: function(data){
                $("#alllawyer").html("");
                $("#alllawyer").html(data);
            }
        });
          $("#appointment").modal('show');
      }

      function makeAssign(lid,userlid,doc_id){
        $.ajax({
            url: '{{ url("user-list/assign-user") }}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: "lawyer_id="+lid+"&lawyer_user_id="+userlid+"&doc_id="+doc_id,
            success: function(data){
                swal({
                    title: "Thank You!",
                    text: "Notary Assigned Successfully",
                    type: "success"
                }).then(function() {
                    location.reload();
                });
            }
        });
     }

     function makeAssignDocs(lid,userlid,doc_id){
        $.ajax({
            url: '{{ url("user-list/assign-user-docs") }}',
            type: 'POST',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: "lawyer_id="+lid+"&lawyer_user_id="+userlid+"&doc_id="+doc_id,
            success: function(data){
                swal({
                    title: "Thank You!",
                    text: "Notary Assigned Successfully",
                    type: "success"
                }).then(function() {
                    location.reload();
                });
            }
        });
     }

    </script>

    <?php
       if(Auth()->user()->user_type=='Admin'){

      }
    ?>

@endsection