@extends('admin.layouts.app')
@section('content')
 <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      
       @include('admin.includes.nav')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
         @include('admin.includes.sidebar')
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <!-- Page Title Header Starts-->
            <div class="row page-title-header">
              <div class="col-12">
                <div class="page-header">
                  <h4 class="page-title">{{$page_title}}</h4>
                  <!-- <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap"> -->
                    <ul class="quick-links">
                      <li><a href="{{route('Dashboard')}}">Dashboard</a></li>
                     
                    </ul>
                  
                  <!-- </div> -->
                </div>
              </div>
             
            </div>
            <!-- Page Title Header Ends-->
            <div class="row">
              <div class="col-md-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-lg-12 col-md-12">
                        <div class="d-flex pull-right">
                          <a href="javascript:void(0);" onclick="get_dtl('');">+ Add New</a>
                        </div>
                      </div>
                     
                     
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">

                        <table id="grouplist" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    
                                    <th>Full Name</th>
                                    <th>Email ID</th>
                                    <th>Contact No.</th>
                                    <th>Profile <br> Image</th>
                                    <th>NNA Certified</th>
                                    <th>Total <br> Experience</th>
                                    <th>Priority</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                  </div>
              </div>
            
            </div>

          </div>
   



           <!-- content-wrapper ends -->

            <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">
               
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                      <h4 class="modal-title">Edit </h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                           <div class="col-md-12 grid-margin stretch-card">
                              <div class="card">
                                <div class="card-body">
                                  <form class="forms-sample" id="submit-form" method="POST">
                                         <div class="form-group">
                                              <label for="f_nm">First Name</label>
                                              <input type="text" class="form-control" name="f_nm" id="f_nm" name="First Name" placeholder="Enter First Name..." required>
                                              <input type="hidden" name="hidden_id" id="hidden_id">
                                          </div>      

                                          <div class="form-group">
                                              <label for="l_nm">Last Name</label>
                                              <input type="text" class="form-control" id="l_nm" name="l_nm" placeholder="Enter Last Name..." required>
                                          </div>
                                          <div class="form-group">
                                              <label for="eml">Email id</label>
                                              <input type="email" class="form-control" id="eml" name="eml" placeholder="Enter Email id..." required>
                                          </div>
                                          <div class="form-group">
                                              <label for="phone">Contact No.</label>
                                              <input type="text" class="form-control card_number" id="phone" name="phone" placeholder="Enter Contact No..." minlength="10" maxlength="10" required>
                                          </div>
                                          <div class="form-group">
                                              <label>Profile Image</label>
                                              <div class="input-group col-xs-12">
                                                <input type="file" class="form-control file-upload-info" name="upload_img" id="upload_img"  placeholder="Upload Image">
                                                <input type="hidden" name="old_img" id="old_img">
                                              </div>
                                               <img src="" id="display_img" style="display: none;" width="80" height="60">
                                          </div>
                                           <div class="form-group">
                                              <label for="state_id">Where are you commissioned</label>
                                              <select class="form-control" id="state_id" name="state_id" required onchange="get_city(this.value);" >
                                                    <option value="">--select--</option>
                                              </select>
                                          </div>
                                          <div class="form-group">
                                              <label for="city_id">City of Residence</label>
                                              <select class="form-control" id="city_id" name="city_id" required >
                                                    <option value="">--select--</option>
                                              </select>
                                          </div>

                                          <div class="form-group">
                                              <label for="NNA_Certified">Are you NNA Certified</label>
                                              <select class="form-control" id="NNA_Certified" name="NNA_Certified" required>
                                                    <option value="">--select--</option>
                                                    <option value="Yes">Yes</option>
                                                    <option value="No">No</option>
                                              </select>
                                          </div>

                                          <div class="form-group">
                                              <label for="short_desc">How long have you been a notary</label>
                                              <select class="form-control" id="notary_exp" name="notary_exp" required>
                                                    <option value="">--select--</option>
                                                    <option value="1">1 year</option>
                                                    <option value="1-5">1-5 years</option>
                                                    <option value="5">5 or more years</option>
                                              </select>
                                          </div>
                                          <div class="form-group">
                                              <label for="short_desc">Set Priority</label>
                                              <input type="number" class="form-control" id="priority" name="priority" required min="0">
                                             
                                          </div>
                                        <button type="submit" class="btn btn-primary" id="submit_btn">Submit</button>
                                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                  <!--   <div class="modal-footer">
                      <button type="submit" class="btn btn-primary" id="target_btn" disabled >Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div> -->
                  </div>
               
              </div>
          </div>
  @endsection
  @section('script')

<script>
    $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4',
            });
    });

    fetch_data();
    get_states();

    function fetch_data(){
            var dataTable =  $('#grouplist').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route("fetch-lawyer")}}',
                columns: [
                    { data: 'name'},
                    { data: 'eml'},
                    { data: 'phone'},
                    { data: 'profile_pic'},
                    { data: 'NNA_Certified'},
                    { data: 'notary_exp'},
                    { data: 'priority'},
                    { data: 'action'}
                ]
            });
           
    }
   
    function get_states() {
            $.ajax({
                url: '{{url("get-states")}}',
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
                data: {get_states:"Get States"},
                success:function(res){
                  var obj=JSON.parse(res);
                  $('#state_id').children('option:not(:first)').remove();
                   $.each(obj, function (key, val) {
                         $("#state_id").append('<option value="'+val.id+'">'+val.state_name+'</option>');
                   });
                }
            });
    }

    function get_city(state_id) {
            $.ajax({
                url: '{{url("get-city")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},               
                data: {state_id:state_id},
                success:function(res){
                   var obj=JSON.parse(res);
                   $('#city_id').children('option:not(:first)').remove();
                   $.each(obj, function (key, val) {
                         $("#city_id").append('<option value="'+val.id+'">'+val.city+'</option>');
                   });
                }
            });
    }
    $("#submit-form").submit(function(e){
        e.preventDefault();
         $.ajax({
                url: '{{route("submit-lawyer")}}',
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                cache       : false,
                contentType : false,
                processData : false,
                data: new FormData(this),
                success:function(res){
                 var obj=JSON.parse(res);
                       if(obj.status==200){
                          toastr.success(obj.message);
                       }else if(obj.status==201){
                          toastr.error(obj.message);
                       }
                       else{
                         toastr.error('Something went wrong please try again later.');
                       }
                       $("#myModal").modal('hide');
                       $('.table').DataTable().destroy();
                      fetch_data();    
                   
                }
            });
    });
    function get_dtl(id) {
        if(id!=''){
          $("#display_img").show();
          $(".modal-title").text('Edit Data');
        }else{
          $("#display_img").hide();
          $(".modal-title").text('Add Data');
          $("#submit-form").trigger("reset");
           $("#eml").removeAttr('readonly');
        }
        $("#myModal").modal('show');
         $.ajax({
                    url: '{{route("edit-lawyer")}}',
                    type: 'POST',
                    dataType:"html",
                    data:{id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(response){
                      var res=JSON.parse(response);
                        $("#hidden_id").val(id);
                        var full_name=res.name.split(" ");
                        $("#f_nm").val(full_name[0]);
                        $("#l_nm").val(full_name[1]);
                        $("#eml").val(res.eml);
                        $("#eml").attr('readonly','readonly');
                        $("#phone").val(res.phone);
                        $("#state_id").val(res.state_id);
                        $("#old_img").val(res.profile_pic);
                        var image    = "{{asset('/public/')}}";
                        $("#display_img").attr('src',image+'/'+res.profile_pic);
                        get_city(res.state_id);
                        setTimeout(function(){ $("#city_id").val(res.city_id); },1500);
                        $("#NNA_Certified").val(res.NNA_Certified);
                        $("#notary_exp").val(res.notary_exp);
                         $("#priority").val(res.priority);
                       
                    }
          });
    }
    function remove_lawyer(id) {
         if(confirm('Are sure want to remove this?.')){
           $.ajax({
                    url: '{{route("remove-lawyer")}}',
                    type: 'POST',
                    dataType:"json",
                    data:{id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(response){
                     //console.log(response);
                         var obj=response;
                         if(obj.status==200){
                            toastr.success(obj.message);
                         }else{
                           toastr.error('Something went wrong please try again later.');
                         }
                         $('.table').DataTable().destroy();
                         fetch_data();
                    }
         
          });
        }
    }
    $(".card_number").keydown(function(e) {
        -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || 65 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 67 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || 88 == e.keyCode && (!0 === e.ctrlKey || !0 === e.metaKey) || e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.preventDefault()
    });
    </script>
@endsection