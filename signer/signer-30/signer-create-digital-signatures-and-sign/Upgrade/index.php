<?php
$oldservername = "localhost";
$oldusername = "oldsignerusername";
$oldpassword = "oldsignerpassword";
$olddbname = "oldsignerdatabase";

//////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Upgrade Signer to version 3">
    <meta name="author" content="Simcy Creative">
    <link rel="icon" type="image/png" sizes="16x16" href="https://simcycreative.com/wp-content/uploads/2018/09/Icon.png">
    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">
    <title>Signer Updater</title>
    <style type="text/css">
    	body {
    		font-family: Merienda;
    	}
    </style>
</head>
<body>
	<h1 style="color:#3DA4FF;">Signer Upgrader</h1>
<?php 

ini_set('display_errors', 'On');
error_reporting(0);

include_once '../vendor/autoload.php';
use DotEnvWriter\DotEnvWriter;

$env = new DotEnvWriter('../.env');

$servername = $env->get("DB_HOST")['value'];
$username = $env->get("DB_USERNAME")['value'];
$password = $env->get("DB_PASSWORD")['value'];
$dbname = "newsigner";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("<h2>Database connection failed: " . $conn->connect_error."</h2>");
}

$oldconn = new mysqli($oldservername, $oldusername, $oldpassword, $olddbname);
if ($oldconn->connect_error) {
    die("<h2>Old database connection failed: " . $oldconn->connect_error."</h2>");
}

echo '<p>Upgrading.....</p>';

/**
 * Import companies from old database
 * 
 */
$companySql = "SELECT * FROM companies";
$companyResult = $oldconn->query($companySql);
if ($companyResult->num_rows > 0) {
	while($oldCompany = $companyResult->fetch_object()) {

		/* Check if company ID already exists */
		$newCompanyCheckSql = "SELECT * FROM companies WHERE id = ".$oldCompany->id;
		$newCompanyCheckResult = $conn->query($newCompanyCheckSql);
		if ($newCompanyCheckResult->num_rows > 0) {
			$newCompanySave = "UPDATE companies SET reminders = '" .$oldCompany->reminders. "', name = '" .$oldCompany->name. "', email = '" .$oldCompany->email. "', phone = '" .$oldCompany->phone . "' WHERE id = ".$oldCompany->id; 
		}else{
			$newCompanySave = "INSERT INTO companies (id, reminders, name, email, phone) VALUES ('".$oldCompany->id."', '".$oldCompany->reminders."', '".$oldCompany->name."', '".$oldCompany->email."', '".$oldCompany->phone."')"; 
		}
		if(!mysqli_query($conn,$newCompanySave)){
			exit('<p style="color:red;">Oops!  <strong>'.mysqli_error($conn).'</strong></p>');
		}
	}
}
echo '<p>Companies upgrage complete!</p>';

/**
 * Import users from old database
 * 
 */
$userSql = "SELECT * FROM users";
$userResult = $oldconn->query($userSql);
if ($userResult->num_rows > 0) {
	while($oldUser = $userResult->fetch_object()) {

		/* Check if user ID already exists */
		$newUserCheckSql = "SELECT * FROM users WHERE id = ".$oldUser->id;
		$newUserCheckResult = $conn->query($newUserCheckSql);
		if ($newUserCheckResult->num_rows > 0) {
			$newUserSave = "UPDATE users SET fname = '" .$oldUser->fname. "', lname = '" .$oldUser->lname. "', email = '" .$oldUser->email. "', phone = '" .$oldUser->phone . "', address = '" .$oldUser->address . "', avatar = '" .$oldUser->avatar . "', token = '" .$oldUser->token . "', company = '" .$oldUser->company . "', role = '" .$oldUser->role . "', signature = '" .$oldUser->signature . "', lastnotification = '" .$oldUser->lastnotification . "' WHERE id = ".$oldUser->id; 
		}else{
			$newUserSave = "INSERT INTO users (id, fname, lname, email, phone, address, avatar, token, company, role, signature, lastnotification) VALUES ('".$oldUser->id."', '".$oldUser->fname."', '".$oldUser->lname."', '".$oldUser->email."', '".$oldUser->phone."', '".$oldUser->address."', '".$oldUser->avatar."', '".$oldUser->token."', '".$oldUser->company."', '".$oldUser->role."', '".$oldUser->signature."', '".$oldUser->lastnotification."')"; 
		}
		if(!mysqli_query($conn,$newUserSave)){
			exit('<p style="color:red;">Oops!  <strong>'.mysqli_error($conn).'</strong></p>');
		}
	}
}
echo '<p>Users upgrage complete!</p>';

/**
 * Import folders from old database
 * 
 */
$sql = "SELECT * FROM folders";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if folder ID already exists */
		$newCheckSql = "SELECT * FROM folders WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			$newSave = "UPDATE folders SET company = '" .$old->company. "', created_by = '" .$old->created_by. "', created_on = '" .$old->created_on. "', name = '" .$old->name . "', folder = '" .$old->folder . "' WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO folders (id, company, created_by, created_on, name, folder) VALUES ('".$old->id."', '".$old->company."', '".$old->created_by."', '".$old->created_on."', '".$old->name."', '".$old->folder."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			exit('<p style="color:red;">Oops!  <strong>'.mysqli_error($conn).'</strong></p>');
		}
	}
}
echo '<p>Folders upgrage complete!</p>';

/**
 * Import files from old database
 * 
 */
$sql = "SELECT * FROM files";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if ID already exists */
		$newCheckSql = "SELECT * FROM files WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			if ($old->status == "signed") { $old->status = "Signed"; }else{ $old->status = "Unsigned"; }
			if ($old->template == 1) { $old->template = "Yes"; }else{ $old->template = "No"; }
			$newSave = "UPDATE files SET company = '" .$old->company. "', uploaded_by = '" .$old->uploaded_by. "', uploaded_on = '" .$old->uploaded_on. "', name = '" .$old->name . "', folder = '" .$old->folder . "', filename = '" .$old->filename . "', extension = '" .$old->type . "', document_key = '" .$old->sharing_key . "', status = '" .$old->status . "', is_template = '" .$old->template . "', sign_reason = '" .$old->sign_reason . "'WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO files (id, company, uploaded_by, uploaded_on, name, folder, filename, extension, document_key, status, is_template, sign_reason) VALUES ('".$old->id."', '".$old->company."', '".$old->uploaded_by."', '".$old->uploaded_on."', '".$old->name."', '".$old->folder."', '".$old->filename."', '".$old->type."', '".$old->sharing_key."', '".$old->status."', '".$old->template."', '".$old->sign_reason."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			exit('<p style="color:red;">Oops!  <strong>'.mysqli_error($conn).'</strong></p>');
		}
	}
}
echo '<p>Files upgrage complete!</p>';

/**
 * Import reminders from old database
 * 
 */
$sql = "SELECT * FROM reminders";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if ID already exists */
		$newCheckSql = "SELECT * FROM reminders WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			$newSave = "UPDATE reminders SET company = '" .$old->company. "', days = '" .$old->days. "', subject = '" .$old->subject. "', message = '" .$old->message . "' WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO reminders (id, company, days, subject, message) VALUES ('".$old->id."', '".$old->company."', '".$old->days."', '".$old->subject."', '".$old->message."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			exit('<p style="color:red;">Oops!  <strong>'.mysqli_error($conn).'</strong></p>');
		}
	}
}
echo '<p>Reminders upgrage complete!</p>';

/**
 * Import requests from old database
 * 
 */
$sql = "SELECT * FROM requests";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if ID already exists */
		$newCheckSql = "SELECT * FROM requests WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			if ($old->status == 1) { $old->status = "Signed"; }elseif($old->status == 2){ $old->status = "Declined"; }elseif($old->status == 3){ $old->status = "Cancelled"; }else{ $old->status = "Pending"; }
			$newSave = "UPDATE requests SET company = '" .$old->company. "', signing_key = '" .$old->signingkey. "', document = '" .$old->file. "', positions = '" .$old->positions . "', send_time = '" .$old->time_ . "', update_time = '" .$old->actiontime_ . "', email = '" .$old->email . "', sender = '" .$old->user . "', status = '" .$old->status . "' WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO requests (id, company, signing_key, document, positions, send_time, update_time, email, sender, status) VALUES ('".$old->id."', '".$old->company."', '".$old->signingkey."', '".$old->file."', '".$old->positions."', '".$old->time_."', '".$old->actiontime_."', '".$old->email."', '".$old->user."', '".$old->status."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			echo '<p style="color:red;">Oops!  Error upgrading requests: <strong>'.mysqli_error($conn).'</strong></p>';
		}
	}
}
echo '<p>Requests upgrage complete!</p>';

/**
 * Import chats from old database
 * 
 */
$sql = "SELECT * FROM chat";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if ID already exists */
		$newCheckSql = "SELECT * FROM chat WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			$newSave = "UPDATE chat SET sender = '" .$old->sender. "', time_ = '" .$old->time_. "', message = '" .$old->message . "', file = '" .$old->file . "' WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO chat (id, sender, time_, message, file) VALUES ('".$old->id."', '".$old->sender."', '".$old->time_."', '".$old->message."', '".$old->file."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			echo '<p style="color:red;">Oops!  Error upgrading chats: <strong>'.mysqli_error($conn).'</strong></p>';
		}
	}
}
echo '<p>Chats upgrage complete!</p>';

/**
 * Import history from old database
 * 
 */
$sql = "SELECT * FROM history";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if ID already exists */
		$newCheckSql = "SELECT * FROM history WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			$newSave = "UPDATE history SET company = '" .$old->company. "', type = '" .$old->type. "', time_ = '" .$old->time_. "', activity = '" .$old->activity . "', file = '" .$old->file . "' WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO history (id, company, type, time_, activity, file) VALUES ('".$old->id."', '".$old->company."', '".$old->type."', '".$old->time_."', '".$old->activity."', '".$old->file."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			echo '<p style="color:red;">Oops!  Error upgrading history: <strong>'.mysqli_error($conn).'</strong></p>';
		}
	}
}
echo '<p>History upgrage complete!</p>';

/**
 * Import notifications from old database
 * 
 */
$sql = "SELECT * FROM notifications";
$result = $oldconn->query($sql);
if ($result->num_rows > 0) {
	while($old = $result->fetch_object()) {

		/* Check if ID already exists */
		$newCheckSql = "SELECT * FROM notifications WHERE id = ".$old->id;
		$newCheckResult = $conn->query($newCheckSql);
		if ($newCheckResult->num_rows > 0) {
			$newSave = "UPDATE notifications SET user = '" .$old->user. "', time_ = '" .$old->time_. "', message = '" .$old->message . "', type = '" .$old->type . "' WHERE id = ".$old->id; 
		}else{
			$newSave = "INSERT INTO notifications (id, user, type, time_, message) VALUES ('".$old->id."', '".$old->user."', '".$old->type."', '".$old->time_."', '".$old->message."')"; 
		}
		if(!mysqli_query($conn,$newSave)){
			echo '<p style="color:red;">Oops!  Error upgrading notifications: <strong>'.mysqli_error($conn).'</strong></p>';
		}
	}
}
echo '<p>Notifications upgrage complete!</p>';

/**
 * Import complete
 * 
 */
echo '<h2 style="color:green;">Upgrade successfully completed.</h2>';
echo '<h4 style="color:#3DA4FF;">You should now delete the <strong>"upgrade"</strong> folder for secutity reasons.</h4>';

?>
</body>
</html>
