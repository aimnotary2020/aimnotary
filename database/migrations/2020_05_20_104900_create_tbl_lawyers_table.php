<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblLawyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_lawyers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            // $table->string('f_nm');
            // $table->string('l_nm');
            // $table->string('eml');
            // $table->string('phone');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->enum('NNA_Certified',['Yes','No'])->default('No');
            $table->string('notary_exp');
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_lawyers');
    }
}
