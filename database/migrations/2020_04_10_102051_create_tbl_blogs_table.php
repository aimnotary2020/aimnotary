<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('featured', ['Y', 'N'])->default('N');
            $table->enum('is_case_study', ['Y', 'N'])->default('N');
            $table->string('title');
            $table->string('generate_url');
            $table->integer('posted_by');
            $table->string('image');
            $table->string('short_desc');
            $table->longText('description');
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_blogs');
    }
}
