<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function (Blueprint $table) {

            $table->string('user_otp')->nullable()->after('email_verified_at');
            $table->enum('is_privacy',['0','1'])->default('0')->after('password');
            $table->enum('is_terms',['0','1'])->default('0')->after('is_privacy');
            $table->string('country')->after('is_terms');
            $table->string('city')->after('country');
            $table->string('add1')->after('city');
            $table->string('add2')->nullable()->after('add1');
            $table->string('zip_code')->after('add2');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
