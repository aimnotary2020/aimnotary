<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblSubscriptionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_subscription_details', function (Blueprint $table) {
            $table->bigIncrements('subs_id');
            $table->string('subs_date');
            $table->integer('user_id')->default(0);
            $table->string('pay_id')->nullable();
            $table->string('card_type')->nullable();
            $table->decimal('product_price',10,2);
            $table->decimal('total_cost',10,2);
            $table->string('card_number')->nullable();
            $table->string('card_expiry')->nullable();
            $table->string('card_holder')->nullable();
            $table->integer('coupon_id')->nullable();
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_subscription_details');
    }
}
