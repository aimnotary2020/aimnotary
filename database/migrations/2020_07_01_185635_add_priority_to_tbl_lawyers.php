<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPriorityToTblLawyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_lawyers', function (Blueprint $table) {
            //
            $table->integer('posted_by')->after('user_id');
            $table->integer('priority')->after('notary_exp');
            $table->enum('status',['Busy','Free'])->default('Free')->after('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_lawyers', function (Blueprint $table) {
            //
        });
    }
}
