<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCompanyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_company_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('head_name');
            $table->string('head_designatin');
            $table->string('c_name');
            $table->string('c_logo');
            $table->string('c_open_daytime');
            $table->string('c_close_daytime');
            $table->string('c_contact_no');
            $table->string('personal_no');
            $table->longText('c_address');
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_company_infos');
    }
}
