<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblContentMngtSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_content_mngt_systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category');
            $table->integer('sub_category')->nullable();
            $table->string('name')->nullable();
            $table->string('occupation')->nullable();
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('file_image')->nullable();
            $table->string('url_link')->nullable();
            $table->timestamps();
            $table->softDeletes()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_content_mngt_systems');
    }
}
