<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
    Auth::routes();
    

    //Route::
    Route::get('/', 'FrontController@index')->name('Home');
    Route::post('/pdfsigner/updateSignerPdf', 'FrontController@updateSignerPdf')->name('updateSignerPdf');
    Route::post('/pdfsigner/getDocStatus', 'FrontController@getDocStatus')->name('getDocStatus');
    Route::get('/about', 'FrontController@about_us')->name('about');
    Route::get('/contact', 'FrontController@contact')->name('contact');
    Route::get('/faq', 'FrontController@faq')->name('faq');
    Route::get('/privacy-policy', 'FrontController@privacy_policy')->name('privacy-policy');
    Route::get('/terms-and-conditions', 'FrontController@terms_and_conditions')->name('terms-and-conditions');
    Route::get('/how-it-works', 'FrontController@how_it_works')->name('how-it-works');
    Route::get('/solutions-for-business-and-teams', 'FrontController@business_solution')->name('solutions-for-business-and-teams');
    Route::get('/solutions-for-individual', 'FrontController@solutions_for_individual')->name('solutions-for-individual');
    Route::get('/blog', 'FrontController@blog')->name('blog');
    Route::get('/blog-detail/{url}', 'FrontController@blog_detail')->name('blog-detail');
    Route::get('/pricing', 'FrontController@pricing')->name('pricing');
   
    Route::get('/recover-password', 'FrontController@recover_password')->name('recover-password');
    Route::get('/basic_email', 'FrontController@basic_email')->name('basic_email');
    Route::post('/contact_us', 'FrontController@contact_us')->name('contact_us');
    Route::post('/subscribe', 'FrontController@subscribe')->name('subscribe');

    //user login signup
    Route::get('/login', 'FrontController@login')->name('login');
    Route::post('/check_credentials', 'FrontController@check_credentials')->name('check_credentials');
    Route::get('/sign-up', 'FrontController@register')->name('sign-up');
    Route::post('/user_registration', 'FrontController@user_registration')->name('user_registration');
    Route::get('/verify-email/{email}', 'FrontController@verify_email')->name('verify-email');
    Route::get('/checkEmailAjax', 'FrontController@checkEmailAjax')->name('checkEmailAjax');
    Route::get('/checkPasswordAjax', 'FrontController@checkPasswordAjax')->name('checkPasswordAjax');
    
    Route::post('/add_additional_docs', 'FrontController@add_additional_docs')->name('add_additional_docs');
    Route::post('/enterprise-form-submit', 'FrontController@enterprise_form_submit')->name('enterprise-form-submit');
    Route::post('/make-subscription-payments', 'FrontController@make_subscription_payments')->name('make-subscription-payments');
    Route::get('/transaction-update', 'FrontController@transaction_update')->name('transaction-update');
    
    
    Route::get('/online-notary-texas', 'StaticBlogController@index');
    Route::get('/online-notary-tennessee', 'StaticBlogController@index');
    Route::get('/online-notary-alabama', 'StaticBlogController@index');
    Route::get('/online-notary-florida', 'StaticBlogController@index');
    Route::get('/online-notary-new-york', 'StaticBlogController@index');
    Route::get('/online-notary-kentucky', 'StaticBlogController@index');
    
    Route::get('/online-notary-alaska', 'StaticBlogController@index');
    Route::get('/online-notary-arizona', 'StaticBlogController@index');
    Route::get('/online-notary-arkansas', 'StaticBlogController@index');
    Route::get('/online-notary-colorado', 'StaticBlogController@index');
    Route::get('/online-notary-hawaii', 'StaticBlogController@index');
    
    Route::get('/online-notary-kansas', 'StaticBlogController@index');
    Route::get('/online-notary-iowa', 'StaticBlogController@index');
    Route::get('/online-notary-illinois', 'StaticBlogController@index');
    Route::get('/online-notary-indiana', 'StaticBlogController@index');
   
   // Route::get('/payment', 'FrontController@payment')->name('payment');
    Route::get('/payment/{price}', 'FrontController@payment')->name('payment');
    Route::post('/get-checkout-module','FrontController@getCheckout');
   //check_coupon
    Route::post('/check_coupon', 'FrontController@check_coupon')->name('check_coupon');
    //paid subscription
    Route::post('/paid_subscription', 'FrontController@paid_subscription')->name('paid_subscription');
    Route::post('/resubscription', 'FrontController@resubscription')->name('resubscription');
    //state city route
    Route::get('/get-states', 'FrontController@get_states')->name('get-states');
    Route::post('/final_payment', 'FrontController@final_payment')->name('final_payment');
    
    Route::post('/get-city', 'FrontController@get_city')->name('get-city');
    Route::post('/rescover_password', 'FrontController@rescover_password')->name('rescover_password');
    Route::post('/pdf-update', 'FrontController@pdfUpdate')->name('pdf-update');
    // Enter Prise Form Subscription'
    
    Route::get('/enterprise-form', 'FrontController@enterprise_form')->name('enterprise-form');
    
    //Argentum Chat
    Route::get('/check_cookie', 'FrontController@check_cookie')->name('check_cookie');
    Route::get('/get_chat_list', 'FrontController@get_chat_list')->name('get_chat_list');
    Route::get ('/open_chat','FrontController@open_chat')->name('open_chat');
    Route::get ('/close_chat','FrontController@close_chat')->name('close_chat');
    Route::get ('/send_chat','FrontController@send_chat')->name('send_chat');
    Route::get ('/notification_timer','FrontController@notification_timer')->name('notification_timer');
    Route::get ('/send_offline_chat','FrontController@send_offline_chat')->name('send_offline_chat');
    Route::get ('/user_reply','FrontController@user_reply')->name('user_reply');
    Route::get ('/user_reply1','FrontController@user_reply1')->name('user_reply1');
    
    
         //chekck ssn
    Route::group(['middleware' => 'auth'], function(){

        Route::get('/redirect-to-new-doc/{doc_id}', 'UserauthController@redirect_to_new_doc')->name('redirect-to-new-doc');
        Route::post('/check_ssn', 'UserauthController@check_ssn')->name('check_ssn');
        Route::post('/verify_ssn_nm', 'UserauthController@verify_ssn_nm')->name('verify_ssn_nm');
        Route::post('/verify_ssn_address', 'UserauthController@verify_ssn_address')->name('verify_ssn_address');
        Route::post('/verify_ssn_phone', 'UserauthController@verify_ssn_phone')->name('verify_ssn_phone');
        
        Route::get('/dashboard', 'UserauthController@dashboard')->name('dashboard');
        Route::get('/pay-for-documents/{doc_id}', 'UserauthController@pay_for_documents')->name('pay-for-documents');
        Route::get('/upload', 'UserauthController@upload')->name('upload');
        Route::post('/upload_documents', 'UserauthController@upload_documents')->name('upload_documents');
        Route::get('/confirm-detail', 'UserauthController@confirm_detail')->name('confirm-detail');
        Route::get('/verify-identity/{doc_id}', 'UserauthController@verify_identity')->name('verify-identity');
        Route::get('/verify-documents/{doc_id}', 'UserauthController@verify_documents')->name('verify-documents');
        Route::get('/update-documents/{doc_id}', 'UserauthController@editdocuments')->name('update-documents');
        Route::get('/check-ssn-verification', 'UserauthController@check_ssn_verification')->name('check-ssn-verification');
        Route::get('/check-question-two', 'UserauthController@check_question_two')->name('check-question-two');
        Route::get('/check-question-one', 'UserauthController@check_question_one')->name('check-question-one');
        Route::get('/scedule-meeting/{doc_id}', 'UserauthController@scedule_meeting')->name('scedule-meeting');
        Route::post('/get-scedules/', 'UserauthController@get_scedules')->name('get-scedules');
        Route::post('/get-single-scedules/', 'UserauthController@get_single_scedules')->name('get-single-scedules');
        Route::post('/submit-schedule/', 'UserauthController@submit_schedule')->name('submit_schedule');
        Route::post('/verify-documents-submit/', 'UserauthController@verify_documents_submit')->name('verify_documents_submit');
        Route::get('/settings/', 'UserauthController@settings')->name('settings');
        Route::post('/update-account/', 'UserauthController@update_account')->name('update-account');
        Route::post('/change-user-password/', 'UserauthController@change_password')->name('change-password');
        

        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
        Route::get('/fetch_documents', 'UserauthController@fetch_documents')->name('fetch_documents');
        Route::post('/remove_document', 'UserauthController@remove_document')->name('remove_document');
        Route::post('/edit_documents', 'UserauthController@edit_documents')->name('edit_documents');

        Route::post('/set_time_session', 'UserauthController@set_time_session')->name('set_time_session');
        Route::get('/check-question-success', 'UserauthController@check_question_success')->name('check-question-success');
        Route::get('/start-video-call-wih-us', 'VchatController@start_video_call_wih_us')->name('start-video-call-wih-us');
        
        Route::get('/start-chat', 'VchatController@chat')->name('start-chat');

        Route::get('/start-video/{user_data}', 'VchatController@start_video')->name('start-video');
        Route::get('/start-video-call/{user_data}', 'VchatController@start_video')->name('start-video-call');
        
        Route::post('/start-archieve', 'VchatController@start_archieve')->name('start-archieve');
        Route::get('/stop-archieve/{archiveID}', 'VchatController@stop_archieve')->name('stop-archieve');

        Route::get('/save_archieve', 'VchatController@save_archieve_id')->name('save_archieve');
        Route::get('/save_astream', 'VchatController@save_astream_id')->name('save_astream');
        
    });









    /*admin login logout */
    Route::get('/admin/logout', 'Auth\LoginController@logout')->name('AdminLogout');
    Route::get('/notary/logout', 'Auth\LoginController@logout')->name('AdminLogout');
    Route::get('/admin/login', 'admin\DashboardController@login')->name('adminlogin');
    Route::get('/notary/login', 'admin\DashboardController@login')->name('adminlogin');
    Route::post('/admin/check_login', 'admin\DashboardController@check_login')->name('check_login');
    Route::post('/get-documents-list/', 'admin\DashboardController@get_documents_list')->name('get_documents_list');
    Route::post('/uploadRepositoryDocument/', 'admin\DashboardController@uploadRepositoryDocument')->name('uploadRepositoryDocument');
    Route::get('/deletDocument/', 'admin\DashboardController@deletDocument')->name('deletDocument');
    Route::post('/get-merge-documents-list/', 'admin\DashboardController@get_merge_documents_list')->name('get-merge-documents-list');
    Route::post('/mergerDocuments/', 'admin\DashboardController@mergerDocuments')->name('mergerDocuments');
    Route::get('/couponlist/', 'admin\DashboardController@couponlist')->name('couponlist');
    
    



    Route::group(['namespace' => 'admin', 'middleware' => 'auth'], function(){
        //Route::get('/admin/login', 'DashboardController@index')->name('home');

        //password routes
        Route::post('/check-password', 'DashboardController@check_current_password')->name('check-current-password');
        Route::post('/change-password', 'DashboardController@change_profile_password')->name('change-profile-password');

         //cms routes
        Route::group(['prefix' => 'admin'], function(){
                Route::get('/', 'DashboardController@index')->name('Dashboard');
                Route::get('/about-us', 'DashboardController@about_us')->name('about-us');
                Route::get('/website_traffic', 'DashboardController@website_traffic')->name('website_traffic');
                Route::get('/website_traffic_cms', 'DashboardController@fetch_website_traffic_dtl')->name('website_traffic_cms');
                Route::get('/remove-weblit/{id}', 'DashboardController@remove_weblit')->name('remove-weblit');
                Route::get('/fetch-cms', 'DashboardController@fetch_cms_dtl')->name('fetch-cms');
                Route::post('/get-cms-dtl', 'DashboardController@get_cms_dtl')->name('get-cms-dtl');
                Route::post('/add_edit_cms', 'DashboardController@add_edit_cms')->name('add_edit_cms');
                Route::post('/get_subcat', 'DashboardController@get_subcat')->name('get_subcat');
                Route::post('/remove-cms', 'DashboardController@remove_cms')->name('remove-cms');
                Route::get('/show-video/{url}', 'DashboardController@showVideo')->name('show-video');
                Route::get('/transaction/', 'DashboardController@transaction')->name('transaction');
                Route::get('/meta-title-management/{myid}', 'DashboardController@meta_title_management')->name('meta-title-management');
                Route::post('/add-meta-details/', 'DashboardController@add_meta_details')->name('add-meta-details');
                Route::post('/refund/', 'DashboardController@refund')->name('refund');
                Route::post('/updateCouponValue/', 'DashboardController@updateCouponValue')->name('updateCouponValue');
                Route::post('/addCoupon/', 'DashboardController@addCoupon')->name('addCoupon');
                Route::get('/getVideoStopStatus/{doc_id}', 'DashboardController@getVideoStopStatus')->name('getVideoStopStatus');

        });
                Route::group(['prefix' => 'blog-list'], function(){
                Route::get('/', 'BlogController@index')->name('blog-list');
                Route::get('/fetch-blog','BlogController@fetch_blog')->name('fetch-blog');
                Route::post('/edit-blog', 'BlogController@edit_blog')->name('edit-blog');
                Route::post('/submit-blog', 'BlogController@submit_blog')->name('submit-blog');
                Route::post('/remove-blog', 'BlogController@remove_blog')->name('remove-blog');
        });


        Route::group(['prefix' => 'lawyer-list'], function(){
                Route::get('/', 'BlogController@lawyer_list')->name('lawyer-list');
                Route::get('/fetch-lawyer','BlogController@fetch_lawyer')->name('fetch-lawyer');
                Route::post('/edit-lawyer', 'BlogController@edit_lawyer')->name('edit-lawyer');
                Route::post('/submit-lawyer', 'BlogController@submit_lawyer')->name('submit-lawyer');
                Route::post('/remove-lawyer', 'BlogController@remove_lawyer')->name('remove-lawyer');
        });


        Route::group(['prefix' => 'user-list'], function(){
                Route::get('/', 'DashboardController@user_list')->name('user-list');
                Route::get('/enterprise-user-list', 'DashboardController@enterprise_user_list')->name('enterprise-user-list');
                Route::get('/document-lists/{user_id}', 'DashboardController@document_lists')->name('document-lists');
                Route::get('/user-docs/{user_id}', 'DashboardController@fetch_user_wise_docs')->name('user-docs');
                Route::post('/wave-off-documents', 'DashboardController@wave_off_documents')->name('wave-off-documents');
                

                Route::post('/credit-update', 'DashboardController@credit_update')->name('credit-update');

                Route::get('/schedule-appointment','DashboardController@schedule_appointment')->name('schedule-appointment');
                Route::get('/fetch-schedule-user', 'DashboardController@fetch_schedule_user')->name("fetch-schedule-user");
                Route::post('/fetch_lawyer/', 'DashboardController@fetch_lawyer')->name('fetch_lawyer');
                Route::post('/fetch_lawyer_docs/', 'DashboardController@fetch_lawyer_docs')->name('fetch_lawyer_docs');
                Route::post('/assign-user','DashboardController@assign_user')->name('assign-user');
                Route::post('/assign-user-docs','DashboardController@assign_user_doc')->name('assign-user-docs');
                

                Route::get('/fetch-user','DashboardController@fetch_user')->name('fetch-user');
                Route::get('/fetch-enterprise-user','DashboardController@fetch_enterprise_user')->name('fetch-enterprise-user');
                Route::post('/enterprise-user-registration','DashboardController@enterprise_user_registration')->name('enterprise-user-registration');
                Route::post('/insert-enterprise-subscription','DashboardController@insert_enterprise_subscription')->name('insert-enterprise-subscription');
                
        });
        

        Route::group(['prefix' => 'lawyer'], function(){
                Route::get('/', 'DashboardController@index')->name('Dashboard');
        });
        Route::get('/pending-document/{type_id}', 'DashboardController@user_document')->name('pending-document');

        Route::get('/completed-document/{type_id}', 'DashboardController@user_document')->name('completed-document');


        Route::get('/user-video-call', 'DashboardController@user_video_call')->name('user-video-call');
        Route::get('/fetch_user_documents/{type_id}', 'DashboardController@fetch_user_documents')->name('fetch_user_documents');
        Route::post('/start_call', 'DashboardController@start_call')->name('start_call');
        Route::post('/update_call_status', 'DashboardController@update_call_status')->name('update_call_status');
        

});