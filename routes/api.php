<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/enterprise-subscription','ApiController@enterprise_subscription_get');
Route::post('/enterprise-payment','ApiController@make_subscription_payments');
Route::post('/insert-enterprise-subscription','ApiController@insert_enterprise_subscription');
Route::post('/total-pay-price','ApiController@pay_for_documents');
Route::post('/check-count','ApiController@check_doc_count');
Route::post('/payment-final','ApiController@final_payment');
Route::post('/status_change','ApiController@status_change');
Route::post('/reg','ApiController@register_user');
Route::post('/existEmail','ApiController@isExist_emailId');
Route::post('/regIOS','ApiController@register_user_IOS');
Route::post('/reg_enterprise','ApiController@register_user_enterprise');
Route::post('/del_user','ApiController@delete_user_email');
Route::post('/update_user','ApiController@user_account_update');
Route::post('/change_password','ApiController@change_pwd');
Route::post('/resubscription', 'ApiController@resubscription_user');
Route::post('/submit-schedule', 'ApiController@submit_schedule_user');
Route::post('/get-schedule', 'ApiController@get_scedule');
Route::post('/login','ApiController@get_user');
Route::post('/add_additional_docs','ApiController@add_additional_docs');
Route::post('/password-reset','ApiController@reset_password');
Route::post('/cus','ApiController@contact_us');
Route::post('/upload','ApiController@upload_file');
Route::post('/verify_document','ApiController@verify_documents_submit_user');
Route::post('/about-us','ApiController@about_us');
Route::post('/how-it-works','ApiController@how_works');
Route::post('/privacy-policy','ApiController@privacy_policy');
Route::post('/terms-conditions','ApiController@terms_conditions');
Route::post('/user-docs','ApiController@user_docs');
Route::post('/remove-docs','ApiController@remove_document');

Route::match(['get','post'],'/verify-identity','ApiController@verify_identity');
Route::post('/check-ssn', 'ApiController@check_ssn');
Route::post('/verify-ssn-name', 'ApiController@verify_ssn_nm');
Route::post('/verify-ssn-phone', 'ApiController@verify_ssn_phone');
Route::post('/start-chat','ApiController@start_chat');
Route::post('/signed-doc','ApiController@signed_document');

Route::post('/pend','ApiController@pending_docs');


