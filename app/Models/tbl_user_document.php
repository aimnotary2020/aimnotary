<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class tbl_user_document extends Model
{
    use SoftDeletes;
    public function user_dtl()
    {
    	 return $this->hasOne('App\User','id','user_id');
    }

      public function lawyer_dtl()
    {
    	 return $this->hasOne('App\User','id','lawyer_id');
    }
    
}
