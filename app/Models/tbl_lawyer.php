<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
class tbl_lawyer extends Authenticatable
{
    use SoftDeletes,HasApiTokens;
    protected $primaryKey = 'id';
    protected $guarded	  = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function get_state(){
    	return $this->hasOne('App\Models\tbl_state','id','state_id');
    }

     public function lawyer_dtl(){
        return $this->belongsTo('App\User','user_id','id');
    }

  
}
