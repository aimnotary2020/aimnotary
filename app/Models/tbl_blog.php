<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class tbl_blog extends Model
{
    //
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $guarded	  = ['id'];

     public function get_user_dtl(){
    	return $this->hasOne('App\User','id','posted_by');
    }
}
