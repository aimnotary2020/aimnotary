<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tbl_cms_categorie extends Model
{
    //
    use SoftDeletes;
    protected $primaryKey = 'id';
    protected $guarded	  = ['id'];

    public function get_category(){
    	return $this->hasMany('App\Models\tbl_cms_categorie','parent_id','id');
    }
}
