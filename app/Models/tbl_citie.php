<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_citie extends Model
{
     protected $primaryKey = 'city_id';
     protected $guarded	  = ['city_id'];
}
