<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class role extends Model
{
    //use SoftDeletes;
    protected $primaryKey	= 'id';
    protected $guarded		= ['id'];
    //protected $dates 		= ['deleted_at'];

    public function get_related_profiles(){
        return $this->hasOne('App\user','id','id');
    }
}
