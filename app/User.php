<?php

namespace App;
use Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasRoles, HasApiTokens, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function hasRole($role)
    // {
    //     $check_admin= User::where('user_type', $role)->get();
    //     if(!$check_admin->isEmpty()){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

    public function get_subscription_dtl(){
        return $this->hasOne('App\Models\tbl_subscription_detail','user_id','id');
    }

    public function lawyer_dtl(){
        return $this->hasOne('App\Models\tbl_lawyer','user_id','id');
    }
   
}
