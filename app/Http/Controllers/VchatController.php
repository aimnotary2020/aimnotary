<?php

namespace App\Http\Controllers\Client;
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;
use App\Models\tbl_stream_video; 
use App\Models\tbl_archive_video;
use App\Models\tbl_vid_chat_session;
use App\Models\tbl_user_document;
use DB;
class VchatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
  

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
  


		public function start_video_call_wih_us(Request $request)
		{
			 $data['page_title'] = 'start_video_call_wih_us';
	         $data['company_info'] = $this->company_info;
	         return view('front.user.start-video-call-wih-us')->with($data);
		}

		public function chat(Request $request) 
        {
           // initialze api using api key/secret
        // 	echo env('OPENTOK_API_KEY');
        // 	echo "<br>";
        // 	echo env('OPENTOK_API_SECRET');
        // echo "<br>";

           // $openTokAPI = new OpenTok(env('OPENTOK_API_KEY'), env('OPENTOK_API_SECRET'));

           // // let's cache the session for an hour i.e. 60 mins
           // $session_token = \Cache::remember('open_tok_session_key', 60, function () use ($openTokAPI) {             
           //      return $openTokAPI->createSession(['mediaMode' => MediaMode::ROUTED]);
           // });

           // $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));


           // // now, that we have session token we generate opentok token
           // $opentok_token = $openTokAPI->generateToken($session->session_id, [
           //      'exerciseireTime' => time()+60,
           //      'data'       => "name=nusrat"
           // ]);
           $opentok = new OpenTok(env('OPENTOK_API_KEY'), env('OPENTOK_API_SECRET'));
     		// Create a session that attempts to use peer-to-peer streaming:
			$session = $opentok->createSession();

			// A session that uses the OpenTok Media Router, which is required for archiving:
			$session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

			// A session with a location hint:
			$session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

			// An automatically archived session:
			$sessionOptions = array(
			    'archiveMode' => ArchiveMode::ALWAYS,
			    'mediaMode' => MediaMode::ROUTED
			);
			$session = $opentok->createSession($sessionOptions);

			// Store this sessionId in the database for later use
			$sessionId = $session->getSessionId();

			// Generate a Token from just a sessionId (fetched from a database)
			$token = $opentok->generateToken($sessionId);
			// Generate a Token by calling the method on the Session (returned from createSession)
			$token = $session->generateToken();

			// Set some options in a token
			$token = $session->generateToken(array(
			    'role'       => Role::MODERATOR,
			    'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
			    'data'       => 'name=Johnny',
			    'initialLayoutClassList' => array('focus')
			));

			$count_vidsession=tbl_vid_chat_session::select('*')->where(['sessionId'=>$sessionId])->count();
			if($count_vidsession==0){
				 tbl_vid_chat_session::insert(['sessionId'=>$sessionId,'created_at'=>NOW()]);
				$request->session()->put('sessionId', $sessionId);
			}

			// $stream = $opentok->getStream($sessionId, $streamId);

			// // Stream properties
			// $stream->id; // string with the stream ID
			// $stream->videoType; // string with the video type
			// $stream->name; // string with the name
			// $stream->layoutClassList;

			// // echo $session.'<br>'.$token;
			// // die();
           return view('front.user.vchat', [
              'session_token' => $session,
              'opentok_token' => $token
           ]);
        }

        public function start_video(Request $request,$user_data='')
        {        
			$data=array();
			$user_dtl=explode('~~', base64_decode($user_data));
			if(isset($user_dtl[0]) && isset($user_dtl[0])){
				$username = DB::table('users')->select("*")->where(['id'=>$user_dtl[1]])->take(1)->get();
				$user_doc_info=tbl_user_document::select('*')->where(['id'=> $user_dtl[0],'user_id'=>$user_dtl[1]])->get()->first()->toArray();
				if($user_doc_info['is_call_started']=="N"){
					$update=tbl_user_document::where('id',$user_dtl[0])->update(['is_call_started'=>'Y','updated_at'=>NOW()]);
				}
				$data['user_doc_info']=$user_doc_info;
				$data['user_cannel']=$user_dtl[0].$user_dtl[1];
				$data['doc_id']=$user_dtl[0];
				$data['user_info']=$username[0];
				$scduleData=DB::table("tbl_scedule_date")->where(['document_id'=>$user_dtl[0]])->get()->toArray();
				if(!empty($scduleData)){
					DB::table('tbl_scedule_date')->where('document_id', $user_dtl[0])->delete();
				}
				return view('front.user.vchat',$data);
			}
        }
        public function start_with_audiance(Request $request,$user_data='')
        {
        	     $data=array();
        	     $user_dtl=explode('~~', base64_decode($user_data));
        	     if(isset($user_dtl[0]) && isset($user_dtl[0])){
	        	     $data['user_cannel']=$user_dtl[0].$user_dtl[1];
	        	     $data['doc_id']=$user_dtl[0];
	        		 return view('front.user.audiance',$data);
        	     }
        }
        public function start_archieve(Request $request)
        {
        	$opentok = new OpenTok(env('OPENTOK_API_KEY'), env('OPENTOK_API_SECRET'));
        	$sessionId=session('sessionId');

        	$archive = $opentok->startArchive($sessionId);


			// Create an archive using custom options
			$archiveOptions = array(
			    'name' => 'Important Presentation',     // default: null
			    'hasAudio' => true,                     // default: true
			    'hasVideo' => true,                     // default: true
			    'outputMode' => OutputMode::COMPOSED,   // default: OutputMode::COMPOSED
			    'resolution' => '1280x720'              // default: '640x480'
			);
			$archive = $opentok->startArchive($sessionId, $archiveOptions);

			// Store this archiveId in the database for later use
			echo $archiveId = $archive->id;
			$count_vidsession=tbl_archive_video::select('*')->where(['sessionId'=>$sessionId,'archiveId'=>$archiveId])->count();
			if($count_vidsession==0){
				 tbl_archive_video::insert(['sessionId'=>$sessionId,'archiveId'=>$archiveId,'created_at'=>NOW()]);
			}
        }
        public function stop_archieve(Request $request, $archiveId='')
        {
        	$opentok = new OpenTok(env('OPENTOK_API_KEY'), env('OPENTOK_API_SECRET'));
        	echo $archiveId;
        	if($archiveId!=''){
	       	 	// Stop an Archive from an archiveId (fetched from database)
				$opentok->stopArchive($archiveId);
				// Stop an Archive from an Archive instance (returned from startArchive)
				$archive->stop();
			}
        }

        public function save_archieve_id(Request $request)
        {
        	
        }
        public function save_astream_id(Request $request)
        {
        	
        }



        public function start_broadcasting($value='')
        {
        	 $opentok = new OpenTok(env('OPENTOK_API_KEY'), env('OPENTOK_API_SECRET'));
        	$broadcast = $opentok->startBroadcast($sessionId);
			// Start a live streaming broadcast of a session, using broadcast options
			$options = array(
			    'layout' => Layout::getBestFit(),
			    'maxDuration' => 5400,
			    'resolution' => '1280x720'
			);
			$broadcast = $opentok->startBroadcast($sessionId, $options);

			// Store the broadcast ID in the database for later use
			$broadcastId = $broadcast->id;
        }


}
