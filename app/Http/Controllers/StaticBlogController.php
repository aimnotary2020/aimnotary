<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\tbl_company_info;
use App\Models\tbl_content_mngt_system;
use App\Models\tbl_blog;
use Validator;
use Auth;
use Hashids;
use App\User;
use App\Session;
//use Session;
use App\Models\tbl_login_log;
use App\Models\tbl_subscription;
use App\Models\tbl_coupon;
use App\Models\tbl_subscription_detail;
use App\Models\tbl_state;
use App\Models\tbl_citie;
use App\Models\tbl_lawyer;
use App\Models\tbl_user_document;
use Cookie;
//use Mail;1085
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Imagick;
require(base_path()."/pdfparser/vendor/autoload.php");
require(base_path()."/fpdf.php");

class StaticBlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public $data=array();
    protected $company_info=array();
    public function __construct()
    {
        $this->company_info=tbl_company_info::select('*')->where([['id','=','1']])->first();
        $url=$_SERVER['REQUEST_URI'];
        $resulr = DB::table('tbl_meta_data')->where(['link'=>$url])->get()->first();
        $this->data['meta_details'] = $resulr;
    }
    
    public function index()
    {
        
        $url=$_SERVER['REQUEST_URI'];
        
        if($url=='/online-notary-texas'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Texas',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Texas';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_texas')->with($this->data);
        }
        
        if($url=='/online-notary-tennessee'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Tennessee',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Tennessee';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_tennessee')->with($this->data);
        }
        
        if($url=='/online-notary-alabama'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service In Alabama',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service In Alabama';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_alabama')->with($this->data);
        }
        
        if($url=='/online-notary-florida'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service In Florida',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service In Florida';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_florida')->with($this->data);
        }
        
        if($url=='/online-notary-new-york'){
            
        
            $data = (object) array(
                'title' => 'Online notary new york',
                'description'=>'Online notary new york',
                'keyword'=>'Online notary new york',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Online notary new york';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_new_york')->with($this->data);
        }
        
        
        if($url=='/online-notary-kentucky'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service In Kentucky',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service In Kentucky';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_kentucky')->with($this->data);
        }
        
        // add 04-10-2021
        
        if($url=='/online-notary-alaska'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Alaska',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Alaska';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_alaska')->with($this->data);
        }
        
        if($url=='/online-notary-arizona'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Arizona',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Arizona';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_arizona')->with($this->data);
        }
        
        if($url=='/online-notary-arkansas'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Arkansas',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Arkansas';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_arkansas')->with($this->data);
        }
        
        if($url=='/online-notary-colorado'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Colorado',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Colorado';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_colorado')->with($this->data);
        }
        
        if($url=='/online-notary-hawaii'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Hawaii',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Hawaii';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_hawaii')->with($this->data);
        }
        
        
        // add 05-10-2021
        
        
        if($url=='/online-notary-kansas'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Kansas',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Kansas';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_kansas')->with($this->data);
        }
        
        if($url=='/online-notary-iowa'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Iowa',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Iowa';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_iowa')->with($this->data);
        }
        
        if($url=='/online-notary-illinois'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Illinois',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Illinois';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_illinois')->with($this->data);
        }
        
        if($url=='/online-notary-indiana'){
            
        
            $data = (object) array(
                'title' => 'Remote Online Notarization (RON) 24/7 Service in Indiana',
                'description'=>'',
                'keyword'=>'',
                'extratag'=>'',
                
            );
            $this->data['meta_details'] = $data;
            
            $this->data['page_title'] = 'Remote Online Notarization (RON) 24/7 Service in Indiana';
            $this->data['company_info'] = $this->company_info;
            return view('front.online_notary_indiana')->with($this->data);
        }
        
        
        
    }
    
}