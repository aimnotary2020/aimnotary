<?php

//namespace App\Http\Controllers\Client;
namespace App\Http\Controllers;
use Hash;
use Mail;
use Auth;
use DB;

use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

use App\User;
use App\Models\tbl_lawyer;
use App\Models\tbl_subscription;
use App\Models\tbl_coupon;
use App\Models\tbl_subscription_detail;
use App\Models\tbl_content_mngt_system;
use App\Models\tbl_user_document;
use App\Models\tbl_ssn_fail;
use App\Models\tbl_company_info;
use App\Models\tbl_user_doc_count;

use App\Models\tbl_stream_video; 
use App\Models\tbl_archive_video;
use App\Models\tbl_vid_chat_session;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public $data=array();
    protected $company_info=array();
    public function __construct()
    {
        //$this->middleware('auth');
        $this->company_info=tbl_company_info::select('*')->where([['id','=','1']])->first();
    }
    public function get_access_token()
    {
        $data = array('client_id'=>'qgRJ4mvwSSQgQJPyeAn8YZPngVFxyYAK','client_secret'=>'0f3TMbPYvQOmUswm','grant_type'=>'client_credentials');
        $data = json_encode($data);
        $url="https://apitest.microbilt.com/OAuth/GetAccessToken";
        $header = array("Content-Type: application/json");
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response1 = curl_exec($ch);
        curl_close($ch);
        
        if($response1)
        {
            $res=json_decode($response1);
            return $res->access_token;
        }
        else
        {
            return false;
        }
    } 

    public function email_oriug1($from,$to,$subject,$message) 
    {
        try 
        { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
            $headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
            $headers .= 'Bcc: rohitmajumder1983@gmail.com,amitdasgupta1980@gmail.com,markr@ez8a.com' . "\r\n";
            if(mail($to, $subject, $message, $headers))
            {
                return true;
            } 
            else
            {
                return false;
            }
        }
        catch(Exception $e) 
        {
              echo 'Message: ' .$e->getMessage();
        }
    }

    public function email($from,$to,$subject,$message) 
    {
        try 
        { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
            $headers .= 'Cc: esther@aimnotary.com ' . "\r\n";
                      
            //Multiple BCC, same as CC above;
            $headers .= 'Bcc: rohitmajumder1983@gmail.com,info@aimnotary.com' . "\r\n";
            // Sending email
            if(mail($to, $subject, $message, $headers))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch(Exception $e) 
        {
              echo 'Message: ' .$e->getMessage();
        }
    }

    public function delete_user_email(Request $req)
    {
		$jsonobj=$req->json()->all();
		
		$status=User::where('email',$jsonobj['email'])->forceDelete();
		if($status > 0) {
		
			echo json_encode(['status' => 200, 'msg' =>'users with email deleted !']);
		}
    }
    public function register_user_enterprise(Request $req)
    {
    	$jsonobj=$req->json()->all();
    	$check_email=User::where('email',$jsonobj['email'])->count();
        if($check_email > 0)
        {
   	        echo json_encode(['status' => 201, 'msg' =>'Your Request has already been submitted !']);
   	        die();
        }
        $userDSocData=array(
                                'name'=>$jsonobj['first_name']." ".$jsonobj['last_name'],
                                'email'=>$jsonobj['email'],
                                'personal_mobile'=>$jsonobj['mobile'],
                                'state'=>$jsonobj['state'],
                                'city'=>$jsonobj['city'],
                                'zip_code'=>$jsonobj['zip'],
                                'add1'=>$jsonobj['addr'],
                                'password'=>Hash::make($jsonobj['password']),
                                'is_privacy'=>1,
                                'is_terms'=>1,
                                'user_category' => 'E',
                                'created_at'=>NOW(),
                                'is_active' => 0,
        );
                                
        $data=DB::table("users")->insert($userDSocData);
        // $update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW());
        //User::where('email',$jsonobj['email'])->update($update_arr);
        $user_data="

            <b><p>1. First Name - <span style='color : blue;'>".$jsonobj['first_name']."</span></p></b>

            <b><p>2. Last Name - <span style='color : blue;'>".$jsonobj['last_name']."</span></p></b>

            <b><p>3. Business Email Address - <span style='color : blue;'>".$jsonobj['email']."</span></p></b>

            <b><p>4. State - <span style='color : blue;'>".$jsonobj['state']."</span></p></b>

            <b><p>5. City - <span style='color : blue;'>".$jsonobj['city']."</span></p></b>

            <b><p>6. Address - <span style='color : blue;'>".$jsonobj['addr']."</span></p></b>

            <b><p>7. Company Name - <span style='color : blue;'>".$jsonobj['company_name']."</span></p></b>

            <b><p>8. Mobile - <span style='color : blue;'>".$jsonobj['mobile']."</span></p></b>

            <b><p>9. How Many Notarizations Does Your Business Complete Monthly? - <span style='color : blue;'>".$jsonobj['nt_required']."</span></p></b>

            <b><p>10. What best describes your company's Notarization needs? - <span style='color : blue;'>".$jsonobj['nt_cmp_describe']."</span></p></b>

            <b><p>11. License and/or Outsource? (License=Your Notary Publics Complete Outsource=Aim notary’s Professional Notary Publics Complete) - <span style='color : blue;'>".$jsonobj['lic_out']."</span></p></b>

        ";

        $userhtml="
            <h3>Thanks for contacting Aim Notary. Now sit back and relax. Our executive will reach you shortly.</h3><br>

            <b><p>1. First Name - <span style='color : blue;'>".$jsonobj['first_name']."</span></p></b>

            <b><p>2. Last Name - <span style='color : blue;'>".$jsonobj['last_name']."</span></p></b>

            <b><p>3. Business Email Address - <span style='color : blue;'>".$jsonobj['email']."</span></p></b>

            <b><p>4. Password - <span style='color : blue;'>".$jsonobj['password']."</span></p></b>

            <b><p>5. State - <span style='color : blue;'>".$jsonobj['state']."</span></p></b>

            <b><p>6. City - <span style='color : blue;'>".$jsonobj['city']."</span></p></b>

            <b><p>7. Address - <span style='color : blue;'>".$jsonobj['addr']."</span></p></b>

            <b><p>8. Company Name - <span style='color : blue;'>".$jsonobj['company_name']."</span></p></b>

            <b><p>9. Mobile - <span style='color : blue;'>".$jsonobj['mobile']."</span></p></b>

            <b><p>10. How Many Notarizations Does Your Business Complete Monthly? - <span style='color : blue;'>".$jsonobj['nt_required']."</span></p></b>

            <b><p>11. What best describes your company's Notarization needs? - <span style='color : blue;'>".$jsonobj['nt_cmp_describe']."</span></p></b>

            <b><p>12. License and/or Outsource? (License=Your Notary Publics Complete Outsource=Aim notary’s Professional Notary Publics Complete) - <span style='color : blue;'>".$jsonobj['lic_out']."</span></p></b>

        ";
        $html = View('emails.enterprise-mail', ['user'=> $user_data,'admin'=>'false']);
        $mail_status=$this->smtp_auth_email("info@aimnotary.com", $subject = 'Thanks For registering to aimnotary.com', $message = $html);
        $mail_status=$this->smtp_auth_email("esther@aimnotary.com", $subject = 'Thanks For registering to aimnotary.com', $message = $html);
                                
        $html = View('emails.enterprise-mail', ['user'=> $userhtml,'admin'=>'false']);
        $mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $jsonobj['email'], $subject = 'Thanks For registering to aimnotary.com', $message = $html);
        
        echo json_encode(['status' => 200, 'msg' =>'Your Request has been submitted successfully']);
    }

    public function change_pwd(Request $req)
    {
    	$jsonobj=$req->json()->all();
    	$user_id=$jsonobj['user_id'];
        $user_data=array('password'=>Hash::make($jsonobj['password']));
        $a= User::where('id',$user_id)->update($user_data);
        echo json_encode(['status' => 200, 'msg' =>'details successfully updated.']);
    }
    public function user_account_update(Request $req)
    {
    	$jsonobj=$req->json()->all();
    	$user_id=$jsonobj['user_id'];
        $user_data=array(
                            'personal_mobile'=>$jsonobj['mobile'],
                            'state' => $jsonobj['state'],
                            'city' => $jsonobj['city'],
                            'zip_code' => $jsonobj['zip'],
                            'add1'=>$jsonobj['addr'],
        );
        $a= User::where('id',$user_id)->update($user_data);
        echo json_encode(['status' => 200, 'msg' =>'details successfully updated.']);
    }
    public function resubscription_user(Request $request)
    {
        $jsonobj=$request->json()->all();
    	$user_id=$jsonobj['user_id'];
        $product_price=$jsonobj['product_price'];
        if($product_price=='25')
        {
            $data=[
                'user_id' => $user_id,
                'doc'   => 1,
                'amount' => NULL,
                'payment_status' => NULL,
                'status' => 'A',
                'date'  => date("Y-m-d"),
                'time' => date("Y-m-d H:i:s")
            ];
            tbl_user_doc_count::insert($data);
            echo json_encode(['status' => 200, 'msg' =>"payment_success,Resubscription Successfull"]);
        }
        else
        {
            $expcard=explode('/',$jsonobj['card_expiry']);
    		$cc_expiry=$expcard[0].$expcard[1];
    		$amount=$product_price;
    		$cardholder_name=$jsonobj['card_holder'];
    		$cc_number=$jsonobj['card_number'];
    		//$url='https://api.demo.globalgatewaye4.firstdata.com/transaction/v11'; //demo
            //$gateway_id='MD6401-51'; $password='EHjdMqG1sz8oiNvp5GOFg9ZfiKgiz0yc'; //demo

            $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
            // $gateway_id='K84040-06'; 
            // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

            $gateway_id='Q66919-01'; 
            $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
            $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" =>$cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 

            //print_r($data);die();
            $data_string= json_encode($data);

            // Initializing curl
            $ch = curl_init( $url );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8;","Accept: application/json" ));

            // Getting results
            $result = curl_exec($ch);
            if($result)
            {
                // Getting jSON result string
                $bankres = json_decode($result);
            }

            if(isset($bankres->bank_resp_code) && $bankres->bank_resp_code==100)
            {
                $data=[
                        'user_id' => $user_id,
                        'doc'   => 5,
                        'amount' => 99,
                        'payment_status' => 'C',
                        'status' => 'A',
                        'date'  => date("Y-m-d"),
                        'time' => date("Y-m-d H:i:s")
                ];
                tbl_user_doc_count::insert($data);
                echo json_encode(['status' => 200, 'msg' =>"payment_success,Resubscription Successfull"]);
    	    }
            else 
            {
    		    echo json_encode(['status' => 201, 'msg' =>"payment failed"]);
    	    }
        }
    }
    public function dateConvert($old_date,$format='jS F, Y')
    {
    	$old_date_timestamp = strtotime($old_date);
    	$new_date = date($format, $old_date_timestamp); 
    	return $new_date;
    }
    public function submit_schedule_user(Request $request)
    {
    	$jsonobj=$request->json()->all();
        $user_id=$jsonobj['user_id'];
    	$select=DB::table("tbl_scedule_date")->select("*")->where('date','=',$jsonobj['starts_at'])->where('document_id','=',$jsonobj['document_id'])->get()->toArray();
        if(empty($select))
        {
    		$data=[
        			'user_id' => $user_id,
        			'title' => $jsonobj['title'],
        			'date' => $jsonobj['starts_at'],
        			'time' => $jsonobj['time'],
        			'status' => 'ACTIVE',
        			'document_id' => $jsonobj['document_id'],
        			'description' => $jsonobj['description'],
        			'timezone' => $jsonobj['timezone'],
        			'location' => $jsonobj['location'],
    		];
    		DB::table('tbl_scedule_date')->insert($data);
    		$user=user::where('id','=',$user_id)->get()->toArray();
    		//$user_data="Appointment has been scheduled on ".$this->dateConvert($jsonobj['starts_at'])." at ".$jsonobj['time'];

            //======================== Code Modified ===========================
            $user_data="Hello ".$user[0]['name'].", Your Appointment has been scheduled on ".$this->dateConvert($jsonobj['starts_at'])." at ".$jsonobj['time']."<br><br>".$this->htmlScedule( $jsonobj['document_id']);
            //==================================================================
            
    		$html = View('emails.enterprise-mail', ['user'=> $user_data,'admin'=>"false"]);
    		$admin = View('emails.enterprise-mail', ['user'=> $user_data,'admin'=>"true"]);
    		$this->email($from = "no-reply@aimnotary.com", $to = $user[0]['email'], $subject = 'New Appointment is Scheduled', $message = $html);
    		$this->smtp_auth_email("info@aimnotary.com", $subject = 'New Appointment is Scheduled', $admin);
    		$ch = curl_init();
    		$fields = $data;
    		$postvars="name=".$user[0]['name']."&email=".$user[0]['email']."&";
    		foreach($fields as $key=>$value) 
            {
    			$postvars .= $key . "=" . $value . "&";
    		}
    		$url = "https://aimnotary.com/calender/cal.php";
    		curl_setopt($ch,CURLOPT_URL,$url);
    		curl_setopt($ch,CURLOPT_POST, 1); //0 for a get request
    		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
    		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
    		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
    		$response = curl_exec($ch);
    		curl_close ($ch); 
    		die(json_encode(['status'=>1,'msg'=>'Appointment Has been Scheduled Successfully']));
    		
    	}
    	else if(!empty($select))
    	{
    		$select2=DB::table("tbl_scedule_date")->select("*")->where('document_id','=',$jsonobj['document_id'])->get()->toArray();
    		if(!empty($select2))
    		{
    			die(json_encode(['status'=>0,'msg'=>'You have already created a schedule for this document']));
    		}
    		else
    		{
    			foreach($select as $dataVal)
    			{
    				if($dataVal->time==$request->time)
    				{
    					die(json_encode(['status'=>0,'msg'=>'This time is already scedule with someone else']));
    				}
    			}
    		}
    	}
    }
    public function get_scedule(Request $request)
    {
    	$jsonobj=$request->json()->all();
    	$result = DB::select('select * from tbl_scedule_date where date>="'.date("Y-m-d").'" AND document_id="'.$jsonobj['document_id'].'"');
    	
    	if(isset($result[0]))
        {
    	    $desc="<b> Title : </b> ".$result[0]->title."<br>";
    	    $desc.="<b> Date : </b> ".$this->dateConvert($result[0]->date)."<br>";
    	    $desc.="<b> Time : </b> ".$result[0]->time."<br>";
    	    if($result[0]->timezone=="-8")
            {
    		    $timezone="Pacific Time (US & Canada)";
    	    }
            elseif($result[0]->timezone=="-7")
            {
    		    $timezone="Mountain Time (US & Canada)";
    	    }
            elseif($result[0]->timezone=="-6")
            {
    	 	    $timezone="Central Time (US & Canada)";
    	    }
            elseif($result[0]->timezone=="-5")
            {
    		    $timezone="Eastern Time (US & Canada)";
    	    }
        	$desc.="<b>Time Zone : </b> ".$timezone."<br>";
        	$desc.="<b>Location : </b>".$result[0]->location."<br>";
        	$desc.="<b>Description : </b>".$result[0]->description."<br>";
        	$response=array('status' => '200','Title' => $result[0]->title,'Date'=> $this->dateConvert($result[0]->date),'Time'=>$result[0]->time,'Time Zone'=> $timezone,'Location' => $result[0]->location,'Description' => $result[0]->description );
        	echo json_encode($response);
    	}
        else 
        {
    	    $response=array('status' => '201','err' => 'scheduled date is less than current date');
    	    echo json_encode($response);
    	}
    }
    public function htmlScedule($id)
    {
        $result = DB::select('select * from tbl_scedule_date where date>="'.date("Y-m-d").'" AND document_id="'.$id.'"');
        $desc="<b> Title : </b> ".$result[0]->title."<br>";
        $desc.="<b> Date : </b> ".$this->dateConvert($result[0]->date)."<br>";
        $desc.="<b> Time : </b> ".$result[0]->time."<br>";
        if($result[0]->timezone=="-8"){
            $timezone="Pacific Time (US & Canada)";
          }elseif($result[0]->timezone=="-7"){
            $timezone="Mountain Time (US & Canada)";
          }elseif($result[0]->timezone=="-6"){
            $timezone="Central Time (US & Canada)";
          }elseif($result[0]->timezone=="-5"){
            $timezone="Eastern Time (US & Canada)";
          }
          $desc.="<b>Time Zone : </b> ".$timezone."<br>";
          $desc.="<b>Location : </b>".$result[0]->location."<br>";
          $desc.="<b>Description : </b>".$result[0]->description."<br>";
          return $desc;
    }
    public function register_user(Request $req)
    {
    	$jsonobj=$req->json()->all();
    	$product_price=$jsonobj['product_price'];
    	$addr1=$addr2=$country=$city=$zip_code='';
    	if(isset($jsonobj['addr1'])){
    		$addr1=$jsonobj['addr1'];
        }
    	if(isset($jsonobj['addr2'])){
    		$addr2=$jsonobj['addr2'];
        }
        if(isset($jsonobj['country'])){
    		$country=$jsonobj['country'];
        }
        if(isset($jsonobj['city'])){
    		$city=$jsonobj['city'];
        }
        if(isset($jsonobj['zip_code'])){
    		$zip_code=$jsonobj['zip_code'];
        }
        
        //payment check
        if($product_price == '99')
        {
        
            //first check if user is registered and already paid
            $check_email=User::where('email',$jsonobj['email'])->count();
        		 
            if($check_email > 0)
            {
            	$user = User::where('email',$jsonobj['email'])->first();
                $user_id=$user->id;
                $check_subs= tbl_subscription_detail::where('user_id',$user_id)->count();
                if($check_subs > 0)
                {
                    echo json_encode(['status' => 201, 'msg' =>'you have already registerd and paid !']);
                	die();
                }
            }
            $expcard=explode('/',$jsonobj['payment']['card_expiry']);
    		$cc_expiry=$expcard[0].$expcard[1];
    		$amount=$product_price;
    		$cardholder_name=$jsonobj['payment']['card_holder'];
    		$cc_number=$jsonobj['payment']['card_number'];
    		//$url='https://api.demo.globalgatewaye4.firstdata.com/transaction/v11'; //demo
            //$gateway_id='MD6401-51'; $password='EHjdMqG1sz8oiNvp5GOFg9ZfiKgiz0yc'; //demo

            //$url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
            //$gateway_id='K84040-06'; $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live
                
            $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
            // $gateway_id='K84040-06'; 
            // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

            $gateway_id='Q66919-01'; 
            $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
            $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" =>$cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 

            //print_r($data);die();
            $data_string= json_encode($data);

            // Initializing curl
            $ch = curl_init( $url );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8;","Accept: application/json" ));

            // Getting results
            $result = curl_exec($ch);
            if($result)
            {
                // Getting jSON result string
                $newres = json_decode($result);
            }
            if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100)
            {
            
            }
            else 
            {
            	echo json_encode(['status' => 202, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
                die();
            }
        }
        
    	////user registration
    	    	
    	$name=$jsonobj['first_name'].' ' .$jsonobj['last_name'];
        $user_data=array(
                            'name'=>$name,
                            'email'=>$jsonobj['email'],
                            'personal_mobile'=>$jsonobj['mobile'],
                            'password'=>Hash::make($jsonobj['password']),
                            'add1'=>$addr1,
                            'add2'=>$addr2,
                            'country'=>$country,
                            'city'=>$city,
                            'zip_code'=>$zip_code,
                            'is_privacy'=>'0',
                            'is_terms'=>$jsonobj['terms'],
                            //'email_verified_at'=>NOW(),
                            'created_at'=>NOW()
        );
    	$check_email=User::where('email',$jsonobj['email'])->count();
    		 
        if($check_email == 0)
        {
        	if($product_price == '99')
            {
        		$user_data['email_verified_at'] = NOW();
        		$user_data['updated_at'] = NOW();
        	}
            $user_data['created_at']=NOW();
            $add_user=User::insertGetId($user_data);
            $user_id=$add_user;
            //put inti tbl_doc_user_count
            $tbl_doc_data=array();
            $tbl_doc_data['user_id']=$user_id;
            $tbl_doc_data['amount']=$product_price;
            if($product_price == '25' || $product_price== '79')
            {
            	$tbl_doc_data['payment_status'] = 'P';
            	$tbl_doc_data['doc']='1';
            }
            if($product_price == '99')
            {
            	$tbl_doc_data['payment_status'] = 'C';
            	$tbl_doc_data['doc']='5';
            }
            if( $product_price== '79')
            {
            	$tbl_doc_data['is_usa'] = 'Y';
            }
            else 
            {
            	$tbl_doc_data['is_usa'] = 'N';
            }
           
            $tbl_doc_data['date']=date("Y-m-d");
            $tbl_doc_data['time']=date("Y-m-d H:i:s");
            $tbl_doc_data['status']='A';
            $a= tbl_user_doc_count::insert($tbl_doc_data);
            //send verification email for 25 and 79
            if ($product_price == '25' || $product_price == '79')
            {
                $html = View('emails.verification-mail', ['user'=> $user_data]);
                $mail_status=$this->email($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Thanks For registering to aimnotary.com. Please verify your email address.', $message = $html);
                echo json_encode(['status' => 200, 'message' => 'We have sent you a Verification Email']);
            }
            if ($product_price == '99')
            {
            	//payment done, insert into suscription table
            	if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100)
                {
                    $subs_arr=array();
                    $subs_arr['card_number']=$cc_number; 
                    $subs_arr['card_expiry']=$jsonobj['payment']['card_expiry'];
                    $subs_arr['card_holder']=$cardholder_name;
                    $subs_arr['product_price']=$product_price;
                    $subs_arr['total_cost']=$amount; 
                    $subs_arr['pay_id']=$newres->authorization_num;
                    $subs_arr['card_type']=$newres->credit_card_type;
                    $subs_arr['subs_date']=NOW();
                    //$subs_arr['coupon_id']=$jsonobj['coupon_id'];
                    $subs_arr['user_id']=$user_id;
                    $subs_arr['trans_json']='abc';
                    $subs_arr['no_of_signers']='1';
                    $subs_arr['no_of_stamps']='1';
                    $subs_arr['created_at']=NOW();

                    $subs= tbl_subscription_detail::insert($subs_arr);
                   
                    $html = View('emails.paid-subscription', ['user'=> $user_data]);
                    $mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Thanks For subscribing to AimNotary.", $message = $html);

                    $html1 = View('emails.admin_subs_mail', ['user'=> $user_data]);
                    $mail_status1=$this->email($from = "no-reply@aimnotary.com", $to = "esther@aimnotary.com", $subject = "New User subscribed  to AimNotary", $message = $html1);
                    if(($mail_status > 0) && ($mail_status1 > 0))
                    {
                        echo json_encode(['status' => 200, 'msg' =>'Thanks For subscribing to AimNotary!']);
                    }
                    else
                    {
                        echo json_encode(['status' => 201, 'msg' =>'Mail could not be sent,something went wrong']);
                    }
                }
                else
                {
                   echo json_encode(['status' => 202, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
                   die();
                }
            }
        }
        else
        {
            $user_data['updated_at']=NOW();
            $user = User::where('email',$jsonobj['email'])->first();
            $user_id=$user->id;
            User::where('id',$user_id)->update($user_data);
            echo json_encode(['status' => 201, 'message' => 'You have already registerd!']);
        }
    }
    
    //===================== Check user-email already exists ==============================

    public function isExist_emailId(Request $req)
    {
        $jsonobj=$req->json()->all(); //data='{"email":"sandip.ray@gmail.com"}'
        $check_email=User::where('email',$jsonobj['email'])->count();
        if($check_email>0)
        {
            $errMsg=array(
                            'status'=>200,
                            'isRegistered'=>1,
                            'msg'=>'User exists'
            );
            echo json_encode($errMsg);
        }
        else
        {
           $errMsg=array(
                            'status'=>202,
                            'isRegistered'=>0,
                            'msg'=>'User does not exist'
            );
            echo json_encode($errMsg); 
        }
    }

    //=================================== END ============================================

    //===================== Registration of user from IOS only ===========================

    public function register_user_IOS(Request $req)
    {
        $jsonobj=$req->json()->all(); //data='{"user_type": "User", "password": "123456", "receipt": "MIITqwYJKoZIhvcNAQcCoIITnDCCE5gCAQExCzAJBgUrDgMCGgUAMI", "mobile": "9831098310", "last_name": "Y", "is_active": "1", "email": "Amit.y@aqbsolutions.com", "first_name": "Amit", "privacy": "1", "product_price": "99", "terms": "1"}'
        //echo $jsonobj['last_name'];die;
        $product_price=$jsonobj['product_price'];
        $addr1=$addr2=$country=$city=$zip_code='';
        if(isset($jsonobj['addr1'])){
            $addr1=$jsonobj['addr1'];
        }
        if(isset($jsonobj['addr2'])){
            $addr2=$jsonobj['addr2'];
        }
        if(isset($jsonobj['country'])){
            $country=$jsonobj['country'];
        }
        if(isset($jsonobj['city'])){
            $city=$jsonobj['city'];
        }
        if(isset($jsonobj['zip_code'])){
            $zip_code=$jsonobj['zip_code'];
        }
        
        //payment check
        if($product_price == '99')
        {
            //first check if user is registered and already paid
            $check_email=User::where('email',$jsonobj['email'])->count();
            if($check_email > 0)
            {
                $user = User::where('email',$jsonobj['email'])->first();
                $user_id=$user->id;
                $check_subs= tbl_subscription_detail::where('user_id',$user_id)->count();
                if($check_subs > 0)
                {
                    echo json_encode(['status' => 201, 'msg' =>'you have already registerd and paid !']);
                    die();
                }
            }
            //============================ Receipt Validation in APPLE Production Server ========================   

            $url='https://buy.itunes.apple.com/verifyReceipt'; //Live
            $data = array("receipt-data" => $jsonobj['receipt'], "password" => 'c1e47853c6be4d369ce2018f2ed4a834'); 
            //print_r($data);die();
            $data_string= json_encode($data);

            // Initializing curl
            $ch = curl_init( $url );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8;","Accept: application/json"));

            // Getting results
            $result = curl_exec($ch);
            if($result)
            {
                // Getting jSON result string
                $newres = json_decode($result);
                //echo $newres->status;die;
                if($newres->status==0)
                {
                    $str=$newres->receipt->in_app[0]->product_id;
                    if(($str=='Business99Pro'))
                    {
                        $name=$jsonobj['first_name'].' ' .$jsonobj['last_name'];
                        $user_data=array(
                                            'name'=>$name,
                                            'email'=>$jsonobj['email'],
                                            'personal_mobile'=>$jsonobj['mobile'],
                                            'password'=>Hash::make($jsonobj['password']),
                                            'add1'=>$addr1,
                                            'add2'=>$addr2,
                                            'country'=>$country,
                                            'city'=>$city,
                                            'zip_code'=>$zip_code,
                                            'is_privacy'=>'0',
                                            'is_terms'=>$jsonobj['terms'],
                                            //'email_verified_at'=>NOW(),
                                            'created_at'=>NOW()
                        );
                        $check_email=User::where('email',$jsonobj['email'])->count();
                        if($check_email == 0) // If the user does not exists.
                        {
                            if($product_price == '99')
                            {
                                $user_data['email_verified_at'] = NOW();
                                $user_data['updated_at'] = NOW();
                            }
                            $user_data['created_at']=NOW();
                            $add_user=User::insertGetId($user_data);
                            $user_id=$add_user;
                            if($user_id)
                            {
                                //put inti tbl_doc_user_count
                                $tbl_doc_data=array();
                                $tbl_doc_data['user_id']=$user_id;
                                $tbl_doc_data['amount']=$product_price;
                                if($product_price == '25' || $product_price== '79')
                                {
                                    $tbl_doc_data['payment_status'] = 'P';
                                    $tbl_doc_data['doc']='1';
                                }
                                if($product_price == '99')
                                {
                                    $tbl_doc_data['payment_status'] = 'C';
                                    $tbl_doc_data['doc']='5';
                                }
                                if( $product_price== '79')
                                {
                                    $tbl_doc_data['is_usa'] = 'Y';
                                }
                                else 
                                {
                                    $tbl_doc_data['is_usa'] = 'N';
                                }
                               
                                $tbl_doc_data['date']=date("Y-m-d");
                                $tbl_doc_data['time']=date("Y-m-d H:i:s");
                                $tbl_doc_data['status']='A';
                                $a= tbl_user_doc_count::insert($tbl_doc_data);
                                //send verification email for 25 and 79
                                if ($product_price == '25' || $product_price == '79')
                                {
                                    $html = View('emails.verification-mail', ['user'=> $user_data]);
                                    $mail_status=$this->email($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Thanks For registering to aimnotary.com. Please verify your email address.', $message = $html);
                                    echo json_encode(['status' => 200, 'message' => 'We have sent you a Verification Email']);
                                }
                                $errMsg=array(
                                                'status'=>200,
                                                'msg'=>'Thanks For subscribing to AimNotary!'
                                );
                                echo json_encode($errMsg);     
                            }
                        }
                        else
                        {
                            $user_data['updated_at']=NOW();
                            $user = User::where('email',$jsonobj['email'])->first();
                            $user_id=$user->id;
                            User::where('id',$user_id)->update($user_data);
                            echo json_encode(['status' => 201, 'message' => 'You have already registerd!']);
                        }
                    }
                    else
                    {
                        $errMsg=array(
                                        'status'=>202,
                                        'msg'=>'Invalid Product'
                        );
                        echo json_encode($errMsg);
                    }
                }
                ///============================ Receipt Validation in Sandbox Server ========================  
                elseif($newres->status==21007)
                {
                    $sandbox_url='https://sandbox.itunes.apple.com/verifyReceipt'; //Sandbox
                    $data = array("receipt-data" => $jsonobj['receipt'], "password" => 'c1e47853c6be4d369ce2018f2ed4a834'); 

                    //print_r($data);die();
                    $data_string= json_encode($data);

                    // Initializing curl
                    $ch1 = curl_init($sandbox_url );
                    curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch1, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch1, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8;","Accept: application/json"));
                    $rval=curl_exec($ch1);
                    //var_dump($rval);die;
                    $newjsobj=json_decode($rval);
                    //echo $newjsobj->receipt->in_app[0]->product_id;
                    $str=$newjsobj->receipt->in_app[0]->product_id;
                    //if(($newjsobj->latest_receipt_info[0]->product_id)=='99businesspro')
                    if(($str=='Business99Pro'))
                    {
                        $name=$jsonobj['first_name'].' ' .$jsonobj['last_name'];
                        $user_data=array(
                                            'name'=>$name,
                                            'email'=>$jsonobj['email'],
                                            'personal_mobile'=>$jsonobj['mobile'],
                                            'password'=>Hash::make($jsonobj['password']),
                                            'add1'=>$addr1,
                                            'add2'=>$addr2,
                                            'country'=>$country,
                                            'city'=>$city,
                                            'zip_code'=>$zip_code,
                                            'is_privacy'=>'0',
                                            'is_terms'=>$jsonobj['terms'],
                                            //'email_verified_at'=>NOW(),
                                            'created_at'=>NOW()
                        );
                        $check_email=User::where('email',$jsonobj['email'])->count();
                        if($check_email == 0)
                        {
                            if($product_price == '99')
                            {
                                $user_data['email_verified_at'] = NOW();
                                $user_data['updated_at'] = NOW();
                            }
                            $user_data['created_at']=NOW();
                            $add_user=User::insertGetId($user_data);
                            $user_id=$add_user;
                            if($user_id)
                            {
                                //put inti tbl_doc_user_count
                                $tbl_doc_data=array();
                                $tbl_doc_data['user_id']=$user_id;
                                $tbl_doc_data['amount']=$product_price;
                                if($product_price == '25' || $product_price== '79')
                                {
                                    $tbl_doc_data['payment_status'] = 'P';
                                    $tbl_doc_data['doc']='1';
                                }
                                if($product_price == '99')
                                {
                                    $tbl_doc_data['payment_status'] = 'C';
                                    $tbl_doc_data['doc']='5';
                                }
                                if( $product_price== '79')
                                {
                                    $tbl_doc_data['is_usa'] = 'Y';
                                }
                                else 
                                {
                                    $tbl_doc_data['is_usa'] = 'N';
                                }
                               
                                $tbl_doc_data['date']=date("Y-m-d");
                                $tbl_doc_data['time']=date("Y-m-d H:i:s");
                                $tbl_doc_data['status']='A';
                                $a= tbl_user_doc_count::insert($tbl_doc_data);
                                //send verification email for 25 and 79
                                if ($product_price == '25' || $product_price == '79')
                                {
                                    $html = View('emails.verification-mail', ['user'=> $user_data]);
                                    $mail_status=$this->email($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Thanks For registering to aimnotary.com. Please verify your email address.', $message = $html);
                                    echo json_encode(['status' => 200, 'message' => 'We have sent you a Verification Email']);
                                }
                                $errMsg=array(
                                                'status'=>200,
                                                'msg'=>'Thanks For subscribing to AimNotary!'
                                );
                                echo json_encode($errMsg);     
                            }
                        }
                        else
                        {
                            $user_data['updated_at']=NOW();
                            $user = User::where('email',$jsonobj['email'])->first();
                            $user_id=$user->id;
                            User::where('id',$user_id)->update($user_data);
                            echo json_encode(['status' => 201, 'message' => 'You have already registerd!']);
                        }
                    }
                    //die();
                }
                else
                {
                    $errMsg=array(
                                    'status'=>202,
                                    'msg'=>'Failed'
                    );
                    echo json_encode($errMsg); 
                    die();
                }
            }
        }
        
        ////user registration
                
        /*$name=$jsonobj['first_name'].' ' .$jsonobj['last_name'];
        $user_data=array(
                            'name'=>$name,
                            'email'=>$jsonobj['email'],
                            'personal_mobile'=>$jsonobj['mobile'],
                            'password'=>Hash::make($jsonobj['password']),
                            'add1'=>$addr1,
                            'add2'=>$addr2,
                            'country'=>$country,
                            'city'=>$city,
                            'zip_code'=>$zip_code,
                            'is_privacy'=>'0',
                            'is_terms'=>$jsonobj['terms'],
                            //'email_verified_at'=>NOW(),
                            'created_at'=>NOW()
        );
        $check_email=User::where('email',$jsonobj['email'])->count();
             
        if($check_email == 0)
        {
            if($product_price == '99')
            {
                $user_data['email_verified_at'] = NOW();
                $user_data['updated_at'] = NOW();
            }
            $user_data['created_at']=NOW();
            $add_user=User::insertGetId($user_data);
            $user_id=$add_user;
            //put inti tbl_doc_user_count
            $tbl_doc_data=array();
            $tbl_doc_data['user_id']=$user_id;
            $tbl_doc_data['amount']=$product_price;
            if($product_price == '25' || $product_price== '79')
            {
                $tbl_doc_data['payment_status'] = 'P';
                $tbl_doc_data['doc']='1';
            }
            if($product_price == '99')
            {
                $tbl_doc_data['payment_status'] = 'C';
                $tbl_doc_data['doc']='5';
            }
            if( $product_price== '79')
            {
                $tbl_doc_data['is_usa'] = 'Y';
            }
            else 
            {
                $tbl_doc_data['is_usa'] = 'N';
            }
           
            $tbl_doc_data['date']=date("Y-m-d");
            $tbl_doc_data['time']=date("Y-m-d H:i:s");
            $tbl_doc_data['status']='A';
            $a= tbl_user_doc_count::insert($tbl_doc_data);
            //send verification email for 25 and 79
            if ($product_price == '25' || $product_price == '79')
            {
                $html = View('emails.verification-mail', ['user'=> $user_data]);
                $mail_status=$this->email($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Thanks For registering to aimnotary.com. Please verify your email address.', $message = $html);
                echo json_encode(['status' => 200, 'message' => 'We have sent you a Verification Email']);
            }

            // if ($product_price == '99')
            // {
            //     //payment done, insert into suscription table
            //     if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100)
            //     {
            //         $subs_arr=array();
            //         $subs_arr['card_number']=$cc_number; 
            //         $subs_arr['card_expiry']=$jsonobj['payment']['card_expiry'];
            //         $subs_arr['card_holder']=$cardholder_name;
            //         $subs_arr['product_price']=$product_price;
            //         $subs_arr['total_cost']=$amount; 
            //         $subs_arr['pay_id']=$newres->authorization_num;
            //         $subs_arr['card_type']=$newres->credit_card_type;
            //         $subs_arr['subs_date']=NOW();
            //         //$subs_arr['coupon_id']=$jsonobj['coupon_id'];
            //         $subs_arr['user_id']=$user_id;
            //         $subs_arr['trans_json']='abc';
            //         $subs_arr['no_of_signers']='1';
            //         $subs_arr['no_of_stamps']='1';
            //         $subs_arr['created_at']=NOW();

            //         $subs= tbl_subscription_detail::insert($subs_arr);
                   
            //         $html = View('emails.paid-subscription', ['user'=> $user_data]);
            //         $mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Thanks For subscribing to AimNotary.", $message = $html);

            //         $html1 = View('emails.admin_subs_mail', ['user'=> $user_data]);
            //         $mail_status1=$this->email($from = "no-reply@aimnotary.com", $to = "esther@aimnotary.com", $subject = "New User subscribed  to AimNotary", $message = $html1);
            //         if(($mail_status > 0) && ($mail_status1 > 0))
            //         {
            //             echo json_encode(['status' => 200, 'msg' =>'Thanks For subscribing to AimNotary!']);
            //         }
            //         else
            //         {
            //             echo json_encode(['status' => 201, 'msg' =>'Mail could not be sent,something went wrong']);
            //         }
            //     }
            //     else
            //     {
            //        echo json_encode(['status' => 202, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
            //        die();
            //     }
            // }
        }
        else
        {
            $user_data['updated_at']=NOW();
            $user = User::where('email',$jsonobj['email'])->first();
            $user_id=$user->id;
            User::where('id',$user_id)->update($user_data);
            echo json_encode(['status' => 201, 'message' => 'You have already registerd!']);
        }*/
    } 

    //================================= END ==============================================
    public function register_user_orig(Request $req)
    {
        $jsonobj=$req->json()->all(); // data='{"product_price":"25","user_type":"User","first_name":"Sandip","last_name":"Ray","mobile":"9867443210","email":"sandip.pr@aqbsolutions.com","password":"123456","privacy":"1","terms":"1","country":"231","city":"kol","addr1":"APC boulevard","addr2":"","zip":"0564","token":"","is_active":"1","payment":{"card_number":"5123456789012346","card_expiry":"05/21","card_holder":"sandip ray"}}'
        
        $expcard=explode('/',$jsonobj['payment']['card_expiry']);
        $cc_expiry=$expcard[0].$expcard[1];
        $amount='0.01';
        $cardholder_name=$jsonobj['payment']['card_holder'];
        $cc_number=$jsonobj['payment']['card_number'];
        $name=$jsonobj['first_name'].$jsonobj['last_name'];
        $user_data=array(
                            'name'=>$name,
                            'email'=>$jsonobj['email'],
                            'personal_mobile'=>$jsonobj['mobile'],
                            'password'=>Hash::make($jsonobj['password']),
                            'add1'=>$jsonobj['addr1'],
                            'add2'=>($jsonobj['addr2']!='' ? $jsonobj['addr2'] : 'NA'),
                            'country'=>$jsonobj['country'],
                            'city'=>$jsonobj['city'],
                            'zip_code'=>$jsonobj['zip'],
                            'is_privacy'=>'0',
                            'is_terms'=>$jsonobj['terms'],
                            'email_verified_at'=>NOW(),
                            'created_at'=>NOW()
        );
        $check_email=User::where('email',$jsonobj['email'])->count();
        if($check_email==0)
        {
            $user_data['created_at']=NOW();
            $add_user=User::insertGetId($user_data);
            $user_id=$add_user;
        }
        else
        {
            $user_data['updated_at']=NOW();
            $user = User::where('email',$jsonobj['email'])->first();
            $user_id=$user->id;
            User::where('id',$user_id)->update($user_data);
        }
        $check_subs= tbl_subscription_detail::where('user_id',$user_id)->count();
        if($check_subs==0)
        {
          //  $url='https://api.demo.globalgatewaye4.firstdata.com/transaction/v11'; //demo
           // $gateway_id='MD6401-51'; $password='EHjdMqG1sz8oiNvp5GOFg9ZfiKgiz0yc'; //demo

        $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
        // $gateway_id='K84040-06'; 
        // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

        $gateway_id='Q66919-01'; 
        $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
            //$url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
            //$gateway_id='K84040-06'; $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live
            
            $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" =>$cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 

            //print_r($data);die();
            $data_string= json_encode($data);

            // Initializing curl
            $ch = curl_init( $url );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8;","Accept: application/json" ));

            // Getting results
            $result = curl_exec($ch);
            //echo $result;die();
            if($result)
            {
                // Getting jSON result string
                $newres = json_decode($result);
                if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100)
                {
                    $subs_arr=array();
                    $subs_arr['card_number']=$cc_number; 
                    $subs_arr['card_expiry']=$jsonobj['payment']['card_expiry'];
                    $subs_arr['card_holder']=$cardholder_name;
                    $subs_arr['product_price']=$jsonobj['product_price'];
                    $subs_arr['total_cost']=$amount; 
                    $subs_arr['pay_id']=$newres->authorization_num;
                    $subs_arr['card_type']=$newres->credit_card_type;
                    $subs_arr['subs_date']=NOW();
                    //$subs_arr['coupon_id']=$jsonobj['coupon_id'];
                    $subs_arr['user_id']=$user_id;
                    $subs_arr['trans_json']='abc';
                    $subs_arr['no_of_signers']='1';
                    $subs_arr['no_of_stamps']='1';
                    $subs_arr['created_at']=NOW();

                    $subs= tbl_subscription_detail::insert($subs_arr);
                    // if($subs)
                    // {
                    //     if($request->coupon_id!='')
                    //     {
                    //         tbl_coupon::where('coupon_id',$request->coupon_id)->update(array('status'=>'0','updated_at'=>NOW()));
                    //     }
                    // }
                    // Mail::send(['html'=>'emails.paid-subscription'],  ['user' => $user_data], function($message) use ($user_data) {
                    //     $message->to($user_data['email'])->subject('Thanks For subscribing to AimNotary.');
                    //     $message->from('no-reply@aimnotary.com','AimNotary');
                    // });

                    // Mail::send(['html'=>'emails.admin_subs_mail'],  ['user' => $user_data], function($message) use ($user_data) {
                    //     $message->to('sukalyan.b@aqbsolutions.com')->subject('New User subscribed  to AimNotary.');
                    //     //$message->cc('markr@ez8a.com');
                    //     //$message->bcc('rohitmajumder1983@gmail.com');
                    //     //$message->bcc('amitdasgupta1980@gmail.com');
                    //     $message->from('no-reply@aimnotary.com','AimNotary');
                    // });

                    $html = View('emails.paid-subscription', ['user'=> $user_data]);
                    $mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Thanks For subscribing to AimNotary.", $message = $html);

                    $html1 = View('emails.admin_subs_mail', ['user'=> $user_data]);
                    $mail_status1=$this->email($from = "no-reply@aimnotary.com", $to = "esther@aimnotary.com", $subject = "New User subscribed  to AimNotary", $message = $html1);
                    if(($mail_status > 0) && ($mail_status1 > 0))
                    {
                        echo json_encode(['status' => 200, 'msg' =>'Thanks For subscribing to AimNotary!']);
                    }
                    else
                    {
                        echo json_encode(['status' => 201, 'msg' =>'Mail could not be sent,something went wrong']);
                    }
                    
                }
                else
                {
                   echo json_encode(['status' => 201, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
                }
            }
            else
            {
                echo json_encode(['status' => 201, 'msg' =>'Something Went Wrong!']);
            }
        }
        else
        {
            echo json_encode(['status' => 202, 'msg' =>'You have already subscribed.Please go to the login page!']);
        }
    }
    //----------- User Login -----------

    public function get_user(Request $req)
    {
        $jsonobj=($req->json()->all()); // data='{"email":"sandip.pr@gmail.com","password":"123456"}'
        // Checking if email exists in `users`-table
        $user=User::where(['email'=>$jsonobj['email']])->count();
        if($user<1) // Email doesn't exist.
        {
            return response()->json([

                'status'=>0,
                'msg'=>'Invalid email'
            ]);
        }
        elseif($user>0) // Email exists
        {
            $user_pwd=User::select('password')->where(['email'=>$jsonobj['email']])->first();
            $matchpwd=Hash::check($jsonobj['password'],$user_pwd->password);
            // Checking if the password matches login successful else err: invalid password
            if($matchpwd>0)
            {
    
           $user=User::select('id','remember_token','user_type','email_verified_at','name','personal_mobile','add1','add2','country','city','zip_code','is_active','state','user_category')->where(['email'=>$jsonobj['email']])->first();
                
               if($user->email_verified_at!=''){
               $name=$user->name;
               $first_name=explode(" ",$name);
               $first_name=$first_name['0'];
               $last_name=explode(" ",$name);
               $last_name=$last_name['1'];
               //get subscription
               //table::select('amount')->where('id',$id)->first();
               $subs=tbl_user_doc_count::select('amount')->where('user_id', $user->id)->first();
               $subs=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$user->id])->orderBy("id","desc")->take(1)->get()->toArray();
              
              $product_price=$subs['0']->amount;
                return response()->json([
                    'status'=>1,
                    'msg'=>'Login Successfull',
                    'user_id'=>$user->id,
                    'api_token'=>$user->remember_token,
                    'user_type'=>$user->user_type,
                    'first_name'=>$first_name,
                    'last_name' => $last_name,
                    'email' => $jsonobj['email'],
                    'mobile' => strval($user->personal_mobile),
                    'zip' => $user->zip_code,
                    'city' => $user->city,
                    'addr1' => $user->add1,
                    'addr2' => $user->add2,
                    'state' => $user->state,
                   'product_price' => $product_price,
                   'user_category' => $user->user_category,
                ]);
              }else {
              if($user->is_active == '0'){
              		echo json_encode(['status' => 201, 'msg' => 'your username is not active !']);
              }else {
              	 echo json_encode(['status' => 201, 'msg' => 'please verify your email address']);
              	 }
              }  
            }
            else
            {
                return response()->json([
                    'status'=>0,
                    'msg'=>'Invalid Password'
                ]);
            }
        }
        else
        {
            $user=User::select('id','remember_token')->where(['email'=>$jsonobj['email']])->first();
            return response()->json([
                'status'=>1,
                'msg'=>'Login Successfull',
                'user_id'=>$user->id,
                'api_token'=>$user->remember_token
            ]);
        }
    }
    public function reset_password(Request $req)
    {
        $jsonobj=($req->json()->all()); // data='{"user_mail":"sandip.ray@gmail.com"}'
        $email=$jsonobj['user_mail'];
        $check_email=user::where('email',$email)->count();
        if($check_email>0)
        {
            $rand=rand(000000,999999);
              
            $update_arr=array('password'=>Hash::make($rand),'updated_at'=>NOW());
              
            User::where('email',$email)->update($update_arr);
            $user_data=array('email'=>$email,'password'=>$rand);
            // Mail::send(['html'=>'emails.reset-password'],  ['user' => $user_data], function($message) use ($user_data) {
            //     $message->to($user_data['email'])->subject('Your Recovered Password of aimnotary.com.');
            //     $message->from('no-reply@aimnotary.com','AimNotary');
            // });

            $html = View('emails.reset-password', ['user'=> $user_data]);
            $mail_status=$this->email($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Please verify your email address .', $message = $html);
            if($mail_status > 0)
            {
                echo json_encode(['status' => 200, 'msg' => 'Your Recovered Password of aimnotary.com.']);
            }
            else
            {
                echo json_encode(['status' => 201, 'msg' => 'Your Password could not be re-covered,something went wrong,try again later.']);
            }
        }
        else
        {
            echo json_encode(['status' => 201, 'msg' => 'You have already registerd!']);
        }
    }
    public function contact_us(Request $req)
    {
        $jsonobj=($req->json()->all()); // data='{"name":"Sandip Ray","email":"sandip.pr@aqbsolutions.com","phone":"9836457280","subject":"Personal","msg":"This is a test message."}'
        $user=array(
            'f_name'         => $jsonobj['name'],
            'user_eml'       => $jsonobj['email'],
            'user_contact_no'=> $jsonobj['phone'],
            'msg_subject'    => $jsonobj['subject'],
            'message'        => $jsonobj['msg']
        );
        // Mail::send(['html'=>'emails.contact-us'],  ['user' => $user], function($message) use ($user) {
        //     $message->to('sukalyan.b@aqbsolutions.com', 'AimNotary')->subject((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us' );
        //  $message->from('no-reply@aimnotary.com','AimNotary');

        $html = View('emails.contact-us', ['user'=> $user]);
        $mail_status=$this->email($from = "no-reply@aimnotary.com", $to ='esther@aimnotary.com', $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
        if($mail_status > 0)
        {
            $info=array(
                            'status'=>1,
                            'msg'=>'Mail send successfully'
            );
            echo json_encode($info);
        }
        else
        {
            $info=array(
                            'status'=>0,
                            'msg'=>'Something went wrong,mail sending unsuccessfull'
            );
            echo json_encode($info);
        }
    }

    //---------------- Upload Documents ---------------

public function check_doc_count(Request $req){
$jsonobj=$req->json()->all();
$user_id=$jsonobj['user_id'];
$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$user_id])->orderBy("id","desc")->take(1)->get()->first();
			$docno=$result;
			if($docno->doc==1){
			
				$checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$docno->time)->where('payment_status','=','P')->where(['user_id'=>$user_id])->where('deleted_at','=',NULL)->get();
			}else if($docno->doc==5){
			
				$checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$docno->time)->where(['user_id'=>$user_id])->where('deleted_at','=',NULL)->get();
			}else if($docno->doc>5){
			
				$checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$docno->time)->where(['user_id'=>$user_id])->where('deleted_at','=',NULL)->get();
			}
			
			
			if(count($checkingValidation)>=$docno->doc)
			{
				return  json_encode(['status'=>204,'msg'=>'You have reached Maximum number of documents as per your subscription']);
				die();
			}else {
			return  json_encode(['status'=>201,'msg'=>'You can upload !']);
			}
}

	public function insert_enterprise_subscription(Request $req){
		$jsonobj=$req->json()->all();
		$user=[
		    'user_id'=> $jsonobj['user_id'],
		    'amount' => $jsonobj['amount'],
		    'doc'    => $jsonobj['doc'],
		    'payment_status'=> 'P',
		    'date'  => date('Y-m-d'),
		    'time'  => date('Y-m-d H:i:s'),
		];
		$insert=DB::table("tbl_user_doc_counts")->insert($user);

		$update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW(),'is_active'=>1,'user_category'=>'E');
		User::where('id',$jsonobj['user_id'])->update($update_arr);

		$userResult=User::where('id',$jsonobj['user_id'])->get()->toArray();

		$user_data="Your Subscription is confirmed now you can login to continue";
		$html = View('emails.enterprise-mail2', ['user'=> $user_data]);
		$mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $userResult[0]['email'], $subject = 'Your Subscription is confirmed', $message = $html);
		return  json_encode(['status'=>200,'msg'=>'Subscription Addedd Successfully']);
		//return redirect($_SERVER['HTTP_REFERER'])->with('message', 'Subscription Addedd Successfully');
	    }
	    
	    public function enterprise_subscription_get(Request $req){
	    	$jsonobj=$req->json()->all();
	    	$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$jsonobj['user_id']])->orderBy("id","desc")->take(1)->get()->toArray();
	    	$response=array('document' => $result[0]->doc,'amount' => $result[0]->amount);
	    	echo json_encode($response);	
	    }
	    public function make_subscription_payments(Request $req){
        // echo "<pre>"; print_r($request->request); exit;
        $jsonobj=$req->json()->all();
        $result_db=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$jsonobj['user_id']])->orderBy("id","desc")->take(1)->get()->toArray();
        
        $expairy=explode("/",$jsonobj['card_expiry']);
        $cc_expiry= $expairy['0']. $expairy['1'];
       // $bankres=$this->payment_set(['amount'=>$request->subscription_amount,'holder_name'=>$request->card_holder,'cn'=>$request->card_number,'ex'=>$expairy[0].$expairy[1]]);
        $cardholder_name=$jsonobj['card_holder'];
        $cc_number=$jsonobj['card_number'];

       // $url='https://api.demo.globalgatewaye4.firstdata.com/transaction/v11'; //demo
         //   $gateway_id='MD6401-51'; $password='EHjdMqG1sz8oiNvp5GOFg9ZfiKgiz0yc';
          $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
        // $gateway_id='K84040-06'; 
        // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

        $gateway_id='Q66919-01'; 
        $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
        $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $jsonobj['subscription_amount'],"cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 

        // $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //true
        // $data = array("gateway_id" => "K84040-06", "password" => "HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq", "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); //true
        $data_string= json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8;','Accept: application/json' ));
        $result = curl_exec($ch);
        $newres=json_decode($result);
        
       if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100){
        	 $update_arr=array('payment_status'=>'C');
            DB::table("tbl_user_doc_counts")->where('id',$result_db[0]->id)->update($update_arr);
            echo json_encode(['status'=>1,'msg'=>'Your payment has been successfull']);
        }else{
            echo json_encode(['status'=>0,'msg'=>'Payment Fail']);
        }
        
           
    }
    public function upload_file(Request $req)
    {
        $allowedfileExtension=['pdf','docx'];
        $files=$req->file('datafile');
        $user_id=$req->input('uid');
        $sendMail=$req->input('sendMail');
        $add;
        //check doc count
        $result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$user_id])->orderBy("id","desc")->take(1)->get()->first();
	$docno=$result;
			
        $actual_file_nm = strtolower(str_replace(' ', '-', $files->getClientOriginalName()));
        $extension = $files->getClientOriginalExtension();
        $size = $files->getSize();
        $rand=$this->random(32);
        $filename = $rand.".".$extension;  
        $check=in_array($extension,$allowedfileExtension);
        if($check)
        {
                $files->move(public_path('upload/user_doc/'), $filename);

                // $path_parts = 'C:/xampp/htdocs/signer/uploads/files/';
                $path_parts = '/home/aimnotary/public_html/signer/uploads/files/';
                copy(public_path('upload/user_doc/').$filename, $path_parts.$filename);

                //$path_parts1 = 'C:/xampp/htdocs/signer/uploads/copies/';

                $path_parts1 = '/home/aimnotary/public_html/signer/uploads/copies/';
                copy(public_path('upload/user_doc/').$filename, $path_parts1.$filename);

                $store_doc='upload/user_doc/'.$filename;

                $user_data=array([
                    'user_id' => $user_id,
                    'doc' => $store_doc,
                    'doc_nm' => $actual_file_nm,
                    'document_key'=>$rand,
                   // 'status'=>'0',
                    'send_mail'=>'0',
                    'created_at' => NOW(),
                    'subscription_id' => $docno->id,
                    ]);

                $add=tbl_user_document::insert($user_data);  
                
                // $mysqli =  mysqli_connect(env('DB_HOST'),'root','','paid_pdf');
                $mysqli =  mysqli_connect('localhost','aimnotar_pdfsign','G@~YKypkW_kW','aimnotar_pdfsign');
                
                if (mysqli_connect_errno()) 
                {
                  echo "Failed to connect to MySQL: " . mysqli_connect_error();
                  exit();
                }

                $result = mysqli_query($mysqli, "INSERT INTO `files`(`company`, `folder`, `uploaded_by`,`name`, `filename`, `extension`, `size`, `document_key`, `status`,`is_template`) VALUES ('1','1','1','".$actual_file_nm."','".$filename."','".$extension."','".$size."','".$rand."','Unsigned','No')");
        }
        else
        {
            return  json_encode(['status'=>201,'msg'=>'Sorry Only Upload pdf , docx']);
        }
        
        if($add)
        {
            return  json_encode(['status'=>200,'msg'=>'File successfully uploaded']);
        }
    }
    public static function random($length = 16) 
    {
        $string = '';
        while (($len = strlen($string)) < $length) 
        {
            $size = $length - $len;
            $bytes = static::randomBytes($size);
            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }
        return $string;
    }
    public static function randomBytes($length = 16) 
    {
        return random_bytes($length);
    }

    // Fetching/listing all user documents.
    public function verify_documents_submit_user(Request $req)
		{
			$jsonobj=$req->json()->all();
			$front=$this->upload_image_user($jsonobj['front']);
			$back=$this->upload_image_user($jsonobj['back']);
			$id_type=$jsonobj['id_type'];
			$id=$jsonobj['doc_id'];
			$data_arr=[
				'front' => $front,
				'back'	=> $back,
				'doc_type' => $jsonobj['id_type'],
			];
			tbl_user_document::where('id',$id)->update($data_arr);
			 $response=array('status' => 200,'msg' => 'update successful');
        //echo url("/verify-documents/".base64_encode($doc_id));
        echo json_encode($response);
			
		}

		public function upload_image_user($data)
		{
			$folderName = 'publicupload/user_id/'.date("Y").'/'.date("m").'/'.date("d");
			if(!is_dir($folderName))
			{
				mkdir($folderName,0777,true); 
			}
			$image_parts = explode(";base64,", $data);
			$image_type_aux = explode("image/", $image_parts[0]);
			$image_type = $image_type_aux[1];
			$image_base64 = base64_decode($image_parts[1]);
			$file = $folderName.'/' . uniqid() . '.png';
			file_put_contents($file, $image_base64);
			return $file;
		}
    public function add_additional_docs(Request $req)
    {
    $jsonobj=$req->json()->all();
        $cost=$jsonobj['total_cost'];
        $doc_id=$jsonobj['doc_id'];
        $stamp=($jsonobj['no_of_stamps']==0) ? 1 : ($jsonobj['no_of_stamps']+1);
        $signer=($jsonobj['no_of_signers']==0) ? 1 : ($jsonobj['no_of_signers']+1);
        $update=[
            'total_price' => $cost,
            'user_require_stamp' => $stamp,
            'user_require_signer' => $signer,
            'updated_at' => NOW()
        ];
        $update=tbl_user_document::where('id',$doc_id)->update($update);
        $response=array('status' => 200,'msg' => 'update successful');
        //echo url("/verify-documents/".base64_encode($doc_id));
        echo json_encode($response);
    }
    
    public function status_change(Request $req){
    	$jsonobj=$req->json()->all(); 
    	$doc_id=$jsonobj['doc_id'];
    	$update=['status' => "2",'updated_at' => NOW()];
    	//'total_price' => $cost,
          //  'user_require_stamp' => $stamp,
           // 'user_require_signer'
            //
    	$update=tbl_user_document::where('id',$doc_id)->first();
    	echo $update->total_price;
    	
    }
     public function final_payment(Request $req)
    {
        $jsonobj=$req->json()->all(); 
        $expcard=explode('/',$jsonobj['payment']['card_expiry']);
        $cc_expiry=$expcard[0].$expcard[1];
        
        
       
        $amount=$jsonobj['product_price'];
        //$amount='0.01';
        $cardholder_name=$jsonobj['payment']['card_holder'];
        $cc_number=$jsonobj['payment']['card_number'];

     //   $url='https://api.demo.globalgatewaye4.firstdata.com/transaction/v11'; //demo
          //  $gateway_id='MD6401-51'; $password='EHjdMqG1sz8oiNvp5GOFg9ZfiKgiz0yc';
           $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
        // $gateway_id='K84040-06'; 
        // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

        $gateway_id='Q66919-01'; 
        $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
        $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 

        // $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //true
        // $data = array("gateway_id" => "K84040-06", "password" => "HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq", "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); //true
        $data_string= json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8;','Accept: application/json' ));
        $result = curl_exec($ch);
        $newres=json_decode($result);
        
       if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100){
      
            $doc_details=tbl_user_document::where('id', $jsonobj['doc_id'] )->get();
            $txn_data=[
                'user_id' => $doc_details[0]->user_id,
                'document_id' => $jsonobj['doc_id'],
                'transaction_json' => json_encode($newres),
                'crated_date'       => date('Y-m-d'),
                'created_time'      => date('Y-m-d H:i:s'),
                'amount'            => $amount,
                'status'            => 'A',
            ];
            
            $data=DB::table("tbl_transactions")->insert($txn_data);

            $billing=[
                'First_Name' => $jsonobj['first_name'],
                'Last_Name' => $jsonobj['last_name'],
                'Email' => $jsonobj['email'],
                'Phone_Number' => $jsonobj['mobile'],
                'Address1' => $jsonobj['addr'],
                'Address2' => $jsonobj['addr2'],
                'Country' => $jsonobj['country'],
                'State' => $jsonobj['city'],
                'Zipcode' => $jsonobj['zip'],
            ];
            $billingJosn=json_encode($billing);
            $update=tbl_user_document::where('id',$jsonobj['doc_id'])->update(array('payment_status'=>'C','billing_address'=>$billingJosn,'updated_at'=>NOW()));
            echo json_encode(['status' => 200, 'msg' =>'Thanks For subscribing to AimNotary!']);
        }
        else
        {
            echo json_encode(['status' => 201, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
        }
    }
    public function pay_for_documents(Request $req){
    	$jsonobj=$req->json()->all();	
   	$doc_id=$jsonobj['document_id'];
   	$user_id=$jsonobj['user_id'];
   	$docs_info=tbl_user_document::select([
						'tbl_user_documents.*',
						'tbl_user_doc_counts.doc',
						'tbl_user_doc_counts.is_usa',
						])
														->leftjoin('tbl_user_doc_counts','tbl_user_doc_counts.id','=','tbl_user_documents.subscription_id')
->where(['tbl_user_documents.id'=>$doc_id])
->get()
->first()
->toArray();
			$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$user_id])->orderBy("id","desc")->take(1)->get()->toArray();
			
   		if($docs_info['doc']==1){
   			if($docs_info['is_usa']=='Y'){
				$myproductprice=79;
			}else{
				$myproductprice=25;
			}
   		}else
		{
			$myproductprice=0;
		}
		
		
		
		if($docs_info['user_require_signer']>0 || $docs_info['user_require_signer']!=""){
			if($docs_info['notary_confirm_signer']==NULL || $docs_info['notary_confirm_signer']==""){
				$signerTotal=($docs_info['user_require_signer'] - 1)*5;
			}else {
				$signerTotal=($docs_info['notary_confirm_signer'] - 1)*5;
			}
		}
		if($docs_info['notary_confirm_stamp']==NULL || $docs_info['notary_confirm_stamp']==""){
			$StampTotal=($docs_info['user_require_stamp'] - 1)*5;
		}else {
		
			$StampTotal=($docs_info['notary_confirm_stamp'] - 1)*5;
		}
		
		$totalAmount=$myproductprice+$signerTotal+$StampTotal;
		$response=array('base_price' => $myproductprice,'additional_signer' => $signerTotal , 'additional_stamp' =>$StampTotal,'total_price' => $totalAmount );
		
   		echo json_encode($response);
    }
    
    public function user_docs(Request $req)
    {
        $arrsigned=[];
        $arrunsigned=[];
        $arrcompleted=[];
        $jsonobj=$req->json()->all(); // {"user_id":"15"}
        
        $user = User::select('id','remember_token','user_type','email_verified_at','name','personal_mobile','add1','add2','country','city','zip_code','is_active','state','user_category')->where(['id'=>$jsonobj['user_id']])->first();
        
            $user_category=$user->user_category;
         $result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$jsonobj['user_id']])->orderBy("id","desc")->take(1)->get()->toArray();
       // $getdoc=tbl_user_document::select('id','doc','doc_nm','document_key','status')->where('user_id',$jsonobj['user_id'])->get();
       $complete_docs= tbl_user_document::select([
														'tbl_user_documents.id'])
											->leftJoin('tbl_scedule_date','tbl_scedule_date.document_id','=','tbl_user_documents.id')
											->where('tbl_user_documents.user_id',$jsonobj['user_id'])->where('tbl_user_documents.status','2')->where('tbl_user_documents.created_at','>',$result[0]->time)->orderBy('tbl_user_documents.id','DESC')
											->get();
       
       //user payment status
       if($user_category=='E'){  
					if($result[0]->payment_status=="P"){
						$payment_button="pay now";
					}else{
						$payment_button='upload document';
					}
				  }else if($user_category=='U'){
					if($result[0]->doc==1){
						if($result[0]->is_usa=='Y'){
							$payment_button='upload document';
						}else{
							$payment_button='update subscription and upload document';
						}
					}
					else{
						if( ($result[0]->doc - count($complete_docs) ) == 0){
							$payment_button='Resubscribe';
						}else{
							$payment_button='upload document';
						}
					}
				  }
       
       
       
       /////
       $getdoc= tbl_user_document::select([
					'tbl_user_documents.id', 
					'tbl_user_documents.user_id', 
					'tbl_user_documents.doc as docname', 
					'tbl_user_documents.user_require_stamp', 
					'tbl_user_documents.user_require_signer',
					'tbl_user_documents.doc_nm',
					'tbl_user_documents.document_key', 
					'tbl_user_documents.status', 
					'tbl_user_documents.payment_status' ,
					'tbl_scedule_date.title',
					'tbl_scedule_date.date',
					'tbl_user_doc_counts.doc',
					'tbl_user_documents.created_at'
					])
		->leftJoin('tbl_scedule_date','tbl_scedule_date.document_id','=','tbl_user_documents.id')
		->leftJoin('tbl_user_doc_counts','tbl_user_doc_counts.id','=','tbl_user_documents.subscription_id') 												
		->where('tbl_user_documents.user_id',$jsonobj['user_id'])
		->orderBy('tbl_user_documents.id','DESC')
		->get();
		$documents_subscribed=$result[0]->doc;
		$balanced_documents=$result[0]->doc - count($complete_docs);
		
	foreach($getdoc As $row)
        {
            if($row->status==0) // Status 0 => Unsigned
            {
                $unsign=array(
                                'doc_id'=>$row->id,
                                'doc_url'=>url('/public').'/'.$row->docname,
                                'doc_name'=>$row->doc_nm,
                                'doc_key'=>$row->document_key,
                                'doc_status'=>$row->status
                );
                array_push($arrunsigned,$unsign);
            }
            elseif($row->status==1) // Status 1 => Signed
            {
                $sign=array(
                                'doc_id'=>$row->id,
                                'doc_url'=>url('/public').'/'.$row->docname,
                                'doc_name'=>$row->doc_nm,
                                 'doc_key'=>$row->document_key,
                                'doc_status'=>$row->status,
                                
                );
                if($row->title ==NULL || $row->title == ''){
                	$sign['schedule_status'] = 'schedule';	
                }else {
                $result1 = DB::select('select * from tbl_scedule_date where date>="'.date("Y-m-d").'" AND document_id="'.$row->id.'"');
			
			if(isset($result1[0])){
				$sign['schedule_status'] = 'Already Scheduled';
			}else {
                	$sign['schedule_status'] = 'schedule';	
                	}
                }
                array_push($arrsigned,$sign);
            }
            else // Status 2 => Completed
            {
                $complete=array(
                                    'doc_id'=>$row->id,
                                    'doc_url'=>url('/public').'/'.$row->docname,
                                    'doc_name'=>$row->doc_nm,
                                    'doc_status'=>$row->status,
                                    
                                    
                );
                 if($row->title ==NULL || $row->title == ''){
                	$complete['schedule_status'] = 'schedule';	
                }else {
                	$complete['schedule_status'] = 'Already Scheduled';	
                }
                if($result[0]->doc==1){
                	if($row->doc==1){
                		if($row->payment_status=='P'){
                			$complete['payment']="pay";
                		}else {
                			$complete['payment']="view";
                		}
                	}else if($row->doc>= 5){
                		if($row->user_require_stamp==1 && $row->user_require_signer==1){
                			$complete['payment']="view";
                		}elseif(($row->user_require_stamp>1 || $row->user_require_signer>1) && $row->payment_status=='P'){
                			$complete['payment']="pay";
                		}elseif(($row->user_require_stamp>1 || $row->user_require_signer>1) && $row->payment_status!='P'){
                			$complete['payment']="view";
                		}
                	}
                }elseif($result[0]->doc>=5){
                	if($row->doc==1){
                		if($row->payment_status=='P'){
                			$complete['payment']="pay";	
                		}else {
                			$complete['payment']="view";
                		}
                	}elseif($row->doc>=5){
                		if($row->user_require_stamp==1 && $row->user_require_signer==1){
                			$complete['payment']="view";
                		}elseif(($row->user_require_stamp>1 || $row->user_require_signer>1) && $row->payment_status=='P'){
                			$complete['payment']="pay";
                		}elseif(($row->user_require_stamp>1 || $row->user_require_signer>1) && $row->payment_status!='P'){
                			$complete['payment']="view";
                		}
                		
                	}
                }
                array_push($arrcompleted,$complete);
            }
        }
        $mainArr=array(
                        'Signed'=>$arrsigned,
                       // 'Unsigned'=>$arrunsigned,
                        'Completed'=>$arrcompleted,
                        'document_subscription' => $documents_subscribed,
                        'document_balance' => $balanced_documents,
                        'payment_subscription_status' => $payment_button,
        );
        echo json_encode($mainArr);
    }
    public function remove_document(Request $req)
    {
        
        //$doc_id=$req->doc_id;
        //$get_data   = tbl_user_document::select('*')->where('id',$req->id)->count();
        $jsonobj=$req->json()->all(); // {"doc_id":"15"}
        $get_data   = tbl_user_document::select('*')->where('id',$jsonobj['doc_id'])->count();
        if($get_data>0)
        {
            $data_arr['deleted_at']=NOW();
            tbl_user_document::where('id',$jsonobj['doc_id'])->update($data_arr);
            echo json_encode(['status' => 200, 'msg' => 'Document removed successfully!']);
        }
        else
        {
            echo json_encode(['status' => 404, 'msg' => 'This document doesn\'t exits!']);
        }
    }

    public function signed_document(Request $req)
    {
        $allowedfileExtension=['pdf','docx'];
        $files=$req->file('datafile');
        $docKey=$req->input('doc_key');
        $docId=$req->input('doc_id');
        //$sendMail=$req->input('sendMail');
        $updt;
        $actual_file_nm = strtolower(str_replace(' ', '-', $files->getClientOriginalName()));
        $extension = $files->getClientOriginalExtension();
        $size = $files->getSize();
        $filename = $docKey.".".$extension;
        $check=in_array($extension,$allowedfileExtension);
        if($check)
        {
                $files->move(public_path('upload/user_doc/'), $filename);

                // $path_parts = 'C:/xampp/htdocs/signer/uploads/files/';
                $path_parts = '/home/aimnotary/public_html/signer/uploads/files/';
                copy(public_path('upload/user_doc/').$filename, $path_parts.$filename);

                //$path_parts1 = 'C:/xampp/htdocs/signer/uploads/copies/';

                $path_parts1 = '/home/aimnotary/public_html/signer/uploads/copies/';
                copy(public_path('upload/user_doc/').$filename, $path_parts1.$filename);

                $store_doc='upload/user_doc/'.$filename;

                $user_data=array(
                                    'doc' => $store_doc,
                                    'doc_nm' => $actual_file_nm,
                                    'status'=>"1",
                                    'updated_at' => NOW()
                );
               
                $updt=tbl_user_document::where('document_key',$docKey)->update($user_data);  
                                
                // $mysqli =  mysqli_connect(env('DB_HOST'),'root','','paid_pdf');
                $mysqli =  mysqli_connect('localhost','aimnotar_pdfsign','G@~YKypkW_kW','aimnotar_pdfsign');
                
                if (mysqli_connect_errno()) 
                {
                  echo "Failed to connect to MySQL: " . mysqli_connect_error();
                  exit();
                }

                $result = mysqli_query($mysqli, "Update `files` Set `name`='".$actual_file_nm."', `filename`='".$filename."', `extension`='".$extension."', `size`='".$size."',`status`='Signed',`editted`='Yes' Where `document_key`='".$docKey."'");
        }
        else
        {
            return  json_encode(['status'=>201,'msg'=>'Sorry Only Upload pdf , docx']);
        }
        
        if($updt)
        {
            return  json_encode(['status'=>200,'msg'=>'File successfully uploaded']);
        }
        else
        {
             return  json_encode(['status'=>201,'msg'=>'File upload failed']);
        }
    }

    //----------------------End --------------------------


    //---------------------- Signer ----------------------

    
    public function verify_identity(Request $req)
    {
        $new_doc_id=$req->input('doc_id');
        $user_id=$req->input('uid');
        //$curr_Time_zone=date_default_timezone_set('America/Kentucky/Monticello');
        //$curTime=date('H:i:sa',time());
        //echo $curTime;

        // Checking if the time is between 9am and 6pm then the mail will be shot to lawyer else an alert mail would be shot to the current user to contact lawyer the next day
        // if($curTime>= 9 && $curTime < 18)
        // {
            //echo "Its still time";
                        
            $link=url("start-video-call").'/'.base64_encode($new_doc_id.'~~'.$user_id);
            $get_lowyer=tbl_lawyer::select(['id','user_id'])->with('lawyer_dtl')->where('status','Free')->first();
            if(isset($get_lowyer->id))
            {
                $lawyer_id=$get_lowyer->id;
                $lawyer_user_id=$get_lowyer->user_id;
                $name=$get_lowyer->lawyer_dtl['name'];
                $email=$get_lowyer->lawyer_dtl['email'];
            }
            else
            {
                $prio_lawyer=tbl_lawyer::select(['id','user_id'])->with('lawyer_dtl')->orderBy('priority','ASC')->first();
                $lawyer_id=$prio_lawyer->id;
                $lawyer_user_id=$prio_lawyer->user_id;
                $name=$prio_lawyer->lawyer_dtl['name'];
                $email=$prio_lawyer->lawyer_dtl['email'];
            }
            $user_data=array(
                                'name'=>$name,
                                'email'=>$email,
                                'link'=>$link,
                                'send_to'=>'Lawyer'
            );

            // Mail::send(['html'=>'emails.call-request-mail'],  ['user' => $user_data], function($message) use ($user_data) {
            //     $message->to($user_data['email'])->subject('Start Video Calling to verify The user document.');
            //     $message->from('no-reply@aimnotary.com','AimNotary');
            // });
            $html = View('emails.call-request-mail', ['user'=> $user_data]);
            $mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Start Video Calling to verify The user document.", $message = $html);
            //echo $mail_status;die();
            if($mail_status > 0)
            {
                tbl_lawyer::where('id',$lawyer_id)->update(array('status'=>'Busy','updated_at'=>NOW()));
                tbl_user_document::where('id',$new_doc_id)->update(array('lawyer_id'=>$lawyer_user_id,'updated_at'=>NOW()));
                $infomsg=array(
                                'status'=>1,
                                'msg'=>'Mail sent successfully to notary',
                                'channel-id'=>base64_encode($new_doc_id.'~~'.$user_id)
                );
                echo json_encode($infomsg);
            }
            else
            {
                $infomsg=array(
                                'status'=>0,
                                'msg'=>'Mail could not be sent to notary'
                );
                echo json_encode($infomsg);
            }
            
        // }
        // else
        // {
        //     //echo "Its too late";
        //     $user_mail=User::select('email','name')->where('id',$user_id)->first();
        //     $user_data=array(
        //                         'name'=>$user_mail->name,
        //                         'email'=>$user_mail->email
        //     );
        //     //print_r($user_data);die();
        //     Mail::send(['html'=>'emails.call-postponed-mail'],  ['user' => $user_data], function($message) use ($user_data) {
        //         $message->to($user_data['email'])->subject('Cannot Start Video Calling to verify The user document.');
        //         $message->from('no-reply@aimnotary.com','AimNotary');
        //     });
        //     if(count(Mail::failures()) < 1)
        //     {
        //         $infomsg=array(
        //                         'status'=>-1,
        //                         'msg'=>'Mail sent successfully to user'
        //         );
        //         echo json_encode($infomsg);
        //     }
        //     else
        //     {
        //         $infomsg=array(
        //                         'status'=>-2,
        //                         'msg'=>'Mail could not be sent to user'
        //         );
        //         echo json_encode($infomsg);
        //     }
        // }
    
        // $data['page_title'] = 'verify_identity';
        // $data['company_info'] = $this->company_info;
        // return view('front.user.verify-identity')->with($data);
    }
    public function check_ssn(Request $req)
    {
        $access_token=$this->get_access_token();
        //$ssn_val=implode('', $req->input('ssn'));
        $ssn_val=$req->input('ssn');
        //echo $ssn_val;die;
        $user_data = array('SSN'=>$ssn_val);
        // $user_data = array('SSN'=>'268-86-9287');
        $user_data = json_encode($user_data);
        $url="https://apitest.microbilt.com/SSNValidation/GetReport";
        $header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response2 = curl_exec($ch);
        curl_close($ch);
        $decode_res=json_decode($response2);
        //print_r($decode_res);
        //echo $decode_res->SSNStatus;
        if(isset($decode_res->SSNStatus) && $decode_res->SSNStatus=='Item1')
        {
            // if (!$request->session()->has('SSN_VAL')) {
                //$request->session()->put('SSN_VAL', $ssn_val);
            // }
            
            echo json_encode(['status'=>200,'ssn'=>$ssn_val,'msg'=>'Successfully verified SSN!!']);
        }
        else
        {
            $user_id=$req->input('uid');
            $get_data   = tbl_ssn_fail::select('*')->where('user_id',$user_id)->count();

            if($get_data>0)
            {
                $data_arr['updated_at']=NOW();
                tbl_ssn_fail::where('user_id',$user_id)->update(['ssn'=>DB::raw('ssn+1')]);
               
            }
            else
            {
                $ssn_fail_arr=array('user_id'=>$user_id,'ssn'=>'1','ssn_full_nm'=>'0','ssn_address'=>'0','ssn_contact'=>'0','created_at'=>NOW());
                tbl_ssn_fail::insert($ssn_fail_arr);
            }
            echo json_encode(['status'=>201,'msg'=>'Something Went Wrong!']);
        }
    }
    // public function verify_ssn_nm(Request $req)
    // {
    //     $access_token=$this->get_access_token();

    //     $ssn=$req->input('ssn');
    //     $f_name=$req->input('f_name');
    //     $m_name=$req->input('m_name');
    //     $l_name=$req->input('l_name');
    //     $user_data = array('SSN'=>$ssn, 'Name'=>array('FirstName'=>$f_name,'MiddleName'=>$m_name,'LastName'=>$l_name));
    //     $user_data = json_encode($user_data);
    //     $url="https://apitest.microbilt.com/SSNNameVerification/";
    //     $header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
    //     $ch = curl_init($url);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    //     curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     $response2 = curl_exec($ch);
    //     curl_close($ch);
    //     $decode_res=json_decode($response2);
    //     //print_r($decode_res);
    //     $vefiy_nm_stat=$decode_res->VerificationFlags;
    //     if(isset($vefiy_nm_stat->VerificationFlag) && $vefiy_nm_stat->VerificationFlag=='Y')
    //     {
    //         echo json_encode(['status'=>200,'msg'=>$vefiy_nm_stat->VCodeDescription]);
    //     }
    //     else
    //     {
    //         $user_id=$req->input('uid');
    //         $get_data   = tbl_ssn_fail::select('*')->where('user_id',$user_id)->count();

    //         if($get_data>0)
    //         {
    //             $data_arr['updated_at']=NOW();
    //             tbl_ssn_fail::where('user_id',$user_id)->update(['ssn_full_nm'=>DB::raw('ssn_full_nm+1')]);
               
    //         }
    //         else
    //         {
    //             $ssn_fail_arr=array('user_id'=>$user_id,'ssn'=>'0','ssn_full_nm'=>'1','ssn_address'=>'0','ssn_contact'=>'0','created_at'=>NOW());
    //             tbl_ssn_fail::insert($ssn_fail_arr);
    //         }
    //         echo json_encode(['status'=>201,'msg'=>$vefiy_nm_stat->VCodeDescription]);
    //     }
    // }
    // public function verify_ssn_phone(Request $req)
    // {
    //     $access_token=$this->get_access_token();
    //     $ssn=$req->input('ssn');
    //     $PhoneNumber=$req->input('PhoneNumber');
    //     $make_intenatin_num=substr($PhoneNumber, 0,3).'-'.substr($PhoneNumber, 3,3).'-'.substr($PhoneNumber, 6,4);
    //     $user_data = array('SSN'=>$ssn, 'PhoneNumber'=>$make_intenatin_num);
    //     $user_data = json_encode($user_data);
        
    //     $url="https://apitest.microbilt.com/SSNPhoneVerification/";
    //     $header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
    //     $ch = curl_init($url);
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    //     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    //     curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     $response2 = curl_exec($ch);
    //     curl_close($ch);
    //     $decode_res=json_decode($response2);
    //     //print_r($decode_res);
    //     $vefiy_nm_stat=$decode_res->VerificationFlags;
    //     if(isset($vefiy_nm_stat->VerificationFlag) &&$vefiy_nm_stat->VerificationFlag=='Y')
    //     {
    //         echo json_encode(['status'=>200,'msg'=>$vefiy_nm_stat->VCodeDescription]);
    //     }
    //     else
    //     {
    //         echo json_encode(['status'=>201,'msg'=>$vefiy_nm_stat->VCodeDescription]);
    //     }
    // }

    public function verify_ssn_phone(Request $req)
    {
        echo json_encode(['status'=>200,'msg'=>'Phone verified']);
    }

    public function verify_ssn_nm(Request $req)
    {
        echo json_encode(['status'=>200,'msg'=>'verified']);
    }

    //------------------------ End -----------------------

    //---------------------- Lawyer/Notary --------------------

    public function pending_docs(Request $req)
    {
        $docArr=[];
        $type_id=$req->input('type_id');
        $lawyer_id=$req->input('user_id');
        $user_doc = tbl_user_document::with(['user_dtl','lawyer_dtl'])->where(['status'=>$type_id,'user_id'=>$lawyer_id])->orderByDesc('id')->get();
        //print_r($user_doc->toArray());die();
        foreach($user_doc As $row)
        {
            $tmpArr=array(
                            'fullname'=>$row->user_dtl['name'],
                            'email'=>$row->user_dtl['email'],
                            'country'=>(($row->user_dtl['country']=='231')?'USA':$row->user_dtl['country']),
                            'city'=>$row->user_dtl['city']
            );
            array_push($docArr,$tmpArr);
        }
        echo json_encode($docArr);
    }

    //-------------------- Agora Chat Token-Generation -------------------

    // public function start_chat(Request $req) 
    // {
    //     $opentok = new OpenTok(env('OPENTOK_API_KEY'), env('OPENTOK_API_SECRET'));
    //     // Create a session that attempts to use peer-to-peer streaming:
    //     $session = $opentok->createSession();

    //     // A session that uses the OpenTok Media Router, which is required for archiving:
    //     $session = $opentok->createSession(array( 'mediaMode' => MediaMode::ROUTED ));

    //     // A session with a location hint:
    //     $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

    //     // An automatically archived session:
    //     $sessionOptions = array(
    //         'archiveMode' => ArchiveMode::ALWAYS,
    //         'mediaMode' => MediaMode::ROUTED
    //     );
    //     $session = $opentok->createSession($sessionOptions);
    //     //echo "Thesession value is : ".$session;die();
    //     // Store this sessionId in the database for later use
    //     $sessionId = $session->getSessionId();

    //     // Generate a Token from just a sessionId (fetched from a database)
    //     $token = $opentok->generateToken($sessionId);
    //     // Generate a Token by calling the method on the Session (returned from createSession)
    //     $token = $session->generateToken();

    //     // Set some options in a token
    //     $token = $session->generateToken(array(
    //         'role'       => Role::MODERATOR,
    //         'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
    //         'data'       => 'name=Johnny',
    //         'initialLayoutClassList' => array('focus')
    //     ));

    //     $count_vidsession=tbl_vid_chat_session::select('*')->where(['sessionId'=>$sessionId])->count();
    //     if($count_vidsession==0){
    //         tbl_vid_chat_session::insert(['sessionId'=>$sessionId,'created_at'=>NOW()]);
    //         //$req->session()->put('sessionId', $sessionId);
    //     }
    //     $tokenArr=array(
    //                         'opentok_token' => $token,
    //                         'session_token' => $sessionId
    //     );
    //     echo json_encode($tokenArr);
    // }   

    
    //-------------------- Static Pages -----------------------

    public function about_us()
    {
        $data = tbl_content_mngt_system::select('*')->where(['category'=>'1','sub_category'=>'16'])->first();
        if($data!=NULL)
        {
            $about=array(
                        'title'=>$data->title,
                        'desc'=>$data->description,
                        'img'=>$data->file_image
            );
            echo json_encode($about);
        }
        else
        {
            $errinfo=array(
                            'status'=>0,
                            'msg'=>'Data not found'
            );
            echo json_encode($errinfo);
        }
    }
    public function privacy_policy(Request $req)
    {
        $data = tbl_content_mngt_system::select('*')->where(['category'=>'10','sub_category'=>'18'])->first();
        if($data!=NULL)
        {
            $about=array(
                        'title'=>$data->title,
                        'desc'=>$data->description,
                        'img'=>$data->file_image
            );
            echo json_encode($about);
        }
        else
        {
            $errinfo=array(
                            'status'=>0,
                            'msg'=>'Data not found'
            );
            echo json_encode($errinfo);
        }
    }
    public function terms_conditions(Request $req)
    {
        $data = tbl_content_mngt_system::select('*')->where(['category'=>'11','sub_category'=>'17'])->first();
        if($data!=NULL)
        {
            $about=array(
                        'title'=>$data->title,
                        'desc'=>$data->description,
                        'img'=>$data->file_image
            );
            echo json_encode($about);
        }
        else
        {
            $errinfo=array(
                            'status'=>0,
                            'msg'=>'Data not found'
            );
            echo json_encode($errinfo);
        }
    }

 function smtp_auth_email($to="info@aimnotary.com",$subject,$body){
        include(base_path()."/phpmailer/vendor/autoload.php");
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'no-reply@aimnotary.com';                 // SMTP username
        $mail->Password = 'pn8;A8_!f<]z~Bm$';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                              // TCP port to connect to
        
        $mail->setFrom('no-reply@aimnotary.com', 'Aimnotary');
        $mail->addAddress($to, 'Aimnotary');  
        
        $mail->isHTML(true);                                  // Set email format to HTML
        
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->send();
    }
   
    // public function test(Request $req)
    // {
    //     $con =  mysqli_connect('localhost','aimnotar_pdfsign','G@~YKypkW_kW','aimnotar_pdfsign');
    //     if (!$con) 
    //     {
    //       echo "Failed to connect to MySQL: " . mysqli_connect_error();
    //       exit();
    //     }
    //     else
    //     {
    //         //echo "Database Connected";
    //         //$sql="Select `document_key` from `files`  where `name`='virtual-reality.pdf'";
    //         $sql="Select * from `files`";
    //         $result=mysqli_query($con,$sql);
    //         if (mysqli_num_rows($result) > 0) 
    //         {
    //             while($row = mysqli_fetch_assoc($result)) 
    //             {
    //                 echo $row["uploaded_on"]."  ".$row["name"]."  ".$row["filename"]."  ".$row["extension"]."  ".$row["size"]."  ".$row["document_key"]."  ".$row["status"]."\n\n";
    //             }
    //         } 
    //         else
    //         {
    //           echo "0 results";
    //         }
    //         mysqli_close($con);
    //     }
    // }
    
   
}

