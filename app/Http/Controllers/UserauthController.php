<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OpenTok\Role;
use Validator;
use Auth;
use Hashids;
use Hash;
use DB;
use App\User;
// use App\Session;
use Session;
use App\Models\tbl_company_info;
use App\Models\tbl_user_document;
use App\Models\tbl_ssn_fail;
use App\Models\tbl_lawyer;
use Mail;
require(base_path()."/phpmailer/vendor/autoload.php");
class UserauthController extends Controller
{
		
		public $data=array();
		protected $company_info=array();

		
		public function __construct()
		{
			
			//$this->middleware('auth');
			//https://www.aimnotary.com/signer/users/csrf_token_curl
			// https://www.aimnotary.com/signer/get_my_csrf
			$this->company_info=tbl_company_info::select('*')->where([['id','=','1']])->first();
			// $result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>Auth::user()->id])->orderBy("id","desc")->take(1)->get()->toArray();
			// prijnt_r($result);

		}

		function smtp_auth_email($to="info@aimnotary.com",$subject,$body){
			
			$mail = new \PHPMailer\PHPMailer\PHPMailer;
			$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
			$mail->SMTPAuth = true;                               // Enable SMTP authentication
			$mail->Username = 'no-reply@aimnotary.com';                 // SMTP username
			$mail->Password = 'pn8;A8_!f<]z~Bm$';                           // SMTP password
			$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
			$mail->Port = 465;                              // TCP port to connect to
			
			$mail->setFrom('no-reply@aimnotary.com', 'Aimnotary');
			$mail->addAddress($to, 'Aimnotary');  
			
			$mail->isHTML(true);                                  // Set email format to HTML
			
			$mail->Subject = $subject;
			$mail->Body    = $body;
			$mail->send();
		}

		public function get_signer_csrf_token()
		{

			$username = "user";
			$password = "pass";
			$url = "https://www.aimnotary.com/signer/get_my_csrf";
			
			$cookie= "c:\\cookies.txt";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			return $response;
			
		}

		public function dateConvert($old_date,$format='jS F, Y')
		{
			$old_date_timestamp = strtotime($old_date);
			$new_date = date($format, $old_date_timestamp); 
			return $new_date;
		}
         
        public function get_access_token()
        {
     	    $data = array('client_id'=>'qgRJ4mvwSSQgQJPyeAn8YZPngVFxyYAK','client_secret'=>'0f3TMbPYvQOmUswm','grant_type'=>'client_credentials');
			$data = json_encode($data);
			$url="https://apitest.microbilt.com/OAuth/GetAccessToken";
			$header = array("Content-Type: application/json");
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response1 = curl_exec($ch);
			curl_close($ch);
			
			if($response1)
			{
				$res=json_decode($response1);
				return $res->access_token;
			}
			else
			{
				return false;
			}
		} 
		public function check_ssn(Request $request)
		{
			$access_token=$this->get_access_token();
			$ssn_val=implode('', $request->ssn);
			$user_data = array('SSN'=>$ssn_val);
			// $user_data = array('SSN'=>'268-86-9287');
			$user_data = json_encode($user_data);
			$url="https://apitest.microbilt.com/SSNValidation/GetReport";
			$header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response2 = curl_exec($ch);
			curl_close($ch);
			$decode_res=json_decode($response2);
			//print_r($decode_res);
			// echo $decode_res->SSNStatus;
			if(isset($decode_res->SSNStatus) && $decode_res->SSNStatus=='Item1')
			{
				// if (!$request->session()->has('SSN_VAL')) {
				    $request->session()->put('SSN_VAL', $ssn_val);
				// }
				
				echo json_encode(['status'=>200,'msg'=>'Successfully verified SSN!!']);
			}
			else
			{
				$user_id=Auth::user()->id;
				$get_data   = tbl_ssn_fail::select('*')->where('user_id',$user_id)->count();

		        if($get_data>0)
		        {
		            $data_arr['updated_at']=NOW();
		            tbl_ssn_fail::where('user_id',$user_id)->update(['ssn'=>DB::raw('ssn+1')]);
		           
		        }
		        else
		        {
		        	$ssn_fail_arr=array('user_id'=>$user_id,'ssn'=>'1','ssn_full_nm'=>'0','ssn_address'=>'0','ssn_contact'=>'0','created_at'=>NOW());
		            tbl_ssn_fail::insert($ssn_fail_arr);
		        }
				echo json_encode(['status'=>201,'msg'=>'Something Went Wrong!']);
			}
		}


		public function verify_ssn_nm(Request $request)
		{
			$access_token=$this->get_access_token();

			//ssn name
			$f_name=$request->f_name;
			$m_name=$request->m_name;
			$l_name=$request->l_name;
			$user_data = array('SSN'=>session('SSN_VAL'), 'Name'=>array('FirstName'=>$f_name,'MiddleName'=>$m_name,'LastName'=>$l_name));
			$user_data = json_encode($user_data);
			$url="https://apitest.microbilt.com/SSNNameVerification/";
			$header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response2 = curl_exec($ch);
			curl_close($ch);
			$decode_res=json_decode($response2);
			 //print_r($decode_res);
			 $vefiy_nm_stat=$decode_res->VerificationFlags;
			if(isset($vefiy_nm_stat->VerificationFlag) && $vefiy_nm_stat->VerificationFlag=='Y')
			{
				echo json_encode(['status'=>200,'msg'=>$vefiy_nm_stat->VCodeDescription]);
			}
			else
			{
				$user_id=Auth::user()->id;
				$get_data   = tbl_ssn_fail::select('*')->where('user_id',$user_id)->count();

		        if($get_data>0)
		        {
		            $data_arr['updated_at']=NOW();
		            tbl_ssn_fail::where('user_id',$user_id)->update(['ssn_full_nm'=>DB::raw('ssn_full_nm+1')]);
		           
		        }
		        else
		        {
		        	$ssn_fail_arr=array('user_id'=>$user_id,'ssn'=>'0','ssn_full_nm'=>'1','ssn_address'=>'0','ssn_contact'=>'0','created_at'=>NOW());
		            tbl_ssn_fail::insert($ssn_fail_arr);
		        }
				echo json_encode(['status'=>201,'msg'=>$vefiy_nm_stat->VCodeDescription]);
			}
		}

		public function verify_ssn_address(Request $request)
		{
		 	$access_token=$this->get_access_token();
			//ssn name
			$addr1=$request->addr1;
			$city=$request->city;
			$state=$request->state;
			$zip=$request->zip;
			$user_data = array('SSN'=>session('SSN_VAL'), 'Address'=>array('Addr1'=>$addr1,'City'=>$city,'State'=>$state,'Zip'=>$zip));
			$user_data = json_encode($user_data);
			$url="https://apitest.microbilt.com/SSNAddressVerification/";
			$header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response2 = curl_exec($ch);
			curl_close($ch);
			$decode_res=json_decode($response2);
			 //print_r($decode_res);
			 $vefiy_nm_stat=$decode_res->VerificationFlags;
			if(isset($vefiy_nm_stat->VerificationFlag) && $vefiy_nm_stat->VerificationFlag=='Y')
			{
				echo json_encode(['status'=>200,'msg'=>$vefiy_nm_stat->VCodeDescription]);
			}
			else
			{
				echo json_encode(['status'=>201,'msg'=>$vefiy_nm_stat->VCodeDescription]);
			}
		}

		public function verify_ssn_phone(Request $request)
		{
		 	$access_token=$this->get_access_token();
			$PhoneNumber=$request->PhoneNumber;
			$make_intenatin_num=substr($PhoneNumber, 0,3).'-'.substr($PhoneNumber, 3,3).'-'.substr($PhoneNumber, 6,4);
			$user_data = array('SSN'=>session('SSN_VAL'), 'PhoneNumber'=>$make_intenatin_num);
		    $user_data = json_encode($user_data);
			
			$url="https://apitest.microbilt.com/SSNPhoneVerification/";
			$header = array("Content-Type: application/json","Accept: application/json","Authorization: Bearer ".$access_token."");
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch,CURLOPT_POSTFIELDS,$user_data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response2 = curl_exec($ch);
			curl_close($ch);
			$decode_res=json_decode($response2);
			 //print_r($decode_res);
			 $vefiy_nm_stat=$decode_res->VerificationFlags;
			if(isset($vefiy_nm_stat->VerificationFlag) &&$vefiy_nm_stat->VerificationFlag=='Y')
			{
				echo json_encode(['status'=>200,'msg'=>$vefiy_nm_stat->VCodeDescription]);
			}
			else
			{
				echo json_encode(['status'=>201,'msg'=>$vefiy_nm_stat->VCodeDescription]);
			}
		}

		public function dashboard()
	    {
			
    	   	if(Auth::user()->user_type=='Lawyer'){

            		return redirect('pending-document/1');
			}
			elseif(Auth::user()->user_type=='Admin'){

				return redirect('admin/');
			}
			else{
				  $user_id=Auth::user()->id;
				  $username = DB::table('users')->select("*")->where(['id'=>Auth::user()->id])->take(1)->get()->toArray();
				  $result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>Auth::user()->id])->orderBy("id","desc")->take(1)->get();
				  //$docno=$result->toArray();
				  $data['complete_docs'] = tbl_user_document::select([
														'tbl_user_documents.id'])
											->leftJoin('tbl_scedule_date','tbl_scedule_date.document_id','=','tbl_user_documents.id')
											->where('tbl_user_documents.user_id',$user_id)->where('tbl_user_documents.status','2')
											->where('tbl_user_documents.created_at','>',$result[0]->time)
											->where('tbl_user_documents.deleted_at','=', null)
											->orderBy('tbl_user_documents.id','DESC')
											->get();

					// echo ($result[0]->doc - count($data['complete_docs']) ); exit;
				  if(Auth::user()->user_category=='E'){  
					if($result[0]->payment_status=="P"){
						$data['payment_button']="<a data-toggle='modal' data-target='#paymentModal' class='default-btn page-btn text-white float-right' href='javascript:void();'>Pay Now</a>";
					}else{
						$data['payment_button']='<a class="default-btn page-btn text-white float-right" href="'.url('upload').'">Upload Document</a>';
					}
				  }else if(Auth::user()->user_category=='U'){
					if($result[0]->doc==1){
						if($result[0]->is_usa=='Y'){
							$data['payment_button']='<a  class="default-btn page-btn text-white float-right" href="'.url('upload').'">Upload Document</a>';
						}else{
							$data['payment_button']='<a class="default-btn page-btn text-white float-right" data-toggle="modal" data-target="#resubscribeModal" style="margin-left : 15px;" href="javascript:void(0);">Update Subscription</a> 
												 <a  class="default-btn page-btn text-white float-right" href="'.url('upload').'">Upload Document</a>';
						}
					}
					else{
						if( ($result[0]->doc - count($data['complete_docs']) ) == 0){
							$data['payment_button']='<a class="default-btn page-btn text-white float-right" data-toggle="modal" data-target="#resubscribeModal" href="javascript:void(0);">Resubscribe</a>';
						}else{
							$data['payment_button']='<a class="default-btn page-btn text-white float-right" href="'.url('upload').'">Upload Document</a>';
						}
					}
				  }
				  $data['result']=$result;
		          $data['page_title'] = 'Dashboard';
				  $data['user_info'] = $username[0];
		          $data['company_info'] = $this->company_info;
		          $data['documents'] = tbl_user_document::select([
																	'tbl_user_documents.id', 
																	'tbl_user_documents.user_id', 
																	'tbl_user_documents.doc as docname', 
																	'tbl_user_documents.user_require_stamp', 
																	'tbl_user_documents.document_key', 
																	'tbl_user_documents.user_require_signer',
																	'tbl_user_documents.doc_nm',
																	'tbl_user_documents.document_key', 
																	'tbl_user_documents.status', 
																	'tbl_user_documents.payment_status' ,
																	'tbl_user_documents.notary_confirm_stamp' ,
																	'tbl_user_documents.notary_confirm_signer' ,
																	'tbl_scedule_date.title',
																	'tbl_scedule_date.date',
																	'tbl_user_doc_counts.doc',
																	'tbl_user_documents.created_at'])
														->leftJoin('tbl_scedule_date','tbl_scedule_date.document_id','=','tbl_user_documents.id')
														->leftJoin('tbl_user_doc_counts','tbl_user_doc_counts.id','=','tbl_user_documents.subscription_id')
														->where('tbl_user_documents.user_id',$user_id)
														->where('tbl_user_documents.deleted_at','=', null)
														->orderBy('tbl_user_documents.id','DESC')
														->get();
					
		         return view('front.user.dashboard')->with($data);
            }
    	  
		}
		
		

	    public function upload()
	    {
			$user_id=Auth::user()->id;
			$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>Auth::user()->id])->orderBy("id","desc")->take(1)->get()->toArray();
			//$docno=$result->toArray();
			if(Auth::user()->user_category=='E'){  
				if($result[0]->payment_status=="P"){
					return redirect('dashboard');
				}
			}
	    	$data['page_title'] = 'upload Document';
	        $data['company_info'] = $this->company_info;
	        return view('front.user.upload')->with($data);
	    }
	    public static function randomBytes($length = 16) 
	    {
   			return random_bytes($length);
    	}
	    public static function random($length = 16) 
	    {
	        $string = '';
	        while (($len = strlen($string)) < $length) 
	        {
	            $size = $length - $len;
	            $bytes = static::randomBytes($size);
	            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
	        }
	        return $string;
	    }

	    public function upload_documents(Request $request)
	    {

			$user_id=Auth::user()->id;
			
			$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>Auth::user()->id])->orderBy("id","desc")->take(1)->get()->first();
			$docno=$result;
			if($docno->doc==1){
				$checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$docno->time)->where('payment_status','=','P')->where(['user_id'=>Auth::user()->id])->where('deleted_at','=',NULL)->get();
			}else if($docno->doc==5){
				$checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$docno->time)->where(['user_id'=>Auth::user()->id])->where('deleted_at','=',NULL)->get();
			}else if($docno->doc>5){
				$checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$docno->time)->where(['user_id'=>Auth::user()->id])->where('deleted_at','=',NULL)->get();
			}
			
			
			if(count($checkingValidation)>=$docno->doc)
			{
				return  json_encode(['status'=>204,'msg'=>'You have reached Maximum number of documents as per your subscription']);
			}
			else
			{
				$this->validate($request, [
					'files'=>'required',
				]);
				if($request->hasFile('files'))
				{
					$allowedfileExtension=['pdf','docx'];
					$files = $request->file('files');
					foreach($files as $file){
						$id=$docno->id;
						$actual_file_nm = strtolower(str_replace(' ', '-', $file->getClientOriginalName()));
						$extension = $file->getClientOriginalExtension();
						$size = $file->getSize();
						$rand=$this->random(32);
						$filename = $rand.".".$extension;  
						$check=in_array($extension,$allowedfileExtension);
						if($check)
						{
				                $file->move(public_path('upload/user_doc/'), $filename);

				                // $path_parts = 'C:/xampp/htdocs/signer/uploads/files/';
				                $path_parts = '/home/aimnotary/public_html/signer/uploads/files/';
                           		copy(public_path('upload/user_doc/').$filename, $path_parts.$filename);

                           		//$path_parts1 = 'C:/xampp/htdocs/signer/uploads/copies/';

                           		$path_parts1 = '/home/aimnotary/public_html/signer/uploads/copies/';
                           		copy(public_path('upload/user_doc/').$filename, $path_parts1.$filename);

				                $store_doc='upload/user_doc/'.$filename;

				                $user_data=array([
									'user_id' => $user_id,
									'doc' => $store_doc,
									'doc_nm' => $actual_file_nm,
									'document_key'=>$rand,
									'send_mail'=>'0',
									'subscription_id' => $id,
									'created_at' => NOW(),
								]);

							   $add=DB::table("tbl_user_documents")->insert($user_data);  
							   $id = DB::getPdo()->lastInsertId();
							   
				               // $mysqli =  mysqli_connect(env('DB_HOST'),'root','','paid_pdf');
				               	$mysqli =  mysqli_connect('localhost','aimnotar_pdfsign','G@~YKypkW_kW','aimnotar_pdfsign');
				               	
								if (mysqli_connect_errno()) {
								  echo "Failed to connect to MySQL: " . mysqli_connect_error();
								  exit();
								}

								$result = mysqli_query($mysqli, "INSERT INTO `files`(`doc_id` ,`company`, `folder`, `uploaded_by`,`name`, `filename`, `extension`, `size`, `document_key`, `status`,`is_template`) VALUES ('".$id."','1','1','1','".$actual_file_nm."','".$filename."','".$extension."','".$size."','".$rand."','Unsigned','No')");
						}
						else
						{
							return  json_encode(['status'=>201,'msg'=>'Sorry Only Upload pdf file type']);
						}
				    }
				}
				if($add)
				{
					return  json_encode(['status'=>200,'msg'=>'Successs']);
				}
			}
    	    	
		}

		public function edit_documents(Request $request)
		{
			// echo $request->doc_id;
			// die();
			    $user_id=Auth::user()->id;
			    $this->validate($request, [
						'files'=>'required',
						]);
					if($request->hasFile('files'))
					{
						$allowedfileExtension=['pdf','docx'];
						$files = $request->file('files');
						foreach($files as $file){
							// $filename = strtolower(str_replace(' ', '-', $file->getClientOriginalName()));
							// $extension = $file->getClientOriginalExtension();
							$actual_file_nm = strtolower(str_replace(' ', '-', $file->getClientOriginalName()));
							$extension = $file->getClientOriginalExtension();
							$size = $file->getSize();
							$get_doc_key=tbl_user_document::select('document_key')->where('id',$request->doc_id)->first();
							$rand=$get_doc_key->document_key;
							$filename = $rand.".".$extension;  

							$check=in_array($extension,$allowedfileExtension);
							if($check)
							{
					                $file->move(public_path('upload/user_doc/'), $filename);

					                //$path_parts = 'C:/xampp/htdocs/signer/uploads/files/';
					                $path_parts = '/home/aimnotary/public_html/signer/uploads/files/';
                               		copy(public_path('upload/user_doc/').$filename, $path_parts.$filename);

                               		//$path_parts1 = 'C:/xampp/htdocs/signer/uploads/copies/';
                               		$path_parts1 = '/home/aimnotary/public_html/signer/uploads/copies/';
                               		copy(public_path('upload/user_doc/').$filename, $path_parts1.$filename);
					                $store_doc='upload/user_doc/'.$filename;

					                $user_data=array([
										'doc' => $store_doc,
										'doc_nm' => $actual_file_nm,
										'updated_at' => NOW()
										]);

					               $update=tbl_user_document::where(['id'=>$request->doc_id,'user_id' => $user_id])->update($user_data);
					               	$mysqli =  mysqli_connect('localhost','aimnotar_pdfsign','G@~YKypkW_kW','aimnotar_pdfsign');
									if (mysqli_connect_errno()) {
									  echo "Failed to connect to MySQL: " . mysqli_connect_error();
									  exit();
									}

								    $result = mysqli_query($mysqli, "UPDATE `files` SET `name`='".$actual_file_nm."',`filename`='".$filename."',`extension`='".$extension."',`size`='".$size."' WHERE document_key='".$rand."'");  
							}
							else
							{
								return  json_encode(['status'=>201,'msg'=>'Sorry Only Upload pdf , docx']);
							}
					    }
				}
				if($update){
					return  json_encode(['status'=>200,'msg'=>'Successs']);
				}
		}

		public function fetch_documents(Request $request)
		{
			$user_id=Auth()->user()->id;
			$user_doc=tbl_user_document::select(['id', 'user_id','document_key', 'doc', 'doc_nm', 'status', 'created_at'])->where('user_id',$user_id)->orderBy('id','DESC')->get();
			  $emp_list = [];
        
	        if(!$user_doc->isEmpty()){
	            foreach($user_doc as $eachDoc){
	                $emp_list[] = [
	                    'id'      => $eachDoc->id,
	                    'doc'     => $eachDoc->doc,
	                    'doc_nm'  => $eachDoc->doc_nm,
	                    'document_key'  => $eachDoc->document_key,
	                    'status'  => $eachDoc->status,
	                ];
	            }
	        }
	        echo json_encode($emp_list);
		}

		public function remove_document(Request $request)
		{
			// echo $request->id;
			// die();
			 $get_data   = tbl_user_document::select('*')->where('id',$request->id)->count();
	         if($get_data>0){
	            $data_arr['deleted_at']=NOW();
	            tbl_user_document::where('id',$request->id)->update($data_arr);
	            echo json_encode(['status' => 200, 'msg' => 'Document removed successfully!']);
	        }else{
	            echo json_encode(['status' => 404, 'msg' => 'This document doesn\'t exits!']);
	        }
		}

		public function email($from,$to,$subject,$message) {
		    try { 
		        // To send HTML mail, the Content-type header must be set
		        $headers  = 'MIME-Version: 1.0' . "\r\n";
		        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		         
		        // Create email headers
		        $headers .= 'From: '.$from."\r\n".
		                    'Reply-To: '.$from."\r\n" .
		                    'X-Priority: 1 (Highest)'."\r\n".
		                    'X-MSMail-Priority: High'."\r\n".
		                    'X-Mailer: PHP/' . phpversion();
		         
				$headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
				//Multiple BCC, same as CC above;
				$headers .= 'Bcc: rohitmajumder1983@gmail.com,amitdasgupta1980@gmail.com' . "\r\n";
		        // Sending email
		        if(mail($to, $subject, $message, $headers)){
		            return true;
		        } else{
		            return false;
		        }
		    }
		    catch(Exception $e) {
		          echo 'Message: ' .$e->getMessage();
		    }
		}

		public function verify_identity(Request $request,$doc_id='')
		{
			$new_doc_id=base64_decode($doc_id);
			$request->session()->put('Document_ID', $new_doc_id);
			$link=url("start-video").'/'.base64_encode($new_doc_id.'~~'.Auth()->user()->id);
	        $get_lowyer=tbl_lawyer::select(['id','user_id'])->with('lawyer_dtl')->where('status','Free')->first();
	        if(isset($get_lowyer->id)){
	        	    $lawyer_id=$get_lowyer->id;
	        	    $lawyer_user_id=$get_lowyer->user_id;
	        		$name=$get_lowyer->lawyer_dtl['name'];
	        		$email=$get_lowyer->lawyer_dtl['email'];
	        }else{
	        	$prio_lawyer=tbl_lawyer::select(['id','user_id'])->with('lawyer_dtl')->orderBy('priority','ASC')->first();
	        	$lawyer_id=$prio_lawyer->id;
	        	$lawyer_user_id=$prio_lawyer->user_id;
	        	$name=$prio_lawyer->lawyer_dtl['name'];
	        	$email=$prio_lawyer->lawyer_dtl['email'];
	        }
	        $user_data=array('name'=>$name,
	                             'email'=>$email,
	                             'link'=>$link,
	                             'send_to'=>'Lawyer'
	                           );
			$html = View('emails.call-request-mail', ['user'=> $user_data]);
	        $mail_status=$this->email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Start Video Calling to verify The user document.", $message = $html);
	        //die();
		    // Mail::send(['html'=>'emails.call-request-mail'],  [user=> $user_data], function($message) use ($user_data) {
		    //      $message->to($user_data['email'])->subject('Start Video Calling to verify The user document.');
		    //      $message->from('no-reply@aimnotary.com','AimNotary');
		    // });
		    
		    tbl_lawyer::where('id',$lawyer_id)->update(array('status'=>'Busy','updated_at'=>NOW()));
		    tbl_user_document::where('id',base64_decode($doc_id))->update(array('lawyer_id'=>$lawyer_user_id,'updated_at'=>NOW()));

			$data['page_title'] = 'verify_identity';
			$data['company_info'] = $this->company_info;
			return view('front.user.verify-identity')->with($data);
		}

		public function verify_documents(Request $request,$doc_id='')
		{
			$new_doc_id=base64_decode($doc_id);
			$data['new_doc_id']=base64_decode($doc_id);
			$data['encoded_new_doc_id']=$doc_id;
			$request->session()->put('Document_ID', $new_doc_id);
			$data['link']=url("verify-identity").'/'.$doc_id;
			return view('front.user.verify-documents')->with($data);
		}

		public function editdocuments(Request $request,$doc_id='')
		{
			$new_doc_id=base64_decode($doc_id);
			$data['new_doc_id']=base64_decode($doc_id);
			$data['encoded_new_doc_id']=$doc_id;
			$request->session()->put('Document_ID', $new_doc_id);
			$data['link']=url("dashboard");
			$data['documents']=tbl_user_document::where('id',base64_decode($doc_id))->get()->first()->toArray();
			return view('front.user.edit-documents')->with($data);
		}

		public function verify_documents_submit(Request $request,$doc_id=''){
			$front=$this->upload_image($request->front);
			$back=$this->upload_image($request->back);
			$id_type=$request->id_type;
			$id=$request->doc_id;
			$data_arr=[
				'front' => $front,
				'back'	=> $back,
				'doc_type' => $request->id_type,
			];
			tbl_user_document::where('id',$id)->update($data_arr);
		}

		public function upload_image($data)
		{
			$folderName = 'public/upload/pdfimage/';
			if(!is_dir($folderName)){
				mkdir($folderName,0777,true); 
			}
			$image_parts = explode(";base64,", $data);
			$image_type_aux = explode("image/", $image_parts[0]);
			$image_type = $image_type_aux[1];
			$image_base64 = base64_decode($image_parts[1]);
			$file = $folderName.'/' . uniqid() . '.png';
			file_put_contents($file, $image_base64);
			return $file;
		}

		public function confirm_detail(Request $request)
		{
			if(!$request->session()->has('SSN_VAL')){
				return redirect('verify-identity');
			}
			 $data['page_title'] = 'confirm-detail';
	         $data['company_info'] = $this->company_info;
	         return view('front.user.confirm-detail')->with($data);
		     
		}
		
		public function check_ssn_verification()
		{
			 if(!$request->session()->has('SSN_VAL')){
				return redirect('verify-identity');
			}
			 $data['page_title'] = 'check-ssn-verification';
	         $data['company_info'] = $this->company_info;
	         return view('front.user.check-ssn-verification')->with($data);
		}

		public function check_question_one(Request $request)
		{
			if(!$request->session()->has('SSN_VAL')){
				return redirect('verify-identity');
			}
			 $data['page_title'] = 'check-question-one';
	         $data['company_info'] = $this->company_info;
	         // $data['state']=tbl_state::orderBy('state_name','ASC')->get();
	        // $data['city']=tbl_state::orderBy('state_name','ASC')->get();
	         return view('front.user.check-question-one')->with($data);
		}
		public function check_question_two(Request $request)
		{
			if(!$request->session()->has('SSN_VAL')){
				return redirect('verify-identity');
			}
			 $data['page_title'] = 'check-question-two';
	         $data['company_info'] = $this->company_info;
	         return view('front.user.check-question-two')->with($data);
		}

		public function check_question_success()
		{
			 $data['page_title'] = 'check-question-success';
	         $data['company_info'] = $this->company_info;
	         return view('front.user.check-question-success')->with($data);
		}

		public function logout(Request $request)
		{

			return redirect('login');
		}

		public function set_time_session(Request $request)
		{
			//echo $request->session_time;
			$time=$request->session_time;
			$exp_time=explode(':', $time);
			$time_to_sec=(($exp_time[0]*60)+$exp_time[1]);
			Session::put('session_time', $time_to_sec);
			echo Session::get('session_time');
		}

		public function scedule_meeting(Request $request,$doc_id)
		{
			$data=[];
			$data['document_id']=$doc_id;
			return view('front.user.scedule_meeting')->with($data);
		}

		public function get_scedules()
		{
			$result = DB::select('select * from tbl_scedule_date where date>="'.date("Y-m-d").'" AND document_id="'.$_GET['document_id'].'"');
			if(!empty($result))
			{
				foreach($result as $res)
				{
					$json[]=['title'=>$res->title,'start'=>$res->date];
				}
			}
			else
			{
				$json=[];
			}
			echo json_encode($json);
		}

		public function get_single_scedules(Request $request){
			$result = DB::select('select * from tbl_scedule_date where date>="'.date("Y-m-d").'" AND document_id="'.$request->document_id.'"');
			$desc="<b> Title : </b> ".$result[0]->title."<br>";
			$desc.="<b> Date : </b> ".$this->dateConvert($result[0]->date)."<br>";
			$desc.="<b> Time : </b> ".$result[0]->time."<br>";
			if($result[0]->timezone=="-8"){
				$timezone="Pacific Time (US & Canada)";
			  }elseif($result[0]->timezone=="-7"){
				$timezone="Mountain Time (US & Canada)";
			  }elseif($result[0]->timezone=="-6"){
				$timezone="Central Time (US & Canada)";
			  }elseif($result[0]->timezone=="-5"){
				$timezone="Eastern Time (US & Canada)";
			  }
			  $desc.="<b>Time Zone : </b> ".$timezone."<br>";
			  $desc.="<b>Location : </b>".$result[0]->location."<br>";
			  $desc.="<b>Description : </b>".$result[0]->description."<br>";
			  echo $desc;
		}

		public function htmlScedule($id){
			$result = DB::select('select * from tbl_scedule_date where date>="'.date("Y-m-d").'" AND document_id="'.$id.'"');
			$desc="<b> Title : </b> ".$result[0]->title."<br>";
			$desc.="<b> Date : </b> ".$this->dateConvert($result[0]->date)."<br>";
			$desc.="<b> Time : </b> ".$result[0]->time."<br>";
			if($result[0]->timezone=="-8"){
				$timezone="Pacific Time (US & Canada)";
			  }elseif($result[0]->timezone=="-7"){
				$timezone="Mountain Time (US & Canada)";
			  }elseif($result[0]->timezone=="-6"){
				$timezone="Central Time (US & Canada)";
			  }elseif($result[0]->timezone=="-5"){
				$timezone="Eastern Time (US & Canada)";
			  }
			  $desc.="<b>Time Zone : </b> ".$timezone."<br>";
			  $desc.="<b>Location : </b>".$result[0]->location."<br>";
			  $desc.="<b>Description : </b>".$result[0]->description."<br>";
			  return $desc;
		}

		public function email_function($from,$to,$subject,$message) {
			try { 
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				 
				// Create email headers
				$headers .= 'From: '.$from."\r\n".
							'Reply-To: '.$from."\r\n" .
							'X-Priority: 1 (Highest)'."\r\n".
							'X-MSMail-Priority: High'."\r\n".
							'X-Mailer: PHP/' . phpversion();
				 
			$headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
			//Multiple BCC, same as CC above;
			$headers .= 'Bcc: rohitmajumder1983@gmail.com,amitdasgupta1980@gmail.com,markr@ez8a.com' . "\r\n";
				// Sending email
				if(mail($to, $subject, $message, $headers)){
					return true;
				} else{
					return false;
				}
			}
			catch(Exception $e) {
				  echo 'Message: ' .$e->getMessage();
			}
		}
		
		public function submit_schedule(Request $request)
		{
			
			$select=DB::table("tbl_scedule_date")->select("*")->where('date','=',$request->starts_at)->where('document_id','=',$request->document_id)->get()->toArray();
			
			if(empty($select)){
				
				$data=[
					'user_id' => Auth::user()->id,
					'title' => $request->title,
					'date' => $request->starts_at,
					'time' => $request->time,
					'status' => 'ACTIVE',
					'document_id' => $request->document_id,
					'description' => $request->description,
					'timezone' => $request->timezone,
					'location' => $request->location,
				];
				DB::table('tbl_scedule_date')->insert($data);
				$user=user::where('id','=',Auth::user()->id)->get()->toArray();
				
				$user_data="Hello ".$user[0]['name'].", Your Appointment has been scheduled on ".$this->dateConvert($request->starts_at)." at ".$request->time."<br><br>".$this->htmlScedule($request->document_id);
				$html = View('emails.enterprise-mail', ['user'=> $user_data,'admin'=>"false"]);
				$admin = View('emails.enterprise-mail', ['user'=> $user_data,'admin'=>"true"]);
				$this->email_function($from = "no-reply@aimnotary.com", $to = $user[0]['email'], $subject = 'New Appointment is Scheduled', $message = $html);
				$this->smtp_auth_email("info@aimnotary.com", $subject = 'New Appointment is Scheduled', $admin);
				
				$ch = curl_init();
				$fields = $data;
				$postvars="name=".$user[0]['name']."&email=".$user[0]['email']."&";
				foreach($fields as $key=>$value) {
					$postvars .= $key . "=" . $value . "&";
				}
				$url = "https://aimnotary.com/calender/cal.php";
				curl_setopt($ch,CURLOPT_URL,$url);
				curl_setopt($ch,CURLOPT_POST, 1); //0 for a get request
				curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
				curl_setopt($ch,CURLOPT_TIMEOUT, 20);
				$response = curl_exec($ch);
				curl_close ($ch); 
				die(json_encode(['status'=>1,'msg'=>'Appointment Has been Scheduled Successfully']));
				
			}
			else if(!empty($select))
			{
				$select2=DB::table("tbl_scedule_date")->select("*")->where('document_id','=',$request->document_id)->get()->toArray();
				if(!empty($select2))
				{
					die(json_encode(['status'=>0,'msg'=>'You have already created a schedule for this document']));
				}
				else
				{
					foreach($select as $dataVal)
					{
						if($dataVal->time==$request->time)
						{
							die(json_encode(['status'=>0,'msg'=>'This time is already scedule with someone else']));
						}
					}
				}
			}
			
			
		}

		public function pay_for_documents($doc_id)
		{
			$doc_id=base64_decode($doc_id);
			$data['docs_info']=tbl_user_document::select([
														'tbl_user_documents.*',
														'tbl_user_doc_counts.doc',
														'tbl_user_doc_counts.is_usa',
														])
														->leftjoin('tbl_user_doc_counts','tbl_user_doc_counts.id','=','tbl_user_documents.subscription_id')
														->where(['tbl_user_documents.id'=>$doc_id])
														->get()
														->first()
														->toArray();
			$data['result']=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>Auth::user()->id])->orderBy("id","desc")->take(1)->get()->toArray();
			return view('front.user.pay_for_documents')->with($data);
		}

		public function redirect_to_new_doc($doc_id){
			$result=DB::table("tbl_user_documents")->select("*")->where(['id'=>$doc_id])->get()->toArray();
			// echo "<pre>"; print_r($result); exit;
			return redirect("https://aimnotary.com/signer/document/".$result[0]->document_key."?auth=".md5(rand(1111,99999))."&xss-filter=true&mode=sign&modal=false&_token=true&value=".md5(rand(1111,99999))."&typ=".base64_encode(Auth::user()->user_type));
			
		}

		public function settings(){
			$data['data']=User::where('id',Auth::user()->id)->get()->toArray();
			
			return view('front.user.settings')->with($data);
		}

		public function update_account(Request $request){
			
			$update_arr=[
				'personal_mobile' => $request->personal_mobile,
				'state' => $request->state,
				'city' => $request->city,
				'add1' => $request->add1,
				'zip_code' => $request->zip_code,
			];
			User::where('id',Auth::user()->id)->update($update_arr);
			return redirect("settings")->with("message","Account information has been updated");
		}

		public function change_password(Request $request){
			$pass=[
				'password' => Hash::make($request->cnf_pass),
			];
			User::where('id',Auth::user()->id)->update($pass);
			return redirect("settings")->with("message","Password has been updated");
		}

		
    
}
