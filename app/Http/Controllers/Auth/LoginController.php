<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Auth;
use App\User;
use App\Session;
use App\Models\tbl_login_log;
use Cookie;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /*
    *   custom login function for user authentication
    *   accepts email and password
    *   use Hash to compare password with given string
    */
    public function login(Request $request){
        $username = $request->username;
        $password = $request->password;
        $remember = $request->remember == 1 ? true : false;
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        ]);

        /*
        * ajax based login
        */
        $user = User::where('email',$username)->first();
        if(empty($user)){
            echo json_encode(['status' => 404]);
        }else{
            $userdata = array(
                'email'    => $username,
                'password'  => $password
            );
            

            if (Auth::attempt($userdata,$remember)) 
            {
                // cookies set value for remember function
                $minutes = 720;
                if($remember == 1)
                {
                    Cookie::queue(Cookie::make('username', $username, 720));
                    Cookie::queue(Cookie::make('userpassword', $password, 720));
                }
                else
                {
                    Cookie::queue(Cookie::forget('username'));
                    Cookie::queue(Cookie::forget('userpassword'));
                }
                //check for session//
                //$is_session_exist = Session::where('user_id',$user->id)->get();
                //Session::where('user_id',$user->id)->forceDelete();
                if($user->id == 1){
                    $access_token = $user->createToken('SSIL')->accessToken; //create token
                    $request->session()->put('auth_token',$access_token); // store token
                    //check for login logs
                    $is_exist_log = tbl_login_log::where('user_id',$user->id)->get();
                    
                    $log_array = [
                        'user_id'      => $user->id,
                        'logged_in_ip' => request()->ip(),
                        'created_at'   => NOW(),
                    ];
                    tbl_login_log::insert($log_array);
                    $is_exist_count = tbl_login_log::where('user_id',$user->id)->count();
                    if(!$is_exist_log->isEmpty() && $is_exist_count > 2){
                        //delete rows except last 2
                        tbl_login_log::where('user_id',$user->id)->take(1)->forceDelete();
                    }
            
                    //return view('home');
                    echo json_encode(['status' => 200]);
                }else{
                    if($user->is_active == 1){
                        $access_token = $user->createToken('SSIL')->accessToken; //create token
                        $request->session()->put('auth_token',$access_token); // store token
                        //check for login logs
                        $is_exist_log = tbl_login_log::where('user_id',$user->id)->get();
                        
                        $log_array = [
                            'user_id'      => $user->id,
                            'logged_in_ip' => request()->ip(),
                            'created_at'   => NOW(),
                        ];
                        tbl_login_log::insert($log_array);
                        $is_exist_count = tbl_login_log::where('user_id',$user->id)->count();
                        if(!$is_exist_log->isEmpty() && $is_exist_count > 2){
                            //delete rows except last 2
                            tbl_login_log::where('user_id',$user->id)->take(1)->forceDelete();
                        }
                        //return view('home');
                        echo json_encode(['status' => 200,'cookie'=>Cookie::get()]); 
                    }else{
                        echo json_encode(['status' => 404]);
                    }
                }
            }else{
                echo json_encode(['status' => 403]);
            }
        }
    }


    /*
    *   overrides login form
    */
    // public function showLoginForm()
    // {
    //     return view('auth.login')->with(['page_title' => 'Login']);
    // }
}
