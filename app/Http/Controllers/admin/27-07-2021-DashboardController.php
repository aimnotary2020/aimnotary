<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;
use Validator;
use Auth;
use Hashids;
use App\User;
use App\Session;
use DB;
//use Session;
use App\Models\tbl_login_log;
use Cookie;
use Mail;
use Yajra\Datatables\Datatables;
use App\Models\tbl_cms_categorie;
use App\Models\tbl_content_mngt_system;
use App\Models\tbl_user_doc_counts;
use App\Models\tbl_user_document;
use App\Models\tbl_lawyer;
use App\Models\web_contact;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon AS CarbonDateFormat;
require(base_path()."/phpmailer/vendor/autoload.php");
set_time_limit(0);
ini_set('memory_limit', '-1');

class DashboardController extends Controller
{
    //
    public function index()
    {
        $data['page_title'] = 'Dashboard';
     // print_r(Auth::user()->hasRole('Admin'));
        if(Auth::user()->user_type=='Admin'){
             return view('admin.dashboard.dashboard')->with($data);
        }else{
              return view('admin.lawyer.lawyerdashboard')->with($data);
        }
    }
    public function login()
    {
        if(Auth::user()!='' && (Auth::user()->user_type=='Admin' || Auth::user()->user_type=='Lawyer')){
          if(Auth::user()->user_type=='Admin'){
              return redirect('admin/');
          }else{
            return redirect('lawyer/');
          } 
        }else{
          // echo Hash::make('123456');
          // die();
        	return view('admin.auth.login')->with(['page_title' => 'Login']);
        }
    }
    function smtp_auth_email($to="info@aimnotary.com",$subject,$body){
			
        $mail = new \PHPMailer\PHPMailer\PHPMailer;
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'no-reply@aimnotary.com';                 // SMTP username
        $mail->Password = 'pn8;A8_!f<]z~Bm$';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                              // TCP port to connect to
        
        $mail->setFrom('no-reply@aimnotary.com', 'Aimnotary');
        $mail->addAddress($to, 'Aimnotary');  
        
        $mail->isHTML(true);                                  // Set email format to HTML
        
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->send();
    }
    public function check_login(Request $request)
    {
    	  $username = $request->username;
        $password = $request->password;
        $remember = $request->remember == 1 ? true : false;
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        ]);
        /*
        * ajax based login
        */
        $user = User::where(['email'=>$username])->first();
        //$lawyer = tbl_lawyer::where('eml',$username)->first();
        if(empty($user)){
            echo json_encode(['status' => 404]);
        }else{
            $userdata = array(
                'email'    => $username,
                'password'  => $password
            );
            if(Auth::attempt($userdata,$remember)) 
            {
                // cookies set value for remember function
                $minutes = 720;
                if($remember == 1)
                {
                    Cookie::queue(Cookie::make('username', $username, 720));
                    Cookie::queue(Cookie::make('userpassword', $password, 720));
                }
                else
                {
                    Cookie::queue(Cookie::forget('username'));
                    Cookie::queue(Cookie::forget('userpassword'));
                }
                //check for session//
                // $is_session_exist = Session::where('user_id',$user->id)->get();
                // Session::where('user_id',$user->id)->forceDelete();
                if($user->user_type=='Admin' || $user->user_type=='Lawyer' ){
                    $access_token = $user->createToken('SSIL')->accessToken; //create token
                    Session::put('user', Auth::user());
                    $request->session()->put('auth_token',$access_token); // store token
                    //check for login logs
                    $is_exist_log = tbl_login_log::where('user_id',$user->id)->get();
                    
                    $log_array = [
                        'user_id'      => $user->id,
                        'logged_in_ip' => request()->ip(),
                        'created_at'   => NOW(),
                    ];
                    tbl_login_log::insert($log_array);
                    $is_exist_count = tbl_login_log::where('user_id',$user->id)->count();
                    if(!$is_exist_log->isEmpty() && $is_exist_count > 2){
                        //delete rows except last 2
                        tbl_login_log::where('user_id',$user->id)->take(1)->forceDelete();
                    }
            
                    
                    echo json_encode(['status' => 200]);
                }else{
                     echo json_encode(['status' => 403]);
                }
            }else{
               echo json_encode(['status' => 403]);
            }
        }
    }
    public function check_admin()
    {
        if(Auth::user()->user_type!='Admin'){
            return redirect('admin/'); 
        }
    }
    public function about_us()
    {
        $this->check_admin();
        $cms_list           = tbl_cms_categorie::with('get_category')->where('parent_id','0')->get();
        $data['cms_list']   = $cms_list;
        $data['page_title'] = 'About Us';
        return view('admin.cms.about_us')->with($data);
    }
    
    public function website_traffic()
    {
        $this->check_admin();
        $cms_list  = DB::table('web_contacts')->where('status','0')->get();
     //  print_r($cms_list); exit;
        $data['cms_list']   = $cms_list;
        $data['page_title'] = 'Website traffic';
        return view('admin.cms.website_traffic')->with($data);
    }
    
     public function remove_weblit($id){
         
            web_contact::where('id', $id)->update(['status'=>1]);
            return redirect('admin/website_traffic')->with('success', 'Dealer updated successfully');
           // echo json_encode(['status' => 200, 'message' => 'Data deleted sucessfully!']);
       
     }
    
    public function meta_title_management($myid){
        $this->check_admin();
        $data['meta_list']  = DB::table('tbl_meta_data')->get();
        if($myid>0){
            $data['meta_details']  = DB::table('tbl_meta_data')->where(['id'=>$myid])->get()->first();
        }
        $data['page_title'] = 'Meta Title Description';
        return view('admin.cms.meta-details')->with($data);
    }
    
    public function add_meta_details(Request $request){
        if($request->id){
            $id=$request->id;
            unset($request->id);
            $request->link = trim($request->link);
            DB::table('tbl_meta_data')->where('id',$id)->update($request->toArray());
            die(json_encode(['status'=>200,'url'=>'/'.$id,'message'=>'Meta Data Updated successfully']));
        }else{
            $request->link = trim($request->link);
            DB::table('tbl_meta_data')->insert($request->toArray());
            die(json_encode(['status'=>200,'url'=>'/0','message'=>'Meta Data Created successfully']));
        }
    }
    
    public function fetch_cms_dtl(){
       
        $data = [];
        $cms_list = tbl_content_mngt_system::orderBy('id','DESC')->get();
       // echo '<pre>';print_r($cms_list->toArray());die;
        //$total_emp_list = employee_tbl::with('get_user_data')->offset(201)->limit(100)->get();
        $emp_list = [];
        
        if(!$cms_list->isEmpty()){
            foreach($cms_list as $eachEmp){
                $id = $eachEmp->id;
                $category = tbl_cms_categorie::with('get_category')->where('id',$eachEmp->category)->get();
                $sub_cat_name = tbl_cms_categorie::with('get_category')->where('id',$eachEmp->sub_category)->get();
                $category_nm='';
                if(!$category->isEmpty()){
                        $category_nm=$category[0]->name;
                }
                $sub_cat='';
                if(!$sub_cat_name->isEmpty()){
                        $sub_cat=$sub_cat_name[0]->name;
                }
                
                $image=(($eachEmp->file_image!='')?$eachEmp->file_image:'no-image.png');
                $emp_list[] = [
                    'category'         => $category_nm,
                    'sub_category'     => $sub_cat,
                    'name'             => (($eachEmp->title!='')?$eachEmp->title:$eachEmp->name),
                    'file_image'       =>'<img src="'.asset('/public/'.$image).'" >',
                    'description'      =>wordwrap(substr($eachEmp->description, 0, 100),20,"<br>\n").'...',
                    'action'           =>'<a class="deptedit" href="javascript:void(0);"title="View"   style="font-size:20px;"  data-toggle="modal" data-target="#myModal" onclick="get_dtl(\''.$id.'\')"  ><i class="typcn typcn-edit view_icon"></i></a><a class="delete" href="javascript:void(0);"title="View"   style="font-size:20px; color:red;" onclick="delete_cms(\''.$id.'\')"  ><i class="typcn typcn-trash view_icon"></i></a>',
                ];
            }
        }
       
        return Datatables::of($emp_list)->escapeColumns([])->make(true);
    } 
    public function get_cms_dtl(Request $request)
     {
           $id  = $request->id;
          
           $cms_dtl=tbl_content_mngt_system::select('*')->where([['id','=',$id]])->get();
            $target_list='';
            if(!$cms_dtl->isEmpty()){
                    foreach ($cms_dtl as $eachEt) {
                        
                          $target_list = [
                                'category'        =>$eachEt->category,
                                'sub_category'    =>$eachEt->sub_category,
                                'name'            =>$eachEt->name,
                                'title'           =>$eachEt->title,
                                'file_image'      =>$eachEt->file_image,
                                'occupation'      =>$eachEt->occupation,
                                'description'     =>$eachEt->description,
                                'url_link'        =>$eachEt->url_link,
                            ];
                    }
            }
            echo json_encode($target_list);
     }
     public function get_subcat(Request $request)
     {
        $cat_id=$request->category_id;
         $sub_category    = tbl_cms_categorie::select('*')->where('parent_id',$cat_id)->orderBy('name','ASC')->get();
          $sub_cat_list = [];
            if(!$sub_category->isEmpty()){
                    foreach ($sub_category as $each) {
                        
                          $sub_cat_list[] = [
                                'id'        =>$each->id,
                                'name'    =>$each->name,
                                
                            ];
                    }
            }
            echo json_encode($sub_cat_list);
     }
    public function add_edit_cms(Request $request)
    {
        //echo "hiii";
           $category=$request->category;
           $sub_category=$request->sub_category;
           $name=$request->name;
           $title=$request->title;
           $file_image=$request->upload_img;
           
           $occupation=$request->occupation;
           $description=addslashes($request->description);
           $url_link=$request->url_link;
           $hidden_id=$request->hidden_id;
           $data_arr=array();
           $data_arr['category']=$category;
           $data_arr['sub_category']=$sub_category;
           $data_arr['name']=$name;
           $data_arr['title']=$title;
          
           $data_arr['occupation']=$occupation;
           $data_arr['description']=$description;
           $data_arr['url_link']=$url_link;
        // $cms=tbl_content_mngt_system::select('*')->where([['category','=',$category],['sub_category','=',$sub_category]])->get();
           if($request->upload_img!=''){
                   // $request->validate([
                   //      'upload_img' => 'required|mimes:png,jpg,jpeg|max:2048',
                   //  ]);
              
                    $fileName = time().'.'.$request->upload_img->extension();  
                    
                    $request->upload_img->move(public_path('upload'), $fileName);
                    $data_arr['file_image']='upload/'.$fileName;
           }else{
               $data_arr['file_image']=$request->old_img;
           }
       if($hidden_id!=''){
            $data_arr['updated_at']=NOW();
            tbl_content_mngt_system::where('id',$hidden_id)->update($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data update sucessfully!']);
       }
       else{
            $data_arr['created_at']=NOW();
            tbl_content_mngt_system::insert($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data added sucessfully!']);
       }
    }
    public function remove_cms(Request $request)
    {
         $get_data   = tbl_content_mngt_system::select('*')->where('id',$request->id)->get();
         if(!$get_data->isEmpty()){
            $data_arr['deleted_at']=NOW();
            tbl_content_mngt_system::where('id',$request->id)->update($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data deleted sucessfully!']);
        }else{
            echo json_encode(['status' => 404, 'message' => 'Data Doesn\'t exits!']);
        }
    }
    public function user_list()
    {
        $data['page_title'] = 'User list';
        return view('admin.users.user_list')->with($data);
    }

    public function enterprise_user_list(){
        $data['page_title'] = 'Enterprise User list';
        return view('admin.users.enterprise_user_list')->with($data);
    }

    public function fetch_user(Request $request)
    {
        $data = [];
        $user = DB::table('users')->select([
            'users.*'
        ])->where('user_category','=','U')
        ->where('user_type','=','User')
        ->orderBy('users.id','DESC')->get();
        $user_list = [];
        
        if(!$user->isEmpty()){
            foreach($user as $eachUser){
				$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$eachUser->id])->orderBy("id","desc")->take(1)->get()->toArray();
                if(count($result)>0){ 
                    $checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$result[0]->time)->where('status','!=','2')->get();
                    $completed=DB::table("tbl_user_documents")->where("created_at",'>=',$result[0]->time)->where('status','=','2')->get();
                    $paymentStatus=($result[0]->payment_status=='P') ? 'PENDING' : 'COMPLETED'; 
                    $doc=$result[0]->doc;
                    $amount=$result[0]->amount;
                    $date=$result[0]->date;
                    $subsid=$result[0]->id;
                }else{
                    $checkingValidation=[];
                    $completed=[];
                    $paymentStatus="NOT SUBSCRIBED";
                    $doc=0;
                    $amount=0;
                    $date="";
                    $id="";
                }
                
                $id = $eachUser->id;
                $creditLog=DB::table("tbl_credit_log")->where(['user_id'=>$id])->orderBy('id','desc')->get();
                $creditLogHtml="<table style='margin-top : 10px;' class='table table-bordered'>
                                    <tr>
                                        <td>Credited</td>
                                        <td>Date Time</td>
                                    </tr>";
                if(!empty($creditLog)){
                    foreach($creditLog as $cl){
                        $creditLogHtml.="<tr> <td> ".$cl->credit." </td> <td> ".$this->dateConvert($cl->time,"jS F,Y")." </td> </tr>";
                    }
                }else{
                    $creditLogHtml.="<tr> <td colspan='2'> No Credit Log Found </td> </tr>";
                }
                $creditLogHtml.="</table>";
                $subs_date='';
                $subs_amt='';
                if(!empty($eachUser->get_subscription_dtl)){
                    $subs_date=$eachUser->get_subscription_dtl['subs_date'];
                    $subs_amt=$eachUser->get_subscription_dtl['product_price'];
                }
                $user_list[] = [
                    'name'     => $eachUser->name,
                    'email'    => $eachUser->email,
                    'country'  => (($eachUser->country=='231') ? 'USA' : $eachUser->country),
                    'city'     => $eachUser->city,
                    'add1'     => $eachUser->add1,
                    'action'    => (count($result)>0 && $result[0]->doc>=5) ? '
                    
                    <a href="javascript:void(0);" class="btn btn-warning" data-toggle="modal" data-target="#myModal'.$eachUser->id.'">+ Add Subscription</a>
                    <a href="javascript:void(0);" title="view current subscription" class="btn btn-success" data-toggle="modal" data-target="#vcs'.$eachUser->id.'">VCS</a>

                    <div class="modal" id="myModal'.$eachUser->id.'">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form method="post" action="'.url('user-list/insert-enterprise-subscription').'" novalidate="novalidate">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <input class="form-control" required="required" type="text" name="doc" id="doc" placeholder="Required Documents">
                                                <input type="hidden" value="'.$eachUser->id.'" name="user_id" />
                                                <input type="hidden" value="'.csrf_token().'" name="_token"/>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" required="required" type="text" name="amount" id="amount" placeholder="Amount">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-success" />
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="modal" id="vcs'.$eachUser->id.'">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h3>Current Subscription</h3>
                                <p><b>Required Docs - </b>'.$doc.'</p>
                                <p><b>Amount Charged - </b>$'.$amount.'</p>
                                <p><b>Subscription Date - </b>'.$this->dateConvert($date).'</p>
                                <p><b>Payment Status - </b>'.$paymentStatus.'</p>
                                <p><b>Document Uploaded in current plan - </b>'.count($checkingValidation).'</p>
                                <p><b>Notarized documents in current plan - </b>'.count($completed).'</p>
                                <form method="post" action="'.url('user-list/credit-update').'">
                                    <div class="form-gorup">
                                        <label>Choose no. of documents you want to credit the user</label><br>
                                        <input type="hidden" name="_token" value="'.csrf_token().'" />
                                        <input type="hidden" name="doc_count_id" value="'.$subsid.'" />
                                        <input type="number" name="creditdoc" />
                                        <input type="hidden" name="redirect" value="user-list" />
                                        <input type="submit" class="btn btn-info" value="Update Credit"/>
                                    </div>
                                </form>
                                '.$creditLogHtml.'
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>' : '<a href="'.url('user-list/document-lists/').'/'.$eachUser->id.'" class="btn btn-warning">View Document List</a>',
                ];
            }
        }
        return Datatables::of($user_list)->escapeColumns([])->make(true);
    }

    public function credit_update(Request $request){
        DB::table("tbl_user_doc_counts")->where(['id'=>$request->doc_count_id])->increment('doc' , $request->creditdoc);
        $result=DB::table("tbl_user_doc_counts")->where(['id'=>$request->doc_count_id])->get()->first();
        $table=[
            'user_id' => $result->user_id,
            'credit'  => $request->creditdoc,
            'date'    => date("Y-m-d"),
            'time'     => date("Y-m-d H:i:s"),
        ];
        DB::table('tbl_credit_log')->insert($table);
        return redirect($request->redirect)->with("message","Credited Successfully");
    }  

    public function document_lists($user_id){
        $data['user_id']=$user_id;
        $data['page_title']="Document List";
        return view('admin.users.document_list')->with($data);
    }

    public function wave_off_documents(Request $request){
        $update_arr=['payment_status'=>'C'];
        DB::table("tbl_user_documents")->where('id',$request->doc_id)->update($update_arr);
        die(json_encode(['status'=>200,'message'=> 'Success']));
    }   

    public function fetch_user_wise_docs($user_id){
        $data = [];
        $lawyer_id=Auth()->user()->id;
        if(Auth()->user()->user_type=='Lawyer'){
            $user_doc = tbl_user_document::with(['user_dtl','lawyer_dtl'])->where(['user_id'=>$user_id,'lawyer_id'=>$lawyer_id])->orderByDesc('id')->get();
        }else{
            $user_doc = tbl_user_document::with(['user_dtl','lawyer_dtl'])->where(['user_id'=>$user_id])->orderByDesc('id')->get();
        }
        $user_doc_list = [];
        
        if(!$user_doc->isEmpty()){
            foreach($user_doc as $eachUser){
                $id = $eachUser->id;
                $subs_date='';
                $subs_amt='';
                if(!empty($eachUser->get_subscription_dtl)){
                    $subs_date=$eachUser->get_subscription_dtl['subs_date'];
                    $subs_amt=$eachUser->get_subscription_dtl['product_price'];
                }
                $action='';
                $link=url("start-video").'/'.base64_encode($id.'~~'.$eachUser->user_dtl['id']);
                
                if($eachUser->payment_status=="P"){
                    $action.='<a class="btn btn-info" title="Wave Of" href="javascript:void(0);" onclick="waveOff('.$id.')" style="font-size:20px;"> Wave Off </a>';
                }else{
                    $action.='<b style="color : green;">Payment Done</b>';
                }
                
               
                $user_doc_list[] = [
                    'name'     => $eachUser->user_dtl['name'],
                    'email'    => $eachUser->user_dtl['email'],
                    'country'  => (($eachUser->user_dtl['country']=='231')? 'USA' : $eachUser->user_dtl['country']),
                    'city'     => $eachUser->user_dtl['city'],
                    'lawyer_detail'=>'<b>'.(isset($eachUser->lawyer_dtl['name'])?'Name:- '.$eachUser->lawyer_dtl['name'].'<br>Email ID:- '.$eachUser->lawyer_dtl['email']:'Not Yet Assigned').'</b>',
                    'dateTime' => $this->dateConvert($eachUser->created_at,'jS M,Y'),
                    'action'   =>(isset($eachUser->lawyer_dtl['name']) ? $action : 'Not Notarized')
                ];
            }
        }
        // Carbon::createFromFormat('d/m/Y', $val)->toDateTimeString();
        return Datatables::of($user_doc_list)->escapeColumns([])->make(true);
    }

    public function dateConvert($old_date,$format='jS F,Y')
		{
			$old_date_timestamp = strtotime($old_date);
			$new_date = date($format, $old_date_timestamp); 
			return $new_date;
		}

    public function fetch_enterprise_user(Request $request)
    {
         $data = [];
        // $user = User::with('tbl_user_doc_counts')->where('user_category','=','E')->orderBy('id','DESC')->get();
        /*$user = DB::select([
            'users.*',
            'tbl_user_doc_counts.id as subsid',
            'tbl_user_doc_counts.doc'
        ])->leftJoin('tbl_user_doc_counts','tbl_user_doc_counts.user_id','=','users.id')
        ->where('tbl_user_doc_counts.user_category','=','E')
        ->orderBy('users.id','DESC')->get(); */
        $user = DB::table('users')->select([
            'users.*'
        ])->where('user_category','=','E')
        ->orderBy('users.id','DESC')->get();
        $user_list = [];
        
        if(!$user->isEmpty()){
            foreach($user as $eachUser){
				$result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$eachUser->id])->orderBy("id","desc")->take(1)->get()->toArray();
                if(isset($result) && !empty($result)){ 
                    $checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$result[0]->time)->where('status','!=','2')->get();
                    $completed=DB::table("tbl_user_documents")->where("created_at",'>=',$result[0]->time)->where('status','=','2')->get();
                    $paymentStatus=($result[0]->payment_status=='P') ? 'PENDING' : 'COMPLETED'; 
                    $doc=$result[0]->doc;
                    $amount=$result[0]->amount;
                    $date=$result[0]->date;
                    $unid=$result[0]->id;
                }else{
                    $checkingValidation=[];
                    $completed=[];
                    $paymentStatus="NOT SUBSCRIBED";
                    $doc=0;
                    $amount=0;
                    $date="";
                    $unid='';
                }
                
                $id = $eachUser->id;
                $creditLog=DB::table("tbl_credit_log")->where(['user_id'=>$id])->orderBy('id','desc')->get();
                $creditLogHtml="<table style='margin-top : 10px;' class='table table-bordered'>
                                    <tr>
                                        <td>Credited</td>
                                        <td>Date Time</td>
                                    </tr>";
                if(!empty($creditLog)){
                    foreach($creditLog as $cl){
                        $creditLogHtml.="<tr> <td> ".$cl->credit." </td> <td> ".$this->dateConvert($cl->time,"jS F,Y")." </td> </tr>";
                    }
                }else{
                    $creditLogHtml.="<tr> <td colspan='2'> No Credit Log Found </td> </tr>";
                }
                $creditLogHtml.="</table>";
                $subs_date='';
                $subs_amt='';
                if(!empty($eachUser->get_subscription_dtl)){
                    $subs_date=$eachUser->get_subscription_dtl['subs_date'];
                    $subs_amt=$eachUser->get_subscription_dtl['product_price'];
                }
                $user_list[] = [
                    'name'     => $eachUser->name,
                    'email'    => $eachUser->email,
                    'country'  => (($eachUser->country=='231')?'USA':$eachUser->country),
                    'city'     => $eachUser->city,
                    'add1'     => $eachUser->add1,
                    'action'    => '
                    
                    <a href="javascript:void(0);" class="btn btn-warning" data-toggle="modal" data-target="#myModal'.$eachUser->id.'">+ Add Subscription</a>
                    <a href="javascript:void(0);" title="view current subscription" class="btn btn-success" data-toggle="modal" data-target="#vcs'.$eachUser->id.'">VCS</a>

                    <div class="modal" id="myModal'.$eachUser->id.'">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form method="post" action="'.url('user-list/insert-enterprise-subscription').'" novalidate="novalidate">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <input class="form-control" required="required" type="text" name="doc" id="doc" placeholder="Required Documents">
                                                <input type="hidden" value="'.$eachUser->id.'" name="user_id" />
                                                <input type="hidden" value="'.csrf_token().'" name="_token"/>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control" required="required" type="text" name="amount" id="amount" placeholder="Amount">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-success" />
                                            </div>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>

                    <div class="modal" id="vcs'.$eachUser->id.'">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <h3>Current Subscription</h3>
                                <p><b>Required Docs - </b>'.$doc.'</p>
                                <p><b>Amount Charged - </b>$'.$amount.'</p>
                                <p><b>Subscription Date - </b>'.$this->dateConvert($date).'</p>
                                <p><b>Payment Status - </b>'.$paymentStatus.'</p>
                                <p><b>Document Uploaded in current plan - </b>'.count($checkingValidation).'</p>
                                <p><b>Notarized documents in current plan - </b>'.count($completed).'</p>
                                <form method="post" action="'.url('user-list/credit-update').'">
                                    <div class="form-gorup">
                                        <label>Choose no. of documents you want to credit the user</label><br>
                                        <input type="hidden" name="_token" value="'.csrf_token().'" />
                                        <input type="hidden" name="doc_count_id" value="'.$unid.'" />
                                        <input type="number" name="creditdoc" />
                                        <input type="hidden" name="redirect" value="user-list/enterprise-user-list" />
                                        <input type="submit" class="btn btn-info" value="Update Credit"/>
                                    </div>
                                </form>
                                '.$creditLogHtml.'
                            </div>
                            <!-- Modal footer -->
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>

                    
                    ',
                ];
            }
        }
        return Datatables::of($user_list)->escapeColumns([])->make(true);
    }

     

    public function insert_enterprise_subscription(Request $request){
        $user=[
            'user_id'=> $request->user_id,
            'amount' => $request->amount,
            'doc'    => $request->doc,
            'payment_status'=> 'P',
            'date'  => date('Y-m-d'),
            'time'  => date('Y-m-d H:i:s'),
        ];
        $insert=DB::table("tbl_user_doc_counts")->insert($user);

        $update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW(),'is_active'=>1,'user_category'=>'E');
        User::where('id',$request->user_id)->update($update_arr);

        $userResult=User::where('id',$request->user_id)->get()->toArray();

        $user_data="Your Subscription is confirmed now you can login to continue";
        $html = View('emails.enterprise-mail2', ['user'=> $user_data]);
        $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = $userResult[0]['email'], $subject = 'Your Subscription is confirmed', $message = $html);

        return redirect($_SERVER['HTTP_REFERER'])->with('message', 'Subscription Addedd Successfully');
    }

    public function enterprise_user_registration(Request $request)
    {
        $name=$request->f_name.' '.$request->l_name;
        $email=$request->u_email;
        $personal_mobile=$request->personal_mobile;
        $password=$request->u_password;
        $is_privacy=1;
        $is_terms=1;
        $user_data=array(    'name'=>$name,
                             'email'=>$email,
                             'personal_mobile'=>$personal_mobile,
                             'password'=>Hash::make($password),
                             'is_privacy'=>$is_privacy,
                             'is_terms'=>$is_terms,
                             'created_at'=>NOW(),
                             'user_category'=>'E',
                             'city' => $request->city,
                             'add1' => $request->address,
                           );
        $user_id=User::insertGetId($user_data);
        $userDSocData=[
            'user_id' => $user_id,
            'doc'     => $request->doc_no,
            'status'    => 'A',
            'amount'    => $request->amount,
            'payment_status' => 'p',
            'date'      => date("Y-m-d"),
            'time'      => date("Y-m-d H:i:s"),
        ];
        $data=DB::table("tbl_user_doc_counts")->insert($userDSocData);
        $update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW());
        User::where('email',$email)->update($update_arr);
        echo json_encode(['status' => 200, 'mode'=>'redirect','message' => 'Thank you for registration!']);

    }

    public function user_document(Request $request,$type_id='')
    {
        $this->check_admin();
        // $cms_list           = tbl_user_document::with('user_dtl')->where('status',$type_id)->get();
        // $data['cms_list']   = $cms_list;
        $data['type_id']=$type_id;
        $data['page_title'] = 'Notarization';
        return view('admin.lawyer.document_list')->with($data);
    }
    public function fetch_user_documents(Request $request,$type_id='')
    {
        
        $data = [];
        $user_id=Auth()->user()->id;
        if(Auth()->user()->user_type=='Lawyer'){
            $user_doc = tbl_user_document::with(['user_dtl','lawyer_dtl'])->where(['status'=>$type_id,'lawyer_id'=>$user_id])->orderByDesc('id')->get();
        }else{
            $user_doc = tbl_user_document::with(['user_dtl','lawyer_dtl'])->where(['status'=>$type_id])->orderByDesc('id')->get();
        }
         //echo '<pre>';print_r($user_doc->toArray());die;
        //$total_emp_list = employee_tbl::with('get_user_data')->offset(201)->limit(100)->get();
        $user_doc_list = [];
        
        if(!$user_doc->isEmpty()){
            foreach($user_doc as $eachUser){
                // echo "<pre>"; print_r($eachUser->toArray()); exit;
                $id = $eachUser->id;
                $subs_date='';
                $subs_amt='';
                if(!empty($eachUser->get_subscription_dtl)){
                    $subs_date=$eachUser->get_subscription_dtl['subs_date'];
                    $subs_amt=$eachUser->get_subscription_dtl['product_price'];
                }
                $action='';
                $link=url("start-video").'/'.base64_encode($id.'~~'.$eachUser->user_dtl['id']);
                if(Auth()->user()->user_type=='Lawyer'){
                  
                    if($eachUser->send_mail=='0' && $eachUser->status<2){
                      $action.='<a class="deptedit" href="javascript:void(0);"title="Send Video Call Link"   style="font-size:20px;"   onclick="send_video_link(\''.$id.'\',\''.$eachUser->user_dtl['id'].'\',\''.$eachUser->user_dtl['name'].'\',\''.$eachUser->user_dtl['email'].'\')"  ><i class="typcn typcn-arrow-forward view_icon"></i></a> ';
                    }elseif($eachUser->send_mail=='1' && $eachUser->status<2){
                      $action.='Link Sent';
                    }
                    if($eachUser->status=='2'){
                        $action.=($type_id==2) ? ' Completed | <a href="http://aimnotary.com/m3u8/index.php?video='.$eachUser->video.'" target="_blank">Check Video</a>' : 'Completed';
                    }elseif($eachUser->status=='1'){
                        // $action.='<a class="btn btn-success btn-sm" href="https://www.aimnotary.com/signer/document/'.$eachUser->document_key.'" target="_blank"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Notarize</a>';
                        $action.=' <a class="deptedit" title="Start Video Call" href="'.$link.'" target="_blank"    style="font-size:20px;"  ><i class="typcn typcn-video view_icon"></i></a>';
                    }
                }elseif(Auth()->user()->user_type=='Admin'){
                        $action.='<a class="deptedit" title="Start Video Call" href="'.$link.'"   style="font-size:20px;" target="_blank"  ><i class="typcn typcn-video view_icon"></i></a>';
                }
                $front=($eachUser->front!="" || $eachUser->front!=NULL) ? url('/')."/".$eachUser->front : url('/')."/public/no-image.png";
                $back=($eachUser->back!="" || $eachUser->back!=NULL) ? url('/')."/".$eachUser->back : url('/')."/public/no-image.png";
                if($eachUser->doc_type=="DL") {  $doctype="Driving License"; }else if($eachUser->doc_type=="OG"){  $doctype="Others US Government-Issued ID"; }else if($eachUser->doc_type=="PS"){   $doctype="Passport"; }else{  $doctype="No ID Uploded"; };
                $changeLawyer=($eachUser->is_call_started=="N") ? '| <a href="javascript:void();" onclick=OpenAssignModal('.$eachUser->id.','.$eachUser->lawyer_id.')>Change Notary</a>' : '';
                $user_doc_list[] = [
                    'name'     => $eachUser->user_dtl['name'],
                    'email'    => $eachUser->user_dtl['email'],
                    'country'  => (($eachUser->user_dtl['country']=='231')? 'USA' : $eachUser->user_dtl['country']),
                    'city'     => $eachUser->user_dtl['city'],
                    'lawyer_detail'=>'<b>'.(isset($eachUser->lawyer_dtl['name'])?'Name:- '.$eachUser->lawyer_dtl['name'].'<br>Email ID:- '.$eachUser->lawyer_dtl['email']:'Not Yet Assigned').'</b> <br><br><a href="http://aimnotary.com/m3u8/index.php?video='.$eachUser->video.'" target="_blank">Check Video</a> '.$changeLawyer,
                    'doc'     => '<a href="'.(($eachUser->doc!='')?url('/').'/public/'.$eachUser->doc:'').'" target="_blank">View</a> 
                                  | <a style="text-decoration : underline;color : blue;cursor : pointer;" data-toggle="modal" data-target="#myModal'.$eachUser->id.'">View Identity</a> <br><br>  <a data-toggle="modal" data-target="#myModalMerge" onclick="getMergeDocumentList('.$eachUser->id.');" style="text-decoration : underline;color : blue;cursor : pointer;">Add Additional Doc<a>
                                  <div id="myModal'.$eachUser->id.'" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4>'.$doctype.'</h4>
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <a href="'.@$front.'" target="_blank"><img style="height : 150px; width : 225px;border-radius: 0%;" src="'.@$front.'" /></a>
                                                    <a href="'.@$back.'" target="_blank"><img style="height : 150px; width : 225px;border-radius: 0%;" src="'.@$back.'" /></a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                  ',
                    //'dateTime' => $this->dateConvert($eachUser->created_at,'jS M,Y h:i:s'),
                    'dateTime' => date('Y-m-d H:i:s A',strtotime('-9 hour -30 minutes',strtotime($eachUser->created_at))),
                    
                    'action'   =>$action
                   
                    
                ];
            }
        }
        // Carbon::createFromFormat('d/m/Y', $val)->toDateTimeString();
        return Datatables::of($user_doc_list)->escapeColumns([])->make(true);
    }

    // public function dateConvert($dateString,$format)
    // {
    //     $date = new DateTime($dateString);
    //     return $new_date_format = $date->format($format);
    // }
    public function custom_email($from,$to,$subject,$message) {
        try { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
       // $headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
        //Multiple BCC, same as CC above;
       // $headers .= 'Bcc: shaheen.nusrat2015@gmail.com' . "\r\n";
            // Sending email
            if(mail($to, $subject, $message, $headers)){
                return true;
            } else{
                return false;
            }
        }
        catch(Exception $e) {
              echo 'Message: ' .$e->getMessage();
        }
    }
    public function user_video_call()
    {
        
        $cms_list           = tbl_cms_categorie::with('get_category')->where('parent_id','0')->get();
        $data['cms_list']   = $cms_list;
        $data['page_title'] = 'About Us';
        return view('admin.lawyer.call_list')->with($data);
    }
    public function start_call(Request $request)
    {
        $doc_id=$request->id;
        $link=url("start-video-call").'/'.base64_encode($doc_id.'~~'.$request->user_id);
       
        $user_data=array('name'=>$request->name,
                             'email'=>$request->email,
                             'link'=>$link,
                             'send_to'=>'user'
                           );
      //  Mail::send(['html'=>'emails.call-request-mail'],  ['user' => $user_data], function($message) use ($user_data) {
      //    $message->to($user_data['email'])->subject('Star Video Calling to verify your identity.');
      //    $message->from('no-reply@aimnotary.com','AimNotary');
      // });
        
     $html = View('emails.call-request-mail', ['user'=> $user_data]);
     $mail_status=$this->custom_email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Star Video Calling to verify your identity.", $message = $html);
      $update=tbl_user_document::where('id',$doc_id)->update(['send_mail'=>'1','updated_at'=>NOW()]);
      if($update){
          echo json_encode(['status' => 200, 'message' => 'Mail Sent sucessfully!']); 
      }else{
           echo json_encode(['status' => 201, 'message' => 'Somthing Went Wrong!']);
      }
    }
    public function update_call_status(Request $request)
    {
        
        $lawyer_id=Auth::user()->id;
        $doc_id=$request->doc_id;
        $result=tbl_user_document::where('id',$doc_id)->get()->first()->toArray();
        $user=DB::table('users')->where(['id'=>$result['user_id']])->get()->first();
        $email=$user->email;
        $user=(array)$user;
        

        $notary_confirm_stamp=($request->notary_confirm_stamp=="") ? NULL : $request->notary_confirm_stamp;
        $notary_confirm_signer=($request->notary_confirm_signer=="") ? NULL : $request->notary_confirm_signer;
        tbl_user_document::where('id',$doc_id)->update(array('status'=>'2', 
                                                             'notary_confirm_stamp'=> $notary_confirm_stamp, 
                                                             'notary_confirm_signer'=> $notary_confirm_signer,
                                                             'is_call_started'=> 'D',
                                                             'updated_at'=>NOW()));
                                                            //  echo "<pre>"; print_r($request->toArray()); exit;
        if($request->mode=="video"){
            tbl_user_document::where('id',$doc_id)->update(array('video'=>$request->video,'updated_at'=>NOW()));
        }
        tbl_lawyer::where('user_id',$lawyer_id)->update(array('status'=>'Free','updated_at'=>NOW()));
        if($notary_confirm_stamp>1 || $notary_confirm_signer>1){
            $html = View('emails.payment', ['user'=> $user]);
            $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = $email, $subject = 'Payment Reminder', $message = $html);
        }
    }
    
    public function getVideoStopStatus($doc_id){
        $update=tbl_user_document::where('id',$doc_id)->get()->first()->toArray();
        echo json_encode($update);
    }

    public function email_function($from,$to,$subject,$message) {
        try { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
        $headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
        //Multiple BCC, same as CC above;
        $headers .= 'Bcc: rohitmajumder1983@gmail.com,amitdasgupta1980@gmail.com,markr@ez8a.com' . "\r\n";
            // Sending email
            if(mail($to, $subject, $message, $headers)){
                return true;
            } else{
                return false;
            }
        }
        catch(Exception $e) {
              echo 'Message: ' .$e->getMessage();
        }
    }

    public function get_documents_list()
    {
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id;
        if(!is_dir($baseDir))
        {
            mkdir($baseDir,0777,true);
        }
        $specificLawyerFiles=glob($baseDir."/*.pdf");
        $html="";
        foreach($specificLawyerFiles as $key=>$specFile)
        {
            $html.=$this->htmlGetForFile($specFile,$key);
        }
        echo $html;
    }

    public function uploadRepositoryDocument(Request $request)
    {
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id;
        if(!is_dir($baseDir))
        {
            mkdir($baseDir,0777,true);
        }
            $uploadDir = $baseDir."/"; 
            $allowTypes = array('pdf'); 
            $response = array( 
                'status' => 0, 
                'message' => 'Form submission failed, please try again.' 
            ); 
    
            // If form is submitted 
            $errMsg = ''; 
            $valid = 1; 

            $filesArr = $_FILES["files"]; 
            // Check whether submitted data is not empty 
            if($valid == 1){ 
                $uploadStatus = 1; 
                $fileNames = array_filter($filesArr['name']);
                // Upload file 
                if(file_exists($uploadDir.current($fileNames)))
                {
                    $response['message'] = ['status'=>0,'msg'=>'File Name Already exist']; 
                }
                else
                {
                    $uploadedFile = ''; 
                    if(!empty($fileNames)){  
                        foreach($filesArr['name'] as $key=>$val){  
                            // File upload path  
                            $fileName = basename($filesArr['name'][$key]);  
                            $targetFilePath = $uploadDir . $fileName;  
                            
                            // Check whether file type is valid  
                            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);  
                            if(in_array($fileType, $allowTypes)){  
                                // Upload file to server  
                                if(move_uploaded_file($filesArr["tmp_name"][$key], $targetFilePath)){  
                                    $uploadedFile .= $fileName.','; 
                                }else{  
                                    $uploadStatus = 0; 
                                    $response['message'] = ['status'=>0,'msg'=>'Sorry, there was an error uploading your file.']; 
                                }  
                            }else{  
                                $uploadStatus = 0; 
                                $response['message'] = ['status'=>0,'msg'=>'Sorry, only PDF files are allowed to upload.'];
                            }  
                        }  
                    } 
                    
                    if($uploadStatus == 1){
                        // Insert form data in the database 
                        $uploadedFileStr = trim($uploadedFile, ',');
                        $response['status'] = 1; 
                        $response['message'] = ['status'=>1,'msg'=>'Form data submitted successfully!'];
                    } 
                }
                
            }else{ 
                $response['message'] = 'Please fill all the mandatory fields!'.$errMsg; 
            } 
        // Return response 
        echo json_encode($response['message']);
    }

    public function deletDocument(Request $req)
    {
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id."/".$_GET['file'];
        unlink($baseDir);
    }

    public function get_merge_documents_list()
    {
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id;
        if(!is_dir($baseDir))
        {
            mkdir($baseDir,0777,true);
        }
        $specificLawyerFiles=glob($baseDir."/*.pdf");
        $html="";
        foreach($specificLawyerFiles as $key=>$specFile)
        {
            $html.=$this->htmlGetForFileMerge($specFile,$key);
        }
        $html.="<input type='hidden' value='".$_GET['doc_id']."' name='doc_id' id='doc_id'/>";
        echo $html;

    }

    public function htmlGetForFile($files=[],$key=NULL)
    {   
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id."/";
        $files=str_replace($baseDir,"",$files);
        $fileExplode=explode(".",$files);
        $fileExplode1=current($fileExplode);
        if(!empty($files)){
            $html="<div title='".$files."' class='col-md-2 myFileStyle' style='float : left; cursor : pointer;'>
                        <a title='".$files."' onclick='deleteOption({$key});' id='getDataVal{$key}' data-value='{$files}' style='float : right;position : absolute; background-color : red; padding : 3px; color : white;'><i class='fa fa-trash'></i></a>
                        <img width='60' src='https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/PDF_file_icon.svg/488px-PDF_file_icon.svg.png'>
                        <a title='".$files."' style='font-size : 12px;'>".substr($fileExplode1,0,10)."<a>
                    </div>";
        }
       return $html;
    }

    public function htmlGetForFileMerge($files=[],$key=NULL)
    {   
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id."/";
        $files=str_replace($baseDir,"",$files);
        $fileExplode=explode(".",$files);
        $fileExplode1=current($fileExplode);
        
        if(!empty($files))
        {
            $html="<div title='".$files."' class='col-md-2 myFileStyle' style='float : left; cursor : pointer;'>
                        <input type='checkbox' name='allFiles[]' value='{$files}'>
                        <a title='".$files."' onclick='deleteOption({$key});' id='getDataVal{$key}' data-value='{$files}' style='float : right;position : absolute; background-color : red; padding : 3px; color : white;'><i class='fa fa-trash'></i></a>
                        <img width='80' src='https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/PDF_file_icon.svg/488px-PDF_file_icon.svg.png'>
                        <a title='".$files."' style='font-size : 12px;'>".substr($fileExplode1,0,10)."<a>
                    </div>";
        }
       return $html;
    }

    public function mergerDocuments(Request $req)
    {

        $documentKey=tbl_user_document::select("document_key")->where('id',$req->doc_id)->get()->first()->toArray()['document_key'];
        $newBasPathForSigner=base_path();
        $basePath = $newBasPathForSigner;
        // $basePath=str_replace('beta','', $newBasPathForSigner);
        $lawyer_id=Auth::user()->id;
        $baseDir="document_cloud/".$lawyer_id."/";
        $docsDetails=tbl_user_document::where('id',$req->doc_id)->get()->toArray();
        $oldfile=base_path().'/public/'.current($docsDetails)['doc'];
        $newName = str_replace('upload/user_doc/','',current($docsDetails)['doc']);
        $files[0] = $oldfile;
        foreach($req->allFiles as $singleFile){
            $files[] = base_path().'/'.$baseDir.$singleFile;
        }
        $files['doc_name'] = $newName;

        $ch = curl_init();
        $fields = $files;
        $postvars = '';
        foreach($fields as $key=>$value) {
            $postvars .= $key . "=" . $value . "&";
        }
        $url = "https://www.aimnotary.com/Fpdi_working/practice.php";
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        curl_close ($ch);
        return redirect($_SERVER['HTTP_REFERER'])->with('message', 'Documents Merged Successfully');
    }
    

    public function schedule_appointment(){
        $data['data']=[];
        $data['page_title']="Schedule Appointment List";
        return view("admin.schedule.schedule_appointment")->with($data);
    }

    public function assign_user_doc(Request $request){
        // tbl_user_document::where('id',$request->doc_id)->update(array('lawyer_id'=>$request->lawyer_user_id,'updated_at'=>NOW()));
        // 
        $old_user=tbl_user_document::where('id',$request->doc_id)->get()->first();
        $user=DB::table("users")->where("id","=",$old_user->lawyer_id)->get()->first();
        $email=$user->email;
        tbl_user_document::where('id',$request->doc_id)->update(array('lawyer_id'=>$request->lawyer_user_id,'updated_at'=>NOW()));
        $newUser=DB::table("users")->where("id","=",$request->lawyer_user_id)->get()->first();
        $new_email=$newUser->email;
        $result=DB::table("tbl_scedule_date")->where("document_id","=",$request->doc_id)->get();

        $desc="<h3>Hello ".$user->name.", We are sorry to inform you that we have cancelled your appointment</h3>";
        // Cancel  Email
        $user_data=$desc;
        $html = View('emails.schedule-email-appoint', ['user'=> $user_data]);
        $this->smtp_auth_email($email, $subject = 'Appointment Cancelled', $html);
        $desc="<h3>Hello ".$newUser->name.", you have been appointed as the online notary for one of our customers. Please check your Aim Notary Dashboard to complete the online notarization process.</h3>";
        // Sent  Email
        $user_data=$desc;
        $html = View('emails.schedule-email-appoint', ['user'=> $user_data]);
        $this->smtp_auth_email($new_email, $subject = 'Aim Notary - New Appointment Confirmation', $html);

    }

    public function assign_user(Request $request){

        /*// tbl_lawyer::where('id',$request->lawyer_id)->update(array('status'=>'Busy','updated_at'=>NOW())); */
        DB::table("tbl_scedule_date")->where('document_id','=',$request->doc_id)->update(['lawyer_id'=>$request->lawyer_user_id]);
		tbl_user_document::where('id',$request->doc_id)->update(array('lawyer_id'=>$request->lawyer_user_id,'updated_at'=>NOW()));
        $result=DB::table("tbl_scedule_date")->where("document_id","=",$request->doc_id)->get();
        $user=DB::table("users")->where("id","=",$result[0]->user_id)->get();
        $lawyer=DB::table("users")->where("id","=",$request->lawyer_user_id)->get();
        $desc="<h3>".$result[0]->title.". You have been assigned a new notarization.</h3>";
        $desc.="<b> Date : </b> ".$this->dateConvert($result[0]->date)."<br>";
        $desc.="<b> Time : </b> ".$result[0]->time."<br>";
        if($result[0]->timezone=="-8"){
                $timezone="Pacific Time (US & Canada)";
            }elseif($result[0]->timezone=="-7"){
                $timezone="Mountain Time (US & Canada)";
            }elseif($result[0]->timezone=="-6"){
                $timezone="Central Time (US & Canada)";
            }elseif($result[0]->timezone=="-5"){
                $timezone="Eastern Time (US & Canada)";
            }
            $desc.="<b>Time Zone : </b> ".$timezone."<br>";
            $desc.="<b>Location : </b>".$result[0]->location."<br>";
            $desc.="<b>Description : </b>".$result[0]->description."<br>";
        
        $user_data=$desc;
        $html = View('emails.schedule-email-appoint', ['user'=> $user_data]);
        $this->smtp_auth_email("info@aimnotary.com", $subject = 'New Appointment is Scheduled', $html);
	    /* $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = "amith.econstra@gmail.com", $subject = "Start Video Calling to verify The user document.", $message = $html); */
        $this->smtp_auth_email($lawyer[0]->email, $subject = 'New Appointment is Scheduled', $html);


        $userEmail="Hello ".$user[0]->name.",<br>Your Appointment with Aim Notary is scheduled as requested. Please login to to your account at the scheduled date/ time to notarize your document.";
        $html = View('emails.schedule-email-appoint', ['user'=> $userEmail]);
	    $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = $user[0]->email, $subject = "Aim Notary - Confirmation of Appointment", $message = $html);
    }

    public function differenceInHours($startdate,$enddate){
        $starttimestamp = strtotime($startdate);
        $endtimestamp = strtotime($enddate);
        $difference = abs($endtimestamp - $starttimestamp)/3600;
        return $difference;
    }

    public function fetch_lawyer(Request $request){
        $users=DB::table("tbl_scedule_date")->select([
                        'tbl_scedule_date.*',
                        'users.*',
                        'tbl_user_documents.id as doc_id'
                    ])
                    ->leftjoin("tbl_user_documents","tbl_user_documents.id","=","tbl_scedule_date.document_id")
                    ->leftjoin("users","tbl_scedule_date.user_id","=","users.id")
                    ->where("tbl_user_documents.lawyer_id","=",0)
                    ->where("tbl_user_documents.id","=",$request->doc_id)
                    ->get();
        
        if(!$users->isEmpty()){
            foreach($users as $eachUser){
                $get_schedule=DB::table("tbl_scedule_date")
                    ->select(['tbl_scedule_date.time','tbl_scedule_date.lawyer_id'])
                    ->where("tbl_scedule_date.date","=",$eachUser->date)
                    ->where("tbl_scedule_date.lawyer_id","!=",0)
                    ->get();
                $tdHtml="";
                if(!empty($get_schedule->toArray())){
                    
                    foreach($get_schedule as $key=>$getSc){
                        echo $this->differenceInHours($eachUser->time,$getSc->time);
                        if($this->differenceInHours($eachUser->time,$getSc->time)<1){
                            $lawyer_not_free[]=$getSc->lawyer_id;
                        }else{
                            $lawyer_free[]=$getSc->lawyer_id;
                        }
                    }
                    // print_r($lawyer_free); exit;
                    if(empty($lawyer_not_free)){
                        $get_lowyer=tbl_lawyer::select(['*'])->where('status','=','Free')->with('lawyer_dtl')->get();
                    }else{
                        $lawyer_not_freeimplode=implode(",",$lawyer_not_free);
                        $get_lowyer=tbl_lawyer::select(['*'])->with('lawyer_dtl')->whereNotIn('id', [$lawyer_not_freeimplode])->get();
                    }
                    
                    if(!empty($get_lowyer)){
                       
                        foreach($get_lowyer as $lawyer){
                           
                            if(!empty($lawyer_not_free)){
                                if(in_array($lawyer['user_id'],$lawyer_not_free)){
                                    // $tdHtml.="";
                                }else{
                                    $successfullyMaked=DB::table("tbl_user_documents")->where('tbl_user_documents.lawyer_id','=',$lawyer['id'])->get();
                                    $lid=$lawyer['id'];
                                    $luserid=$lawyer['user_id'];
                                    $tdHtml.="<tr>
                                                <td><img src='".url('public/'.$lawyer['profile_pic'])."' width='50'/></td>
                                                <td>".$lawyer['lawyer_dtl']['name']."</td>
                                                <td>".$lawyer['lawyer_dtl']['email']."</td>
                                                <td>".$lawyer['lawyer_dtl']['personal_mobile']."</td>
                                                <td>".$lawyer['priority']."</td>
                                                <td>".$lawyer['NNA_Certified']."</td>
                                                <td>".count($successfullyMaked)."</td>
                                                <td> <button data-id='".$lawyer['id']."' data-userid='".$lawyer['user_id']."' onclick='makeAssign({$lid},{$luserid},{$eachUser->doc_id});'>Assign</button> </td>
                                            </tr>";
                                }
                            }else{
                                $successfullyMaked=DB::table("tbl_user_documents")->where('tbl_user_documents.lawyer_id','=',$lawyer['id'])->get();
                                $lid=$lawyer['id'];
                                $luserid=$lawyer['user_id'];
                                $tdHtml.="<tr>
                                            <td><img src='".url('public/'.$lawyer['profile_pic'])."' width='50'/></td>
                                            <td>".$lawyer['lawyer_dtl']['name']."</td>
                                            <td>".$lawyer['lawyer_dtl']['email']."</td>
                                            <td>".$lawyer['lawyer_dtl']['personal_mobile']."</td>
                                            <td>".$lawyer['priority']."</td>
                                            <td>".$lawyer['NNA_Certified']."</td>
                                            <td>".count($successfullyMaked)."</td>
                                            <td> <button data-id='".$lawyer['id']."' data-userid='".$lawyer['user_id']."' onclick='makeAssign({$lid},{$luserid},{$eachUser->doc_id});'>Assign</button> </td>
                                        </tr>";
                            }
                            
                            
                        }
                    }
                }else{
                    
                    $get_lowyer=tbl_lawyer::select(['*'])->with('lawyer_dtl')->get();
                    if(!empty($get_lowyer)){
                        foreach($get_lowyer as $lawyer){
                            $successfullyMaked=DB::table("tbl_user_documents")->where('tbl_user_documents.lawyer_id','=',$lawyer['id'])->get();
                            $lid=$lawyer['id'];
                            $luserid=$lawyer['user_id'];
                            $tdHtml.="<tr>
                                        <td><img src='".url('public/'.$lawyer['profile_pic'])."' width='50'/></td>
                                        <td>".$lawyer['lawyer_dtl']['name']."</td>
                                        <td>".$lawyer['lawyer_dtl']['email']."</td>
                                        <td>".$lawyer['lawyer_dtl']['personal_mobile']."</td>
                                        <td>".$lawyer['priority']."</td>
                                        <td>".$lawyer['NNA_Certified']."</td>
                                        <td>".count($successfullyMaked)."</td>
                                        <td> <button data-id='".$lawyer['id']."' data-userid='".$lawyer['user_id']."' onclick='makeAssign({$lid},{$luserid},{$eachUser->doc_id});'>Assign</button> </td>
                                    </tr>";
                            
                        }
                    }
                }


                

                echo $tdHtml;

            }
        }
    }

    public function fetch_lawyer_docs(Request $request){
        
        $get_lowyer=tbl_lawyer::select(['*'])->with('lawyer_dtl')->where([])->get();
        $tdHtml="";
        if(!empty($get_lowyer)){
            foreach($get_lowyer as $lawyer){
                    $successfullyMaked=DB::table("tbl_user_documents")->where('tbl_user_documents.lawyer_id','=',$lawyer->lawyer_dtl->id)->get();
                    $lid=$lawyer->lawyer_dtl->id;
                    $luserid=$lawyer['user_id'];
                    if((int)$request->lawyer_id!=(int)$lawyer->lawyer_dtl->id){
                    $tdHtml.="<tr>
                                <td><img src='".url('public/'.$lawyer['profile_pic'])."' width='50'/></td>
                                <td>".$lawyer['lawyer_dtl']['name']."</td>
                                <td>".$lawyer['lawyer_dtl']['email']."</td>
                                <td>".$lawyer['lawyer_dtl']['personal_mobile']."</td>
                                <td>".$lawyer['priority']."</td>
                                <td>".$lawyer['NNA_Certified']."</td>
                                <td>".count($successfullyMaked)."</td>
                                <td> <button data-id='".$lawyer['id']."' data-userid='".$lawyer['user_id']."' onclick='makeAssignDocs({$lid},{$luserid},{$request->doc_id});'>Assign</button> </td>
                            </tr>";
                    }
            }
        }
    
        echo $tdHtml;
        
    }

    public function fetch_schedule_user(){
            $users=DB::table("tbl_scedule_date")->select([
                            'tbl_scedule_date.*',
                            'users.*',
                            'tbl_user_documents.id as doc_id'
                        ])
                        ->leftjoin("tbl_user_documents","tbl_user_documents.id","=","tbl_scedule_date.document_id")
                        ->leftjoin("users","tbl_scedule_date.user_id","=","users.id")
                        ->where("tbl_user_documents.lawyer_id","=",0)
                        ->orderBy("tbl_scedule_date.date",'ASC')
                        ->get();

            $json="";
            

            if(!$users->isEmpty()){
                // echo "<pre>"; print_r($users); exit;
                foreach($users as $eachUser){
                    $result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>$eachUser->id])->orderBy("id","desc")->take(1)->get()->toArray();
                    if(isset($result) && !empty($result)){ 
                        $checkingValidation=DB::table("tbl_user_documents")->where("created_at",'>=',$result[0]->time)->where('status','!=','2')->get();
                        $completed=DB::table("tbl_user_documents")->where("created_at",'>=',$result[0]->time)->where('status','=','2')->get();
                        $paymentStatus=($result[0]->payment_status=='P') ? 'PENDING' : 'COMPLETED'; 
                        $doc=$result[0]->doc;
                        $amount=$result[0]->amount;
                        $date=$result[0]->date;
                    }else{
                        $checkingValidation=[];
                        $completed=[];
                        $paymentStatus="NOT SUBSCRIBED";
                        $doc=0;
                        $amount=0;
                        $date="";
                    }
                    
                    $id = $eachUser->id;
                    $subs_date='';
                    $subs_amt='';
                    if(!empty($eachUser->get_subscription_dtl)){
                        $subs_date=$eachUser->get_subscription_dtl['subs_date'];
                        $subs_amt=$eachUser->get_subscription_dtl['product_price'];
                    }

                   
                    // print_r($lawyer_not_free); exit;
                    $user_list[] = [
                        'name'     => $eachUser->name,
                        'email'    => $eachUser->email,
                        'country'  => (($eachUser->country=='231')?'USA':$eachUser->country),
                        'city'     => $eachUser->city,
                        'add1'     => $eachUser->add1,
                        'date'     => $this->dateConvert($eachUser->date),
                        'time'     => $eachUser->time,
                        'action'    => '<a href="javascript:void(0);" title="view current subscription" class="btn btn-success" onclick=OpenAssignModal('.$eachUser->doc_id.')>Appoint</a>',
                    ];
                    
                }
                
            }
            // $get_lowyer['lawyer_dtl']['name']
            if(!empty($user_list)){
                return Datatables::of($user_list)->escapeColumns([])->make(true);
            }else{
                return Datatables::of([])->escapeColumns([])->make(true);
            }
           

    }


    public function transaction(){
        $data['page_title'] = 'Transaction List';

        $data['transaction_list'] = DB::table("tbl_transactions")->select([
                                        'tbl_transactions.*',
                                        'users.*',
                                        'tbl_user_documents.doc as doc'
                                    ])
                                    ->join('users', 'users.id', '=', 'tbl_transactions.user_id')
                                    ->join('tbl_user_documents', 'tbl_user_documents.id', '=', 'tbl_transactions.document_id')
                                    ->orderBy('tbl_transactions.txn_id','desc')
                                    ->get();
        return view('admin.transaction.transaction_list')->with($data);
    }

    public function refund(Request $request){
        // print_r($request->toArray()); exit;
        

        $apiKey = "92f2IMFYZ8ADBcEONKM4svIkKGqZlnbA";
        $apiSecret = "6e91dc229e8d3f6222c21d6994da54d96be6d27dc3ea18c495e0f89002998441";
        $nonce = strval(hexdec(bin2hex(openssl_random_pseudo_bytes(4, $cstrong))));
        $timestamp = strval(time()*1000); //time stamp in milli seconds
        $token = "fdoa-bcfe5186f7dc9d9194f2fdcbcd4670e962761f80d057a8b5";
        $payloadFields=[
            'transaction_tag' => $request->transaction_tag,
            'transaction_type' => 'refund',
            'amount'          => $request->amount,
            'currency_code'   => 'USD',
            'method' => 'credit_card',
        ];
        $payload = json_encode($payloadFields);
        $data = $apiKey . $nonce . $timestamp . $token . $payload;
        $hashAlgorithm = "sha256";
        $hmac = hash_hmac ( $hashAlgorithm , $data , $apiSecret, false );
        $authorization = base64_encode($hmac);
        // echo $authorization;
        $curl = curl_init('https://api.payeezy.com/v1/transactions/'.$request->authno);
        $headers = array(
            'Content-Type: application/json',
            'apikey:'.strval($apiKey),
            'token:'.strval($token),
            'Authorization:'.$authorization,
            'nonce:'.$nonce,
            'timestamp:'.$timestamp,
        );

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
    
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        
        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $response = json_decode($json_response, true);
        $updating=[
            'status' => 'R',
            'refund_json' => $json_response,
        ];
        DB::table('tbl_transactions')->where('txn_id',$request->txnid)->update($updating);
        echo $json_response;

       
    }
    

}
