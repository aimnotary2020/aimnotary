<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\tbl_blog;
use App\Models\tbl_lawyer;
use App\Models\tbl_state;
use App\User;
use Auth;
use Mail;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
class BlogController extends Controller
{
    //
    public function index()
    {
    	$data['page_title'] = 'Blog List';
    	return view('admin.blog.blog_list')->with($data);
    }

    public function fetch_blog()
    {
    	$blog_list=[];
    	$blog=tbl_blog::with('get_user_dtl')->where('is_case_study','N')->orderBy('id','DESC')->get();
    	// echo '<pre>';print_r($blog->toArray());die;
    	if(!$blog->isEmpty()){
	    	foreach ($blog as $key => $eachVal) {
	    		$id=$eachVal->id;
	    		$posted_by=$eachVal->get_user_dtl['name'];
	    		$blog_list[]=[
	    						'posted_by'  =>$posted_by,
	    						'title'      =>$eachVal->title,
	    						'featured'   =>$eachVal->featured,
	    						'image'      =>'<img src="'.asset('/public/'.$eachVal->image).'" >',
	    						'short_desc' =>wordwrap($eachVal->short_desc,20,"<br>\n"),
	    						'action'           =>'<a class="deptedit" href="javascript:void(0);"title="View"   style="font-size:20px;"  data-toggle="modal" data-target="#myModal" onclick="get_dtl(\''.$id.'\')"  ><i class="typcn typcn-edit view_icon"></i></a><a class="delete" href="javascript:void(0);"title="View"   style="font-size:20px; color:red;" onclick="remove_blog(\''.$id.'\')"  ><i class="typcn typcn-trash view_icon"></i></a>',
	    					];
	    	}
    	}

    	 return Datatables::of($blog_list)->escapeColumns([])->make(true);
    }

    public function edit_blog(Request $request)
    {
    	$id = $request->id;
        $get_blog= tbl_blog::where('id',$id)->first();
        echo json_encode($get_blog);
    }

    public function submit_blog(Request $request)
    {
    	$title=$request->title;
    	$res = preg_replace("/[^a-zA-Z0-9\s]/", "", $title);
    	$generate_url=strtolower(str_replace(" ","-",$res));
    	$data_arr=array();
    	$get_blog= tbl_blog::where('title',$title)->count();
        $blog_id=$request->hidden_id;
		if($request->upload_img!=''){
		        $fileName = time().'.'.$request->upload_img->extension();  
		        $request->upload_img->move(public_path('upload'), $fileName);
		        $data_arr['image']='upload/'.$fileName;
		}else{
		   $data_arr['image']=$request->old_img;
		}

		$data_arr['title']=$title;
        
		$data_arr['generate_url']=$generate_url;
		$data_arr['short_desc']=$request->short_desc;
		$data_arr['description']=$request->description;
        
        if($blog_id==''){
        	if($get_blog==0){
                $data_arr['posted_by']=Auth::user()->id;
				 $data_arr['created_at']=NOW();
	            tbl_blog::insert($data_arr);
	            echo json_encode(['status' => 200, 'message' => 'Data added sucessfully!']);	
			}else{
				
	            echo json_encode(['status' => 201, 'message' => 'Data Already Exists!']);
			}
        }else{
        	$data_arr['updated_at']=NOW();
            tbl_blog::where('id',$blog_id)->update($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data update sucessfully!']);
        }
    }

    public function remove_blog(Request $request)
    {
        $get_data   = tbl_blog::select('*')->where('id',$request->id)->get();
         if(!$get_data->isEmpty()){
            $data_arr['deleted_at']=NOW();
            tbl_blog::where('id',$request->id)->update($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data deleted sucessfully!']);
        }else{
            echo json_encode(['status' => 404, 'message' => 'Data Doesn\'t exits!']);
        }
    }


    public function lawyer_list()
    {
        $data['page_title'] = 'Lawyer List';
        return view('admin.lawyer.lawyer_list')->with($data);
    }
   
    public function fetch_lawyer()
    {
        $lawyer_list=[];
        $lawyer=User::with('lawyer_dtl')->where('user_type','Lawyer')->orderBy('id','DESC')->get();
       // $lawyer=tbl_lawyer::with('get_state','lawyer_dtl')->orderBy('id','DESC')->get();
       // echo '<pre>';print_r($lawyer->toArray());die;
        if(!$lawyer->isEmpty()){
            foreach ($lawyer as $key => $eachVal) {
                $id=$eachVal->id;

                // $state=tbl_state::select('id','state_name')->where('id',$eachVal->lawyer_dtl['state_id'])->first();
                // $state_nm=$state->state_name;
                // $city_nm=$eachVal->city_id;
                $lawyer_list[]=[
                                'name'  =>$eachVal->name,
                                'eml'      =>$eachVal->email,
                                'phone'   =>$eachVal->personal_mobile,
                                'profile_pic'      =>'<img src="'.asset('/public/'.$eachVal->lawyer_dtl['profile_pic']).'" >',
                                'NNA_Certified'      =>$eachVal->lawyer_dtl['NNA_Certified'],
                                'notary_exp' =>$eachVal->lawyer_dtl['notary_exp'],
                                'priority' =>$eachVal->lawyer_dtl['priority'],
                                'action'           =>'<a class="deptedit" href="javascript:void(0);"title="View"   style="font-size:20px;"  data-toggle="modal" data-target="#myModal" onclick="get_dtl(\''.$id.'\')"  ><i class="typcn typcn-edit view_icon"></i></a><a class="delete" href="javascript:void(0);"title="View"   style="font-size:20px; color:red;" onclick="remove_lawyer(\''.$id.'\')"  ><i class="typcn typcn-trash view_icon"></i></a>',
                            ];
            }
        }

         return Datatables::of($lawyer_list)->escapeColumns([])->make(true);
    }

    public function custom_email($from,$to,$subject,$message) {
        try { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
       // $headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
        //Multiple BCC, same as CC above;
       // $headers .= 'Bcc: shaheen.nusrat2015@gmail.com' . "\r\n";
            // Sending email
            if(mail($to, $subject, $message, $headers)){
                return true;
            } else{
                return false;
            }
        }
        catch(Exception $e) {
              echo 'Message: ' .$e->getMessage();
        }
    }

    public function submit_lawyer(Request $request)
    {
        $eml=$request->eml;
       //echo $request->f_nm;
        $rand=rand(000000,999999);
        $data_arr=array();
        $user_arr=array();
        $get_lawyer= User::where(['email'=>$eml,'user_type'=>'Lawyer'])->count();
        $lawyer_id=$request->hidden_id;

        $user_arr['user_type']='Lawyer';
        $user_arr['name']=$request->f_nm.' '.$request->l_nm;
        $user_arr['email']=$eml;
        $user_arr['personal_mobile']=$request->phone;

        $data_arr['state_id']=$request->state_id;
        $data_arr['city_id']=$request->city_id;
        $data_arr['NNA_Certified']=$request->NNA_Certified;
        $data_arr['notary_exp']=$request->notary_exp;
       
        $data_arr['priority']=$request->priority;
       
        if($request->upload_img!=''){
                $fileName = time().'.'.$request->upload_img->extension();  
                $request->upload_img->move(public_path('upload/profile/'), $fileName);
                $data_arr['profile_pic']='upload/profile/'.$fileName;
        }else{
           $data_arr['profile_pic']=$request->old_img;
        }

        if($lawyer_id==''){
            if($get_lawyer==0){
                $data_arr['posted_by']=Auth::user()->id;
                $data_arr['status']='Free';
                $user_data=array('email'=>$eml,'password'=>$rand);
                // Mail::send(['html'=>'emails.lawyer-password'],  ['user' => $user_data], function($message) use ($user_data) {
                //    $message->to($user_data['email'])->subject('Your Password of aimnotary.com.');
                //    $message->from('no-reply@aimnotary.com','AimNotary');
                // });

                $html = View('emails.lawyer-password', ['user'=> $user_data]);
                $mail_status=$this->custom_email($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Your Password of aimnotary.com.", $message = $html);
                
                // if (Mail::failures()) {
                //     echo json_encode(['status' => 201, 'message' => 'Mail Error!']);
                // }

                $user_arr['password']=Hash::make($rand);
                $user_arr['add1']='NA';
                $user_arr['add2']='NA';
                $user_arr['country']='NA';
                $user_arr['city']='NA';
                $user_arr['zip_code']='NA';

                $user_arr['is_privacy']='0';
                $user_arr['is_terms']='0';
                $user_arr['email_verified_at']=NOW();
                $user_arr['created_at']=NOW();
                $data_arr['created_at']=NOW();

                $user_id=User::insertGetId($user_arr);
                if($user_id){
                     $data_arr['user_id']=$user_id;
                    tbl_lawyer::insert($data_arr);
                }
                echo json_encode(['status' => 200, 'message' => 'Data added sucessfully!']);    
            }else{
                echo json_encode(['status' => 201, 'message' => 'Data Already Exists!']);
            }
        }else{
           
            $user_arr['updated_at']=NOW();
            User::where('id',$lawyer_id)->update($user_arr);
            $data_arr['updated_at']=NOW();
            tbl_lawyer::where('user_id',$lawyer_id)->update($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data update sucessfully!']);
        }

    }


    public function edit_lawyer(Request $request)
    {
        $id = $request->id;
        //$get_lawyer= tbl_lawyer::where('id',$id)->first();

        $get_lawyer = DB::table('users')
                        ->select('users.name','users.email AS eml','users.personal_mobile AS phone','tbl_lawyers.*')
                        ->leftJoin('tbl_lawyers', 'users.id', '=', 'tbl_lawyers.user_id')
                        ->leftJoin('tbl_states', 'tbl_lawyers.state_id', '=', 'tbl_states.id')
                        ->where('users.id', '=', $id)
                        ->where('users.user_type', '=', 'Lawyer')
                        ->first();
        echo json_encode($get_lawyer);
    }
    public function remove_lawyer(Request $request)
    {
         $get_data   = User::select('*')->where('id',$request->id)->get();
         if(!$get_data->isEmpty()){
            $data_arr['deleted_at']=NOW();
            User::where('id',$request->id)->update($data_arr);
            tbl_lawyer::where('user_id',$request->id)->update($data_arr);
            echo json_encode(['status' => 200, 'message' => 'Data deleted sucessfully!']);
        }else{
            echo json_encode(['status' => 404, 'message' => 'Data Doesn\'t exits!']);
        }
    }
   



}
