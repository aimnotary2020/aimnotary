<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\tbl_company_info;
use App\Models\tbl_content_mngt_system;
use App\Models\tbl_blog;
use Validator;
use Auth;
use Hashids;
use App\User;
use App\Session;
//use Session;
use App\Models\tbl_login_log;
use App\Models\tbl_subscription;
use App\Models\tbl_coupon;
use App\Models\tbl_subscription_detail;
use App\Models\tbl_state;
use App\Models\tbl_citie;
use App\Models\tbl_lawyer;
use App\Models\tbl_user_document;
use Cookie;
//use Mail;1085
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Imagick;
require(base_path()."/pdfparser/vendor/autoload.php");
require(base_path()."/fpdf.php");

class FrontController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public $data=array();
    protected $company_info=array();
    public function __construct()
    {
        $this->company_info=tbl_company_info::select('*')->where([['id','=','1']])->first();
        $url=$_SERVER['REQUEST_URI'];
        $resulr = DB::table('tbl_meta_data')->where(['link'=>$url])->get()->first();
        $this->data['meta_details'] = $resulr;
    }
    
    public function index()
    {
        //print_r($_COOKIE); exit;

        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            //print_r($cookie); exit;
             
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'Home';
        $this->data['company_info'] = $this->company_info;
        $this->data['cms_list'] = tbl_content_mngt_system::select('*')->get();
        $this->data['lawyer_list'] = User::with('lawyer_dtl')->where(['user_type'=>'Lawyer'])->orderBy('id','DESC')->get();
        return view('front.index')->with($this->data);
    }
    
    public function check_cookie(Request $request) {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        echo $_COOKIE['ipauserv'];
    }
    
    public function get_chat_list(Request $request) {
        $visit_id = $request->visit_id;
        
        $url = 'https://argentumconsultinggroup.com/firebase/chat_list.php';
        $data_string = '{"visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        $result = json_decode($resul);
        if($result->job_done == 'SUCCESS') {
            $html = '';
            $data_arr = $result->chat;
            foreach ($data_arr as $val) {
                //echo $val->visit_id;
                if($val->sender_id == $visit_id && $val->send_by == 'U'){
        			$inptcls = "usrchat";
        		}else{
        			$inptcls = "cnschat";
        		}
        		/*if($val->sender_id != $visit_id){
        		    $html .= '<div style="clear:both; width:100%; height: 10px;"></div>';
        		}*/
        		$data = $val->chat_text;
        		$html .= '<div class="'.$inptcls.'">'.$data.'</div>';
            }
            if ($result->reply_done == 'none') {
    		    $html .= '<div class="connecting_image"><img src="https://aimnotary.com/public/front_assets/images/conneting_arg.gif"/></div>';
    		}
            echo $html;
        }
    }
    
    public function open_chat(Request $request) {
        $visit_id = $request->visit_id;
     
        $url = 'https://argentumconsultinggroup.com/firebase/chat_open.php';
        $data_string = '{"visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        echo "done";
    }
 	 
    public function close_chat(Request $request) {
        $visit_id = $request->visit_id;
     
        $url = 'https://argentumconsultinggroup.com/firebase/chat_close.php';
        $data_string = '{"visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        echo "done";
    }
    
    public function notification_timer(Request $request) {
        $visit_id = $request->visit_id;
        
        $url = 'https://argentumconsultinggroup.com/firebase/notify_repeat.php';
        $data_string = '{"visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        $result = json_decode($resul);
        echo $result->job_done;
    }
    
    public function user_reply(Request $request) {
        $visit_id = $request->visit_id;
     
        $url = 'https://argentumconsultinggroup.com/firebase/user_reply.php';
        $data_string = '{"visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        $result = json_decode($resul);
        echo $result->job_done;
    }
    
    public function user_reply1(Request $request) {
        $visit_id = $request->visit_id;
     
        $url = 'https://argentumconsultinggroup.com/firebase/user_reply1.php';
        $data_string = '{"visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        $result = json_decode($resul);
        echo $result->job_done;
    }
    
    public function send_chat(Request $request) {
        $visit_id = $request->visit_id;
        $chat_text = $request->send_chat;
        
        $url = 'https://argentumconsultinggroup.com/firebase/send_chat.php';
        $data_string = '{"chat_text":"'.$chat_text.'","visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        $result = json_decode($resul);
        if($result->job_done == 'SUCCESS') {
            $html = '';
            $data_arr = $result->chat;
            foreach ($data_arr as $val) {
                //echo $val->visit_id;
                if($val->sender_id == $visit_id && $val->send_by == 'U'){
        			$inptcls = "usrchat";
        		}else{
        			$inptcls = "cnschat";
        		}
        		/*if($val->sender_id != $visit_id){
        		    $html .= '<div style="clear:both; width:100%; height: 10px;"></div>';
        		}*/
        		$data = $val->chat_text;
        		$html .= '<div class="'.$inptcls.'">'.$data.'</div>';
            }
            if ($result->reply_done == 'none') {
    		    $html .= '<div class="connecting_image"><img src="https://aimnotary.com/public/front_assets/images/conneting_arg.gif"/></div>';
    		}
    		if ($result->email_apply == 1) {
    		    $contents = $result->mail_content;
    		    $sub='AIM NOTARY OFFLINE ENQUIRY';
                $replyto="noreply@aimnotary.com";
                //print_r($contents); exit;	 
                $to_mail = $result->email_list;
                $to_mail_arr = explode(",",$to_mail);
                foreach ($to_mail_arr as $to_email_id) {
                    $mail_status=$this->email_function($from = $replyto, $to =$to_email_id, $subject = $sub, $message = nl2br($contents));
                    // $send=$this->common_mail_fuction($to_mail, $replyto,$sub,nl2br($contents));
                }
    		}
            echo $html;
        }
    }
    
    public function send_offline_chat(Request $request) {
        $visit_id = $request->visit_id;
        $name = $request->name;
        $email = $request->email;
        $contact_no = $request->contact_no;
        $message = $request->message;
        $org_offline = "0";
        
        //$contents = "Name : ".$name."\nEmail : ".$email."\nContact No. : ".$contact_no."\nMessage : ".$message;
        $contents = "Contact No. : ".$contact_no;
        
        $url = 'https://argentumconsultinggroup.com/firebase/send_offline_chat.php';
        $data_string = '{"name":"'.$name.'","email":"'.$email.'","contact_no":"'.$contact_no.'","message":"'.$message.'","org_offline":"'.$org_offline.'","visit_id":"'.$visit_id.'"}';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
        $resul = curl_exec($ch );
        $err = curl_error($ch);
        curl_close( $ch );
        
        $result = json_decode($resul);
        if($result->job_done == 'SUCCESS') {
            
            $sub='AIM NOTARY OFFLINE ENQUIRY';
            $replyto=$email;
            //print_r($contents); exit;	 
            $to_mail = $result->email_list;
            $to_mail_arr = explode(",",$to_mail);
            foreach ($to_mail_arr as $to_email_id) {
                $mail_status=$this->email_function($from = $replyto, $to =$to_email_id, $subject = $sub, $message = nl2br($contents));
                // $send=$this->common_mail_fuction($to_mail, $replyto,$sub,nl2br($contents));
            }
            
            // if($send){
            //     echo 'Mail Send Successfully';
            // }
            
            echo $result->job_done;
        }
    }

    function smtp_auth_email($to="info@aimnotary.com",$subject,$body){
        include(base_path()."/phpmailer/vendor/autoload.php");
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'no-reply@aimnotary.com';                 // SMTP username
        $mail->Password = 'pn8;A8_!f<]z~Bm$';                           // SMTP password
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 465;                              // TCP port to connect to
        
        $mail->setFrom('no-reply@aimnotary.com', 'Aimnotary');
        $mail->addAddress($to, 'Aimnotary');  
        
        $mail->isHTML(true);                                  // Set email format to HTML
        
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->send();
    }
   
    /*cms start */
    public function about_us()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'About Us';
        $this->data['company_info'] = $this->company_info;
        $this->data['about_us'] = tbl_content_mngt_system::select('*')->where(['category'=>'1','sub_category'=>'16'])->get();
        return view('front.about')->with($this->data);
    }
    public function contact()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'contact';
        $this->data['company_info'] = $this->company_info;
        return view('front.contact')->with($this->data);
    }
     
    
    public function privacy_policy()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'FAQ';
        $this->data['company_info'] = $this->company_info;
        $this->data['detail_list'] = tbl_content_mngt_system::select('*')->where(['category'=>'10'])->get();
        return view('front.privacy-policy')->with($this->data);
    }
    public function terms_and_conditions()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'Terms and conditions';
        $this->data['company_info'] = $this->company_info;
        $this->data['detail_list'] = tbl_content_mngt_system::select('*')->where(['category'=>'11'])->get();
        return view('front.terms-and-conditions')->with($this->data);
    }
    public function faq()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'FAQ';
        $this->data['company_info'] = $this->company_info;
        $this->data['detail_list'] = tbl_content_mngt_system::select('*')->where(['category'=>'12'])->get();
        return view('front.faq')->with($this->data);
    }
     /*cms end */
    public function how_it_works()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'How it work';
        $this->data['company_info'] = $this->company_info;
        return view('front.how-it-works')->with($this->data);
    }
    public function business_solution()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'Solutions For Business and teams';
        $this->data['company_info'] = $this->company_info;
        return view('front.solutions-for-business-and-teams')->with($this->data);
    }
    public function solutions_for_individual()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'Solutions For individual';
        $this->data['company_info'] = $this->company_info;
        return view('front.solutions-for-individual')->with($this->data);
    }
    public function blog()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'blog';
        $this->data['company_info'] = $this->company_info;
        $this->data['blog_list']=tbl_blog::with('get_user_dtl')->where('is_case_study','N')->orderBy('id','DESC')->get();
        return view('front.blog')->with($this->data);
    }
    public function blog_detail(Request $request, $url='')
    {   
        $this->data['page_title'] = 'blog detail';
        $this->data['company_info'] = $this->company_info;
        $this->data['blog_detail']=tbl_blog::with('get_user_dtl')->where(['is_case_study'=>'N','generate_url'=>$url])->first();
        
        //$this->data['meta_details']=tbl_blog::with('get_user_dtl')->where(['is_case_study'=>'N','generate_url'=>$url])->first();
        
        // echo $this->data['blog_detail']['generate_url'];
        // exit;
        
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            $err = curl_error($ch);
            curl_close( $ch );
            
            $cookie_val = json_decode($resul);
            // echo $cookie_val->status;
            // exit;
            
            if($cookie_val->status == 'new') {
                setcookie('ipauserv', $cookie_val->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie_val->cookie_value, time() + 1800, '/');
            } else {
                setcookie('ipauserv', $cookie_val->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie_val->cookie_value, time() + 1800, '/');
            }
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        //echo $url;
        
        // $this->data['blog_detail']=tbl_blog::with('get_user_dtl')->where('is_case_study','N')->where('generate_url',$url)->get();
     
        // print_r($this->data);
        // echo "Blog Details Controller";
        
        // exit;
            
        return view('front.blog-detail')->with($this->data);
    }
    
    public function pricing()
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'pricing';
        $this->data['company_info'] = $this->company_info;
        return view('front.pricing')->with($this->data);
    }

    public function basic_email() {

      $data = array('name'=>"Nusrat Shaheen");
      $user_data=array('name'=>'nUR',
                           'email'=>'Nusrat@SSS.CC'
                         );
      Mail::send(['html'=>'emails.admin_subs_mail'],  ['user' => $user_data], function($message) use ($user_data) {
                 $message->to('nursrat.econstra@gmail.com')->subject('New User subscribed  to AimNotary.');
                 $message->cc('nurshoyaba@gmail.com');
                 $message->bcc('rohitmajumder1983@gmail.com');
                 $message->bcc('amitdasgupta1980@gmail.com');
                 $message->from('no-reply@aimnotary.com','AimNotary');
              });
      // echo "Basic Email Sent. Check your inbox.";
   }

  
   public function recover_password()
   {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
       
      $this->data['page_title'] = 'recover password';
      $this->data['company_info'] = $this->company_info;
      return view('front.recover-password')->with($this->data);
   }

    public function email($from,$to,$subject,$message) {
        try { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
             
            // Create email headers
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
       // $headers .= 'Cc: nurshoyaba@gmail.com' . "\r\n";
        //Multiple BCC, same as CC above;
       // $headers .= 'Bcc: shaheen.nusrat2015@gmail.com' . "\r\n";
            // Sending email
            if(mail($to, $subject, $message, $headers)){
                return true;
            } else{
                return false;
            }
        }
        catch(Exception $e) {
              echo 'Message: ' .$e->getMessage();
        }
    }
   public function contact_us(Request $request)
   {
       
       $data=[
                'uname' => $request->f_name,
                'email'   =>$request->user_eml,
                'phone' => $request->user_contact_no,
                'msg' => $request->message,
                'status' => '0',
                'date'  => date("Y-m-d"),
                'subj' => $request->msg_subject,
                'type' =>'contact',
                'updated_at' => date("Y-m-d")
            ];
            DB::table("web_contacts")->insert($data);
       
   
       $user=array('f_name'         => $request->f_name,
                   'user_eml'       => $request->user_eml,
                   'user_contact_no'=> $request->user_contact_no,
                   
                   'message'        => $request->message
            );
       if(isset($request->business_type))
       {
         $user['business_type']=$request->business_type;
       }
       else
       {
          $user['msg_subject']=$request->msg_subject;
       }
        
      // $subject=ucwords($request->f_name).' wants to contact us.';
      // $email=Mail::send(['html'=>'emails.contact-us'],  ['user' => $user], function($message) use ($user) {
      //    $message->to('toddl@ez8a.com', 'AimNotary')->subject
      //       ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us' );
      //    $message->from('no-reply@aimnotary.com','AimNotary');
      // });

       $html = View('emails.contact-us', ['user'=> $user]);
       //$mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to ='toddl@ez8a.com', $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
       //$mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to ='esther@aimnotary.com', $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
       //$mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to ='santanu.econstra@gmail.com', $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
       
       $mail_status=$this->smtp_auth_email("esther@aimnotary.com", $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
       $mail_status=$this->smtp_auth_email("toddl@ez8a.com", $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
       $mail_status=$this->smtp_auth_email("markr@ez8a.com", $subject = ((isset($user['msg_subject']))?$user['msg_subject']: ucwords($user['f_name']).' wants to contact us'), $message = $html);
        
        echo json_encode(['status' => 200, 'message' => 'Thanks for contacting us.We will contact you ASAP!!.']);
      // }else{
      //      echo json_encode(['status' => 201, 'message' => 'Something went wrong please Try again!']);
      // }
   }
   public function subscribe(Request $request)
   {
       $data=array('user_email'       => $request->user_email);

       $check_email=tbl_subscription::where('user_email',$request->user_email)->count();
       if($check_email==0){
             $this->data['created_at']=NOW();
            tbl_subscription::insert($data);
            // $email=Mail::send(['html'=>'emails.subscribe'],  ['user' => $data], function($message) use ($data) {
            //    $message->to($this->data['user_email'])->subject('Thanks For subscribing aimntary.com');
            //    $message->from('no-reply@aimnotary.com','AimNotary');
            // });

            $html = View('emails.subscribe', ['user'=> $data]);
            $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to =$this->data['user_email'], $subject = 'Thanks For subscribing aimnotary.com', $message = $html);
            $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = "esther@aimnotary.com", $subject = 'Thanks For subscribing aimnotary.com', $message = $html);
            
            echo json_encode(['status' => 200, 'message' => 'Thank you for subscribing!']);
       }else{
            echo json_encode(['status' => 201, 'message' => 'You have already subscribed!']);
       }
   }
   //user login & sign up
   public function login()
   {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
       
      $this->data['page_title'] = 'login';
      $this->data['company_info'] = $this->company_info;
      if(Auth::user()!='' && Auth::user()->user_type=='User'){
         return redirect('dashboard');
      }else{
        return view('front.login')->with($this->data);
      }
    }

   public function register()
   {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
       
      $this->data['page_title'] = 'register';
      $this->data['company_info'] = $this->company_info;
      return view('front.sign-up')->with($this->data);
   }
   //check user credentails
    public function check_credentials(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $remember = $request->remember == 1 ? true : false;
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        ]);

        /*
        * ajax based login
        */
        $user = User::where('email',$username)->first();
        if(empty($user)){
            echo json_encode(['status' => 404]);
        }else{
            $userdata = array(
                'email'    => $username,
                'password'  => $password
            );
            if (Auth::attempt($userdata,$remember)) 
            {
                if($user->email_verified_at!=''){
                    // cookies set value for remember function
                    $minutes = 720;
                    if($remember == 1)
                    {
                        Cookie::queue(Cookie::make('u_name', $username, 720));
                        Cookie::queue(Cookie::make('u_password', $password, 720));
                    }
                    else
                    {
                        Cookie::queue(Cookie::forget('u_name'));
                        Cookie::queue(Cookie::forget('u_password'));
                    }
                    //check for session//
                    //$is_session_exist = Session::where('user_id',$user->id)->get();
                    
                    //Session::where('user_id',$user->id)->forceDelete();
                    
                    if($user->user_type == 'User'){
                        $access_token = $user->createToken('SSIL')->accessToken; //create token
                        Session::put('user', Auth::user());
                        
                        $request->session()->put('auth_token',$access_token); // store token
                        //check for login logs
                        $is_exist_log = tbl_login_log::where('user_id',$user->id)->get();
                        
                        $log_array = [
                            'user_id'      => $user->id,
                            'logged_in_ip' => request()->ip(),
                            'created_at'   => NOW(),
                        ];
                        tbl_login_log::insert($log_array);
                        $is_exist_count = tbl_login_log::where('user_id',$user->id)->count();
                        if(!$is_exist_log->isEmpty() && $is_exist_count > 2){
                            //delete rows except last 2
                            tbl_login_log::where('user_id',$user->id)->take(1)->forceDelete();
                        }
                        echo json_encode(['status' => 200]);
                    }elseif($user->user_type=='Admin' || $user->user_type=='Lawyer'){
                        $access_token = $user->createToken('SSIL')->accessToken; //create token
                        Session::put('user', Auth::user());
                        $request->session()->put('auth_token',$access_token); // store token
                        //check for login logs
                        $is_exist_log = tbl_login_log::where('user_id',$user->id)->get();
                        
                        $log_array = [
                            'user_id'      => $user->id,
                            'logged_in_ip' => request()->ip(),
                            'created_at'   => NOW(),
                        ];
                        tbl_login_log::insert($log_array);
                        $is_exist_count = tbl_login_log::where('user_id',$user->id)->count();
                        if(!$is_exist_log->isEmpty() && $is_exist_count > 2){
                            //delete rows except last 2
                            tbl_login_log::where('user_id',$user->id)->take(1)->forceDelete();
                        }
                        echo json_encode(['status' => 200]);
                    }
                    else{
                         echo json_encode(['status' => 403]);
                    }
                }else{
                  $user_data=array('email'=>$user->email,'name'=>$user->name);
                    // Mail::send(['html'=>'emails.verification-mail'],  ['user' => $user_data], function($message) use ($user_data) {
                    //    $message->to($user_data['email'])->subject('Please verify your email address .');
                    //    $message->from('no-reply@aimnotary.com','AimNotary');
                    // });

                   $html = View('emails.verification-mail', ['user'=> $user_data]);
                   $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Please verify your email address .', $message = $html);
                   
                   echo json_encode(['status' => 201]);
                }
            }else{
               echo json_encode(['status' => 403]);
            }
        }
    }

    public function checkEmailAjax()
    {
        if($_GET['u_email']!=""){
            $check_email=user::where('email',$_GET['u_email'])->count();
            if($check_email==0)
            {
                echo "true";
            }
            else
            {
                echo "false";
            }
        }else{
            echo "true";
        }
        
    }

    public function checkPasswordAjax()
    {
        if($_GET['old_pass']!=""){
            $checkPassword=user::where('password',Hash::make($_GET['old_pass']))->where('id','=',Auth::user()->id)->count();
            if($checkPassword==0){
                echo "false";
            }else{
                echo "true";
            }
        }else{
            echo "true";
        }
        
    }

    

    public function checkMobileAjax()
    {
        $check_email=user::where('personal_mobile',$_GET['u_email'])->count();
        if($check_email==0)
       	{
            echo "true";
        }
        else
        {
            echo "false";
        }
    }


    //user registration
    public function user_registration(Request $request)
    {
        $name=$request->f_name.' '.$request->l_name;
        $email=$request->u_email;
        $personal_mobile=$request->personal_mobile;
        $password=$request->u_password;
        $is_privacy=$request->privacy == 'on' ? '1' : '0';
        $is_terms=$request->terms== 'on' ? '1' : '0';
        $user_data=array();
        $check_email=user::where('email',$email)->count();
       	if($check_email==0)
       	{
            $user_data=array('name'=>$name,
                             'email'=>$email,
                             'personal_mobile'=>$personal_mobile,
                             'password'=>Hash::make($password),
                             'is_privacy'=>$is_privacy,
                             'is_terms'=>$is_terms,
                             'created_at'=>NOW()
                           );

            
                           
            if($request->doc_no==5){
                if($request->card_number!=NULL || $request->card_number!=''){
                    $expcard=explode('/',$request->card_expiry);
                    $cc_expiry=$expcard[0].$expcard[1];
                    $result=$this->payment_set(['cn'=>$request->card_number,'holder_name'=>$request->card_holder,'ex'=>$cc_expiry,'amount'=>99]);
                    
                    if(isset($result->bank_resp_code) && $result->bank_resp_code==100){
                       
                        $user_id=User::insertGetId($user_data);
                        $userDSocData=[
                            'user_id' => $user_id,
                            'doc'     => $request->doc_no,
                            'status'    => 'A',
                            'amount'    => 99,
                            'date'      => date("Y-m-d"),
                            'time'      => date("Y-m-d H:i:s"),
                        ];
                        $data=DB::table("tbl_user_doc_counts")->insert($userDSocData);
                        $update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW());
                        User::where('email',$email)->update($update_arr);
                        echo json_encode(['status' => 200, 'mode'=>'redirect','message' => 'Thank you for registration!']);
                    }
                }else{
                    echo json_encode(['status' => 201, 'message' => 'Something Went Wrong']);
                }
            }else{
                $user_id=User::insertGetId($user_data);
                $userDSocData=[
                    'user_id' => $user_id,
                    'doc'     => $request->doc_no,
                    'status'    => 'A',
                    'date'      => date("Y-m-d"),
                    'time'      => date("Y-m-d H:i:s"),
                    'is_usa'    => $request->is_usa,
                ];
                $data=DB::table("tbl_user_doc_counts")->insert($userDSocData);
                $html = View('emails.verification-mail', ['user'=> $user_data]);
                $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Thanks For registering to aimnotary.com. Please verify your email address.', $message = $html);
                echo json_encode(['status' => 200, 'message' => 'We have sent you a Verification Email']);
            }
            
            
        }
        else
        {
           echo json_encode(['status' => 201, 'message' => 'You have already registerd!']);
        }
    }

    public function verify_email(Request $request, $eml='' )
    {
       
          $eml=explode('~', base64_decode($eml));
          if (isset($eml[0]) && isset($eml[1])) {
            $email=$eml[0];
              $date1 = date_create($eml[1]);
              $date2 = date_create(date('Y-m-d'));

              //difference between two dates
              $diff = date_diff($date1,$date2);

              //count days
              $day=$diff->format("%a");
             
               $check_email=user::where('email',$eml[0])->count();
             if($check_email>0 && $day<=2 ){
                  $update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW());
                   User::where('email',$email)->update($update_arr);
                echo "Email address verified successfully!.";

                return redirect('login');
             }else{
                echo "Invalid credentails.";
             }
          }else{
             echo "Invalid credentails.";
          }
    }
    public function rescover_password(Request $request)
    {
          $email=$request->user_eml;
          $check_email=user::where('email',$email)->count();
          if($check_email>0){
              $rand=rand(000000,999999);
              
              $update_arr=array('password'=>Hash::make($rand),'updated_at'=>NOW());
              
               User::where('email',$email)->update($update_arr);
               $user_data=array('email'=>$email,
                                'password'=>$rand);
            //   Mail::send(['html'=>'emails.reset-password'],  ['user' => $user_data], function($message) use ($user_data) {
            //    $message->to($user_data['email'])->subject('Your Recovered Password of aimnotary.com.');
            //    $message->from('no-reply@aimnotary.com','AimNotary');
            // });
               $html = View('emails.reset-password', ['user'=> $user_data]);
               $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to =$user_data['email'], $subject = 'Please verify your email address .', $message = $html);
               echo json_encode(['status' => 200, 'msg' => 'Your Recovered Password of aimnotary.com.']);
          }else{
            echo json_encode(['status' => 201, 'msg' => 'You have already registerd!']);
          }
    }
    public function payment(Request $request, $price='')
    {
        //if(!isset($_COOKIE['ipauser'])) {
            $url = 'https://argentumconsultinggroup.com/firebase/cookie_setup.php';
            $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 
                            "https" : "http") . "://" . $_SERVER['HTTP_HOST']; 
            if ( !empty($_SERVER['HTTP_CLIENT_IP']) ) {
                // Check IP from internet.
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
                // Check IP is passed from proxy.
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                // Get IP address from remote address.
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            // echo $link."<br>";
            // echo $ip."<br>";
            $data_string = '{"visit_domain":"'.$link.'", "ip_val":"'.$ip.'"}';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json') );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $data_string );
            $resul = curl_exec($ch );
            // echo print_r(curl_exec($ch))."<br>";
            // echo print_r(curl_getinfo($ch))."<br>";
            $err = curl_error($ch);
            // echo "10 error : ".$err;
            curl_close( $ch );
            // exit;
            
            $cookie = json_decode($resul);
            
            
            //print_r($cookie->visit_id);
            
            if($cookie->status == 'new') {
               
                //$_SESSION['visit_id'] = $cookie->visit_id;
                //Session::put('visit_id', $cookie->visit_id);
                setcookie('ipauserv', $cookie->visit_id, time() + 1800, '/');
                setcookie('ipauser', $cookie->cookie_value, time() + 1800, '/');
                
                // request()->cookie('ipauser', 'item_value', 1800);
                
                //$_COOKIE['ipauserv'] = $cookie->visit_id;
                //$_COOKIE['ipauser'] = $cookie->cookie_value;
            }
            // echo "session visit id ".$_SESSION['visit_id'];
            // exit;
        // } else {
        //     if(isset($_COOKIE["ipauserv"])) {
        //         Session::put('visit_id', $_COOKIE["ipauserv"]);
        //     }
        //}
        
        $this->data['page_title'] = 'Payment';
        $this->data['company_info'] = $this->company_info;
        return view('front.user.payment')->with($this->data);
    }

    public function enterprise_form()
    {
        $data=[];
        return view('front.user.enterpriseform')->with($this->data);
    }

    public function getCheckout(Request $request)
    {
        $price=$request->price;
        $documentType=($price==25) ? "One Document" : "Five Documents";
        $html='<table class="table">
                <thead>
                    <tr>
                        <th class="text-left">PRODUCT</th>
                        <th class="text-right">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-left">'.$documentType.'</td>
                        <td class="text-right" id="product_amt">$'.$price.'</td>
                    </tr>
                    <tr id="discount_li" style="display: none;">
                        <td class="text-left">Discount Amount</td>
                        <td class="text-right" id="discount_amount"></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th class="text-left">Order Total</th>
                        <th class="text-right" id="order_grand_total">$'.$price.'</th>
                    </tr>
                </tfoot>
            </table>';
            echo $html;
    }

    public function email_function($from,$to,$subject,$message) {
        try { 
            // To send HTML mail, the Content-type header must be set
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: '.$from."\r\n".
                        'Reply-To: '.$from."\r\n" .
                        'X-Priority: 1 (Highest)'."\r\n".
                        'X-MSMail-Priority: High'."\r\n".
                        'X-Mailer: PHP/' . phpversion();
             
            $headers .= 'Cc: info@aimnotary.com' . "\r\n";
            $headers .= 'Bcc: rohitmajumder1983@gmail.com,developers@ez8a.com' . "\r\n";
            // $headers .= 'Cc: rohitmajumder1983@gmail.com,amitdasgupta1980@gmail.com' . "\r\n";
            if(mail($to, $subject, $message, $headers)){
                return true;
            } else{
                return false;
            }
        }
        catch(Exception $e) {
              echo 'Message: ' .$e->getMessage();
        }
    }
    public function paid_subscription(Request $request)
    {

        $expcard=explode('/',$request->card_expiry);
        $cc_expiry=$expcard[0].$expcard[1];
        $amount=$request->total_cost;
        // $amount='0.01';
        $cardholder_name=$request->card_holder;
        $cc_number=$request->card_number;
        $name=$request->f_nm.' '.$request->l_nm;
        $email=$request->eml;
        $user_data=array('name'=>$name,
                           'email'=>$email,
                           'personal_mobile'=>$request->phone,
                           'password'=>Hash::make($request->password),
                           'add1'=>$request->add1,
                           'add2'=>($request->add2!=''?$request->add2:'NA'),
                           'country'=>$request->country,
                           'city'=>$request->city,
                           'zip_code'=>$request->zip,
                           'is_privacy'=>'0',
                           'is_terms'=>$request->is_terms,
                           'email_verified_at'=>NOW(),
                           'created_at'=>NOW()
                         );
       
          $check_email=user::where('email',$email)->count();
          if($check_email==0){
               $user_data['created_at']=NOW();
                $add_user=User::insertGetId($user_data);
                $user_id=$add_user;
          }else{
                $user_data['updated_at']=NOW();
                $user = User::where('email',$email)->first();
                $user_id=$user->id;
                User::where('id',$user_id)->update($user_data);
          }
          $check_subs= tbl_subscription_detail::where('user_id',$user_id)->count();
          if($check_subs==0){
                    
                    // https://demo.globalgatewaye4.firstdata.com/?lang=en
                    // un: test111 ,pwd:@#$123Nur
                   // $url='https://api.demo.globalgatewaye4.firstdata.com/transaction/v11'; //demo
                    // $gateway_id='MD6401-51'; $password='EHjdMqG1sz8oiNvp5GOFg9ZfiKgiz0yc'; //demo

                    $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
                    // $gateway_id='K84040-06'; 
                    // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

                    $gateway_id='Q66919-01'; 
                    $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
                    $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary");
                    

                    // $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //true
                    // $data = array("gateway_id" => "K84040-06", "password" => "HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq", "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); //true
                    $data_string= json_encode($data);
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8;','Accept: application/json' ));
                    $result = curl_exec($ch);
                    if($result){
                        $newres=json_decode($result);
                        if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100){
                               
                               
                               $subs_arr=array();
                               $subs_arr['card_number']=$cc_number; 
                               $subs_arr['card_expiry']=$request->card_expiry;
                               $subs_arr['card_holder']=$cardholder_name;
                               $subs_arr['product_price']=$request->product_price;
                               $subs_arr['total_cost']=$amount; 
                               $subs_arr['pay_id']=$newres->authorization_num;
                               $subs_arr['card_type']=$newres->credit_card_type;
                               $subs_arr['subs_date']=NOW();
                               $subs_arr['coupon_id']=$request->coupon_id;
                               $subs_arr['user_id']=$user_id;
                               $subs_arr['created_at']=NOW();
                               $subs_arr['trans_json']=$result;
                               $subs_arr['no_of_signers']=$request->no_of_signers;
                               $subs_arr['no_of_stamps']=$request->no_of_stamps;

                              $subs= tbl_subscription_detail::insert($subs_arr);
                              if($subs){
                                 if($request->coupon_id!=''){

                                      tbl_coupon::where('coupon_id',$request->coupon_id)->update(array('status'=>'0','updated_at'=>NOW()));
                                 }

                                 //generate session
                                  // $user = User::where('email',$email)->first();
                                  // $access_token = $user->createToken('SSIL')->accessToken; //create token
                                  // $request->session()->put('auth_token',$access_token); // store token
                                  // //check for login logs
                                  // $is_exist_log = tbl_login_log::where('user_id',$user->id)->get();

                                  // $log_array = [
                                  //   'user_id'      => $user->id,
                                  //   'logged_in_ip' => request()->ip(),
                                  //   'created_at'   => NOW(),
                                  // ];
                                  // tbl_login_log::insert($log_array);
                                  // $is_exist_count = tbl_login_log::where('user_id',$user->id)->count();
                                  // if(!$is_exist_log->isEmpty() && $is_exist_count > 2){
                                  //   //delete rows except last 2
                                  //   tbl_login_log::where('user_id',$user->id)->take(1)->forceDelete();
                                  // }
                              }

                              
                              // Mail::send(['html'=>'emails.paid-subscription'],  ['user' => $user_data], function($message) use ($user_data) {
                              //    $message->to($user_data['email'])->subject('Thanks For subscribing to AimNotary.');
                              //    $message->from('no-reply@aimnotary.com','AimNotary');
                              // });

                              $html = View('emails.paid-subscription', ['user'=> $user_data]);
                              $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = $user_data['email'], $subject = "Thanks For subscribing to AimNotary.", $message = $html);

                              $html1 = View('emails.admin_subs_mail', ['user'=> $user_data]);
                              $mail_status1=$this->email_function($from = "no-reply@aimnotary.com", $to = "toddl@ez8a.com", $subject = "New User subscribed  to AimNotary", $message = $html1);

                              // Mail::send(['html'=>'emails.admin_subs_mail'],  ['user' => $user_data], function($message) use ($user_data) {
                              //    $message->to('toddl@ez8a.com')->subject('New User subscribed  to AimNotary.');
                              //    $message->cc('markr@ez8a.com');
                              //    $message->bcc('rohitmajumder1983@gmail.com');
                              //    $message->bcc('amitdasgupta1980@gmail.com');
                              //    $message->from('no-reply@aimnotary.com','AimNotary');
                              // });

                               echo json_encode(['status' => 200, 'msg' =>'Thanks For subscribing to AimNotary!']);
                        }else{

                          echo json_encode(['status' => 201, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
                        }
                    }else{
                        echo json_encode(['status' => 201, 'msg' =>'Something Went Wrong!']);
                    }
          }else{
              echo json_encode(['status' => 202, 'msg' =>'You have already subscribed.Please go to the login page!']);
          }
        
    }

    public function transaction_update(Request $request){
        return view('front.transaction');
    }

    public function pdfUpdate(){
        $path = "public/upload/";
        $path = $path . basename( $_FILES['uploaded_file']['name']);
        if(move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $path)) {
            echo "The file ".  basename( $_FILES['uploaded_file']['name']). " has been uploaded";
        } else{
            echo "There was an error uploading the file, please try again!";
        }
    }

    public function final_payment(Request $request)
    {
        $expcard=explode('/',$request->card_expiry);
        $cc_expiry=$expcard[0].$expcard[1];
        $amount=$request->total_cost;
        //$amount='0.01';
        $cardholder_name=$request->card_holder;
        $cc_number=$request->card_number;

        $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
        // $gateway_id='K84040-06'; 
        // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]

        $gateway_id='Q66919-01'; 
        $password='xXVxReeIiyQg1D5pmk6kZLBJWO1wx0yg'; //live
        $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 

        // $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //true
        // $data = array("gateway_id" => "K84040-06", "password" => "HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq", "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); //true
        $data_string= json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8;','Accept: application/json' ));
        $result = curl_exec($ch);
        $newres=json_decode($result);
        if(isset($newres->bank_resp_code) && $newres->bank_resp_code==100){
            if(isset($request->coupon_id) && !empty($request->coupon_id)){
                $insert = [
                    'coupon_id' => $request->coupon_id,
                    'user_id' => Auth::user()->id,
                    'usage_date' => date('Y-m-d'),
                ];
                DB::table('tbl_coupon_usage_count')->insert($insert);
            }
            $doc_details=tbl_user_document::where('id', $request->doc_id )->get();
            $txn_data=[
                'user_id' => $doc_details[0]->user_id,
                'document_id' => $request->doc_id,
                'transaction_json' => json_encode($newres),
                'crated_date'       => date('Y-m-d'),
                'created_time'      => date('Y-m-d H:i:s'),
                'amount'            => $amount,
                'status'            => 'A',
            ];
            
            $data=DB::table("tbl_transactions")->insert($txn_data);

            $billing=[
                'First_Name' => $request->f_nm,
                'Last_Name' => $request->l_nm,
                'Email' => $request->eml,
                'Phone_Number' => $request->phone,
                'Address1' => $request->add1,
                'Address2' => $request->add2,
                'Country' => $request->country,
                'State' => $request->city,
                'Zipcode' => $request->zip,
            ];
            $billingJosn=json_encode($billing);
            $update=tbl_user_document::where('id',$request->doc_id)->update(array('payment_status'=>'C','billing_address'=>$billingJosn,'updated_at'=>NOW()));
            echo json_encode(['status' => 200, 'msg' =>'Thanks For subscribing to AimNotary!']);
        }
        else
        {
            echo json_encode(['status' => 201, 'msg' =>(isset($newres->bank_message)?$newres->bank_message:'Payment Failed')]);
        }
    }
    public function check_coupon(Request $request)
    {
        $coupon_code=$request->coupon_code;
        $res=array();
         $result=DB::table('tbl_coupons')->where(['coupon_code'=>$coupon_code])->get();
         if(!$result->isEmpty()){
            $chekcCoupon = DB::table('tbl_coupon_usage_count')->where(['coupon_id'=>$result[0]->coupon_id])->get();
             if($result[0]->coupon_dt > date('Y-m-d')){
                if(count($chekcCoupon) < $result[0]->usage_count){
                    $res['coupon_id']=$result[0]->coupon_id;
                    $res['discount_amt']=$result[0]->discount_amt;
                    echo json_encode(['status' => 200, 'msg'=>'' ,'result' =>$res]);
                 }else{
                    $res = [];
                    echo json_encode(['status' => 400, 'msg'=>'This coupon has reached its maximum limit' ,'result' => $res]);
                 }
             }else{
                echo json_encode(['status' => 400, 'msg'=>'This coupon has expired' ,'result' => $res]);
             }
         }else{
            echo json_encode(['status' => 400,'msg'=>'Invalid Coupon Code', 'result' =>$res]);
         }
    }


    public function get_states()
    {
        $state_list=[];
        $state=tbl_state::orderBy('state_name','ASC')->get();
        if(!$state->isEmpty()){
            foreach ($state as $key => $eachVal) {
                $state_list[]=[
                                'id'         =>$eachVal->id,
                                'state_name' =>$eachVal->state_name,
                                'state_code' =>$eachVal->state_code,
                                
                            ];
            }
        }
        echo json_encode($state_list);
    }
    public function get_city(Request $request)
    {
        $city_list=[];
        if(is_numeric($request->state_id)){
           $city=tbl_citie::where('state_id',$request->state_id)->orderBy('city','ASC')->get();
        }else{
          $state=tbl_state::where('state_code',$request->state_id)->first();
        
          $city=tbl_citie::where('state_id',$state->id)->orderBy('city','ASC')->get();
        }
       
        if(!$city->isEmpty()){
            foreach ($city as $key => $eachVal) {
                $city_list[]=[
                                'id'  =>$eachVal->city_id,
                                'city'      =>$eachVal->city,
                                
                            ];
            }
        }
        echo json_encode($city_list);
    }

    public function add_additional_docs(Request $request)
    {
        $cost=$request->total_cost;
        $doc_id=$request->doc_id;
        $stamp=($request->no_of_stamps==0) ? 1 : ($request->no_of_stamps+1);
        $signer=($request->no_of_signers==0) ? 1 : ($request->no_of_signers+1);
        $update=[
            'total_price' => $cost,
            'user_require_stamp' => $stamp,
            'user_require_signer' => $signer,
            'updated_at' => NOW()
        ];
        $update=tbl_user_document::where('id',$doc_id)->update($update);
        echo url("/verify-documents/".base64_encode($doc_id));
    }

    public function enterprise_form_submit(Request $request){

                $user_data="
        
                    <b><p>1. First Name - <span style='color : blue;'>".$request->first_name."</span></p></b>

                    <b><p>2. Last Name - <span style='color : blue;'>".$request->last_name."</span></p></b>

                    <b><p>3. Business Email Address - <span style='color : blue;'>".$request->u_email."</span></p></b>

                    <b><p>4. State - <span style='color : blue;'>".$request->state."</span></p></b>

                    <b><p>5. City - <span style='color : blue;'>".$request->city."</span></p></b>

                    <b><p>6. Address - <span style='color : blue;'>".$request->add1."</span></p></b>

                    <b><p>7. Company Name - <span style='color : blue;'>".$request->company_name."</span></p></b>

                    <b><p>8. Mobile - <span style='color : blue;'>".$request->mobile."</span></p></b>

                    <b><p>9. How Many Notarizations Does Your Business Complete Monthly? - <span style='color : blue;'>".$request->notarize_no."</span></p></b>

                    <b><p>10. What best describes your company's Notarization needs? - <span style='color : blue;'>".$request->describe."</span></p></b>

                    <b><p>11. License and/or Outsource? (License=Your Notary Publics Complete Outsource=Aim notary’s Professional Notary Publics Complete) - <span style='color : blue;'>".$request->lisence."</span></p></b>
        
                ";

                $userhtml="
                    <h3>Thanks for contacting Aim Notary. Now sit back and relax. Our executive will reach you shortly.</h3><br>

                    <b><p>1. First Name - <span style='color : blue;'>".$request->first_name."</span></p></b>

                    <b><p>2. Last Name - <span style='color : blue;'>".$request->last_name."</span></p></b>

                    <b><p>3. Business Email Address - <span style='color : blue;'>".$request->u_email."</span></p></b>

                    <b><p>4. Password - <span style='color : blue;'>".$request->password."</span></p></b>

                    <b><p>5. State - <span style='color : blue;'>".$request->state."</span></p></b>

                    <b><p>6. City - <span style='color : blue;'>".$request->city."</span></p></b>

                    <b><p>7. Address - <span style='color : blue;'>".$request->add1."</span></p></b>

                    <b><p>8. Company Name - <span style='color : blue;'>".$request->company_name."</span></p></b>

                    <b><p>9. Mobile - <span style='color : blue;'>".$request->mobile."</span></p></b>

                    <b><p>10. How Many Notarizations Does Your Business Complete Monthly? - <span style='color : blue;'>".$request->notarize_no."</span></p></b>

                    <b><p>11. What best describes your company's Notarization needs? - <span style='color : blue;'>".$request->describe."</span></p></b>

                    <b><p>12. License and/or Outsource? (License=Your Notary Publics Complete Outsource=Aim notary’s Professional Notary Publics Complete) - <span style='color : blue;'>".$request->lisence."</span></p></b>
        
                ";
                $userDSocData=array('name'=>$request->first_name." ".$request->last_name,
                                 'email'=>$request->u_email,
                                 'personal_mobile'=>$request->mobile,
                                 'state'=>$request->state,
                                 'city'=>$request->city,
                                 'zip_code'=>$request->zip_code,
                                 'add1'=>$request->add1,
                                 'password'=>Hash::make($request->password),
                                 'is_privacy'=>1,
                                 'is_terms'=>1,
                                 'user_category' => 'E',
                                 'created_at'=>NOW(),
                                 'is_active' => 0,
                                );
                                
                $data=DB::table("users")->insert($userDSocData);
                $update_arr=array('email_verified_at'=>NOW(),'updated_at'=>NOW());
                User::where('email',$request->u_email)->update($update_arr);
        
        $html = View('emails.enterprise-mail', ['user'=> $user_data,'admin'=>'false']);
        $mail_status=$this->smtp_auth_email("info@aimnotary.com", $subject = 'Thanks For registering to aimnotary.com', $message = $html);
        $mail_status=$this->smtp_auth_email("esther@aimnotary.com", $subject = 'Thanks For registering to aimnotary.com', $message = $html);
                                
        $html = View('emails.enterprise-mail', ['user'=> $userhtml,'admin'=>'false']);
        $mail_status=$this->email_function($from = "no-reply@aimnotary.com", $to = $request->u_email, $subject = 'Thanks For registering to aimnotary.com', $message = $html);
        
        return redirect($_SERVER['HTTP_REFERER'])->with('message', 'Your Request has been submitted successfully');
    }

    public function make_subscription_payments(Request $request){
        // echo "<pre>"; print_r($request->request); exit;
        $result=DB::table("tbl_user_doc_counts")->select("*")->where(['user_id'=>Auth::user()->id])->orderBy("id","desc")->take(1)->get()->toArray();
        $expairy=explode("/",$request->card_expiry);
        $bankres=$this->payment_set(['amount'=>$request->subscription_amount,'holder_name'=>$request->card_holder,'cn'=>$request->card_number,'ex'=>$expairy[0].$expairy[1]]);
        
        if(isset($bankres->bank_resp_code) && $bankres->bank_resp_code==100){
            $update_arr=array('payment_status'=>'C');
            DB::table("tbl_user_doc_counts")->where('id',$result[0]->id)->update($update_arr);
            echo json_encode(['status'=>1,'msg'=>'Your payment has been successfull']);
        }else{
            echo json_encode(['status'=>0,'msg'=>'Payment Fail']);
        }
    }

    public function transaction_set($doc_id,$newres=[]){
        $doc_details=tbl_user_document::where('user_id', $request->doc_id )->get();

            $txn_data=[
                'user_id' => $doc_details[0]->user_id,
                'document_id' => $request->doc_id,
                'transaction_json' => json_encode($newres),
                'crated_date'       => date('Y-m-d'),
                'created_time'      => date('Y-m-d H:i:s'),
                'status'            => 'A',
            ];
            
            $data=DB::table("tbl_transactions")->insert($txn_data);
    }

    public function payment_set($mydata=[]){
        // print_r($data); exit;
        $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //live
        // $gateway_id='K84040-06'; 
        // $password='HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq'; //live [OLD ID]
        $amount=$mydata['amount'];
        $cardholder_name=$mydata['holder_name'];
        $cc_number=$mydata['cn'];
        $cc_expiry=$mydata['ex'];
        $gateway_id='Q66919-01'; 
        $password='Sc1zv9Bb9UjVMxKTXdQfPkdS4Dy0t3vO'; //live
        $data = array("gateway_id" => $gateway_id, "password" => $password, "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); 
        // $url='https://api.globalgatewaye4.firstdata.com/transaction/v11'; //true
        // $data = array("gateway_id" => "K84040-06", "password" => "HA31vj8Cz0NeXKGfAzMTykldbjNL6Whq", "transaction_type" => "00", "amount" => $amount, "cardholder_name" => $cardholder_name, "cc_number" => $cc_number, "cc_expiry" => $cc_expiry,"customer_ref"=>"AimNotary"); //true
        $data_string= json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=UTF-8;','Accept: application/json' ));
        $result = curl_exec($ch);
        $newres=json_decode($result);
        return $newres;
    }

    public function getDocStatus(Request $request){
        //$request->mtTenant = 119;
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,PUT,GET");
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Max-Age: 86400");
        $resyult=tbl_user_document::where(['id'=>$request->mtTenant])->get()->first()->toArray();
        echo json_encode($resyult);

    }

    public function updateSignerPdf(Request $request){

        
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: POST,PUT,GET");
        header("Access-Control-Allow-Headers: Content-Type");
        header("Access-Control-Max-Age: 86400");
        if(isset($_FILES['file']) and !$_FILES['file']['error']){
            $pdfpath="/home/aimnotary/public_html/public/" . $request->docname;
            move_uploaded_file($_FILES['file']['tmp_name'], $pdfpath);
            
            // $im = new imagick("/home/aimnotary/public_html/public/" . $request->docname);
            // $im->setImageFormat('png');
            // $im->setImageUnits(imagick::RESOLUTION_PIXELSPERINCH);
            // $im->setImageResolution(720,720);
            // $noPage = $im->getNumberImages();
            // $im->resampleImage(720,720,imagick::FILTER_UNDEFINED,0);
            // print_r($im->getImageResolution()); exit;
            // header('Content-Type: image/jpeg');
            // $im->setImageCompression(imagick::COMPRESSION_JPEG);
            // $im->setImageCompressionQuality(200);
            // echo $im;
            $path = "/home/aimnotary/public_html/public/upload/pdftron/".$request->tenant."/";
            if (!file_exists($path)) {
                mkdir($path,0777,true);
            }
            $pdf = new Pdf($pdfpath);
            $pageNp=$pdf->getNumberOfPages();
            $pdf->setCompressionQuality(100);
            for($i=1;$i<=$pageNp;$i++){
                $pdf->setPage($i)->saveImage($path);
                $imageName[] = $path.$i.".jpg";
            }
            if(unlink($pdfpath)){
                $pdf = new Imagick($imageName);
                $pdf->setImageFormat('pdf');
                $pdf->writeImages($pdfpath, true);
            }else{
                echo "Not Done";
            }
            for($i=1;$i<=$pageNp;$i++){
                unlink($path.$i.".jpg");
            }
        }
        
    }

    public function resubscription(Request $request){
        if($request->package==25){
            $data=[
                'user_id' => Auth::user()->id,
                'doc'   => 1,
                'amount' => NULL,
                'payment_status' => NULL,
                'status' => 'A',
                'date'  => date("Y-m-d"),
                'time' => date("Y-m-d H:i:s")
            ];
            DB::table("tbl_user_doc_counts")->insert($data);
            return redirect("dashboard")->with('payment_success','Resubscription Successfull');
        }else{

            $expairy=explode("/",$request->card_expiry);
            $bankres=$this->payment_set(['amount'=> 99,'holder_name'=>$request->card_holder,'cn'=>$request->card_number,'ex'=>$expairy[0].$expairy[1]]);
            if(isset($bankres->bank_resp_code) && $bankres->bank_resp_code==100){
                $data=[
                    'user_id' => Auth::user()->id,
                    'doc'   => 5,
                    'amount' => 99,
                    'payment_status' => 'C',
                    'status' => 'A',
                    'date'  => date("Y-m-d"),
                    'time' => date("Y-m-d H:i:s")
                ];
                DB::table("tbl_user_doc_counts")->insert($data);
                return redirect("dashboard")->with('payment_success','Resubscription Successfull');
            }
        }
        
    }

}


class Pdf
{
    protected $pdfFile;

    protected $resolution = 144;

    protected $outputFormat = 'jpg';

    protected $page = 1;

    public $imagick;

    protected $numberOfPages;

    protected $validOutputFormats = ['jpg', 'jpeg', 'png'];

    protected $layerMethod = Imagick::LAYERMETHOD_FLATTEN;

    protected $colorspace;

    protected $compressionQuality;

    public function __construct(string $pdfFile)
    {
        $this->imagick = new Imagick();

        $this->imagick->pingImage($pdfFile);

        $this->numberOfPages = $this->imagick->getNumberImages();

        $this->pdfFile = $pdfFile;
    }

    public function setResolution(int $resolution)
    {
        $this->resolution = $resolution;

        return $this;
    }

    public function setOutputFormat(string $outputFormat)
    {

        $this->outputFormat = $outputFormat;

        return $this;
    }

    public function getOutputFormat(): string
    {
        return $this->outputFormat;
    }

    /**
     * Sets the layer method for Imagick::mergeImageLayers()
     * If int, should correspond to a predefined LAYERMETHOD constant.
     * If null, Imagick::mergeImageLayers() will not be called.
     *
     * @param int|null
     *
     * @return $this
     *
     * @throws \Spatie\PdfToImage\Exceptions\InvalidLayerMethod
     *
     * @see https://secure.php.net/manual/en/imagick.constants.php
     * @see Pdf::getImageData()
     */
    public function setLayerMethod(?int $layerMethod)
    {
        $this->layerMethod = $layerMethod;

        return $this;
    }

    public function isValidOutputFormat(string $outputFormat): bool
    {
        return in_array($outputFormat, $this->validOutputFormats);
    }

    public function setPage(int $page)
    {

        $this->page = $page;

        return $this;
    }

    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    public function saveImage(string $pathToImage): bool
    {
        if (is_dir($pathToImage)) {
            $pathToImage = rtrim($pathToImage, '\/').DIRECTORY_SEPARATOR.$this->page.'.'.$this->outputFormat;
        }

        $imageData = $this->getImageData($pathToImage);

        return file_put_contents($pathToImage, $imageData) !== false;
    }

    public function saveAllPagesAsImages(string $directory, string $prefix = ''): array
    {
        $numberOfPages = $this->getNumberOfPages();

        if ($numberOfPages === 0) {
            return [];
        }

        return array_map(function ($pageNumber) use ($directory, $prefix) {
            $this->setPage($pageNumber);

            $destination = "{$directory}/{$prefix}{$pageNumber}.{$this->outputFormat}";

            $this->saveImage($destination);

            return $destination;
        }, range(1, $numberOfPages));
    }

    public function getImageData(string $pathToImage): Imagick
    {
        /*
         * Reinitialize imagick because the target resolution must be set
         * before reading the actual image.
         */
        $this->imagick = new Imagick();

        $this->imagick->setResolution($this->resolution, $this->resolution);

        if ($this->colorspace !== null) {
            $this->imagick->setColorspace($this->colorspace);
        }

        if ($this->compressionQuality !== null) {
            $this->imagick->setCompressionQuality($this->compressionQuality);
        }

        if (filter_var($this->pdfFile, FILTER_VALIDATE_URL)) {
            return $this->getRemoteImageData($pathToImage);
        }

        $this->imagick->readImage(sprintf('%s[%s]', $this->pdfFile, $this->page - 1));

        if (is_int($this->layerMethod)) {
            $this->imagick = $this->imagick->mergeImageLayers($this->layerMethod);
        }

        $this->imagick->setFormat($this->determineOutputFormat($pathToImage));

        return $this->imagick;
    }

    public function setColorspace(int $colorspace)
    {
        $this->colorspace = $colorspace;

        return $this;
    }

    public function setCompressionQuality(int $compressionQuality)
    {
        $this->compressionQuality = $compressionQuality;

        return $this;
    }

    protected function getRemoteImageData(string $pathToImage): Imagick
    {
        $this->imagick->readImage($this->pdfFile);

        $this->imagick->setIteratorIndex($this->page - 1);

        if (is_int($this->layerMethod)) {
            $this->imagick = $this->imagick->mergeImageLayers($this->layerMethod);
        }

        $this->imagick->setFormat($this->determineOutputFormat($pathToImage));

        return $this->imagick;
    }

    protected function determineOutputFormat(string $pathToImage): string
    {
        $outputFormat = pathinfo($pathToImage, PATHINFO_EXTENSION);

        if ($this->outputFormat != '') {
            $outputFormat = $this->outputFormat;
        }

        $outputFormat = strtolower($outputFormat);

        if (! $this->isValidOutputFormat($outputFormat)) {
            $outputFormat = 'jpg';
        }

        return $outputFormat;
    }
}
